<%@ page import="org.keycloak.admin.client.Keycloak" %>
<%@ page import="org.keycloak.admin.client.KeycloakBuilder" %>
<%@ page import="org.keycloak.OAuth2Constants" %>
<%@ page import="org.keycloak.admin.client.resource.UserResource" %>
<%@ page import="org.jboss.resteasy.spi.ResteasyProviderFactory" %>
<%@ page import="org.jboss.resteasy.plugins.providers.jackson.ResteasyJackson2Provider" %>
<%@ page import="org.jboss.resteasy.client.jaxrs.ResteasyClient" %>
<html>
<style>
    .error {background-color: coral}
    .ok {background-color: greenyellow}

</style>
<body><%

    ResteasyJackson2Provider prov = new ResteasyJackson2Provider();

    ResteasyClient cl;

    String uid = request.getParameter("uid");
    String pwd = request.getParameter("pwd");
    String keycloakUrl = request.getParameter("url");
    keycloakUrl = keycloakUrl== null ? "http://localhost:8080/auth" :keycloakUrl;
    pwd = pwd==null ? "admin" : pwd;
    if (uid!=null && !"".equals(uid)) {
        Keycloak kc = KeycloakBuilder.builder()
                .realm("master")
                .clientId("admin-cli")
                .username("admin")
                .password(pwd)
                .serverUrl(keycloakUrl)
/*                .resteasyClient(new ResteasyClientBuilderImpl().connectionPoolSize(10)
                        .register(new CustomJacksonProvider()).build())  */
                .build();

        UserResource user = kc.realm("MC").users().get(uid);
        out.println(user.toRepresentation().getFirstName());
    }
%>
<h1>Server:<%=request.getServletContext().getServerInfo()%></h1>
<br />
<form>
    Server keycloak:<select name="url">
        <option value="http://localhost:8080/auth" >http://localhost:8080/auth</option>
        <option value="http://localhost:8280/auth" >http://localhost:8280/auth</option>
    </select><br/>

    User id:<input name="uid" value="<%=uid !=null ? uid : ""%>"><br/>
    Password:<input name="pwd" value="<%=pwd%>"><br/>
    <input type="submit"/>
</form>

<div class="error">
    JsonBindingProvider, debug AbstractReaderInterceptorContext.class, riga 49
</div>
<div class="ok">
    ResteasyJackson2Provider
</div>


</body>
</html>