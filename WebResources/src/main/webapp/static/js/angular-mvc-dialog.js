window.getBearerToken = function(){
    var defer = jQuery.Deferred();
    var webContext = window.location.href.replace(/(https?\:\/\/[^\/]+(:[0-9]+)?)(\/[^\/]+).*$/,"$3");
    var out = jQuery.ajax({
        dataType:"json",
        method:'GET',
        url:webContext + "/tkn",
        success: function(data){
            defer.resolve(data.token)
        },
        error: function(){
            defer.reject(arguments);
        }
    })
    return defer.promise();
}
angular.module('taxiDialogTemplate', [])
    .directive('dlgContent',function($compile){
        return {
            require:['^taxiTemplate'],
            templateUrl:function(tElem, tAttrs){
                return '${resources.url}'+tAttrs.templateUrl;
            }
        }
    })
    .directive('labelContent',function($compile){
        return {
            require:['^taxiTemplate'],
            template: function(tElem, tAttrs){
                var out = '<span ';
                if (tAttrs.fullContainer) {
                    out+='style="width:100%;height:100%;display: inline-block" ';
                }
                out +='ng-click="toggle()" class="{{labelClass}}">{{label}}</span>';
                return out;
            },
            link: function(scope, iElement, iAttrs, controller) {
                scope.toggle=controller[0].toggle;
            }
        }
    })
    .directive('taxiTemplate',function($compile){
        return {
            template:function(tElem, tAttrs){
                var out = '<span class="iconPointer" ';
                if (tAttrs.fullContainer) {
                    out+='full-container="' + tAttrs.fullContainer +'" style="width:100%;height:100%;display: inline-block"';
                }
                out += ' label-content ></span><span ng-cloak ng-show="showDialog" dlg-content template-url="'+ tAttrs.templateUrl + '" ></span>';
                return out;
                // return '<span style="width:100%;height:100%;display: inline-block" label-content ></span><span ng-cloak ng-show="showDialog" dlg-content template-url="'+ tAttrs.templateUrl + '" ></span>';
            },
            controller:function($scope, $element, $attrs, $transclude){
                $scope.showDialog=false;
                $scope.webContext = window.location.href.replace(/(https?\:\/\/[^\/]+(:[0-9]+)?)\/([^\/]+).*$/,"$3");
                var ctrl = this;
                this.useBootstrap = function(){
                    if (!$scope.nfo) {
                        $scope.nfo=MC_MODAL.info({
                            class:'big',
                            buttons: MC_MODAL.buttons.okButton,
                            title: $attrs.title,
                            elem: $element.find('[dlg-content]'),
                            autoOpen:false
                        }).modal;
                    }
                    $scope.nfo.modal('show')
                }

                this.usejQueryUI = function(){
                    if ($scope.nfo===undefined) {
                        $scope.nfo = info({
                            element: $element.find('[dlg-content]'),
                            title: $attrs.title,
                            width: "60%",
                            height: 230,
                            buttons: [{
                                text:"OK",
                                click:function(e){jQuery( this ).dialog( "close" );}
                            }]})
                    }
                    GET_DIALOGS.getDialog($scope.nfo).addClass('rsm-dialog').dialog("open");
                }


                this.toggle = function(){
                    $scope.showDialog=false;
                    window.getBearerToken().then(function(token){
                        jQuery.ajax({
                            method:'GET',
                            dataType: "json",
                            headers: {
                                'Authorization':'Bearer ' + token
                            },
                            url: $scope.restUrl,
                            success: function(result){
                                $scope.data = result;
                                // nfo.modal.modal('show')
                                $scope.showDialog=true;
                                $scope.$apply();
                                if (!!window.MC_MODAL) { // Bootstrap
                                    ctrl.useBootstrap();
                                } else { // jQuery ui
                                    ctrl.usejQueryUI();
                                }

                            }
                        })
                    })

                }

            },
            restrict: 'A',
            scope: {
                labelClass:'@',
                label:'@',
                templateUrl:'@',
                restUrl:'@',
                fullContainer:'@'
            }
        }
    })
    .config(function($sceDelegateProvider) {
        var wl=[
            // Allow same origin resource loads.
            'self',
            // Allow loading from our assets domain.  Notice the difference between * and **.
            '${resources.url}/**'
        ];
        $sceDelegateProvider.resourceUrlWhitelist(wl);
    });
