/**
 * Oggetto globale per l'utilizzo delle finestre modali di BOOTSTRAP.
 * @global
 * @namespace MC_MODAL
 * @author Antonio Giovannetti
 */
MC_MODAL = {}

/**
 * @typedef MC_MODAL.BUTTONTYPE
 * @property {object}  label  Etichetta
 * @property {object}  clazz  Classe css
 * @property {string}  key Chiave per referenziare il bottone
 * @property {string} innerElementClass Se presente crea un elemento nel button con questa classe
 * @property {boolean|function}  [resolve=true] Se risolve o rigetta la promise, oppure function(promise) {}
 */
MC_MODAL.BUTTONTYPE=function(label,clazz,key,resolve,innerElementClass) {
    this.label = label;
    this.clazz = clazz;
    this.key = key;
    this.innerElementClass = innerElementClass;
    this.resolve = typeof resolve !== 'undefined' ? resolve : true;
}

/**
 * @typedef MC_MODAL.MODALTYPE
 * @property {string}  headerClass Classe css dell'header
 * @property {string}  glyph Glifo dell'header
 */
MC_MODAL.MODALTYPE=function(headerClass,glyph) {
    this.headerClass = headerClass;
    this.glyph = glyph;
}

/**
 * @memberOf MC_MODAL
 * @property {object}  buttons  Bottoni predefiniti
 * @property {Array.<MC_MODAL.BUTTONTYPE>}  buttons.okButton  Bottone OK(resolve)
 * @property {Array.<MC_MODAL.BUTTONTYPE>}  buttons.okCancelButtons  Bottoni OK(resolve)-Cancel(reject)
 * @property {Array.<MC_MODAL.BUTTONTYPE>}  buttons.siNoButtons  Bottoni Si(resolve)/NO(reject)
 * @property {Array.<MC_MODAL.BUTTONTYPE>}  buttons.chiudiButton  Bottone Chiudi(reject)
 */
MC_MODAL.buttons= {
    okButton: [new MC_MODAL.BUTTONTYPE("OK", "btn btn-success","button.ok",true,"fas fa-check")],
    okCancelButtons: [new MC_MODAL.BUTTONTYPE("OK", "btn btn-success","button.ok"), new MC_MODAL.BUTTONTYPE("Cancel", "btn btn-default", "button.cancel",false)],
    siNoButtons: [new MC_MODAL.BUTTONTYPE("Si", "btn btn-success","button.yes",true,"fas fa-thumbs-up"),new MC_MODAL.BUTTONTYPE("No", "btn btn-default", "button.no",false,"fas fa-thumbs-down")],
    chiudiButton: [new MC_MODAL.BUTTONTYPE("Chiudi", "btn btn-default","button.close", false,"fas fa-times")]
}

/**
 * @memberOf MC_MODAL
 * @property {MC_MODAL.MODALTYPE}  info Modale info
 * @property {MC_MODAL.MODALTYPE}  warning Modale warning
 * @property {MC_MODAL.MODALTYPE}  danger Modale danger
 * @property {MC_MODAL.MODALTYPE}  success Modale success
 */
MC_MODAL.modalTypes =  {
    /** Informativa */
    "info":new MC_MODAL.MODALTYPE("bg-info","fa-info-circle"),
    /** Attenzione */
    "warning":new MC_MODAL.MODALTYPE("bg-warning","fa-exclamation-triangle"),
    /** Errore */
    "danger":new MC_MODAL.MODALTYPE("bg-danger","fa-times-circle"),
    /** Successo */
    "success":new MC_MODAL.MODALTYPE("bg-success","fa-check-circle-o")};

/**
 * Risultato della creazione di una finestra modale
 * @typedef {object} ModalWrapper
 * @memberOf MC_MODAL
 * @property {object} promise Oggetto jQuery.promise, rejected se premuto un bottone con la property resolve=false
 * @property {object} modal L'oggetto jQuery della finestra modale
 */

/**
 * Finestra modale informativa generica.
 * @memberOf MC_MODAL
 * @param {object} options - Opzioni della finestra.
 * @param {Array.<MC_MODAL.BUTTONTYPE>} options.buttons I bottoni da visualizzare
 * @param {string|function} options.title - Titolo della finestra.
 * @param {string} options.elem - Elemento HTML o jQuery da trasformare in modal. Se valorizzato viene ignorato 'template'
 * @param {boolean} [options.autoOpen=true] - Si apre automaticamente alla creazione.
 * @param {number} options.closeTimeout - millisecondi dopo i quali si chiude automaticamente.
 * @param {string} options.url Preleva il contenuto da una invocazione AJAX su una url
 * @param {boolean} [options.wrap=true] Se true ed e' valorizzatio elem wrappa elem nel body del panel
 * @param {boolean} [options.class] Classe da aggiungere al modal dialog
 * @param {object|function} options.urlData
 * @param {MC_MODAL.MODALTYPE} options.modalType tipo.
 * @return {MC_MODAL.ModalWrapper} Il risultato
 * @example MC_MODAL.info({
                    buttons: MC_MODAL.buttons.okButton,
                    title: 'Modifiche Sezione',
                    text: "<strong>I dati presenti in questa sezione, poiche' e' stata deselezionata, verranno persi con il salvataggio delle modifiche</strong>"
                });
 */
MC_MODAL.info = function(options) {
    options = options || {};
    options.buttons = options.buttons || MC_MODAL.buttons.okCancelButtons;
    options.title = options.title || "Informazioni";
    options.modalType = options.modalType || MC_MODAL.modalTypes.info;
    options.closeTimeout = options.closeTimeout || 0;
    options.autoOpen = options.autoOpen===undefined ? true : !!options.autoOpen;
    options.wrap = options.wrap===undefined ? true : !!options.wrap;

    // jQuery('.modal.fade').remove();
    // jQuery('.modal').remove();
    // jQuery('.modal-backdrop').remove();
    var template = "<div class=\"modal fade\" tabindex=\"-1\" role=\"dialog\"><div class=\"modal-dialog\" role=\"document\"><div class=\"modal-content\"><div class=\"modal-header\"><h4 class=\"modal-title\"></h4></div><div class=\"modal-body\"></div><div class=\"modal-footer\"></div></div></div></div>";
    var dlg;
    var created=false;
    if (options.elem) {
        if (options.wrap) {
            var _parent = options.elem.parent();
            if (_parent && !_parent.hasClass("modal-body")) {
                dlg = options.elem.wrap('<div class="modal-body"></div>')
                    .parent().wrap("<div class=\"modal-content\"></div>")
                    .parent().prepend('<div class="modal-header"><h4 class="modal-title"></h4></div>')
                    .append('<div class="modal-footer"></div>')
                    .wrap('<div class="modal-dialog" role="document"></div>')
                    .parent().wrap('<div class="modal fade" tabindex="-1"></div>').parent();
                created = true;
            }
            dlg = options.elem.closest(".modal");
        } else {
            dlg = jQuery(options.elem);
        }
    } else {
        dlg = jQuery(template);
        jQuery(document.body).append(dlg);
    }
    var deferred = jQuery.Deferred();
    dlg.data('deferred',deferred)
    if (options.class) {
        dlg.find('.modal-dialog').addClass(options.class);
    }
    dlg.find(".modal-header").addClass(options.modalType.headerClass).children('h4');
    var body = dlg.find(".modal-body");

    if (jQuery.isFunction(options.title)) {
        dlg.data('fncReloadTitle',function(){
            dlg.find(".modal-header h4").text(options.title());
        });
        dlg.find(".modal-header h4").html(options.title());
    } else {
        dlg.find(".modal-header h4").html(options.title);
    }
    var ajaxContentPromise;
    var _onHidden = function() {
        // console.info("hidden.bs.modal",this,arguments);
        this.remove();
    }
    if (body.length!=0) {
        if (options.text) {
            body.html(options.text);
            if (options.autoOpen) {
                dlg.modal({backdrop:'static'}).on('hidden.bs.modal', _onHidden);
            }
        } else {
            if (options.url) {
                var fncReloadContent = function(){
                    var def = jQuery.Deferred();
                    var urlData = undefined;
                    if (jQuery.isFunction(options.urlData)) {
                        urlData = options.urlData();
                    } else {
                        urlData = options.urlData || {};
                    }
                    body.load(options.url, urlData || {}, function () {
                        def.resolve(arguments);
                        if (options.autoOpen) dlg.modal({backdrop:'static'}).on('hidden.bs.modal', _onHidden);
                    });
                    return def.promise();
                }
                ajaxContentPromise = fncReloadContent();
                dlg.data('fncReloadContent',fncReloadContent);

            } else {
                if (!options.elem) {
                    alert("paramtro url o text necessario")
                } else {

                    if (created) {
                        dlg.modal({show:options.autoOpen,backdrop:'static'});
                    } else {
                        dlg.modal('show');
                    }
                }

            }
        }
    }
    // if (options.elem && !options.url && options.autoOpen) {
    //     options.elem.modal('show');
    // }
    dlg.find(".modal-footer").empty();
    jQuery.each(options.buttons,function (idx,elem) {
        var _elem = jQuery.extend({dismiss:true,resolve:true,clazz:"btn",index:idx},elem);
        var btn = jQuery("<span class=\""+_elem.clazz+"\">"+_elem.label+"</span>");
        if (_elem.dismiss) {
            btn.attr("data-dismiss","modal");
        }
        if (elem.innerElementClass) {
            btn.prepend("<i class='"+elem.innerElementClass+"' ></i> ")
        }
        btn.click(function(){
            if (btn.hasClass('disabled')) return false;
            var deferred = dlg.data('deferred');
            if (jQuery.isFunction(_elem.resolve)) {
                _elem.resolve(deferred,dlg);
            } else {
                if (_elem.resolve) {
                    deferred.resolve(_elem,dlg);
                } else deferred.reject(_elem,dlg);
            }
        })
        dlg.find(".modal-footer").append(btn)
    })
    if (options.closeTimeout && options.closeTimeout>0) {
        setTimeout(function(){
            dlg.modal('hide');deferred.resolve({timeout:options.closeTimeout});
        }, options.closeTimeout);
    }
    return {promise:deferred.promise(),modal:dlg,ajaxContentPromise:ajaxContentPromise};
}


/**
 * Riapre la finestra modale chiusa creando un nuovo ciclo promise/resolve/reject.
 * @memberOf MC_MODAL
 * @param {object} modal - Finestra modale.
 * @return {MC_MODAL.ModalWrapper} Il risultato
 * @example MC_MODAL.open(mYModal);
 */
MC_MODAL.reopen = function(modal,reload) {
    var deferred = jQuery.Deferred();
    if (reload===undefined) reload = true;
    modal.data('deferred',deferred);
    var fncReloadContent = modal.data('fncReloadContent');
    if (reload && fncReloadContent) {
        fncReloadContent().then(function(){
            modal.modal('show');
        });
    } else {
        modal.modal('show');
    }
    var fncReloadTitle = modal.data('fncReloadTitle');
    if (reload && fncReloadTitle) {
        fncReloadTitle();
    }

    return {promise:deferred.promise(),modal:modal};
}


/**
 * Finestra di attesa.
 * @memberOf MC_MODAL
 * @param {boolean} enable Abilita la finestra di attesa per chiamate ajax che richiedono troppo tempo
 * @param {number} [timeout] Millisecondi di attesa prima di aprire la finestra.
 */
MC_MODAL.enableAttesa = function(enable,timeout) {
    var _to;
    var _open = function(){
        var elem = jQuery("#waitModal");
        _to=setTimeout(function(){
            jQuery('.cssload-container').show(); return;
            /*if (elem.length!==0) {
                MC_MODAL.info({elem:elem})
            } else {
                MC_MODAL.info({
                    text:"<h2 class=\"text-center text-warning\"><i class=\"fas fa-spinner fa-pulse fa-fw\"></i>Attendere</h2>",
                    template:"<div id=\"waitModal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\"><div class=\"modal-dialog\" role=\"document\"><div class=\"modal-content\"><div class=\"modal-body\"></div></div></div>"})
            } */}, timeout || 800);
    };
    var _close = function(){
        clearTimeout(_to);
        jQuery('.cssload-container').hide(); return;
        /*var elem = jQuery("#waitModal");
        if (elem.length!==0) {
            elem.modal('hide');
        }*/
    };
    jQuery(document).off("ajaxStart").off("ajaxStop");
    if (enable || enable===undefined) {
        jQuery(document).on("ajaxStart", _open).on("ajaxStop", _close);
    } else {
        jQuery(document).off("ajaxStart", _open).off("ajaxStop", _close);
        _close();
    }
    this.openAttesa = _open;
    this.closeAttesa = _close;
}


MC_MODAL.printMessages = function(data) {
    var opt = {
        title: "Messaggi",
        buttons: MC_MODAL.buttons.okButton,
        text:'Contenuto del messaggio',
        closeTimeout: data ? data.closeTimeout : undefined

    }
    msg = jQuery('<div class="messages"></div>');
    data.messages = data.messages || (data.responseJSON ? data.responseJSON.messages : undefined);
    if (data.messages && data.messages.length) {
        m = jQuery('<ul class="alert alert-success"></ul>').css('list-style','none');
        msg.append(m);
        jQuery(data.messages).each(function(idx,elem){
            m.append('<li>'+elem+'</li>');
        })
    }
    data.errors = data.errors || (data.responseJSON ? data.responseJSON.errors : undefined);
    if (data.errors && data.errors.length) {
        opt.title = "Errore";
        e = jQuery('<ul class="alert alert-danger"></ul>').css('list-style','none');
        msg.append(e);
        jQuery(data.errors).each(function(idx,elem){
            e.append('<li>'+elem+'</li>');
        })
        opt.modalType = MC_MODAL.modalTypes.danger;
    }
    opt.text = msg;
    return MC_MODAL.info(opt);
}

/*======================= CONFERMA ====================================*/
/* Uso:
- titolo della finestra di conferma: data("confirm-title") | "Confermare l'operazione?"
- testo della finestra di conferma: data("confirm-text") | attributo "title" |
- se si vuole confermare navigazione <a> o submit <form> aggiungere la classe "confirm" all' <a> o al <form>
    Es. <form data-confirm-text="Salvare rec com?" data-confirm-title="Allora?"
- se si vuole confermare prima di eseguire un codice js su un altro elemento:
- si puo' definire "data-confirm-text" si come attributo che come jQuery().data("confirm-text",'....')
- si puo' definire "data-confirm-title" si come attributo che come jQuery().data("confirm-title",'....')
    jQuery("#elem-to-confirm").data("confirm-ok",function(){
                console.log("Confermato");
            }).data("confirm-text",function(){
                return "Conferma alle ore <b>" + new Date()+"</b>";
            })
 */
MC_MODAL.confirm = function(event) {
    if (MC_MODAL===this) {
        return;
    }
    var target = jQuery(event.target);
    if (target.length===0) target = jQuery(event.currentTarget);
    if (target.data('confirmed')) {
        target.off(event.type,MC_MODAL.confirm);
        event.target[event.type]();
        setTimeout(function(){ target.on(event.type,MC_MODAL.confirm); }, 500);
        return true
    }
    var fnOk = target.data("confirm-ok") || jQuery.noop();
    var confirmTitle = target.data("confirm-title") || target.attr("title");
    var confirmText = target.data("confirm-text");
    switch(jQuery.type(confirmTitle)) {
        case "function": confirmTitle = confirmTitle(); break;
        case "string": break;
        default: confirmTitle = "Confermare l'operazione?";
    }

    switch(jQuery.type(confirmText)) {
        case "function": confirmText = confirmText(); break;
        case "string": break;
        default: confirmText = "Confermare?";
    }
    if (event.cancelable) {
        event.preventDefault();
        var that = this;
        pr = new MC_MODAL.info({
            modalType: MC_MODAL.modalTypes.warning,
            title:confirmTitle,
            text: confirmText,
            buttons: MC_MODAL.buttons.siNoButtons
        });
        pr.promise.then(function(){
            target.data('confirmed', true);
            if (fnOk) fnOk.call(this,event); else  target.trigger(event.type);
            setTimeout(function(){target.data('confirmed', false); }, 500 );

        })
    }
}


jQuery(document)
/*============ Attivazione automatica conferma =========*/
    .on('submit','form.confirm',MC_MODAL.confirm)
    .on('click',':not(form).confirm',MC_MODAL.confirm)
    .ready(function() {

    MC_MODAL.enableAttesa();

    jQuery(document.body).on('click submit','.confirm2222222222',function(e){
        var me = jQuery(this);
        var isForm=me.is('form');
        if (me.data('confirmed') || !e.cancelable || (isForm && e.type=='click')) {
            return true;
        }
        e.preventDefault();
        MC_MODAL.confirm(me.data(),e).promise.then(function(){
            me.data('confirmed',true);
            setTimeout(function(){
                me[0][e.type](); }, isForm ? 500 : 1);
        })
        return false;
    });

})
