function rgbToHex(total) {
    var total = total.toString().split(',');
    var r = total[0].substring(4);
    var g = total[1].substring(1);
    var b = total[2].substring(1,total[2].length-1);
    return (checkNumber((r*1).toString(16))+checkNumber((g*1).toString(16))+checkNumber((b*1).toString(16))).toUpperCase();
}
function checkNumber(i){
    i = i.toString();
    if (i.length == 1) return '0'+i;
    else return i;
}

// written by dodo
// http://pure-essence.net/
// read more about it here http://regretless.com/scripts/basics.php?inverse_color

function HexToR(h) { 
	return parseInt((cutHex(h)).substring(0,2),16)
}
function HexToG(h) { 
	return parseInt((cutHex(h)).substring(2,4),16) 
}
function HexToB(h) {
	return parseInt((cutHex(h)).substring(4,6),16) 
}
function cutHex(h) { 
	return (h.charAt(0)=="#") ? h.substring(1,7) : h
}


var hexbase = "0123456789ABCDEFabcdef";
function DecToHex(number) {
	return hexbase.charAt((number>> 4)& 0xf)+ hexbase.charAt(number& 0xf);
}

var strValidChars = "0123456789ABCDEFabcdef";
function IsNumeric(strString)
//  check for valid numeric strings	
{
	var strChar;
	var blnResult = true;

	if (strString.length == 0) return false;

	//  test strString consists of valid characters listed above
	for (i = 0; i < strString.length && blnResult == true; i++)
	 {
	 strChar = strString.charAt(i);
	 if (strValidChars.indexOf(strChar) == -1)
		{
		blnResult = false;
		}
	 }
	return blnResult;
}



function InverseColor(strColor) {
	if(IsNumeric(strColor) == false) {
		//alert("Please enter a valid hex color code with no leading \"#\"!");
		return null;
	}
	var r = 255 - HexToR(strColor);
	var g = 255 - HexToG(strColor);
	var b = 255 - HexToB(strColor);

	var ans = "#"+DecToHex(r).toString()+DecToHex(g).toString()+DecToHex(b).toString();
	return ans;
}


function invertCSSColor(selectorHTML,selectorCSS){
	$.each($(selectorHTML),function(i,el){
		var color=$(el).css(selectorCSS);
		//console.log(color);
		if(color!=null && color!='' && color.indexOf('rgb')>=0){
			var hexColor=rgbToHex(color);
			var colorInverse=InverseColor(hexColor.replace("#",""));
			if(colorInverse!=null){
				//console.log(hexColor + " -> " + colorInverse);
				$(el).css("background","inverted");
				$(el).css(selectorCSS,colorInverse);
			}
		}
	});
}