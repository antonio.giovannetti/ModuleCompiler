/**
 * The jQuery plugin namespace.
 * @external "jQuery.fn"
 * @see {@link http://learn.jquery.com/plugins/|jQuery Plugins}
 */
/**
 * Abilita/disabilita i controlli (input elements) in base ad un elemento (checkbox)
 * @function external:"jQuery.fn".toggleControls
 * @param {object}                options Opzioni
 * @param {string|jQuery}    options.trigger elemento (checkBox) che abilita/disabilita i controlli
 * Se non definito ed e' presente un solo checkbox, verra' usato quello come trigger
 * @para {string}                [options.triggerEvent='click'] evento che che abilita/disabilita i controlli
 * @param {function}            [options.triggerDecorator=jQuery.noop] funzione che vien applicata al trigger
 * @param {boolean}            [options.fancy=false] mostra il trigger come lucchetto
 * @param {boolean}            [options.reverse=false] se true inverte il comportamento
 * @param {boolean}            [options.initialState=false] stato iniziale, coincide con trigger.prop('checked') se il trigger e' un checkbox
 * @param {boolean}            [options.force=undefined] abilita/disabilita forzatamente il gli elementi, ignorando il valore 'reverse'
 * @param {boolean}            [options.init=true] se e' necessario inizializzare gli elementi
 * @param {boolean}            [options.blind=false] se nasconde/mostra il contenitore
 * @param {string}                [options.fieldSelector='input,select,textarea'] elementi a cui applicare la logica
 *
 * @return {jQuery}
 * @version 1.0
 */
jQuery.fn.extend({

    toggleControls: function (options) {
        "use strict";
        var unlocked = 'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gMFCzoaQACV/wAAAPRJREFUOMut071KQ0EUBOBPiWAVsVG3sdDGypQiaO1TiPsCKRVsUmwt+ARCEK3T2tmlslIEX0CC2PiD4E8RmxsIl71JiA4sC8MwDHPO4Y+YyZHtXmsZJ9grqCscxpCexhq0e60F3KKOi4LexwsaMaTXYf1sJsAxlrATQ2rGkJrYxQqOyuKcwRa6MaT7ARFDukMX22VxLWMwj8cMf4rFyg7avdYcNnCJ51zcAt94iCH9lBOcFWUNcDNieuc4KHewmRF+Fq+MxqgSB3jDGtbxXiWqjVm0r+LvT2NQHxpbfdoEH1XrPqnBdQXfzxl0sDrhEXb8F34BXa0ucEThWyoAAAAASUVORK5CYII=)';
        var locked = 'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gMFDAIZy9luOwAAAPJJREFUOMut071KQ0EUBOBPiaBNrPzZxkIbK1OKoLUPIeITpFQQbnkq8RVsROu0dnaprCKCbxBE8BdBsYjNDYSw14TowLIwDMPsnLP8EVM5slsUSzjFbkld4TBFPIw06BbFPDqo46Kk9/GCRop4HdRPZwIcYxHbKaKZIprYwTKOhsU5g020U8Rdn0gRt2hjaxyDWTxn+CfMVXbQLYoZrOMSj5m4J1jAHu5TxDfUBgRnZVl93FRMroNzHAw/YSMj/izPMBq/ddDHG1axhvcqUW3Eon2Vd28Sg/rA2OqTJvioWvdxDa4r+F7OoIWVMT9hy3/hB9AwMDnZ1Ko/AAAAAElFTkSuQmCC)';

        /**
         * estende le options iniziali
         *
         * @var options
         *
         * @extends  jQuery.fn#toggleControls.options
         *
         * @property {object}        [options.trigger=undefined]
         * @property {Event}        [options.triggerEvent=click]
         * @property {boolean}    [options.triggerDecorator=$.noop]
         * @property {boolean}    [options.fancy=false]
         * @property {object}        [options.force=undefined]
         * @property {boolean}    [options.initialState=false]
         * @property {boolean}    [options.reverse=false]
         * @property {boolean}    [options.showHelper=true]
         * @property {boolean}    [options.blind=false]
         * @property {boolean}    [options.init=false]
         * @property {string}        [options.fieldSelector=input,select,textarea]
         * @property {function} [triggerCallback] Funzione invocata quando si clicca sul trigger. Invocata con i seguenti parametri:
         * @param {object} container Contenitore
         * @param {boolean} compiled Se ci sono campi compilati
         * @param {boolean} isOn Se attivo
         *
         * @memberof GET_UTILS
         */
        options = jQuery.extend({
            trigger: undefined,
            triggerEvent: 'click',
            triggerDecorator: jQuery.noop,
            fancy: false,
            force: undefined,
            initialState: false,
            reverse: false,
            showHelper: true,
            blind: false,
            init: true,
            fieldSelector: 'input,select,textarea',
            triggerCallback: jQuery.noop
        }, options);

        var _containerEvent = 'toggleControlsEvent'
        var _toggleState = function (toggler) {
            if (toggler.is(":checkbox")) {
                return;
            }
            console.log('_toggleState:')
            toggler.data('_isOn', !toggler.data('_isOn'))
        }
        var _isToggledOn = function (toggler) {
            if (toggler.is(":checkbox")) {
                return toggler.is(":checked")
            }
            var ison=toggler.data("_isOn")
            return ison;
        }

        var _fancy = function (elTrigger, triggerEvent) {
            if (elTrigger.data('toggleControlsFancy')) return elTrigger.data('toggleControlsFancy');
            var label = elTrigger.next()
            if (!label.is('label')) {
                label = jQuery('<label />')
                elTrigger.after(label)
            }
            elTrigger.data('toggleControlsFancy', label)
            elTrigger.css({'position': 'absolute', 'clip': 'rect(1px, 1px, 1px, 1px)'})
            label.css({
                'margin': 0,
                'padding': '2px 0 0px 24px',
                'cursor': 'pointer',
                'background-position': 'left center',
                'background-repeat': 'no-repeat'
            })
            if (_isToggledOn(elTrigger)) label.css('background-image', unlocked); else label.css('background-image', locked);
            label.on(triggerEvent, function () {
                elTrigger.trigger(triggerEvent);
                if (_isToggledOn(elTrigger)) label.css('background-image', unlocked); else  label.css('background-image', locked);
            })
            return label;
        }

        var _execute = function (event) {
            var elTrigger = event.data
            var container = jQuery(this)
            var _set = options.force;
            if (!_set) {
                _set = options.reverse ? !_isToggledOn(elTrigger) : !!_isToggledOn(elTrigger);
            }
            if (options.blind && !jQuery.contains(container[0], elTrigger[0])) {
                container.data('_originalHeight', container.data('_originalHeight') || container.height())
                container.css({'overflow': 'hidden', 'min-height': 0})
                container
                    .stop()
                    .animate({height: _set ? container.data('_originalHeight') : 0})
            }
            var compiled = false;
            container
                .toggleClass('controlsEnabled', _set)
                .toggleClass('controlsDisabled', !_set)
                .find(options.fieldSelector).each(function (idx, el2) {
                if (el2 !== elTrigger[0]) {
                    var _el2 = jQuery(el2);
                    if (_set)
                        _el2.removeAttr('disabled');
                    else
                        _el2.attr('disabled', 'disabled');

                    var id = _el2.prop('id');
                    if (tinyMCE && tinyMCE.get(id)) {
                        var tEd = tinyMCE.get(id);
                        if (tEd) {
                            compiled = compiled || !!tEd.getContent();
                            // if (_set ) tEd.hide(); else tEd.show();
                            tEd.setMode(_set ? "design":"readonly"); //.setAttribute('contenteditable', _set)
                        }
                    } else {
                        var _compiled;
                        if (_el2.is(":radio") && !_el2.is(":checked")) {
                            _compiled = false;
                        } else {
                            _compiled = !!_el2.val();
                        }
                        compiled = compiled || _compiled;
                    }
                }
            })
            if (!options.init) {
                options.triggerCallback(container, compiled, _isToggledOn(elTrigger));
            } else {
                options.init=false;
            }
        }

        var _bindEvents = function (event, elTrigger) {  /*nothing*/
        }

        var elTrigger;
        if (options.trigger && options.trigger.jquery) {
            elTrigger = jQuery(options.trigger);
            if (elTrigger.length===0) {
                console.error("toggleControls-Nessun elemento trigger impostato");
            }
            elTrigger.on(options.triggerEvent, function () {
                _toggleState(elTrigger);
            })
            if (options.fancy) _fancy(elTrigger, options.triggerEvent)
            jQuery.proxy(options.triggerDecorator, elTrigger)()
        }

        return this.each(function (idx, el) {
            var container = jQuery(el)
            var localTrigger;
            if (elTrigger !== undefined) localTrigger = elTrigger
            else {
                if (options.trigger) {
                    localTrigger = jQuery(options.trigger, container)
                } else {
                    if (jQuery(':checkbox', container).length === 1) {
                        localTrigger = jQuery(':checkbox', container)
                    }
                }
                if (localTrigger) {
                    if (options.fancy) _fancy(localTrigger, options.triggerEvent)
                    if (localTrigger.is(":checkbox")) {
                        localTrigger.data('_isOn', localTrigger.prop('checked'));
                    } else localTrigger.data('_isOn', options.initialState);
                    localTrigger.on(options.triggerEvent, function () {
                        _toggleState(localTrigger);
                    })
                    jQuery.proxy(options.triggerDecorator, localTrigger)()
                }
            }
            if (!localTrigger) return;


            localTrigger.on(options.triggerEvent, function () {
                container.trigger(_containerEvent, [localTrigger])
            });
            if (options.showHelper) {
                var origColor = container.data('toggleControlsOrigColor')
                if (!origColor) {
                    origColor = container.css('background-color')
                    container.data('toggleControlsOrigColor', origColor);
                }
                var activator = localTrigger.data('toggleControlsFancy') || localTrigger
                activator.hover(
                    function (event) {
                        container.css({'background-color': '#deffa1'})
                    },
                    function (event) {
                        container.css({'background-color': origColor})
                    }
                )
            }
            container.on(_containerEvent, localTrigger, _execute)
            if (options.init) {
                container.trigger(_containerEvent, [localTrigger])
            }

        })
    }  /* FINE DI toggleControls() */


});
