/**
 * Oggetto globale che fornisce alcune funzionalita' javascript.
 * @global
 * @namespace MC_UTILS
 * @author Antonio Giovannetti
 */
MC_UTILS = {};

MC_UTILS.regEx={
    // Thanks to MARKETTO, http://blog.marketto.it/2016/01/regex-validazione-codice-fiscale-con-omocodia/#more-4408
    codiceFiscale: /^(?:[B-DF-HJ-NP-TV-Z](?:[AEIOU]{2}|[AEIOU]X)|[AEIOU]{2}X|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[1256LMRS][\dLMNP-V])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[\dLMNP-V][1-9MNP-V]|[1-9MNP-V][0L]))[A-Z]$/i

}

MC_UTILS.calendarOptions = {
    classes:{
        'DATA_FUTURA':'data_futura', // data futura
        'DATA_FUTURA_OGGI':'data_futura_oggi', // data futura, oggi compreso
        'DATA_PASSATA':'data_passata', // data passata
        'DATA_PASSATA_OGGI':'data_passata_oggi', // data passata, oggi compreso
        'VISTA_MESE':'calendario_mese'
    }
}

/**
 * Aggiunge il calendario a tutti gli input con classe calendario dentro un certo elemento .
 * @memberOf MC_UTILS
 * @param {object} [options] le opzioni
 * @param {string} [options.element=document.body] l'elemento in cui cercare 'input.calendario'
 * @param {string} [options.format=DD/MM/YYYY] il formato
 *
 */
MC_UTILS.addCalendar = function(options) {
    // Calendario data
    options = options || {};
    options.format = options.format || 'DD/MM/YYYY';
    options.element = options.element || document.body;
    jQuery("input.calendario", options.element).each(function (idx, elem) {
        var cal = jQuery(elem);
        if (cal.data("calendario")) return;
        var opt = {format: options.format, locale: 'it',useCurrent:false};
        if (cal.hasClass(MC_UTILS.calendarOptions.classes.DATA_FUTURA)) {
            opt.minDate = moment().add(1,'days').set('h',23);
        }
        if (cal.hasClass(MC_UTILS.calendarOptions.classes.DATA_FUTURA_OGGI)) {
            opt.minDate = moment().set('h',23);
        }
        if (cal.hasClass(MC_UTILS.calendarOptions.classes.DATA_PASSATA)) {
            opt.maxDate = moment().add(-1,'days').set('h',23);
        }
        if (cal.hasClass(MC_UTILS.calendarOptions.classes.DATA_PASSATA_OGGI)) {
            opt.maxDate = moment().set('h',23);
        }
        if (cal.hasClass(MC_UTILS.calendarOptions.classes.VISTA_MESE)) {
            opt.viewMode = 'month';
            opt.format = 'MM/YYYY';
        }

        cal.data("calendario",true)
        cal.wrap("<div class=\"input-group\"></div>");
        // cal.datetimepicker({format: options.format, locale: 'it'})
        cal.after("<div class=\"input-group-addon\"><i class=\"fas fa-calendar\" aria-hidden=\"true\"></i></div>");
        cal.datetimepicker(opt)
            .parent().on('click','.input-group-addon',function(){
            cal.trigger('focus');
        })
    })
}
/**
 * Trasforma un panel bootrstrap in collassabile, dentro un certo elemento.
 * @memberOf MC_UTILS
 * @param {object} [element=document.body] l'elemento in cui cercare '.panel.collapsible'
 */
MC_UTILS.addCollapsible = function(element) {
    jQuery(".panel.collapsible", element || document.body).each(function (idx, elem) {
        var me = jQuery(elem);
        if (jQuery(".panel-heading ._handler", me).length!=0) return;
        // me.data("collapsible",true)
        var body = jQuery(".panel-body", me);
        body.css({'max-height':'500px','overflow':'auto'})
        var collapsed = body.hasClass("collapse");
        var _openClose = ['glyphicon glyphicon-folder-open', 'glyphicon glyphicon-folder-close'];
        _openClose = ['fa-arrow-down', 'fa-arrow-up'];
        var handler = jQuery(".panel-heading .collapsible_handler", me);
        var customHandler = true;
        if (handler.length==0) {
            handler = jQuery('<span class="fa btn btn-sm btn-default _handler" ></span>').css('margin-right','.5em');
            jQuery(".panel-heading", me).prepend(handler);
            customHandler = false;
        } else {
            handler.addClass("_handler")
        }
        if (!customHandler) {
            if (collapsed) {
                handler.addClass(_openClose[1]);
            } else {
                handler.addClass(_openClose[0]);
            }
        } else {
            if (collapsed) {
                handler.removeAttr('checked');
            } else {
                handler.attr('checked','checked');
            }
        }

        handler.click(function (e) {
            var toToggle = jQuery(this).closest(".panel").find(".panel-body");
            collapsed = toToggle.hasClass("collapse");
            var anim = toToggle.data('animating');
            if (anim) return;
            toToggle.data('animating',true);
            var duration = 500; //Math.max(500, 500*Math.atan(toToggle.height()));
            if (!customHandler) jQuery(this).toggleClass("fa-flip-vertical");
            if (collapsed) {
                toToggle.slideDown({
                    duration: duration, easing: 'linear', complete: function () {
                        toToggle.data('animating',false);
                        toToggle.removeClass("collapse");
                    }
                })
            } else {
                toToggle.slideUp({
                    duration: duration, easing: 'linear', complete: function () {
                        toToggle.data('animating',false);
                        toToggle.addClass("collapse");
                    }
                });
            }
        })
    })
}

/**
 * Trasforma le textarea.tinyMCE in rich editor TinyMCE.
 * @memberOf MC_UTILS
 * @param {string} [element=document.body] l'elemento in cui cercare 'textarea.tinyMCE'
 * @param {object} [options] le opzioni da passare a tinymce, e' possibile passare le opzioni
 * anche tramite il parametro 'tinyMCEOptions' di jQuery.data, prioritario rispetto a 'options'
 * @return promise, risolta quando tutti i rich editor sono stati inizializzati
 * @example MC_UTILS.addRichEditor().then(function(editors){
 * console.log("Inizializzati" + editors.length + " tinymce editors");
 * // business logic
 * })
 * @example
 * var container=jQuery('...');
 * container.find('textarea').data("tinyMCEOptions",{height:200});
 * MC_UTILS.addRichEditor(container)
 */
MC_UTILS.addRichEditor = function(element,options) {
        var deferred = jQuery.Deferred();
        var instances = [];
        var idx = 0;
        var _tas = jQuery('textarea.tinyMCE',element || document.body);
        var idx = _tas.length;
        if (idx!==0) {
            var _init_instance_callback = !!options ? options.init_instance_callback : undefined;
            var commonOptions=jQuery.extend({
                theme: "modern",
                init_instance_callback : function(editor) {
                    idx--;
                    instances.push(editor);
                    deferred.notify(editor);
                    if (idx===0) {
                        deferred.resolve(instances);
                    }
                    if (_init_instance_callback) {
                        _init_instance_callback(editor);
                    }
                }
            },options);
            _tas.each(function(idx,elem){
                var dataOptions = jQuery(elem).data("tinyMCEOptions");
                if (dataOptions && dataOptions.init_instance_callback) {
                    _init_instance_callback = dataOptions.init_instance_callback;
                }
                var instanceOptions = jQuery.extend(commonOptions,dataOptions);
                jQuery(elem).tinymce(instanceOptions);
            });
        } else {
            deferred.resolve(instances);
        }
        return deferred.promise();
    }

/**
 * Recupera l'editor associato alla textarea.
 * @memberOf MC_UTILS
 * @param {object} element la textarea
 * @return tinymce.Editor
 */
MC_UTILS.getRichEditor = function(element) {
    var found;
    jQuery.each(tinymce.editors,function(idx,elem){
        if (elem.getElement()===element) {
            found=elem;
        }
    })
    return found;
}


/**
 * Trasforma un elemento in autocomplete, in base alle sue classi.
 * Le classi disponibili per l'autocomplete sono:
 * <ul>
 *     <li>autocomplete-scuola</li>
 *     <li>autocomplete-consorzio</li>
 *     <li>autocomplete-coop-servizio</li>
 * </ul>
 * @memberOf MC_UTILS
 * @param {object} [options] le opzioni
 * @param {string} [options.element] l'elemento '.autocomplete'
 * @param {string} [options.key=id] la property usata come chiave dell'elemento
 * @param {string} [options.getValue='desc'] la property o la funzione per valorizzare l'etichetta
 *
 */

MC_UTILS.addAutocomplete = function(options) {
    if (!options.element || jQuery(options.element).length===0) {
        alert("Autocomplete: e' necessario impostare l'elemento (property element)")
        return;
    }
    options.key = options.key || 'id';
    options.getValue = options.getValue || "desc";
    var elem = jQuery(options.element);
    var url = undefined;
    if (elem.hasClass("autocomplete-scuola")) {
        url = TW_URLS.servicesUrl+'/misc/scuola?q=';
    }
    if (elem.hasClass("autocomplete-consorzio")) {
        url = TW_URLS.servicesUrl+'/anagrafica?tipo=CONSORZIO&q=';
        options.key = "cf";
        options.getValue = "denom";
    }
    if (elem.hasClass("autocomplete-coop-servizio")) {
        url = TW_URLS.servicesUrl+'/anagrafica?tipo=COOP_SERVIZI&q=';
        options.key = "cf";
        options.getValue = "denom";
    }
    if (!url)  {
        alert("Autocomplete: impossibile trovare la url per l'elemento, e' stata impostata correttamente la sua classe?")
        return;
    }
    var elemName = elem.attr('name');
    var relatedElem = undefined;
    if (elemName) {
        var re = /(.*)_([^_]*)_([^_]+)$/;
        var relatedElementName = elemName.replace(re,'$1_codice_$3');
        relatedElem = jQuery(":hidden[name='"+relatedElementName+"']")
    }

    var headers = {'Authorization': null};
    window.getBearerToken()
        .then(function (tk) {
            headers.Authorization = 'Bearer ' + tk
        })
    elem.keydown(function(){
        if (relatedElem) {
            relatedElem.val('')
        }
    }).easyAutocomplete({
        url:function(phrase) {
            return url + phrase;
        },
        getValue: options.getValue,
        list: {
            onChooseEvent: function() {
                // console.dir(arguments)
                var itemData = elem.getSelectedItemData();
                if (relatedElem) {
                    relatedElem.val(itemData[options.key])
                }
            },
            onSelectItemEvent: function() {
                console.info('onSelectItemEvent',arguments);
            },
            match: {
                enabled: true
            }
        },
        ajaxSettings:{
            error: function(){
                window.getBearerToken()
                    .then(function (tk) {
                        headers.Authorization = 'Bearer ' + tk
                    })
            },
            headers: headers
        }

    });

}

MC_UTILS.checkSessionTimeout = function(url) {
    var deferred = jQuery.Deferred();
    url = url || MC_UTILS.webContext + "/fe/mock.json";
    jQuery.ajax({
        url: url,
        type:"GET",
        dataType: "json",
        error: function() {
            deferred.reject(arguments,url);
        },
        success: function(data) {
            if (data.ping) {
                deferred.resolve(data);
            } else {
                deferred.reject(arguments,url);
            }
        }
    })
    return deferred;

}


/**
 * Mostra le informazioni su un utente, l'elemento deve contenere in "data-userid" l'id dellutente ne sistema sso.
 */
MC_UTILS.addUserInfo = function(opt /*elem,url,title*/) {
    opt = opt || {};
    opt.offset = opt.offset || -550;
    if (!opt.elem || opt.elem.length==0) return;
    opt.url = opt.url ||  MC_UTILS.webContext+"/fe/pratica!userInfo.action";
    var uid = opt.elem.data("user-id");
    if (!uid) {
        alert("Impossibile recuperare le info dell'utente");
        return;
    }
    jQuery(document.body).css('overflow-x','hidden');
    opt.elem.parent().css({'position':'relative'});
    var template = jQuery("<div style=\"right:-"+opt.offset+"px\" class=\"tooltip bs-tooltip-bottom\" role=\"tooltip\">" +
        "<span class='lead'>"+opt.title+"</span>" +
        "<div class=\"text-left\"><label >Id:&nbsp;</label><b></b></div>" +
        "<div class=\"text-left\"><label >Nome:&nbsp;</label><b></b></div>" +
        "<div class=\"text-left\"><label >Ruoli:&nbsp;</label><u></u>" +
        "</div></div>");
    opt.elem.after(template);
    jQuery.ajax({
        url: opt.url+"?userId="+uid,
        success:function (data) {
            template.find("div:eq(0) label").next().text(data.userRepresentation.id)
            template.find("div:eq(1) label").next().text(data.userRepresentation.firstName + " " + data.userRepresentation.lastName)
            template.find("div:eq(2) label").next().text(jQuery.map(data.roleRepresentations,function (e,i) {
                return e.name;
            }).join(','));
        }
    });
    template.css({"top":opt.elem.position().top+42,'position':'absolute'});
    opt.elem.click(function () {
        var right = template.css("right");
        template.css("right",right != "0px" ? "0px" : opt.offset+"px");
    })
    opt.elem.attr('data-toggle','button');
    opt.elem.attr('aria-pressed','false');
    opt.elem.attr('autocomplete','off');

    // data-toggle="button" aria-pressed="false" autocomplete="off"
}

/**
 * Crea una promise per invocare i webspcket
 * @memberOf MC_UTILS
 * @param {object} [options] le opzioni
 * @param {string} [options.url] la url su cui e' in ascolto il websocket, relativa al context root
 */
MC_UTILS.createWebSocketPromise = function(opt) {
    opt = opt || {};
    if (!opt.url) throw "Websocket: property url obbligatoria";
    opt.url = MC_UTILS.webContext.replace(/^https?/,"ws") + opt.url;
    var server = new WebSocket(opt.url);
    var dfd = jQuery.Deferred();
    server.onopen = function() {
        // console.info("onopen",arguments)
        // dfd.resolve(server,arguments);
    };
    server.onerror = function(err) {
        dfd.reject(server,arguments);
    };
    server.onmessage = function() {
        console.info("onmessage",arguments)
        dfd.notify(server,arguments);
    };
    return dfd.promise();
}

MC_UTILS.createStandardWebSocketCall = function(opt) {
    var wsk = MC_UTILS.createWebSocketPromise(opt);
    wsk.progress(function(websocket,event) {
        var msg = JSON.parse(event[0].data);
        if (jQuery.isFunction(opt.msgFormatter)) {
            MC_MODAL.info({buttons:MC_MODAL.buttons.okButton, modalType:MC_MODAL.modalTypes.warning,text:opt.msgFormatter(msg.data)})
                .promise.then(function () {
                if (msg.snippet) {
                    jQuery.globalEval(msg.snippet);
                }
                // window.location.reload(true);
            })
        } else {
            if (msg.snippet) {
                jQuery.globalEval(msg.snippet);
            }
        }

    });
    wsk.then(function (websocket) {
        // websocket.close();
    },function (websocket) {
        console.info(arguments);

    });
}



jQuery(document).ready(function() {
    MC_UTILS.addCalendar();
    MC_UTILS.addCollapsible();
})