package net.mysoftworks.modulecompiler.render;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.StringTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.detect.TypeDetector;
import org.apache.tika.mime.MediaType;
import org.apache.tika.mime.MediaTypeRegistry;
import org.apache.tika.mime.MimeTypes;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.DefaultParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;

import javax.xml.bind.DatatypeConverter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FreeMarkerUtils {
    private static final Logger log = Logger.getLogger(FreeMarkerUtils.class.getName());
    public static final String FILE_EXTENSION="ftlh";
    public class DBTemplateLoader implements TemplateLoader {


        @Override
        public Object findTemplateSource(String s) throws IOException {
            return null;
        }

        @Override
        public long getLastModified(Object o) {
            return 0;
        }

        @Override
        public Reader getReader(Object o, String s) throws IOException {
            return null;
        }

        @Override
        public void closeTemplateSource(Object o) throws IOException {

        }
    }


    private static Configuration cfg;

    private static void addDefaulSettings(Configuration cfg) {
        cfg.setDefaultEncoding("UTF-8");
        cfg.setLocale(Locale.ITALY);
    }
//    private static Configuration cfgClassPath;
    static {
//        cfgClassPath = new Configuration(new Version(2, 3, 23));
//        // Some other recommended settings:
//        cfgClassPath.setDefaultEncoding("UTF-8");
//        cfgClassPath.setLocale(Locale.ITALY);
//        cfgClassPath.setClassForTemplateLoading(FreeMarkerUtils.class, "/ftl");

        ClassTemplateLoader ctl = new ClassTemplateLoader(FreeMarkerUtils.class, "/ftl");

        MultiTemplateLoader mtl = new MultiTemplateLoader(new TemplateLoader[] { ctl,new StringTemplateLoader()});

        cfg = new Configuration(new Version(2, 3, 23));
        // Some other recommended settings:
        addDefaulSettings(cfg);
        cfg.setTemplateLoader(mtl);

    }

    public static Template getTemplate(String templateName) {
        // Where do we load the templates from:
        try {
            return cfg.getTemplate(templateName);
        } catch (IOException e) {
            log.log(Level.SEVERE,"Il template >"+templateName+"< non esiste o non e' corretto",e);
        }
        return null;
    }

    public static Template getTemplate(Class controller)  {
        String tplPath = controller.getName().replaceAll("\\.","/")+"." + FILE_EXTENSION;
        log.log(Level.FINE,"Caricamento template: {0}",tplPath);
        return getTemplate(tplPath);

    }


    public static Template getTemplateFromString(String id,String templateContent) {
        Template t = null;
        try {
            t = new Template(id,templateContent,cfg);
        } catch (IOException e) {
            log.log(Level.SEVERE,"Impossibile processare il template "+templateContent+" non esiste",e);
        }
        return t;
//        stringLoader.putTemplate(id, templateContent);
//        return cfg.getTemplate(id);
    }

}
