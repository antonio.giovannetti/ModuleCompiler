package net.mysoftworks.modulecompiler.render;

import freemarker.template.Template;
import net.mysoftworks.modulecompiler.bl.manager.GestoreCampo;
import net.mysoftworks.modulecompiler.model.meta.MetaFileField;
import net.mysoftworks.modulecompiler.model.meta.MetaFileTypes;
import net.mysoftworks.modulecompiler.model.values.Value;
import net.mysoftworks.modulecompiler.model.meta.MetaAnagraficaField;
import net.mysoftworks.modulecompiler.model.meta.MetaField;
import net.mysoftworks.modulecompiler.bl.utils.MCPropDescriptor;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;

import javax.ejb.EJB;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class FieldRenderer extends ObjectRender {

    private DichiarazioneRenderer dichiarazioneRender;
    private boolean withLabel=false;
    private Value value;
    private static final String VIEW_TEMPLATE = "field/view."+FreeMarkerUtils.FILE_EXTENSION;
    private static final String EDIT_TEMPLATE = "field/edit."+FreeMarkerUtils.FILE_EXTENSION;

    @EJB
    protected GestoreCampo gestoreCampo;

    public FieldRenderer(DichiarazioneRenderer dichiarazioneRender, Value value, boolean withLabel, boolean editing,boolean inline) {
        super(editing,editing);
//        super(field.getGruppo().getMetaDichiarazione(),edit);
        this.dichiarazioneRender = dichiarazioneRender;
        this.withLabel= withLabel;
        this.value = value;
//        addToMap("gruppo",field.getGruppo());
        addToMap("value",value);
        addToMap("fieldMeta",value.getField());

        MetaField field = value.getField();
        addToMap("withLabel",withLabel);
        if (field instanceof MetaAnagraficaField) {
            Map<String, MCPropDescriptor> p = MCPropDescriptor.factory(((MetaAnagraficaField) field).getEntity());
            addToMap("properties",p);
        }
//        addToMap(dichiarazioneRender.getModelMap());

    }

//    public DefaultFieldController(MetaDichiarazione d, String filedName,boolean edit,boolean withLabel) {
//        super(d,edit);
//        this._fieldName=filedName;
//        this.withLabel = withLabel;
//    }

    @Override
    public void afterInject() {
        if (value.getField() instanceof MetaFileField) {
            MetaFileField mff = (MetaFileField)value.getField();
            List<String> l = gestoreCampo.getMimeTypes(mff.getFileTypes()).stream()
                    .map(ff -> ff.getMimeType())
                    .collect(Collectors.toList());
            addToMap("mimes",l);
        }

    }

    @Override
    public Template getTemplatePrefix() {
        return null;
    }

    @Override
    public Template getTemplatePostFix() {
        return null;
    }

    @Override
    public Template getTemplate() throws IOException{
        if (isEditing()) return FreeMarkerUtils.getTemplate(EDIT_TEMPLATE );
        return FreeMarkerUtils.getTemplate(VIEW_TEMPLATE);
    }

    @Override
    public String[] getTemplateString() {
        return null;
    }

//    public void onRender(HttpServletRequest httpServletRequest, Writer wr) throws Exception {
//        super.onRender(dichiarazioneRender.getModelMap(),wr);
//    }

}
