package net.mysoftworks.modulecompiler.render;

import freemarker.template.Template;
import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.bl.manager.GestoreValue;
import net.mysoftworks.modulecompiler.model.meta.MetaDichiarazione;
import net.mysoftworks.modulecompiler.model.meta.MetaField;
import net.mysoftworks.modulecompiler.model.meta.MetaProcedimento;
import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.model.values.Pratica;
import net.mysoftworks.modulecompiler.model.values.Value;

import javax.ejb.EJB;
import java.io.IOException;
import java.io.StringWriter;
import java.util.logging.Level;

public class DichiarazioneRenderer extends ObjectRender {

    private static final String DEFAULT_VIEW_DIC_TPL_PRE_KEY = "VIEW_DIC_PRE";
    private static final String DEFAULT_VIEW_DIC_TPL_POST_KEY ="VIEW_DIC_POST";
    private static final String DEFAULT_EDIT_DIC_TPL_PRE_KEY = "EDIT_DIC_PRE";
    private static final String DEFAULT_EDIT_DIC_TPL_POST_KEY = "EDIT_DIC_POST";


    private static final String DEFAULT_VIEW_DIC_TEMPLATE_PRE = "<div class=\"view-dichiarazione\" id=\"${uuid}\" >";
    private static final String DEFAULT_VIEW_DIC_TEMPLATE_POST = "</div>";
    private static final String DEFAULT_EDIT_DIC_TEMPLATE_PRE = "<form method=\"POST\" enctype=\"multipart/form-data\" class=\"form-inline\" id=\"${uuid}\" >"+
            "<input type=\"hidden\" name=\"dichiarazione.id\" value=\"${dichiarazione.id}\" /><input type=\"hidden\" name=\"metaDichiarazione.id\" value=\"${metaDichiarazione.id}\" />";
    private static final String DEFAULT_EDIT_DIC_TEMPLATE_POST = "</form>";

    private final Dichiarazione dichiarazione;
    private final MetaDichiarazione metaDichiarazione;
//    private final Pratica pratica;
//    private final MetaProcedimento procedimento;

    @EJB
    GestoreRender gestoreRender;

    public DichiarazioneRenderer(Dichiarazione dichiarazione, MetaDichiarazione metaDichiarazione, MetaProcedimento procedimento, boolean edit) {
        super(edit,edit);
        if (dichiarazione==null) {
            throw new RuntimeException("Dichiarazione is NULL!!!!!!!!!!!");
        }
        this.dichiarazione = dichiarazione;
        this.metaDichiarazione = metaDichiarazione;
//        this.pratica = dichiarazione.getPratica();
//        this.procedimento = procedimento;

        addToMap("metaDichiarazione", metaDichiarazione);
        addToMap("dichiarazione", dichiarazione);
        addToMap("edit",isEditing());
        addToMap("ru",this);
        addToMap("procedimento",procedimento);
//        addToMap("pratica",pratica);
    }

//    public Dichiarazione getDichiarazione() {
//        return dichiarazione;
//    }
//
//    public MetaDichiarazione getMetaDichiarazione() {
//        return metaDichiarazione;
//    }
//
//    public Pratica getPratica() {
//        return pratica;
//    }
//
//    public MetaProcedimento getProcedimento() {
//        return procedimento;
//    }

    @Override
    public void afterInject() {

    }

    @Override
    public Template getTemplatePrefix() {
        if (isEditing()) {
            return FreeMarkerUtils.getTemplateFromString(DEFAULT_EDIT_DIC_TPL_PRE_KEY, DEFAULT_EDIT_DIC_TEMPLATE_PRE);
        }
        return FreeMarkerUtils.getTemplateFromString(DEFAULT_VIEW_DIC_TPL_PRE_KEY, DEFAULT_VIEW_DIC_TEMPLATE_PRE);
    }

    @Override
    public Template getTemplatePostFix() {
        if (isEditing()) {
            return FreeMarkerUtils.getTemplateFromString(DEFAULT_EDIT_DIC_TPL_POST_KEY, DEFAULT_EDIT_DIC_TEMPLATE_POST);
        }
        return FreeMarkerUtils.getTemplateFromString(DEFAULT_VIEW_DIC_TPL_POST_KEY, DEFAULT_VIEW_DIC_TEMPLATE_POST);
    }

    @Override
    public Template getTemplate() throws IOException {
        Loggers.logOperatore(Loggers.LOG_TEMPLATING,Level.FINE,"Cerco template per MetaDichiarazione: {0,number,#}",principal,metaDichiarazione.getId());
        if (metaDichiarazione.getTemplate() == null) {
            Loggers.logOperatore(Loggers.LOG_TEMPLATING,Level.FINEST,"Cerco template per MetaDichiarazione: {0,number,#} nel class path:{1}",principal,metaDichiarazione.getId(),getClass().getName());
            Template tpl = FreeMarkerUtils.getTemplate(getClass());
            if (tpl!=null) {
                return tpl;
            }
            Loggers.logOperatore(Loggers.LOG_TEMPLATING,Level.FINEST,"Restituisco il template di default per MetaDichiarazione: {0,number,#}",principal,metaDichiarazione.getId());
            if (isEditing()) return FreeMarkerUtils.getTemplate("metaDichiarazione/editWrapper."+FreeMarkerUtils.FILE_EXTENSION);
            else return FreeMarkerUtils.getTemplate("metaDichiarazione/view."+FreeMarkerUtils.FILE_EXTENSION);
        } else {
            return FreeMarkerUtils.getTemplateFromString("TPL_DIC_" + metaDichiarazione.getId(), metaDichiarazione.getTemplate().getStringValue());
        }
//        return FreeMarkerUtils.getTemplate(getClass());
    }

    @Override
    public String[] getTemplateString() {
        Loggers.logOperatore(Loggers.LOG_TEMPLATING,Level.FINE,"Cerco templatestring per MetaDichiarazione: {0,number,#}",principal,metaDichiarazione.getId());
        if (metaDichiarazione.getTemplate()!=null) {
            String tplContent = new String(metaDichiarazione.getTemplate().getValue());
            return new String[]{"d"+ metaDichiarazione.getId(),tplContent};
        } else {
            Loggers.logOperatore(Loggers.LOG_TEMPLATING,Level.FINEST,"Templatestring per MetaDichiarazione: {0,number,#} e'' null",principal,metaDichiarazione.getId());
        }
        return null;
    }


    public String renderField(MetaField mf, boolean withLabel,boolean inline) throws Exception {
        return renderField(mf.getNome(),withLabel,inline);
    }

    public String renderField(String fieldName,boolean withLabel) throws Exception {
        return renderField(fieldName,withLabel,true);
    }

    public String renderField(String fieldName,boolean withLabel,boolean inline) throws Exception {
        Loggers.logOperatore(Loggers.LOG_BL,Level.FINEST,"Render field {0}, with label:{1}",principal,fieldName,withLabel);
        return gestoreRender.viewCampo(this,fieldName,dichiarazione,getModelMap(),isEditing(),withLabel,inline);
//        return gestoreRender.viewCampo(this,fieldName,dichiarazione,null/*getModelMap()*/,isEditing(),withLabel);
//        DefaultFieldController fc = ControllerFactory.getInstance().getFieldController(this, gestoreValue.getValue(value.getId()), withLabel);
//        StringWriter sw=new StringWriter();
//        fc.onRender(ServletActionContext.getRequest(),sw);
//        return sw.toString();
    }


    public String renderValue(String xpath) {
        return gestoreRender.viewValue(xpath);
    }

}
