package net.mysoftworks.modulecompiler.render;

import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.bl.manager.AbstractEJB;
import net.mysoftworks.modulecompiler.bl.manager.GestoreDichiarazione;
import net.mysoftworks.modulecompiler.bl.manager.GestorePratica;
import net.mysoftworks.modulecompiler.bl.manager.GestoreValue;
import net.mysoftworks.modulecompiler.model.meta.*;
import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.model.values.Value;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.CharArrayWriter;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Constructor;
import java.util.*;

@Stateless
@LocalBean
public class GestoreRender extends AbstractEJB {

    @PersistenceContext
    EntityManager em;

    @EJB
    GestoreValue gestoreValue;

    @EJB
    GestoreDichiarazione gestoreDichiarazione;

    @EJB
    GestorePratica gestorePratica;

    @Resource
    SessionContext sessionContext;

    public String viewDichiarazione(Long idDichiarazione, Map<String,Object> externalParams, boolean edit) {
        Dichiarazione d = em.find(Dichiarazione.class, idDichiarazione);
        MetaDichiarazione md = em.find(MetaDichiarazione.class,d.getIdMetaDichiarazione());
        StringWriter writer = new StringWriter();
        try {
            Constructor<DichiarazioneRenderer> constr = RenderFactory.getRendererDichiarazione(md);
            DichiarazioneRenderer render = constr.newInstance(d, md, d.getPratica().getProcedimento(), edit);
            EJBInjector.injectEJB(render,true);
            render.setPrincipal(sessionContext.getCallerPrincipal());
            render.onRender(externalParams, writer);
            writer.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return writer.getBuffer().toString();
    }

    public String viewCampo(DichiarazioneRenderer dr, String fieldName, Dichiarazione d, Map<String,Object> externalParams, boolean edit, boolean withLabel,boolean inline) {
        Value v = gestoreValue.getValueByFieldName(d.getId(),fieldName);
        v.getField().setInline(inline);
        Constructor<FieldRenderer> constr = RenderFactory.getRendererCampo(v.getField());
        StringWriter writer = new StringWriter();
        try {
            FieldRenderer render = constr.newInstance(dr,v,withLabel,edit,inline);
            EJBInjector.injectEJB(render,true);
            render.setPrincipal(sessionContext.getCallerPrincipal());
            render.onRender(externalParams,writer);
            writer.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return writer.getBuffer().toString();
    }


    public String viewValue(String xpath) {
        try {
            XPathExpression xpathexpression = XPathFactory.newInstance().newXPath().compile(xpath);
        } catch (Exception e) {
//            Loggers.logOperatore();
        }
        return null;
    }

}
