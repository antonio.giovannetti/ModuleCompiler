package net.mysoftworks.modulecompiler.render;

import net.mysoftworks.modulecompiler.model.meta.MetaDichiarazione;
import net.mysoftworks.modulecompiler.model.meta.MetaProcedimento;
import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.model.values.Value;

import java.lang.reflect.Constructor;

public class RenderFactory {

    private static <T extends ObjectRender> Constructor<T> getConstructor(Renderable<T> renderable,Class ... contrArgs) {
        Class<T> rc = renderable.getRendererClass();
        if (rc!=null) {
            try {
                return rc.getConstructor(contrArgs);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        try {
            return renderable.getDefaultRendererClass().getConstructor(contrArgs);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;

    }
    public static <T extends DichiarazioneRenderer> Constructor<T> getRendererDichiarazione(Renderable<T> renderable) {
        return getConstructor(renderable,Dichiarazione.class, MetaDichiarazione.class, MetaProcedimento.class, boolean.class);
    }

    public static <T extends FieldRenderer> Constructor<T> getRendererCampo(Renderable<T> renderable) {
        return getConstructor(renderable,DichiarazioneRenderer.class, Value.class, boolean.class,boolean.class,boolean.class);
    }


}
