package net.mysoftworks.modulecompiler.render;

public interface Renderable<T extends ObjectRender> {
    public Class<T> getRendererClass();
    public Class<T> getDefaultRendererClass();

}
