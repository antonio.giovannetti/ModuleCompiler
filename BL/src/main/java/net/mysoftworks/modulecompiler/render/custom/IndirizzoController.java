package net.mysoftworks.modulecompiler.render.custom;

import net.mysoftworks.modulecompiler.model.meta.MetaDichiarazione;
import net.mysoftworks.modulecompiler.model.meta.MetaProcedimento;
import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.render.DichiarazioneRenderer;

import java.io.Writer;
import java.util.Map;

public class IndirizzoController extends DichiarazioneRenderer {

    public IndirizzoController(Dichiarazione dichiarazione, MetaDichiarazione metaDichiarazione, MetaProcedimento procedimento,boolean edit) {
        super(dichiarazione, metaDichiarazione,procedimento,edit);
    }

    @Override
    public void onRender(Map<String,Object> model, Writer wr) throws Exception {
        addToMap("sergio","AAAAAAAAAAAAAA:"+System.currentTimeMillis());
        super.onRender(model, wr);
    }

}
