package net.mysoftworks.modulecompiler.render;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.bl.manager.GestoreValue;
import net.mysoftworks.modulecompiler.render.FreeMarkerUtils;
import org.keycloak.KeycloakSecurityContext;
//import org.apache.struts2.ServletActionContext;

//import javax.servlet.http.HttpServletRequest;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.SessionContext;
import javax.ejb.embeddable.EJBContainer;
import javax.ws.rs.core.SecurityContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.security.Principal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class ObjectRender {
//    protected static final Logger log = Logger.getLogger(ObjectRender.class.getName());
//    protected SDPratica sdPratica;

    private Map<String,Object> modelMap = new HashMap<>();

    private boolean editable = false;
    private boolean editing = false;
    public ObjectRender(boolean editable,boolean editing){
        this.editable = editable;
        if (!editable) this.editing = false; else this.editing = editing;
        String uuid = UUID.randomUUID().toString();
        modelMap.put("uuid", uuid);
    }
//    @EJB
//    protected GestoreValue gestoreValue;
//    @EJB
//    protected GestoreRender gestoreRender;

    protected Principal principal;

    public void setPrincipal(Principal principal) {
        this.principal = principal;
    }

    protected void addToMap(String key, Object value) {
        modelMap.put(key,value);
    }

//    protected void addToMap(Map<String,Object> modelMap) {
//        this.modelMap.putAll(modelMap);
//    }

    public boolean isEditable() {
        return editable;
    }
    public boolean isEditing() {
        return editing;
    }
    public void setEditing(boolean editing) {this.editing = editing;}

    //    public Map<String, Object> getModelMap() {
//        return modelMap;
//    }

    //    protected Map<String,Object> getDefaultMap() {
//        Map<String,Object> modelMap = new HashMap<>();
//        modelMap.put("procedimento",getProcedimento());
//        modelMap.put("pratica",sdPratica.getPratica());
//        modelMap.put("values",sdPratica.getValues());
//        modelMap.put("uuid",ServletActionContext.getRequest().getAttribute(UuidFilter.UUID_ATTRIBUTE));
//
//        return modelMap;
//    }
//    public void addToModel(String key, Object value) {
//        modelMap.put(key,value);
//    }


    private void printFTLFromString(String key,String content,Writer wr)throws IOException {

        Template tpl = FreeMarkerUtils.getTemplateFromString(key,content);
        try {
            tpl.setAutoFlush(false);
            getTemplatePrefix().process(modelMap, wr);
            tpl.process(modelMap, wr);
            getTemplatePostFix().process(modelMap,wr);
        } catch (TemplateException e) {
            e.printStackTrace(new PrintWriter(wr));
        }
    }

    private void printFTL(String ftl,Writer wr)throws Exception {
        Template tpl = FreeMarkerUtils.getTemplate(ftl);
        tpl.setAutoFlush(false);
        getTemplatePrefix().process(modelMap, wr);
        tpl.process(modelMap, wr);
        getTemplatePostFix().process(modelMap,wr);
    }

    private void printFTL(Template tpl,Writer wr)throws Exception {
        tpl.setAutoFlush(false);
        Template pref = getTemplatePrefix();
        if (pref!=null) {
            pref.process(modelMap, wr);
        }
        tpl.process(modelMap, wr);
        Template post = getTemplatePostFix();
        if (post!=null) {
            post.process(modelMap,wr);
        }
    }

    public void onRender(Map<String,Object> model, Writer wr) throws Exception {
//        addToMap("uuid",httpServletRequest.getAttribute(UuidFilter.UUID_ATTRIBUTE));
        if (model!=null) {
            model.entrySet().stream().forEach(e->{
                modelMap.putIfAbsent(e.getKey(),e.getValue());
            });
        }

        Template tpl = getTemplate();
        Boolean edit=null;
        if (tpl!=null) {
            Loggers.logOperatore(Loggers.LOG_BL,Level.FINEST,"{1} {0} using template",principal,getClass().getName(), Boolean.TRUE.equals(edit) ? "Editing":"Rendering");
            printFTL(tpl,wr);
            return;
        }
        String[] s = getTemplateString();
        if (s!=null) {
            Loggers.logOperatore(Loggers.LOG_BL,Level.FINEST,"{1} {0} using templateString",principal,getClass().getName(), Boolean.TRUE.equals(edit) ? "Editing":"Rendering");
            printFTLFromString(s[0],s[1], wr);
            return;
        }

        throw new RuntimeException("Non e' stato definito ne' template ne' templateString");
    }
    public Map<String,Object> getModelMap() {
        return modelMap;
    }

    public abstract void afterInject();
    public abstract Template getTemplatePrefix();
    public abstract Template getTemplatePostFix();
    public abstract Template getTemplate() throws IOException;
    public abstract String[] getTemplateString();

//    public abstract void onRender(HttpServletRequest httpServletRequest, Writer wr) throws Exception;
//    public abstract void onEdit(HttpServletRequest httpServletRequest, Writer wr) throws IOException, TemplateException;
//    public abstract void onSubmit(HttpServletRequest httpServletRequest, Writer wr) throws Exception;

}
