package net.mysoftworks.modulecompiler.render;

import org.apache.commons.lang3.StringUtils;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EJBInjector {
    private static final Logger log = Logger.getLogger(EJBInjector.class.getName());

    public static final String EJB_PREFIX = "java:app/BL/";

    private static void logOperatore(Logger log, Level level, String s, Object... params) {
        log.log(level, s, params);
    }

    private static InitialContext ctx;
    private static void init(){
        try {
            ctx = new InitialContext();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static <S> S lookup(Class<S> clazz,String lookup) {
        String lk = null;
        if (StringUtils.isEmpty(lookup)) {
            lk=EJB_PREFIX + clazz.getSimpleName() + "!" + clazz.getName();
        } else lk=lookup;
        logOperatore(log, Level.FINEST, "Eseguo lookup su {0} per {1}:",lk, clazz);
        if (ctx==null) init();
            try {
                return (S) ctx.lookup(lk);
            } catch (NamingException e) {
                logOperatore(log, Level.SEVERE, "Errore nel lookup per:" + lk, e);
            }
        return null;
    }

    public static void injectEJB(Object actionInstance,boolean parents) {
        if (actionInstance == null) return;
        Class<?> clazz = actionInstance.getClass();
        if (!parents) {
            doInjectEJB(clazz, actionInstance);
        } else {
            while (!Object.class.equals(clazz)) {
                doInjectEJB(clazz, actionInstance);
                clazz = clazz.getSuperclass();
            }
        }
        if (actionInstance instanceof ObjectRender) {
            ((ObjectRender)actionInstance).afterInject();
        }
    }

    private static <P> void doInjectEJB(Class clazz, Object actionInstance) {

        logOperatore(log, Level.FINER, "Checking for EJBs in action {1} of type {0}", clazz, actionInstance);
        for (Field f : clazz.getDeclaredFields()) {
            logOperatore(log, Level.FINEST, "Found field {0}.{1}", actionInstance, f);
                Class cl = f.getType();
                EJB ann = (EJB) f.getAnnotation(EJB.class);
                if (ann != null) {
                    logOperatore(log, Level.FINEST, "Found ejb field {0}.{1}", actionInstance, f);
                    f.setAccessible(true);
                    Object ejbRef = lookup(f.getType(),ann.lookup());
                    try {
                        f.set(actionInstance, ejbRef);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }

        }
    }


}
