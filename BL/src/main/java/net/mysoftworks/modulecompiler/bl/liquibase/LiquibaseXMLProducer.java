package net.mysoftworks.modulecompiler.bl.liquibase;

import net.mysoftworks.modulecompiler.Loggers;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.NotImplementedException;
//import static org.mockito.Mockito.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

public abstract class LiquibaseXMLProducer {
    private abstract class FunnyResultSet implements ResultSet {

    }
    private class ResultSetDummy {
        private String tableName;
        private String[] names;
        private int[] types;
        private List<Object[]> values;

        public ResultSetDummy(String tableName, String[] names, int[] types) {
            this.tableName = tableName;
            this.names = names;
            this.types = types;
        }
        private void addItem(Object[] item){
            if (values==null) values = new ArrayList<>();
            values.add(item);
        }



//        private ResultSet getResultSet() {
//            ResultSet mock=mock(ResultSet.class);
//            return mock;
//
//        }

    }


    protected static final String AUTHOR = "antonio-bkp";
    protected static final String BIN_FILE_EXTENSION = ".bin";
    protected static final String ATTR_NAME_AUTHOR = "author";
    protected static final String ATTR_NAME_ID = "id";
    protected static final String ATTR_NAME_TABLE_NAME = "tableName";
    protected static final String ATTR_NAME_NAME = "name";
    protected static final String ATTR_NAME_V = "value";
    protected static final String ATTR_NAME_V_DATE = "valueDate";
    protected static final String ATTR_NAME_V_BOOL = "valueBoolean";
    protected static final String ATTR_NAME_V_NUMERIC = "valueNumeric";
    protected static final String ATTR_NAME_V_CLOB = "valueClobFile";
    protected static final String EL_NAME_CHANGESET = "changeSet";
    protected static final String EL_NAME_SQL = "sql";
    protected static final String EL_NAME_INSERT = "insert";
    protected static final String EL_NAME_COLUMN = "column";
    protected static final String EXPORT_FILENAME = "export.xml";
    protected static final String SCHEMA = "mc_db.";

    // disable foreign keys mysql: SET FOREIGN_KEY_CHECKS=0;
    // enable foreign keys mysql: SET FOREIGN_KEY_CHECKS=1;

    private static String[] EXPORT_ORDER = new String[]{"MC_LOBS", "MC_M_DICHIARAZIONE", "MC_M_FIELD", "MC_M_ALLOWED_VALUE", "MC_M_PROCEDIMENTO", "MC_M_PR_DIC", "MC_M_PR_ALL"};

    protected final Connection connection;
    private long idSuffix;
    private boolean dropExisting=false;
    private Set<ResultSetDummy> savedAssocs=new HashSet<>();

    public void setDropExisting(boolean dropExisting) {
        this.dropExisting = dropExisting;
    }

    public LiquibaseXMLProducer(Connection connection) {
        this.connection = connection;
    }

    protected abstract String saveLob(byte[] content, String table, String columnName) throws Exception;

    protected abstract void exportChangeLog(Document doc) throws Exception;

    private void addAttribute(Element element, String attrName, Object attrValue) {
        if (attrValue==null) return;
        element.setAttribute(attrName, attrValue.toString());
    }

    private Element createChangeSet(Document doc, String idPrefix) {
        Element element = doc.createElement(EL_NAME_CHANGESET);
        addAttribute(element,ATTR_NAME_AUTHOR, AUTHOR);
        addAttribute(element,ATTR_NAME_ID, idPrefix+"_"+idSuffix);
        doc.getDocumentElement().appendChild(element);
        return element;

    }


    private ResultSetDummy doSaveAssoc(String table, Long idProc) throws Exception{
        StringBuilder sql= new StringBuilder();
        sql.append("select * from ");
        sql.append(SCHEMA).append(table);
        if (idProc!=null) {
            sql.append(" WHERE PR != ").append(idProc);
        }

//        System.out.println("\n\n==============SQL :table="+table+"\n" + sql.toString());
        PreparedStatement prep = connection.prepareStatement(sql.toString());
        ResultSetDummy out = null;
        if (prep.execute()) {
            ResultSetMetaData rsMeta = prep.getMetaData();
            int[] types = new int[rsMeta.getColumnCount()];
            String[] names = new String[rsMeta.getColumnCount()];
            for (int i = 1; i <= rsMeta.getColumnCount(); i++) {
                int ctype = rsMeta.getColumnType(i);
                types[i - 1] = rsMeta.getColumnType(i);
                names[i - 1] = rsMeta.getColumnName(i);
            }
            ResultSet rs = prep.getResultSet();
            while (rs.next()) {
                if (out==null) out = new ResultSetDummy(table,names,types);
                Object[] item = new Object[types.length];
                out.addItem(item);
                for (int j = 1; j <= types.length; j++) {
                    int ctype = types[j - 1];
                    switch (ctype) {
                        case Types.INTEGER:
                            item[j-1] = rs.getInt(j);
                            break;
                        case Types.BIGINT:
                            item[j-1] = rs.getLong(j);
                            break;
                        default:
                            throw new RuntimeException(table + "." + names[j - 1] + "==========save assoc. tipo non gestito:" + ctype);

                    }
                }
            }
        }
        return out;
    }

    private void doRestoreSavedAssoc(Document doc,ResultSetDummy sa) throws Exception{
        if (sa.values==null) return;
        Element elementChangeSet = createChangeSet(doc,"restore_"+sa.tableName);
        addInsertRestore(doc,sa,elementChangeSet);
    }

    private void addInsertRestore(Document doc, ResultSetDummy rs,Element elementChangeSet) throws Exception {
        if (CollectionUtils.isEmpty(rs.values)) return;
        for (Object[] item:rs.values) {
            Element insert = doc.createElement(EL_NAME_INSERT);
            addAttribute(insert, ATTR_NAME_TABLE_NAME, rs.tableName);
            elementChangeSet.appendChild(insert);

            for (int j = 1; j <= rs.types.length; j++) {
                Element column = doc.createElement(EL_NAME_COLUMN);
                insert.appendChild(column);
                addAttribute(column, ATTR_NAME_NAME, rs.names[j - 1]);
                int ctype = rs.types[j - 1];
                switch (ctype) {
                    case Types.TIMESTAMP:
                        addAttribute(column, ATTR_NAME_V_DATE, item[j-1]);
                        break;
                    case Types.BIT:
                        addAttribute(column, ATTR_NAME_V_BOOL, item[j-1]);
                        break;
                    case Types.VARCHAR:
                        addAttribute(column, ATTR_NAME_V, item[j-1]);
                        break;
                    case Types.TINYINT:
                    case Types.INTEGER:
                    case Types.BIGINT:
                        addAttribute(column, ATTR_NAME_V_NUMERIC, item[j-1]);
                        break;
                    case Types.LONGVARBINARY:
                    case Types.BLOB:
                        String fname = saveLob((byte[]) item[j-1], rs.tableName, rs.names[j - 1]);
                        addAttribute(column, ATTR_NAME_V_CLOB, fname);
                        break;
                    case Types.BOOLEAN:
                        addAttribute(column, ATTR_NAME_V_BOOL, item[j-1]);
                        break;
                    default:
                        throw new RuntimeException(rs.tableName+ "." + rs.names[j - 1] + "==========tipo non gestito:" + ctype);
                }
            }
        }


    }

    private void addInsert(Document doc, PreparedStatement prep,Element elementChangeSet,String table) throws Exception{
        if (prep.execute()) {
            ResultSetMetaData rsMeta = prep.getMetaData();
            int[] types = new int[rsMeta.getColumnCount()];
            String[] names = new String[rsMeta.getColumnCount()];
            for (int i = 1; i <= rsMeta.getColumnCount(); i++) {
                int ctype = rsMeta.getColumnType(i);
                types[i - 1] = rsMeta.getColumnType(i);
                names[i - 1] = rsMeta.getColumnName(i);
            }
            ResultSet rs = prep.getResultSet();
            while (rs.next()) {
                Element insert = doc.createElement(EL_NAME_INSERT);
                addAttribute(insert, ATTR_NAME_TABLE_NAME, table);
                elementChangeSet.appendChild(insert);
                for (int j = 1; j <= types.length; j++) {
                    Element column = doc.createElement(EL_NAME_COLUMN);
                    insert.appendChild(column);
                    addAttribute(column, ATTR_NAME_NAME, names[j - 1]);
                    int ctype = types[j - 1];
                    switch (ctype) {
                        case Types.TIMESTAMP:
                            addAttribute(column, ATTR_NAME_V_DATE, rs.getTimestamp(j));
                            break;
                        case Types.BIT:
                            addAttribute(column, ATTR_NAME_V_BOOL, rs.getBoolean(j));
                            break;
                        case Types.VARCHAR:
                            addAttribute(column, ATTR_NAME_V, rs.getString(j));
                            break;
                        case Types.TINYINT:
                        case Types.INTEGER:
                        case Types.BIGINT:
                            addAttribute(column, ATTR_NAME_V_NUMERIC, rs.getLong(j));
                            break;
                        case Types.LONGVARBINARY:
                        case Types.BLOB:
                            String fname = saveLob(rs.getBytes(j), table, names[j - 1]);
                            addAttribute(column, ATTR_NAME_V_CLOB, fname);
                            break;
                        case Types.BOOLEAN:
                            addAttribute(column, ATTR_NAME_V_BOOL, rs.getBoolean(j));
                            break;
                        default:
                            throw new RuntimeException(table + "." + names[j - 1] + "==========tipo non gestito:" + ctype);
                    }
                }
            }



        }
    }

    private void doExportTable(String table, Document doc, Element parentNode, Long idDic, Long idProc,Long newId) throws Exception {
        Loggers.logOperatore(Loggers.LOG_ADMIN,Level.FINE,"Export cfg: id Dic:{0,number,#},idProc:{1,number,#},newId:{2,number,#},",
                null,idDic,idProc,newId);
        Element elementChangeSet = createChangeSet(doc,table);
        StringBuilder sql = new StringBuilder(); //new StringBuilder("select * from ");
        sql.append(SCHEMA).append(table);
        StringBuilder where = new StringBuilder();
        boolean saveAssoc = "MC_M_PR_DIC".equalsIgnoreCase(table);
        switch (table){
            case "MC_M_PR_ALL":
            case "MC_M_PR_DIC":
                if (idProc != null) {
                    sql.append(" WHERE PR = ").append(idProc);
                }
                break;
            case "MC_M_PROCEDIMENTO":
                if (idProc != null) {
                    sql.append(" WHERE id = ").append(idProc);
                }
                break;
            case "MC_LOBS":
                if (idDic != null) {
                    where.append(" WHERE id=").append(idDic);
                }
                if (idProc != null) {
                    where.append(" WHERE id in (SELECT DIC FROM ").append(SCHEMA).append("MC_M_PR_DIC WHERE PR=").append(idProc).append(")");
                }

                sql.append(" where id in (select id_template from ").append(SCHEMA).append("MC_M_DICHIARAZIONE");
                sql.append(where);
                sql.append (")or id in (select id_guida from ").append(SCHEMA).append("MC_M_DICHIARAZIONE");
                sql.append(where).append(")");
                if (idDic==null) {
                    sql.append(" or id in (select id_guida from ").append(SCHEMA).append("MC_M_PROCEDIMENTO");
                    if (idProc != null) {
                        sql.append(" WHERE ID = ").append(idProc);
                    }
                    sql.append(")");
                }
                break;
            case "MC_M_DICHIARAZIONE":
                if (idDic != null) {
                    sql.append(" WHERE id=").append(idDic);
                }
                if (idProc != null) {
                    sql.append(" WHERE id in (SELECT DIC FROM ").append(SCHEMA).append("MC_M_PR_DIC WHERE PR=").append(idProc).append(")");
                }
                break;
            case "MC_M_FIELD":
                if (idDic != null) {
                    sql.append(" WHERE ID_DIC=").append(idDic);
                }
                if (idProc != null) {
                    sql.append(" WHERE ID_DIC in (SELECT DIC FROM ").append(SCHEMA).append("MC_M_PR_DIC WHERE PR=").append(idProc).append(")");
                }
                break;
            case "MC_M_ALLOWED_VALUE":
                if (idDic != null) {
                    sql.append(" WHERE ID in (SELECT id FROM ").append(SCHEMA).append("MC_M_FIELD WHERE ID_DIC=").append(idDic).append(")");
                }
                if (idProc != null) {
                    sql.append(" WHERE ID in (SELECT id FROM ").append(SCHEMA).append("MC_M_FIELD WHERE ID_DIC in (SELECT DIC FROM ").append(SCHEMA).append("MC_M_PR_DIC WHERE PR=").append(idProc).append("))");
                }
                break;

        }
//        if (table.equalsIgnoreCase("MC_LOBS")) {
//            sql.append(" where id in (select id_template from MC_M_DICHIARAZIONE) or id in (select id_guida from MC_M_DICHIARAZIONE) or id in (select id_guida from MC_M_PROCEDIMENTO)");
//        }
//        if (idDic != null) {
//
//        }

        if (saveAssoc) {
            ResultSetDummy sa = doSaveAssoc(table, idProc);
            if (sa!=null) {
                System.out.println("\n\n==============SAVED ASSOC:"+sa);
                savedAssocs.add(sa);
            }
        }
        if (dropExisting) {
            StringBuilder dropStatement = new StringBuilder(sql);
            dropStatement.insert(0,"DROP ");
            Element elementDrop = doc.createElement(EL_NAME_SQL);
            elementDrop.setTextContent(dropStatement.toString());
            elementChangeSet.appendChild(elementDrop);
        }

        sql.insert(0,"select * from ");
//        System.out.println("\n\n==============SQL :table="+table+"\n" + sql.toString());
        PreparedStatement prep = connection.prepareStatement(sql.toString());
        addInsert(doc,prep,elementChangeSet,table);

    }


    private Element createRootNode(Document doc) {
        Element elRoot = doc.createElement("databaseChangeLog");
        addAttribute(elRoot,"xmlns", "http://www.liquibase.org/xml/ns/dbchangelog");
        addAttribute(elRoot,"xmlns:ext", "http://www.liquibase.org/xml/ns/dbchangelog-ext");
        addAttribute(elRoot,"xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        addAttribute(elRoot,"xsi:schemaLocation", "http://www.liquibase.org/xml/ns/dbchangelog-ext http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.6.xsd");
        doc.appendChild(elRoot);
        return elRoot;
    }
    private void disableFK(Document doc,boolean disable) {
        Element cs = createChangeSet(doc, "disable-fk");
        Element el = doc.createElement(EL_NAME_SQL);
        el.setTextContent("SET FOREIGN_KEY_CHECKS=" + (disable ? "0" : "1"));
        cs.appendChild(el);
    }

    private void doExport(Long idMetaDic,Long idProc,Long newId) throws Exception {
        if (newId!=null) throw  new NotImplementedException("newId non implementato");

        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        idSuffix = System.currentTimeMillis();
        Element rootNode = createRootNode(doc);
        if (dropExisting) {
            disableFK(doc,true);
        }
        for (String table : EXPORT_ORDER) {
            if (idMetaDic!=null && ("MC_M_PROCEDIMENTO".equals(table) || "MC_M_PR_DIC".equals(table) || "MC_M_PR_ALL".equals(table) )) continue;
            doExportTable(table, doc, rootNode,idMetaDic,idProc,newId);
        }
        for(ResultSetDummy sa:savedAssocs) {
            doRestoreSavedAssoc(doc,sa);
        }
        if (dropExisting) {
            disableFK(doc,false);
        }
        exportChangeLog(doc);
    }

    public void exportProc(Long idProc,Long newId) throws Exception {
        doExport(null,idProc,newId);
    }

    public void exportDic(Long id,Long newId) throws Exception {
        doExport(id,null,newId);
    }

    public void exportAll() throws Exception {
        doExport(null,null,null);
    }


}
