package net.mysoftworks.modulecompiler.bl.utils;

import org.apache.commons.collections.MapUtils;
import org.w3c.dom.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class JaxbSerializer {
    private final Logger logger = Logger.getLogger(JaxbSerializer.class.getName());

    public static void documentToString(Node doc,Result sr,Map<String,String> attrs) {
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.CDATA_SECTION_ELEMENTS,"stringValue");

            transformer.transform(new DOMSource(doc), sr);
        } catch (Exception ex) {
            ex.printStackTrace();
            // throw new RuntimeException("Error converting to String", ex);
        }
    }

    public static String documentToString(Node doc,Map<String,String> attrs) {
        try {
            StringWriter sw = new StringWriter();
            Result res = new StreamResult(sw);
            documentToString(doc,res,attrs);
            return sw.toString();
        } catch (Exception ex) {
            // Fails silently
            return ex.getMessage();
            // throw new RuntimeException("Error converting to String", ex);
        }
    }

    private Document doc;

    public JaxbSerializer() {
        try {
            doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public <T> void build(T entity,Class<T> clazz) {
        serializeEntity(entity, clazz,doc);

    }

    public Document getDocument() {
        return doc;
    }


    private <T> Node serializeEntity(T entity, Class<T> clazz,Node element) {
//        logOperatore(logger,Level.FINEST, "JAXB serialize entity {0}", entity);
        if (entity!=null)
        try {
            Node node = element==null ? doc.createElement(clazz.getSimpleName()) : element;
//            Class<?> clazz = entity.getClass();
            JAXBContext ctx = JAXBContext.newInstance(clazz);
            Marshaller jaxbMarshaller = ctx.createMarshaller();
            JAXBElement<T> je2 = new JAXBElement(new QName(clazz.getSimpleName()), clazz, entity);
            jaxbMarshaller.marshal(je2, node);
            return node;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addRootNode(String content, String nodeName, Map<String,String> attributes) {
//        Document doc  = ((Document)source);
        String tagname = nodeName+"Container";
        NodeList nl = doc.getElementsByTagName(tagname);
        Element mds = null;
        if (nl.getLength()==0) {
            mds = doc.createElement(tagname);
            doc.getDocumentElement().appendChild(mds);
        } else {
            mds = (Element) nl.item(0);
        }
        Element el = doc.createElement(nodeName);
        CDATASection cdataSection = doc.createCDATASection(content);
        el.appendChild(cdataSection);
        mds.appendChild(el);
        if (MapUtils.isNotEmpty(attributes)) {
            for(Map.Entry<String,String> e: attributes.entrySet()) {
                el.setAttribute(e.getKey(),e.getValue());
            }
        }
    }


    public <T> void addRootNode(Collection<T> entity, Class<T> clazz, Map<String,String> attributes) {
//        Document doc  = ((Document)source);
        String tagname = clazz.getSimpleName()+"Container";
        NodeList nl = doc.getElementsByTagName(tagname);
        Element mds = null;
        if (nl.getLength()==0) {
            mds = doc.createElement(tagname);
            doc.getDocumentElement().appendChild(mds);
        } else {
            mds = (Element) nl.item(0);
        }
        for(T t:entity) {
            Node imp = serializeEntity(t,clazz,null);
            mds.appendChild(imp.getFirstChild());
            if (MapUtils.isNotEmpty(attributes)) {
                for(Map.Entry<String,String> e: attributes.entrySet()) {
                    Element el = (Element)imp;
                    el.setAttribute(e.getKey(),e.getValue());
                }
            }
        }
    }


//    public <T> void addRootNode(Set<Map.Entry<Long,T>> entries, Class<T> clazz) {
//        for (Map.Entry<Long,T> e:entries) {
//            addRootNode(e.getValue(),clazz);
//        }
//    }
}
