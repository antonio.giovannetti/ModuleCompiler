package net.mysoftworks.modulecompiler.bl.manager;

import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.bl.validators.DichiarazioneValidator;
import net.mysoftworks.modulecompiler.bl.validators.ValidationMessage;
import net.mysoftworks.modulecompiler.dto.BLException;
import net.mysoftworks.modulecompiler.dto.FileUpload;
import net.mysoftworks.modulecompiler.model.ValueLob;
import net.mysoftworks.modulecompiler.model.meta.*;
import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.model.values.Pratica;
import net.mysoftworks.modulecompiler.model.values.RawValue;
import net.mysoftworks.modulecompiler.model.values.Value;
import net.mysoftworks.modulecompiler.search.MetaDichiarazioneSearchParam;
import net.mysoftworks.modulecompiler.search.processors.MetaDichiarazioneSearchParamProcessor;
import net.mysoftworks.modulecompiler.search.SearchOutput;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@LocalBean
public class GestoreDichiarazione extends AbstractEJB {
    public static final String CLONE_SUFFIX = " (copia)";
    private static final Logger logger = Logger.getLogger(GestoreDichiarazione.class.getName());
    private @PersistenceContext(unitName = "mc-datasource")
    EntityManager em;

    @Resource
    SessionContext sessionContext;

    public void changeCompiler(Long id, String user) {
        logOperatore(logger, Level.FINE, "Cambio compilatore per la dichiarazione {0,number,#}--->{1}",id,user);
        Dichiarazione d = em.find(Dichiarazione.class,id);
        d.setCompilatore(user);

    }

    public enum MOVE_DIR {UP, DOWN}

    @EJB
    GestoreValue gestoreValue;

    @EJB
    GestoreCampo gestoreCampo;
    public void moveDic(Long idProc, Long idMetaDic, MOVE_DIR moveDir) {
        MetaProcedimento metaProcedimento = em.find(MetaProcedimento.class, idProc);
        int i = 0;
        int last = metaProcedimento.getDichiarazioni().size();
        List<MetaDichiarazione> temp = new ArrayList<>();
        temp.addAll(metaProcedimento.getDichiarazioni());
        metaProcedimento.getDichiarazioni().clear();

        for (MetaDichiarazione md : temp) {
            if (md.getId().equals(idMetaDic)) {
                if (MOVE_DIR.UP.equals(moveDir) && i > 0) {
                    Collections.swap(temp, i, i - 1);
                    break;
                }
                if (MOVE_DIR.DOWN.equals(moveDir) && i < last) {
                    Collections.swap(temp, i, i + 1);
                    break;
                }
            }
            i++;
        }
        metaProcedimento.setDichiarazioni(temp);

    }

    public void removeMeta(Long idMeta) {
//        if (md==null) {
//            logOperatore(logger, Level.WARNING, "La metadichiarazione {0,number,#} non esiste",idMeta);
//            return;
//        }

        List<Long> l = em.createQuery("SELECT d.id FROM Dichiarazione d where d.idMetaDichiarazione = :idMeta", Long.class)
                .setParameter("idMeta", idMeta).getResultList();
        for(Long did:l) {
            try {
                removeData(did);
            } catch (BLException e) {
                logOperatore(Loggers.LOG_ADMIN,Level.WARNING,"Non posso rimuovere la dichiarazione {0,number,#}, probabilmente e'' relativa ad una pratica non in BOZZA",did);
                logger.log(Level.SEVERE,"Errore",e);
            }
        }
        List<MetaField> mfl = em.createQuery("SELECT mf FROM MetaField mf where mf.dichiarazione.id= :idMeta", MetaField.class)
                .setParameter("idMeta", idMeta).getResultList();
        for(MetaField mf:mfl) {
            em.remove(mf);

        }
        MetaDichiarazione md = em.find(MetaDichiarazione.class, idMeta);
        em.remove(md);
    }

    public List<MetaDichiarazione> getLista() {
        logOperatore(logger, Level.FINE, "Lista delle dichiarazioni");
        return em.createNamedQuery(MetaDichiarazione.QR_LISTA).getResultList();
    }

    @Transactional
    public MetaDichiarazione getMeta(Long id) {
        logOperatore(logger, Level.FINE, "Ricerco metadichiarazione {0,number,#}", id);
        MetaDichiarazione out = em.find(MetaDichiarazione.class, id);
        int size = out.getTemplate() != null ? out.getTemplate().getValue().length : 0;
        return out;
    }

    @Transactional
    public Dichiarazione getData(Long idDichData) {

        logOperatore(logger, Level.FINE, "Ricerco dichiarazione {0,number,#}", idDichData);
        Dichiarazione out = em.find(Dichiarazione.class, idDichData);
        if (out==null) {
            logOperatore(logger, Level.WARNING, "La dichiarazione {0,number,#} non e'' presente", idDichData);
            return null;
        }
        MetaDichiarazione metaDichiarazione = getMeta(out.getIdMetaDichiarazione());
        Map<Long, Value> dataFields = out.getValues();
        for (MetaField mf : metaDichiarazione.getFields()) {
            if (dataFields.get(mf.getId()) == null) {
                Value v = gestoreValue.createValue(mf);
                out.addValue(v);
            }

        }
        return out;
    }
    


    /**
     * Clona la dichiarazione, tutti i suoi campi, il template e la guida
     * @return
     */
    public MetaDichiarazione clone(Long idMeta,Long idProc) {
        logOperatore(logger, Level.FINEST, "Clono dichiarazione {0,number,#}",idMeta);
        MetaDichiarazione metaDichiarazione = em.find(MetaDichiarazione.class, idMeta);
        ValueLob tpl = metaDichiarazione.getTemplate();
        if (tpl!=null) {
            em.detach(tpl);
            tpl.setId(null);
        }
        ValueLob gc = metaDichiarazione.getGuidaCompilazione();
        if (gc !=null) {
            gc.setId(null);
            em.detach(gc);
        }
        MetaDichiarazione orig = em.find(MetaDichiarazione.class, idMeta);
        Set<MetaField> fs = orig.getFields();
        em.detach(metaDichiarazione);
        metaDichiarazione.setDescrizione(metaDichiarazione.getDescrizione()+CLONE_SUFFIX);
        metaDichiarazione.setId(null);
        metaDichiarazione.setFields(new HashSet<>());
        em.persist(metaDichiarazione);
        for (MetaField _mf : fs) {
            _mf = gestoreCampo.clone(_mf);
//            Long idField  = _mf.getId();
//            if (_mf instanceof MetaListField) {
//                MetaListField mlf = (MetaListField) _mf;
//                mlf.setAllowedValues(new ArrayList<>(mlf.getAllowedValues()));
////                for(AllowedValue av: mlf.getAllowedValues()) {
////                    em.detach(av);
////                    av.setId(null);
////                }
//            }
//            if (_mf instanceof MetaFileField) {
//                MetaFileField mff = (MetaFileField) _mf;
//                HashSet<Long> sft = new HashSet<>(mff.getFileTypes());
//                mff.setFileTypes(sft);
//            }
//            em.detach(_mf);
//            _mf.setId(null);
//            _mf.setDichiarazione(null);
//            em.persist(_mf);

            _mf.setDichiarazione(metaDichiarazione);
            metaDichiarazione.getFields().add(_mf);
        }
        metaDichiarazione.setTemplate(tpl);
        metaDichiarazione.setGuidaCompilazione(gc);
        if (idProc!=null) {
            em.find(MetaProcedimento.class,idProc).getDichiarazioni().add(metaDichiarazione);
        }
        return metaDichiarazione;
    }

        /**
         * @param metaDichiarazione
         * @param mp
         * @param testoTemplate
         * @return
         */
    public MetaDichiarazione save(MetaDichiarazione metaDichiarazione, MetaProcedimento mp, String testoTemplate) {
        String assoc = mp == null ? " NON associata a procedimento" : " associata al procedimento " + mp.getId();

        if (mp != null) {
            mp = em.find(MetaProcedimento.class, mp.getId());
            if (metaDichiarazione.getId() != null) {
                List<MetaDichiarazione> mds = em.createNamedQuery(MetaProcedimento.QR_FIND_DIC_ASSOC, MetaDichiarazione.class)
                        .setParameter(MetaProcedimento.P_IDDIC, metaDichiarazione.getId())
                        .setParameter(MetaProcedimento.P_IDPROC, mp.getId()).getResultList();
                if (mds.size() != 0) {
                    logOperatore(logger, Level.FINEST, "La dichiarazione {0,number,#} e'' gia'' associata al procedimento {1,number,#}",
                            metaDichiarazione.getId(), mp.getId());
                    mp = null;
                }
            }
        }
        if (metaDichiarazione.getId() == null) {
            logOperatore(logger, Level.FINE, "Salvo dichiarazione {0}" + assoc, metaDichiarazione);
            em.persist(metaDichiarazione);

        } else {
            logOperatore(logger, Level.FINE, "Aggiorno dichiarazione {0}" + assoc, metaDichiarazione);
            metaDichiarazione = em.merge(metaDichiarazione);
        }

        if (mp != null) {
            mp.getDichiarazioni().add(metaDichiarazione);
        }
        if (StringUtils.isNotEmpty(testoTemplate)) {
            ValueLob template = metaDichiarazione.getTemplate()==null ? new ValueLob() : metaDichiarazione.getTemplate();
            template.setValue(testoTemplate.getBytes());
            metaDichiarazione.setTemplate(template);
        } else {
            ValueLob tLob = metaDichiarazione.getTemplate();
            if (tLob!=null) {
                em.remove(tLob);
                metaDichiarazione.setTemplate(null);
            }

        }
        return metaDichiarazione;
    }

//    @Transactional
    public void unlinkDichiarazione(Long idProcedimento, Long idDichiarazione, boolean delete) {
        logOperatore(logger, Level.FINE, "Unlink MetaDichiarazione {0,number,#} da proc {1,number,#},delete:{2}", idDichiarazione, idProcedimento, delete);
        MetaProcedimento mp = em.find(MetaProcedimento.class, idProcedimento);

        Iterator<MetaDichiarazione> it = mp.getDichiarazioni().iterator();
        while (it.hasNext()) {
            MetaDichiarazione md = it.next();
            if (md.getId().equals(idDichiarazione)) {
                it.remove();
            }
        }
        if (delete) removeMeta(idDichiarazione);
    }


    public void validate(Dichiarazione dichiarazione, Document doc) {
        Set<ValidationMessage> out = new DichiarazioneValidator().validate(dichiarazione, doc);
    }

    public Dichiarazione submit(Long idPratica, Long idDichiarazione,Map<String,Object[]> webParams,Map<String, FileUpload[]> uploaded) throws BLException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        logOperatore(Loggers.LOG_BL,Level.FINE,"Salvataggio dichiarazione {0,number,#}",idDichiarazione);
//        Set<String> parameterNames = webParams.keySet();
        Dichiarazione oldDichiarazione = em.find(Dichiarazione.class,idDichiarazione);
        if (!Pratica.Stato.BOZZA.equals(oldDichiarazione.getPratica().getStato()) &&
                !Pratica.Stato.DEMO.equals(oldDichiarazione.getPratica().getStato())) {
            throw new BLException("fe.pratica.not_updatable",oldDichiarazione.getPratica().getId().toString());
        }

        List<String> params=new ArrayList<>();
//        Long idDic = dichiarazione.getId();
//        dichiarazione.getValues().clear();
        boolean clone=false;
        for (String s:webParams.keySet()) {
//        while (parameterNames.hasMoreElements()) {
//            String s = parameterNames.nextElement();
            if (MetaDichiarazione.P_PARAMETER_CLONE.equals(s)) clone=true;
            if (s.startsWith(MetaDichiarazione.P_PARAMETER_PREFIX)) {
                params.add(s);
            }
        }
        logOperatore(Loggers.LOG_BL, Level.FINE,"Salvataggio metaDichiarazione >{0,number,#}< {1}",
                idDichiarazione,clone ? "Clona":"Aggiorna");

        Dichiarazione dichiarazione = null;
        MetaDichiarazione metaDichiarazione = em.find(MetaDichiarazione.class, oldDichiarazione.getIdMetaDichiarazione());
        if (clone) {
            Long idMeta=oldDichiarazione.getIdMetaDichiarazione();
            dichiarazione = new Dichiarazione();
            dichiarazione.setIdMetaDichiarazione(idMeta);

        } else {
            dichiarazione = oldDichiarazione;
        }
        if (uploaded!=null) params.addAll(uploaded.keySet());

        for (MetaField mf : metaDichiarazione.getFields()) {
            if (mf instanceof MetaAnagraficaField) {
                String nome=mf.getNome();
                MetaAnagraficaField af = (MetaAnagraficaField) mf;
                String entityName = af.getEntity();
                Object newInst = Class.forName(entityName).newInstance();
                Map<String,Object> beanProps=new HashMap<>();
                for (Iterator<Map.Entry<String,Object[]>> paramsIter = webParams.entrySet().iterator(); paramsIter.hasNext();){
                    Map.Entry<String, Object[]> ent = paramsIter.next();
                    String key=ent.getKey();

                    if (key.startsWith(MetaDichiarazione.P_PARAMETER_PREFIX +nome)) {
                        String propName = key.replaceFirst("^"+MetaDichiarazione.P_PARAMETER_PREFIX +nome+"\\.","");
                        beanProps.put(propName,ent.getValue());

                    }
                }
                BeanUtils.populate(newInst,beanProps);
            }
        }

        for (Iterator<String> paramsIter = params.iterator(); paramsIter.hasNext();){
            String sparam = paramsIter.next();
            Object[] _values = webParams.get(sparam);
            List<Object> values = _values!=null ? Arrays.asList(_values):null;
            for(MetaField mf : metaDichiarazione.getFields()) {
                if (sparam.equals(MetaDichiarazione.P_PARAMETER_PREFIX +mf.getNome())) {

                    Value v = null;
                    if (!clone) { // update
                        v = dichiarazione.getValues().get(mf.getId());
                    }
                    logOperatore(Loggers.LOG_BL, Level.FINER,"Value per per metafield {0}:{1}",mf,v);
                    if (mf instanceof MetaFileField) {
                        FileUpload[] fi = uploaded.get(sparam);
                        if (fi!=null) {
                            values = Arrays.asList(fi);
                        } else {
                            if (webParams.get(MetaDichiarazione.P_PARAMETER_PREFIX +mf.getNome()+"_delete")!=null) {
                                v=null;
                            }
                            values=null;
                        }
                    }

                    //                        if (!clone) { // update
                    v = gestoreValue.toValue(mf,values,v!=null ?  v.getId():null);
                    logOperatore(Loggers.LOG_BL,Level.FINEST,"Aggiornato value {0} a partire da {1}",v,values);
                    // ?????????????????
//                        }

                    dichiarazione.getValues().put(mf.getId(),v);
                }

            }
        }
        Dichiarazione dich = null;
        if (clone) {
            dich = create(idPratica,dichiarazione.getIdMetaDichiarazione(),dichiarazione.getValues());
        } else {
            dich = getData(dichiarazione.getId());
        }
        return dich;

    }



    @Transactional
    public Dichiarazione create(Long idPratica, Long idMeta, Map<Long, Value> values) throws BLException{
        logOperatore(logger, Level.FINE, "Creazione dichiarazione per da metaDich:{0}, per la pratica:{1}", idMeta, idPratica);
        Pratica p = em.getReference(Pratica.class, idPratica);
        if (!Pratica.Stato.BOZZA.equals(p.getStato())) {
            throw new BLException("fe.pratica.not.updatable",p.getId().toString());
        }
//        validate(p, idMeta);
        Dichiarazione newDich = new Dichiarazione();
        newDich.setPratica(p);
        newDich.setIdMetaDichiarazione(idMeta);
        for (Map.Entry<Long, Value> e : values.entrySet()) {
            Value value = e.getValue();
            em.detach(value);
            value.setId(null);
            for (RawValue rv : value.getRawValues()) {
                rv.setId(null);
                rv.setValue(value);
            }
            em.persist(value);
            newDich.addValue(value);
        }

        em.persist(newDich);
        return newDich;
    }

    public List dicAssociabili(Long idProc, String q) {
        logOperatore(logger, Level.FINER, "dicAssociabili {0,number,#}", idProc);
        Query query = em.createNamedQuery(MetaDichiarazione.QR_LISTA_PER_SELEZIONE)
                .setParameter("idProc", idProc);
        if (StringUtils.isNotEmpty(q)) {
            query.setParameter("q", '%' + q.toLowerCase() + '%');
        } else query.setParameter("q", null);

        return query.getResultList();


    }

    public void associaDicLink(Long idProc, Long idDic) {
        MetaProcedimento metaProcedimento = em.find(MetaProcedimento.class, idProc);
        metaProcedimento.getDichiarazioni().add(em.getReference(MetaDichiarazione.class, idDic));
    }

    public SearchOutput search(MetaDichiarazioneSearchParam param) {
        return new MetaDichiarazioneSearchParamProcessor().process(param, em);
    }


    @Transactional
    public Long saveOrUpdateGuida(Long idDic, String q) {
        logOperatore(logger, Level.FINER, "Salvataggio gc per dic:{0,number,#}; testo:{1}", idDic, q);
        MetaDichiarazione dic = em.find(MetaDichiarazione.class, idDic);
        ValueLob gc = dic.getGuidaCompilazione();
        if (gc == null && StringUtils.isNotEmpty(q)) {
            logOperatore(logger, Level.FINEST, "Creazione gc per dic:{0,number,#}; testo:{1}", idDic, q);
            gc = new ValueLob();
            dic.setGuidaCompilazione(gc);
            gc.setValue(q.getBytes());
            em.persist(gc);
        } else {
            logOperatore(logger, Level.FINEST, "Modifica gc per dic:{0,number,#}; testo:{1}", idDic, q);
            if (StringUtils.isEmpty(q)) {
                dic.setGuidaCompilazione(null);
                em.remove(gc);
            } else {
                gc.setValue(q.getBytes());
            }
        }
        return gc.getId();
    }

    public void removeData(Long idDic) throws BLException{
        logOperatore(logger, Level.FINER, "Cancello la dichiarazione:{0,number,#}; ", idDic);
        Dichiarazione dic = em.find(Dichiarazione.class, idDic);
        if (dic==null) {
            logOperatore(logger, Level.WARNING, "La dichiarazione:{0,number,#} non esiste", idDic);
            return;
        }
        if (!Pratica.Stato.BOZZA.equals(dic.getPratica().getStato())) {
            throw new BLException("fe.pratica.not.updatable",dic.getPratica().getId().toString());
        }
        if (MapUtils.isNotEmpty(dic.getValues())) {
            for (Value v : dic.getValues().values()) {
                logOperatore(logger, Level.FINEST, "Removing value :{0,number,#}; ", v.getId());
                em.remove(v);
            }
        }
        logOperatore(logger, Level.FINEST, "Removing value dic :{0,number,#}; ", idDic);
        em.remove(dic);
    }

    /**
     * @param dataDic
     * @return false se la dichiarazione non deve essere presente
     */
    public boolean init(Dichiarazione dataDic, Document dom) {
        logOperatore(logger, Level.FINER, "Inizializzo la dichiarazione[{0,number,#}]", dataDic.getId());
        MetaDichiarazione metaDichiarazione = em.find(MetaDichiarazione.class, dataDic.getIdMetaDichiarazione());
        if (StringUtils.isNotBlank(metaDichiarazione.getPresentIf())) {
            try {
                logOperatore(logger, Level.FINEST, "Valuto xpath present if [{0}] su {1,number,#}", metaDichiarazione.getPresentIf(), dataDic.getId());
                XPathExpression xpath = XPathFactory.newInstance().newXPath().compile(metaDichiarazione.getPresentIf());
                Boolean present = (Boolean) xpath.evaluate(dom, XPathConstants.BOOLEAN);
                if (Boolean.FALSE.equals(present)) return false;
            } catch (Exception e) {
                e.printStackTrace();
                logger.log(Level.WARNING, "Errore nell''espressione xpath <" + metaDichiarazione.getPresentIf() + ">", e);
            }
        }
        for (MetaField f : metaDichiarazione.getFields()) {
            logOperatore(logger, Level.FINER, "Scorro il campo[{1,number,#}] della dichiarazione[{0,number,#}] ", dataDic.getId(), f.getId());
            if (dataDic.getValues().get(f.getId()) == null) {
                logOperatore(logger, Level.FINEST, "Creo il valore per il campo[{1,number,#}] nella dichiarazione[{0,number,#}]", dataDic.getId(), f.getId());
                Value val = gestoreValue.createValue(f);
                dataDic.addValue(val);
            }

        }
        return true;
    }

    public List checkSave(Long idMetaDic,Long idMetaProc) {
        logOperatore(logger, Level.FINER, "CheckSave per la metadichiarazione[{0,number,#}-metaprocedimento[{1,number,#}]", idMetaDic,idMetaProc);
        return em.createNamedQuery(MetaDichiarazione.QR_CHECK_SAVE)
                .setParameter(MetaProcedimento.P_IDDIC, idMetaDic)
                .setParameter(MetaProcedimento.P_IDPROC, idMetaProc).getResultList();
    }


}
