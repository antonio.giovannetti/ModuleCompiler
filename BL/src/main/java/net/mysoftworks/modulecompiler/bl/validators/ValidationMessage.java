package net.mysoftworks.modulecompiler.bl.validators;

import net.mysoftworks.modulecompiler.model.meta.MetaField;

public class ValidationMessage {


    public enum TYPE{WARNING,ERROR;}
    private final Object[] params;
    private final MetaField field;
    private final String message;
    private final TYPE type;

    public ValidationMessage(MetaField field, TYPE type,String message, Object ... params) {
        this.field = field;
        this.message = message;
        this.type = type;
        this.params = params;
    }

    public MetaField getField() {
        return field;
    }

    public String getMessage() {
        return message;
    }

    public TYPE getType() {
        return type;
    }

    public Object[] getParams() {
        return params;
    }
}
