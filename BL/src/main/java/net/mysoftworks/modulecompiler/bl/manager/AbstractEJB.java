package net.mysoftworks.modulecompiler.bl.manager;

import net.mysoftworks.modulecompiler.Loggers;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import java.security.Principal;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AbstractEJB {

    @Resource
    SessionContext sessionContext;

    protected void logOperatore(Logger log, Level level, String s, Object ... params) {
        Principal pr = sessionContext!=null ? sessionContext.getCallerPrincipal() : null;
        Loggers.logOperatore(log,level,s,pr,params);
    }
    private static final Logger logger = Logger.getLogger(AbstractEJB.class.getName());

}
