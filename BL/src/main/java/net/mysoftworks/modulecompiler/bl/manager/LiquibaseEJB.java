package net.mysoftworks.modulecompiler.bl.manager;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.FileSystemResourceAccessor;
import liquibase.resource.ResourceAccessor;
import net.mysoftworks.modulecompiler.bl.liquibase.LiquibaseXMLProducerZip;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.sql.DataSource;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;

@Singleton
@Startup
@TransactionManagement(TransactionManagementType.BEAN)
public class LiquibaseEJB extends  AbstractEJB {
    private static final Logger logger = Logger.getLogger(LiquibaseEJB.class.getName());

    @Resource(lookup = "java:/jdbc/MCDS2")
    private DataSource dataSource;

    @PostConstruct
    public void initalize()  {
        logger.log(Level.INFO,"===================Aggiornamento db liquibase");
        Liquibase liquibase = null;
        try  (Connection c = dataSource.getConnection();){
            ResourceAccessor resourceAccessor = new ClassLoaderResourceAccessor(getClass().getClassLoader());

            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(c));
            liquibase = new Liquibase("liquibase_db.xml", resourceAccessor, database);
            liquibase.update(new Contexts(), new LabelExpression());

            if ("true".equals(System.getProperty("include.demo.data"))) {
                Liquibase liquibaseDemo = new Liquibase("sample_data/liquibase_db.xml", resourceAccessor, database);
                liquibaseDemo.update(new Contexts(), new LabelExpression());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }



    }
}
