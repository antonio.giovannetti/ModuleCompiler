package net.mysoftworks.modulecompiler.bl.liquibase;

import net.mysoftworks.modulecompiler.bl.utils.JaxbSerializer;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;

import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.sql.*;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class LiquibaseXMLProducerZip extends LiquibaseXMLProducer{

    private final ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    private final ZipOutputStream zipOutputStream = new ZipOutputStream(bytes);

    public LiquibaseXMLProducerZip(Connection connection) {
        super(connection);
    }

    protected String saveLob(byte[] content,String table,String columnName) throws Exception {
        String filename = UUID.randomUUID().toString() + BIN_FILE_EXTENSION;
        String entryName = table + File.separatorChar + filename;
        ZipEntry e = new ZipEntry(entryName);
        zipOutputStream.putNextEntry(e);
        zipOutputStream.write(content);
        zipOutputStream.closeEntry();
        return entryName;
    }

    @Override
    protected void exportChangeLog(Document doc) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JaxbSerializer.documentToString(doc, new StreamResult(baos),null);
        ZipEntry e = new ZipEntry(EXPORT_FILENAME);
        zipOutputStream.putNextEntry(e);
        zipOutputStream.write(baos.toByteArray());
        zipOutputStream.closeEntry();
        zipOutputStream.close();
    }

    public byte[] getContent() {
        return bytes.toByteArray();
    }

    public void createZipFile(String fileName) throws Exception{
        FileOutputStream fos = new FileOutputStream(fileName);
        IOUtils.write(getContent(),fos);
    }
}
