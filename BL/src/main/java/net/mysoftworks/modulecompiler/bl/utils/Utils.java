package net.mysoftworks.modulecompiler.bl.utils;

import net.mysoftworks.modulecompiler.model.ValueLob;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Utils {

    public static ValueLob readFile(InputStream is) throws IOException {
        return readFile(IOUtils.toByteArray(is));
    }

    public static ValueLob readFile(byte[] content) {
        ValueLob out = new ValueLob();
        out.setValue(content);
        return out;
    }

    public static ValueLob readFile(File f) throws Exception {
        return readFile(new FileInputStream(f));
    }
}
