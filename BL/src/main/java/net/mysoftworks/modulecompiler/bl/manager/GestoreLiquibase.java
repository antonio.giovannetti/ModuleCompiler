package net.mysoftworks.modulecompiler.bl.manager;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.FileSystemResourceAccessor;
import net.mysoftworks.modulecompiler.bl.liquibase.LiquibaseXMLProducerZip;
import net.mysoftworks.modulecompiler.model.meta.MetaDichiarazione;
import net.mysoftworks.modulecompiler.model.meta.MetaField;
import net.mysoftworks.modulecompiler.model.values.Value;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@LocalBean
public class GestoreLiquibase extends  AbstractEJB {
    private static final Logger logger = Logger.getLogger(GestoreLiquibase.class.getName());

    @Resource(lookup = "java:/jdbc/MCDS2")
    private DataSource dataSource;

    private java.sql.Connection connection;

    public byte[] exportAll() throws Exception {
        logOperatore(logger,Level.INFO,"Creazione backup configrazione");
        if (connection==null) connection=dataSource.getConnection();
        LiquibaseXMLProducerZip worker = new LiquibaseXMLProducerZip(connection);
        worker.exportAll();
        return worker.getContent();
    }

    public byte[] exportProc(Long idProc) throws Exception {
        logOperatore(logger,Level.INFO,"Creazione backup configrazione procedimento {0,number,#}",idProc);
        if (connection==null) connection=dataSource.getConnection();
        LiquibaseXMLProducerZip worker = new LiquibaseXMLProducerZip(connection);
        worker.setDropExisting(true);
        worker.exportProc(idProc,null);
        return worker.getContent();
    }

    public byte[] exportDic(Long idDic) throws Exception {
        logOperatore(logger,Level.INFO,"Creazione backup configrazione dichiarazione {0,number,#}",idDic);
        if (connection==null) connection=dataSource.getConnection();
        LiquibaseXMLProducerZip worker = new LiquibaseXMLProducerZip(connection);
        worker.setDropExisting(true);
        worker.exportDic(idDic,null);
        return worker.getContent();
    }

    
}
