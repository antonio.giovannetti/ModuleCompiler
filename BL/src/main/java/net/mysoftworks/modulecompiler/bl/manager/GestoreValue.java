package net.mysoftworks.modulecompiler.bl.manager;

import net.mysoftworks.modulecompiler.dto.BLException;
import net.mysoftworks.modulecompiler.dto.FileUpload;
import net.mysoftworks.modulecompiler.bl.utils.Utils;
import net.mysoftworks.modulecompiler.model.ValueLob;
import net.mysoftworks.modulecompiler.model.meta.*;
import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.model.values.RawValue;
import net.mysoftworks.modulecompiler.model.values.Value;
import net.mysoftworks.modulecompiler.model.visitor.MetaFieldVisitor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@LocalBean
public class GestoreValue extends AbstractEJB {
    private static final Logger logger = Logger.getLogger(GestoreValue.class.getName());
    private @PersistenceContext(unitName = "mc-datasource")
    EntityManager em;

    @Resource
    EJBContext ctx;

    @EJB
    KeycloakUtil keycloakUtil;

    @EJB
    FieldValidator fieldValidator;

    public Value getValue(Long idValue) {
        return em.find(Value.class,idValue);
    }

    /**
     *
     * @param idRawValue
     * @return [0,1]: [ {@link RawValue },{@link ValueLob}]
     */
    public Object[] checkAndDownload(Long idRawValue) {
        // TODO: implementare il check (controllo grant)
        Object[] out = null;
        RawValue rv = em.find(RawValue.class, idRawValue);
        if (rv != null) {
            if (rv.getIdLob()==null) {
                throw new RuntimeException("Il rawvalue " + idRawValue + " non contiene un blob");
            }
            ValueLob rvl = em.find(ValueLob.class, rv.getIdLob());
            if (rvl==null) {
                throw new RuntimeException("Il blob " + rv.getIdLob() + " non esiste");
            }
            out  = new Object[]{rv,rvl};
        }
        return out;
    }

    private void checkSize(MetaField mf, Value oldValue,Collection newValues) {
        int size = newValues!=null?newValues.size():0;
        Collection<RawValue> oldValues = oldValue.getRawValues();
        if (mf instanceof MetaFileField) {
            if (size==0) {
                size += oldValues != null ? oldValues.size() : 0;
            } else {
                for (Iterator<RawValue> iter = oldValues.iterator(); iter.hasNext(); ) {
                    RawValue rawValue = iter.next();
                    em.remove(rawValue);
                    iter.remove();
                }
            }

        } else {
//            size+=oldValues!=null?oldValues.size():0;
            for (Iterator<RawValue> iter = oldValues.iterator(); iter.hasNext(); ) {
                RawValue rawValue = iter.next();
                em.remove(rawValue);
                iter.remove();
            }
        }
        if (size>mf.getMaxOccurrences()) {
            throw new RuntimeException("Il campo '" + mf.getEtichetta() + "' deve contenere fino a " + mf.getMaxOccurrences() + " valori, invece ne imposto " + size);
        }
        if (size<mf.getMinOccurrences()) {
            throw new RuntimeException("Il campo '" + mf.getEtichetta() + "' deve contenere almeno " + mf.getMaxOccurrences() + " valori, invece ne imposto " + size);
        }
    }

    public Calendar toCalendar( MetaTemporalField tf,String s) {
        Calendar c = null;
        switch (tf.getTemporalType()) {
            case TIMESTAMP:
            case DATE:
                String _s=StringUtils.rightPad(s,11,"T");
                _s=StringUtils.rightPad(_s,13,"00");
                _s=StringUtils.rightPad(_s,19,":00");
                _s=StringUtils.rightPad(_s,20,".");
                _s=StringUtils.rightPad(_s,23,"0");
                c = DatatypeConverter.parseDateTime(_s);
                break;
            case TIME:
                c = DatatypeConverter.parseDateTime(s);
                break;
        }
        return c;
    }

    public Value toValue(MetaField mf, List<Object> oValues, Long valueId) throws BLException {
        logOperatore(logger,Level.FINER,"Object(s) To Value MetaField:{0}, valueId:{1}, values:{2}",
                mf,valueId,oValues!=null? StringUtils.join(oValues,','):null );

        Value v = null;
        if (valueId != null) {
            v = em.find(Value.class,valueId);
            if (v.getField() instanceof MetaFileField || v.getField() instanceof MetaAnagraficaField) {

            } else {
                for(RawValue rv:v.getRawValues()) {
                    em.remove(rv);
                }
                v.getRawValues().clear();

            }
        }
        else {
            v = new Value();
            v.setField(mf);
        }
        fieldValidator.validate(mf,oValues,v);
        checkSize(mf,v,oValues);
        if (CollectionUtils.isNotEmpty(oValues)) {
            for(Object oValue:oValues) {
                RawValue rv = null;
                if (mf instanceof MetaTextField) {
                    MetaTextField mtf = (MetaTextField) mf;
                    rv = mtf.instantiate(oValue.toString());
                }
                if (mf instanceof MetaListField) {
                    rv = new RawValue();
                    rv.setStringValue((String)oValue);
                }
                if (mf instanceof MetaFileField) {
                    FileUpload p = (FileUpload) oValue;
                    if (p!=null && p.getContent()!=null) {
                        rv = new RawValue();
                        rv.setLobValue(Utils.readFile(p.getContent()));
                        rv.setStringValue(p.getName());
                    }
                }
                if (mf instanceof MetaTemporalField) {
                    MetaTemporalField tf= (MetaTemporalField) mf;
                    rv = new RawValue();
                    String s = (String) oValue;
                    Calendar c = toCalendar(tf,s);
                    rv.setDateValue(c);
                }
                if (rv!=null) {
                    rv.setValue(v);
                    v.getRawValues().add(rv);
                }
            }
        }

        return v;
    }

    public Value createValue(MetaField f/*, Dichiarazione dichiarazione*/) {
        logOperatore(logger,Level.FINEST, "Creo Value per MetaField {0}", f);

        Value val = new Value();
        val.setField(f);
//        val.setPratica(pratica);
//        org.hibernate.Hibernate.initialize(f);
        f.accept(new MetaFieldVisitor() {
            private RawValue create(){
                RawValue rv = new RawValue();
//                rv.setValue(val);
                val.getRawValues().add(rv);
                rv.setValue(val);
                return rv;
            }
            @Override
            public void visit(MetaTextField metaTextField) {
//                RawValue rv = metaTextField.instantiate(null);
                create();
            }

            @Override
            public void visit(MetaFileField metaFileField) {
                RawValue rv = create();
//                rv.setLobValue(new MetaLob());

//                throw new RuntimeException("\n\n===============Visitor not implemented for MetaFileField");
            }

            @Override
            public void visit(MetaAnagraficaField metaAnagraficaField) {
                RawValue rv = create();
                if (true) return; // Non persisto l'entity vuota
                try {
                    System.out.println("\n\n=============visit:" +metaAnagraficaField.getEntity());
                    Class<?> cl = Class.forName(metaAnagraficaField.getEntity());
                    Object entity = cl.newInstance();
                    em.persist(entity);
                    // TODO: recuperare la PK dell'entity e valorizzare il campo Long/String di rv associato
                    Field[] _fields = cl.getDeclaredFields();
                    for(Field _field : _fields) {
                        if (_field.getAnnotation(Id.class)!=null) {
                            Object pkVal = _field.get(entity);
                        }
                    }
                } catch (Exception e ){
                    e.printStackTrace();
                }
            }

            @Override
            public void visit(MetaTemporalField metaTemporalField) {
                RawValue rv = create();
//                throw new RuntimeException("\n\n===============Visitor not implemented for MetaTemporalField");
            }

            @Override
            public void visit(MetaListField metaListField) {
//                logger.warning("\n\n===============Visitor not implemented for MetaListField");
                RawValue rv = create();
            }
        });
        em.persist(val);
        return val;
    }

    public Value getValueByFieldName(Long idMetaDich, String fieldName) {
        logOperatore(logger,Level.FINER, "Cerco value per campo {0} nella dich. {1,number,#}", fieldName,idMetaDich);
        List<Value> l = em.createNamedQuery(Value.QR_FIND_IN_METAFIELD, Value.class)
                .setParameter(Value.P_IDDIC, idMetaDich)
                .setParameter(Value.P_CAMPO, fieldName)
                .getResultList();
        if (l.size()==0) {
            MetaField mf = null;
            Dichiarazione dich = em.find(Dichiarazione.class, idMetaDich);

            List<MetaField> lmf = em.createNamedQuery(MetaField.QR_MF_BY_NAME_AND_META)
                    .setParameter(MetaField.P_TFM_P_NAME, fieldName)
                    .setParameter(MetaField.P_TFM_P_META, dich.getIdMetaDichiarazione())
                    .getResultList();
            if (l.size()==0) {
                throw new RuntimeException("Non trovo il campo " + fieldName + " nella dicharazione " + dich.getIdMetaDichiarazione());
            }
            mf=lmf.get(0);
            Value v = createValue(mf);
            dich.addValue(v);
            return v;
//            throw new RuntimeException("Non trovo il campo " + fieldName + " nella dicharazione " + idDich);
        }
        return l.get(0);
    }
}
