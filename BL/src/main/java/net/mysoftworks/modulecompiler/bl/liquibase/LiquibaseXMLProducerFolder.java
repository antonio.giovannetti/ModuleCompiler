package net.mysoftworks.modulecompiler.bl.liquibase;

import net.mysoftworks.modulecompiler.bl.utils.JaxbSerializer;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;

import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.sql.*;
import java.util.UUID;

public class LiquibaseXMLProducerFolder extends LiquibaseXMLProducer {


    private final String tempDir;

    public LiquibaseXMLProducerFolder(Connection connection, String tempDir) {
        super(connection);
        this.tempDir = tempDir;
        File folder = new File(tempDir);
        if (!folder.exists()) folder.mkdirs();
    }

    protected String saveLob(byte[] content, String table, String columnName) throws Exception {
        File folder = new File(tempDir, table);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        String filename = UUID.randomUUID().toString() + BIN_FILE_EXTENSION;
        File f = new File(folder, filename);
        IOUtils.write(content, new FileOutputStream(f));
        return table + File.separatorChar + filename;
    }

    protected void exportChangeLog(Document doc) throws Exception{
        FileWriter fw = new FileWriter(new File(tempDir, EXPORT_FILENAME));
        JaxbSerializer.documentToString(doc, new StreamResult(fw),null);
    }


}
