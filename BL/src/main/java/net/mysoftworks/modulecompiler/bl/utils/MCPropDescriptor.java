package net.mysoftworks.modulecompiler.bl.utils;

import net.mysoftworks.modulecompiler.PropAnnotation;
import org.apache.commons.lang3.StringUtils;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MCPropDescriptor implements Serializable {
    private Method getter;
    private Method setter;
    private String label;
    private String title;
    private boolean collection=false;
    private Type collectionElementType;
    private Map<String,MCPropDescriptor> nested;

    private static HashMap<String,MCPropDescriptor> addEntityMetadata(String entity){
        HashMap<String,MCPropDescriptor> out = new HashMap<>();
        try {
            Class<?> cl = Class.forName(entity);
            for(PropertyDescriptor pd : Introspector.getBeanInfo(cl).getPropertyDescriptors()){
                if (pd.getReadMethod()!=null && pd.getWriteMethod()!=null) {
                    out.put(pd.getName(),new MCPropDescriptor(pd));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return out;
    }

    public static Map<String,MCPropDescriptor> factory(String entity){
        return addEntityMetadata(entity);
    }

    private boolean isPrimitiveOrWrapper(Class<?> rt) {
        if (rt.isPrimitive()) return true;
        String name = rt.getName();
        return name.startsWith("java.lang.");
    }
    public MCPropDescriptor(PropertyDescriptor pd) {
        getter = pd.getReadMethod();
        setter = pd.getWriteMethod();
        collection = Collection.class.isAssignableFrom(getter.getReturnType());
        Class<?> rt = getter.getReturnType();
        Type genericReturnType = getter.getGenericReturnType();
//        System.out.println("===label:"+pd.getDisplayName()+"===class:"+rt.getName()+"===primitive:"+isPrimitiveOrWrapper(rt));
        if (collection) {
            if (genericReturnType != null) {
                if (genericReturnType instanceof ParameterizedType ) {
                    ParameterizedType pt = (java.lang.reflect.ParameterizedType) genericReturnType;
                    if (pt.getActualTypeArguments().length!=0) {
                        collectionElementType = pt.getActualTypeArguments()[0];
                        nested = addEntityMetadata(collectionElementType.getTypeName());

                    }
                }
            }
        } else {
            if (!isPrimitiveOrWrapper(rt)) {
                nested = addEntityMetadata(rt.getName());
            }
        }

        PropAnnotation ann = getter.getAnnotation(PropAnnotation.class);
        if (ann != null) {
            label = ann.label();
            title = ann.title();
        } else {
            label = StringUtils.capitalize(pd.getDisplayName());
            title = StringUtils.capitalize(pd.getShortDescription());
        }
    }

    public Method getGetter() {
        return getter;
    }

    public Method getSetter() {
        return setter;
    }

    public String getLabel() {
        return label;
    }

    public String getTitle() {
        return title;
    }

    public Map<String, MCPropDescriptor> getNested() {
        return nested;
    }

    public boolean isCollection() {
        return collection;
    }

    public Type getCollectionElementType() {
        return collectionElementType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("\n\tMCPropDescriptor{");
        sb.append("label='").append(label).append('\'');
        sb.append(", title='").append(title).append('\'');
        sb.append(", collection=").append(collection);
        sb.append(", collectionElementType=").append(collectionElementType);
        sb.append(", nested=").append(nested);
        sb.append('}');
        return sb.toString();
    }
}
