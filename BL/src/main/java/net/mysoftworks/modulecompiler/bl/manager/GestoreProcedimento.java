package net.mysoftworks.modulecompiler.bl.manager;

import net.mysoftworks.modulecompiler.bl.utils.JaxbSerializer;
import net.mysoftworks.modulecompiler.model.ValueLob;
import net.mysoftworks.modulecompiler.model.enumeration.StatiProcedimento;
import net.mysoftworks.modulecompiler.model.meta.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@LocalBean
public class GestoreProcedimento extends AbstractEJB {
    private static final Logger logger = Logger.getLogger(GestoreProcedimento.class.getName());
    private @PersistenceContext(unitName = "mc-datasource")
    EntityManager em;

    @Resource
    EJBContext context;

    @EJB
    GestoreDichiarazione gestoreDichiarazione;

    @EJB
    GestorePratica gestorePratica;

    @EJB
    GestoreCampo gestoreCampo;


    /**
     * Restituisce informazioni sugli impatti delle modifiche ai metadati sulle pratiche
     * @param idMetaProc
     * @return
     */
    public List checkSaveProcedimento(Long idMetaProc) {
        return em.createNamedQuery(MetaProcedimento.QR_METADATA_IMPACT)
        .setParameter(MetaProcedimento.P_IDPROC,idMetaProc)
        .getResultList();
    }

    public List<MetaProcedimento> dump() {
        logOperatore(logger,Level.FINE, "Dump dei procedimenti:");
        Query qr = em.createNamedQuery(MetaProcedimento.QR_DUMP);
        return qr.getResultList();

    }

    public List<MetaProcedimento> getLista(boolean soloAttivi) {
        logOperatore(logger,Level.FINE, "Lista dei procedimenti:soloAttivi={0}",soloAttivi);
        Query qr = em.createNamedQuery(MetaProcedimento.QR_LISTA);
        if (!soloAttivi) {
            qr = em.createNamedQuery(MetaProcedimento.QR_LISTA_NO_CHILDREN);
        } else {
            qr = em.createNamedQuery(MetaProcedimento.QR_LISTA);
        }
        return qr.getResultList();
    }

    public Document domProcedimento(Long id) {
        JaxbSerializer ju = new JaxbSerializer();
        ju.build(em.find(MetaProcedimento.class, id),MetaProcedimento.class);
        return ju.getDocument();
    }

    @Transactional
    public MetaProcedimento get(Long id) {
        logOperatore(logger,Level.FINE, "Ricerco procedimento {0,number,#}", id);
        MetaProcedimento out = em.find(MetaProcedimento.class, id);
        if (out==null) {
            logOperatore(logger,Level.WARNING, "Il procedimento {0,number,#} non e'' stato trovato", id);
        } else {
            int size = 0; // lazy loading.........
            for (MetaDichiarazione d : out.getDichiarazioni()) {
                size += d.getId();
            }
            size = 0;
            out.getGuidaCompilazione();
            for (MetaFileField mff : out.getAllegati()) {
                size += mff.getId();
            }
        }
        return out;
    }

    @Transactional
    public MetaProcedimento versiona(Long idProcedimento) {
        MetaProcedimento newVersion = clone(idProcedimento, true);
        newVersion.setParent(idProcedimento);
        newVersion.getDateInfo().setDataCreazione(new Date());
        MetaProcedimento oldVersion = em.find(MetaProcedimento.class,idProcedimento);
        oldVersion.setStato(StatiProcedimento.switchOff(oldVersion.getStato(),StatiProcedimento.COMPILABILE.bytewise()));
        return newVersion;
    }

    @Transactional
    public MetaProcedimento save(MetaProcedimento metaProcedimento) {
        if (metaProcedimento.getId()==null) {
            logOperatore(logger,Level.FINE, "Creo procedimento {0}", metaProcedimento);
            em.persist(metaProcedimento);
        } else {
//            MetaProcedimento old = em.find(MetaProcedimento.class,metaProcedimento.getId())
//            for(MetaFileField mff:old.getAllegati()){
//                em.remove(mff);
//            }
            logOperatore(logger,Level.FINE, "Aggiorno procedimento {0}", metaProcedimento);
//            em.createQuery("DELETE a FROM MetaProcedimento mp JOIN mp.allegati a where mp.id = :idProc")
//                    .setParameter("idProc",metaProcedimento.getId()).executeUpdate();
                metaProcedimento = em.merge(metaProcedimento);
        }
        return metaProcedimento;
    }


    @Transactional
    public Long saveOrUpdateGuida(Long idProc,String q) {
        logOperatore(logger,Level.FINER,"Salvataggio gc per proc:{0,number,#}; testo:{1}",idProc,q);
        MetaProcedimento proc = em.find(MetaProcedimento.class, idProc);
        ValueLob gc = proc.getGuidaCompilazione();
        if (gc==null && StringUtils.isNotEmpty(q)) {
            logOperatore(logger,Level.FINEST,"Creazione gc per proc:{0,number,#};testo:{1}",idProc,q);
            gc = new ValueLob();
            proc.setGuidaCompilazione(gc);
            gc.setValue(q.getBytes());
            em.persist(gc);
        } else {
            logOperatore(logger,Level.FINEST,"Modifica gc per proc:{0,number,#};testo:{1}",idProc,q);
            if (StringUtils.isEmpty(q)) {
                proc.setGuidaCompilazione(null);
                em.remove(gc);
            } else {
                gc.setValue(q.getBytes());
            }
        }
        return gc.getId();
    }


    public ValueLob getGuida(Long idGuida) {
        logOperatore(logger,Level.FINE,"Carico guida alla compilazione:{0,number,#}",idGuida);
        if (idGuida==null) return null;
        return em.find(ValueLob.class,idGuida);
//        List<MetaLob> l = em.createNamedQuery(MetaProcedimento.QR_GUIDA,MetaLob.class)
//                .setParameter(MetaProcedimento.P_IDPROC, idProc).getResultList();
//        if (l.size()==0) return null;
//        return l.get(0);
    }

    @Transactional()
    public MetaProcedimento clone(Long idProc, boolean deep) {
        logOperatore(logger, Level.FINE, "Clono procedimento {0,number,#}",idProc);
        MetaProcedimento oldInstance = em.find(MetaProcedimento.class, idProc);
        MetaProcedimento newProc = new MetaProcedimento();
        List<MetaDichiarazione> toClone = new ArrayList<>(oldInstance.getDichiarazioni());
        Set<MetaFileField> toCloneAll = new HashSet<>();
        toCloneAll.addAll(oldInstance.getAllegati());
        oldInstance.getDichiarazioni().clear();
        em.detach(oldInstance);
        if (oldInstance.getIdGuida() != null) {
            ValueLob gc = em.find(ValueLob.class,oldInstance.getIdGuida());
            gc.setId(null);
            em.detach(gc);
            em.persist(gc);
            newProc.setGuidaCompilazione(gc);
        }

        newProc.setId(null);
//        newProc.setDichiarazioni(new ArrayList<>());
        newProc.setDescrizione(oldInstance.getDescrizione()+" (copia)");
        newProc.setStato(StatiProcedimento.OFFLINE.bytewise());
        newProc.setIcon(oldInstance.getIcon());
//        metaProcedimento.getAllegati().clear();
//        em.flush();
        em.persist(newProc);
        if(deep) {
            newProc.setAllegati(new HashSet<>());
            for(MetaFileField all:oldInstance.getAllegati()) {
                em.detach(all);
                all.setId(null);
                newProc.getAllegati().add(all);
            }
            logOperatore(logger, Level.FINEST, "Clono anche le {0,number,#} dichiarazioni del proc {1,number,#}",toClone.size(),idProc);
            for (MetaDichiarazione _md : toClone ) {
                MetaDichiarazione md = gestoreDichiarazione.clone(_md.getId(),newProc.getId());
            }
            for (MetaFileField _ma : toCloneAll ) {
                MetaField ma = gestoreCampo.clone(_ma);
//                em.detach(_ma);
//                _ma.setId(null);
//                em.persist(_ma);
                newProc.getAllegati().add((MetaFileField) ma);
            }
//            metaProcedimento.setAllegati(toCloneAll);

        } else {
            newProc.setDichiarazioni(toClone);
            newProc.setAllegati(toCloneAll);
        }
        return newProc;
    }

    public List<MetaProcedimento> ancestors(Long idProc) {
        Long parent = em.createQuery("SELECT mp.parent FROM MetaProcedimento mp where mp.id = :idProc",Long.class )
            .setParameter("idProc",idProc).getSingleResult();
        List<MetaProcedimento> res = null;
        TypedQuery<MetaProcedimento> qr = em.createNamedQuery(MetaProcedimento.QR_SUMMARY,MetaProcedimento.class);
        while (parent!=null) {
            if (res==null) res = new ArrayList<>();
            MetaProcedimento mp = qr.setParameter(MetaProcedimento.P_IDPROC, parent).getSingleResult();
            res.add(mp);
            parent = mp.getParent();
        }
        return res;
    }

    public List<MetaProcedimento> children(Long idParent) {
        List<MetaProcedimento> children = em.createQuery("SELECT mp FROM MetaProcedimento mp where mp.parent = :idProc",MetaProcedimento.class )
                .setParameter("idProc",idParent ).getResultList();
        return children;
    }



    @Transactional()
    public void remove(Long idMetaProc, boolean deep) {
        logOperatore(logger, Level.FINE, "Cancello procedimento {0,number,#}, deep:{1}",idMetaProc,deep);
        MetaProcedimento metaProc = em.find(MetaProcedimento.class, idMetaProc);
        if (CollectionUtils.isNotEmpty(metaProc.getChildren())) {
            throw new IllegalArgumentException("il procedimento " + idMetaProc + " non puo' esssere cancellato perche' ha dei figli" );
        }
//        for(MetaFileField all: metaProc.getAllegati()) {
//            em.remove(all);
//        }
//        em.flush();
//        metaProc.getAllegati().clear();
        if (deep) {
            Iterator<MetaDichiarazione> it = metaProc.getDichiarazioni().iterator();
            while (it.hasNext()) {
                MetaDichiarazione d = it.next();
                logOperatore(logger, Level.FINEST, "Cancello la dichiarazione {0,number,#}",d.getId());
                gestoreDichiarazione.removeMeta(d.getId());
                it.remove();
            }
        }
        List<Long> l = em.createQuery("select p.id from Pratica p where p.idProcedimento = :idMetaProc",Long.class)
                .setParameter("idMetaProc",idMetaProc).getResultList();
        for(Long pid: l) {
            logOperatore(logger, Level.FINEST, "Cancello la pratica {0,number,#}",pid);
            gestorePratica.deletePratica(pid);
        }
        em.remove(metaProc);
    }

    public Set<String> listaUtentiCoinvolti(Long idMetaProc) {
        logOperatore(logger, Level.FINEST, "Carico la lista degli utenti coinvolti in pratiche del procedimento {0,number,#}",idMetaProc);
        String qr1 = "select distinct r.user FROM Pratica p JOIN p.ruoli r WHERE r.user IS NOT NULL AND p.idProcedimento = :"+MetaProcedimento.P_IDPROC;
        List<String> l1 = em.createQuery(qr1, String.class).setParameter(MetaProcedimento.P_IDPROC, idMetaProc).getResultList();

        String qr2 = "select distinct d.compilatore FROM Dichiarazione d JOIN d.pratica p JOIN p.ruoli r WHERE d.compilatore IS NOT NULL AND p.idProcedimento = :"+MetaProcedimento.P_IDPROC;
        List<String> l2 = em.createQuery(qr2, String.class).setParameter(MetaProcedimento.P_IDPROC, idMetaProc).getResultList();

        Set<String> res = new HashSet<>();
        res.addAll(l1);
        res.addAll(l2);
        return res;
    }

}
