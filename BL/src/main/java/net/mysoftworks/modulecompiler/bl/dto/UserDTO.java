package net.mysoftworks.modulecompiler.bl.dto;


import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.List;

public class UserDTO  {
    private UserRepresentation userRepresentation;
    private List<RoleRepresentation> roleRepresentations;

    public UserDTO(UserRepresentation userRepresentation, List<RoleRepresentation> roleRepresentations) {
        this.userRepresentation = userRepresentation;
        this.roleRepresentations = roleRepresentations;
    }

    public UserRepresentation getUserRepresentation() {
        return userRepresentation;
    }

    public List<RoleRepresentation> getRoleRepresentations() {
        return roleRepresentations;
    }
}
