package net.mysoftworks.modulecompiler.bl.manager;

import net.mysoftworks.modulecompiler.dto.BLException;
import net.mysoftworks.modulecompiler.dto.FileUpload;
import net.mysoftworks.modulecompiler.model.meta.*;
import net.mysoftworks.modulecompiler.model.values.RawValue;
import net.mysoftworks.modulecompiler.model.values.Value;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.Tika;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@LocalBean
public class FieldValidator extends AbstractEJB {
    private static final Logger logger = Logger.getLogger(FieldValidator.class.getName());
    private static final Tika defaultTika = new Tika();
    @PersistenceContext(unitName = "mc-datasource")
    private EntityManager em;
    public void validate(MetaField mf, List<Object> oValues, Value v) throws BLException{
        logOperatore(logger,Level.FINER,"Validate MetaField:{0}, value:{1}, values:{2}",
                mf,v,oValues!=null? StringUtils.join(oValues,','):null );
        validateSize(mf,oValues,v);
        if (mf instanceof MetaFileField ) {
            validateFile((MetaFileField) mf,oValues,v);
            return;
        }
    }

    private void validateSize(MetaField mf, List<Object> oValues, Value v) throws BLException{
        int size = 0;
        if (CollectionUtils.isNotEmpty(oValues)) {
            size = oValues !=null  ? oValues.size() : 0;
        } else {
            size = v.getRawValues() !=null  ? v.getRawValues().size() : 0;
        }
        if (mf.getMinOccurrences()>size) throw new BLException("fe.pratica.dich.field.minocc", mf.getEtichetta(), String.valueOf(mf.getMinOccurrences()));
        if (mf.getMaxOccurrences()<size) throw new BLException("fe.pratica.dich.field.maxocc", mf.getEtichetta(), String.valueOf(mf.getMaxOccurrences()));
    }

    private void validateMimeType(MetaFileField mf, String type) throws BLException {
        if (CollectionUtils.isEmpty(mf.getFileTypes())) return;
        logOperatore(logger,Level.FINEST,"Validate MetaFileField mime types:{0}, type:{1}",
                mf.getFileTypes(),type);
        List<MetaFileTypes> l = em.createNamedQuery(MetaFileTypes.FIND_BY_IDS, MetaFileTypes.class)
                .setParameter(MetaFileTypes.P_IDS, mf.getFileTypes())
                .getResultList();
        boolean testOk=false;
        for(MetaFileTypes allowed:l) {
            testOk = allowed.getMimeType().contains(type);
            if (testOk) break;
        }
        if (!testOk) throw new BLException("fe.pratica.dich.field.filetype.not_allowed", type , mf.getEtichetta());
    }

    private void validateFile(MetaFileField mf, List<Object> oValues, Value v) throws BLException{
        if (CollectionUtils.isNotEmpty(oValues)) {
            for (Object o : oValues) {
                FileUpload fu = (FileUpload) o;
                String type = defaultTika.detect(fu.getContent());
                validateMimeType(mf,type);
            }
        }
        if (v!=null) {
            for (RawValue rv : v.getRawValues()) {
                if (rv.getIdLob()!=null) {
                    String type = defaultTika.detect(rv.getLobValue().getValue());
                    validateMimeType(mf, type);
                }
            }
        }
    }
}
