package net.mysoftworks.modulecompiler.bl.manager;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.mysoftworks.modulecompiler.bl.dto.UserDTO;
import net.mysoftworks.modulecompiler.config.Config;
import net.mysoftworks.modulecompiler.search.SearchOutput;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.*;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.security.Principal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class KeycloakUtil extends AbstractEJB  {
    private static final Logger logger = Logger.getLogger(KeycloakUtil.class.getName());
    @Resource
    EJBContext sessionContext;



    private static String serverUrl = Config.getProperty("keycloak.authserver.url");
    public static final String REALM = "MC";


    public static final String KEYCLOAK_CLIENT = "mc-webapp";
    private static String KEYCLOAK_CLIENT_ID = null;
    private Map<String,RoleRepresentation> CLIENT_ROLES = new HashMap<>();
    private Map<String,RoleRepresentation> REALM_ROLES = new HashMap<>();


    private org.keycloak.KeycloakPrincipal getPrincipal() {
        return (org.keycloak.KeycloakPrincipal)sessionContext.getCallerPrincipal();
    }

    public RoleRepresentation getClientRoleByName(String clientRoleName) {
        if (MapUtils.isEmpty(CLIENT_ROLES)) {
            Keycloak cli = getClient();
            cli.close();
        }
        for (Map.Entry<String,RoleRepresentation> e: CLIENT_ROLES.entrySet()) {
            if (clientRoleName.equals(e.getValue().getName())) {
                return e.getValue();
            }
        }
        return null;
    }

    public RoleRepresentation getRealmRoleByName(String realmRoleName) {
        if (MapUtils.isEmpty(REALM_ROLES)) {
            Keycloak cli = getClient();
            cli.close();
        }
        for (Map.Entry<String,RoleRepresentation> e: REALM_ROLES.entrySet()) {
            if (realmRoleName.equals(e.getValue().getName())) {
                return e.getValue();
            }
        }
        return null;
    }

    private Keycloak getClient() {
        String keycloakUrl = Config.getProperty("keycloak.authserver.url");
        logOperatore(logger, Level.FINE,"Keycloak url: {0} - REALM: {1}",keycloakUrl,REALM);
        Keycloak out = KeycloakBuilder.builder()
                .realm("master")
                .clientId("admin-cli")
                .username("admin")
                .password("admin")
                .serverUrl(keycloakUrl)
/*                .resteasyClient(new ResteasyClientBuilderImpl().connectionPoolSize(10)
                        .register(new CustomJacksonProvider()).build())  */
                .build();


        if (KEYCLOAK_CLIENT_ID == null) {
            RealmResource realm = out.realm(REALM);
            ClientsResource clnts = realm.clients();
            List<ClientRepresentation> cli = realm.clients().findAll();
            for(ClientRepresentation c:cli) {
                if (c.getClientId().equals(KEYCLOAK_CLIENT)) {
                    KEYCLOAK_CLIENT_ID = c.getId();
                }
            }
            List<RoleRepresentation> clientRoles = out.realm(REALM).clients().get(KEYCLOAK_CLIENT_ID).roles().list();
            for(RoleRepresentation cRole:clientRoles) {
                logOperatore(logger, Level.FINE,"Keyloak role CLIENT {0} - {1}",cRole.getId(),cRole.getName());
                CLIENT_ROLES.put(cRole.getName(),cRole);
            }
            List<RoleRepresentation> realMRoles = out.realm(REALM).roles().list();
            for(RoleRepresentation rRole:realMRoles ) {
                logOperatore(logger, Level.FINE,"Keyloak role REALM {0} - {1}",rRole.getId(),rRole.getName());
                REALM_ROLES.put(rRole.getName(),rRole);
            }
        }
        return out;
    }

    public Set<String> roles() {
        org.keycloak.KeycloakPrincipal pr = getPrincipal();
        KeycloakSecurityContext sc = pr.getKeycloakSecurityContext();
        return sc.getToken().getResourceAccess(KEYCLOAK_CLIENT).getRoles();
    }

    public SearchOutput usersByClientRole(String role, String term) {
        Keycloak cli = getClient();
        ClientResource cres = cli.realm(REALM).clients().get(KEYCLOAK_CLIENT_ID);
        Set<UserRepresentation> l = cres.roles().get(role).getRoleUserMembers();
        SearchOutput output = null;

        if (StringUtils.isNotBlank(term)) {
            Set<UserRepresentation> filtered = l.stream()
                    .filter(p -> StringUtils.indexOfIgnoreCase(p.getUsername(), term) != -1
                            || StringUtils.indexOfIgnoreCase(p.getFirstName(), term) != -1
                            || StringUtils.indexOfIgnoreCase(p.getLastName(), term) != -1).collect(Collectors.toSet());
            output = new SearchOutput(filtered.size(),l.size(),filtered.size(),filtered);
        } else {
            output = new SearchOutput(l.size(),l.size(),l.size(),l);

        }
        cli.close();
        return output;

    }

    public Map<String,String> businessRoles() {
        Keycloak cli = getClient();
        Map<String,String> out=new HashMap<>();
        ClientResource cres = cli.realm(REALM).clients().get(KEYCLOAK_CLIENT_ID);
        List<RoleRepresentation> roles = cres.roles().list();
        for (RoleRepresentation role : roles) {
            out.put(role.getId(), role.getName());
        }
        logOperatore(logger, Level.FINE,"Ruoli di business:{0}",out);
        cli.close();
        return out;
    }

    private void addRoleToUser(RealmResource admin,String userId,List<String>  roles,boolean client) {
        if (CollectionUtils.isNotEmpty(roles)) {
            List<RoleRepresentation> toBeAdded = new ArrayList<>();
            for(String r : roles) {

                RoleRepresentation rrep = null;
                if (client) {
                    rrep = CLIENT_ROLES.get(r);
                } else {
                    rrep = REALM_ROLES.get(r);
                }
                if (rrep!=null) {
                    logOperatore(logger, Level.FINER,"Create user, adding {0} role [{1}-{2}] to user-{3}",client ?  "CLIENT" : "REALM",r,rrep.getId(),userId);
                    toBeAdded.add(rrep);
                } else {
                    logOperatore(logger, Level.WARNING,"Create user, not found {0} role {1} to user-{2}",client ?  "CLIENT" : "REALM",r,userId);

                }
            }
            if (client) {
                admin
                        .users().get(userId).roles()
                        .clientLevel(KEYCLOAK_CLIENT_ID)
                        .add(toBeAdded);

            } else {
                admin
                        .users().get(userId).roles()
                        .realmLevel()
                        .add(toBeAdded);
            }
        }
    }

    public void addRoleToUser(String userId,List<String>  realmRoles,List<String>  clientRoles) {
        Keycloak admin = getClient();
        addRoleToUser(admin.realm(REALM),userId,realmRoles,clientRoles);
        admin.close();
    }


    private void addRoleToUser(RealmResource admin ,String userId,List<String>  realmRoles,List<String>  clientRoles) {
        addRoleToUser(admin,userId,clientRoles,true);
        addRoleToUser(admin,userId,realmRoles,false);
    }


    public String createUser(String username, String pwd,List<String> realmRoles,List<String> clientRoles) {
        logOperatore(logger, Level.FINE,"Create user:{0}-[{1}], clientRoles: {2}",username,pwd.replaceAll(".","*"),
                clientRoles!=null ? StringUtils.join(clientRoles,','):null);
        UserRepresentation ur = new UserRepresentation();
        Keycloak adm = getClient();
//        if (CollectionUtils.isNotEmpty(clientRoles)) {
//            Map<String,List<String>> cr = new HashMap<String,List<String>>();
//            cr.put(KEYCLOAK_CLIENT_ID, Arrays.asList(new String[]{"compiler","b9792c0f-82df-4ae7-9654-d6c98864e024"}));
//            cr.put(KEYCLOAK_CLIENT, Arrays.asList(new String[]{"compiler","b9792c0f-82df-4ae7-9654-d6c98864e024"}));
//            ur.setClientRoles(cr);
//
//        }
        ur.setUsername(username);
        List<CredentialRepresentation> credentials = new ArrayList<>();
        CredentialRepresentation cr = new CredentialRepresentation();
        cr.setTemporary(Boolean.FALSE);
        cr.setValue(pwd);
        cr.setType(CredentialRepresentation.PASSWORD);
        credentials.add(cr);
        ur.setCredentials(credentials);
        Response resp = adm.realm(REALM).users().create(ur);
//        MultivaluedMap<String, Object> h = resp.getHeaders();
//        for (Map.Entry<String, List<Object>> e : h.entrySet()) {
//            System.out.println("entry:"  + e.getKey()+"====>" + e.getValue());
//        }
        int status = resp.getStatus();
        if (status<200 || status > 201)  {
            switch (status) {
                case 409: throw new RuntimeException("Create Keycloak user, user " + username + " aldready exists");
                default: throw new RuntimeException("Create Keycloak user, bad response status:" + status);
            }
        }
        String loc = resp.getHeaderString("Location");
        logOperatore(logger, Level.FINEST,"Create user, location:{0}",loc);
        String userId = loc.replaceAll("(.*\\/)([^\\/]*)$","$2");
        logOperatore(logger, Level.FINE,"Create user, created user {0}",userId);

        addRoleToUser(adm.realm(REALM),userId,realmRoles,clientRoles);
        adm.close();
        return userId;
    }


    public UserDTO getUserInfo(String userId) {
        Keycloak adm = getClient();
        UserResource ures = adm.realm(REALM).users().get(userId);
        UserRepresentation ur = ures.toRepresentation();
        RoleScopeResource rres = ures.roles().clientLevel(KEYCLOAK_CLIENT_ID);
        List<RoleRepresentation> lRoles = rres.listAll();
        adm.close();
        return new UserDTO(ur,lRoles);

    }

}
