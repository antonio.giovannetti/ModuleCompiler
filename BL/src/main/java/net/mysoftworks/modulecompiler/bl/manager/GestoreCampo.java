package net.mysoftworks.modulecompiler.bl.manager;

import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.model.AllowedValue;
import net.mysoftworks.modulecompiler.model.meta.*;
import net.mysoftworks.modulecompiler.model.values.Value;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@LocalBean
public class GestoreCampo extends  AbstractEJB {
//    private static final Logger logger = Logger.getLogger(GestoreCampo.class.getName());
//
    @PersistenceContext(unitName = "mc-datasource")
    private EntityManager em;
//
//    public List<MetaField> getLista() {
//        logOperatore(logger,Level.FINE, "Lista dei campi");
//        return em.createNamedQuery(MetaField.QR_LISTA).getResultList();
//    }
//
    public List<MetaFileTypes> getMimeTypes(Set<Integer> ftypes) {
        if (CollectionUtils.isNotEmpty(ftypes)) {
            return em.createNamedQuery(MetaFileTypes.FIND_BY_IDS, MetaFileTypes.class)
                    .setParameter(MetaFileTypes.P_IDS, ftypes)
                    .getResultList();
        } else {
            return em.createNamedQuery(MetaFileTypes.QR_LIST_ALL,MetaFileTypes.class).getResultList();
        }
    }

    @Transactional
    public MetaField get(Long id) {
        logOperatore(Loggers.LOG_ADMIN,Level.FINE, "Ricerco campo {0,number,#}", id);
        MetaField out = em.find(MetaField.class, id);
        return out;
    }
    @Transactional
    public MetaField saveOrUpdate(MetaField _mf,Long idDich) {
        logOperatore(Loggers.LOG_ADMIN,Level.FINE, "SaveOrUpdate campo {0}, idDic:{1,number,#}",
                _mf, idDich);
        if (StringUtils.isEmpty(_mf.getNome())) throw new RuntimeException("Il nome del campo non puo' essere vuoto");
        MetaDichiarazione d = em.find(MetaDichiarazione.class, idDich);
        if (d==null) throw new RuntimeException("Impossibile aggiornare il campo, la dichiarazione "+idDich+" non esiste");
        for(MetaField mf:d.getFields()) {
            if (_mf.getNome().equalsIgnoreCase(mf.getNome())) {
                em.createQuery("DELETE AllowedValue av where av.idField = " + mf.getId()).executeUpdate();

                _mf.setId(mf.getId());
                return em.merge(_mf);
            }
        }

        _mf.setDichiarazione(d);
        if (_mf.getId()==null) {
            em.persist(_mf);
        } else {
            _mf = em.merge(_mf);
        }
        return _mf;
    }

    public Long findValuesWithField(Long idCampo) {
        return (Long)em.createNamedQuery(Value.QR_FIND_COUNT_IN_METAFIELD)
                .setParameter(Value.P_FIND_IN_METAFIELD_P_IDCAMPO,idCampo).getSingleResult();

    }

    public MetaField clone(MetaField _mf) {
        Long idField  = _mf.getId();
        if (_mf instanceof MetaListField) {
            MetaListField mlf = (MetaListField) _mf;
            mlf.setAllowedValues(new ArrayList<>(mlf.getAllowedValues()));
//                for(AllowedValue av: mlf.getAllowedValues()) {
//                    em.detach(av);
//                    av.setId(null);
//                }
        }
        if (_mf instanceof MetaFileField) {
            MetaFileField mff = (MetaFileField) _mf;
            HashSet<Integer> sft = new HashSet<>(mff.getFileTypes());
            mff.setFileTypes(sft);
        }
        em.detach(_mf);
        _mf.setId(null);
        _mf.setDichiarazione(null);
        em.persist(_mf);
        return _mf;
    }


    @Transactional
    public void delete(Long idCampo) {
        logOperatore(Loggers.LOG_ADMIN,Level.FINEST, "Cancellazione campo id {0,number,#}",idCampo);

//        MetaField mf = unlink(idCampo);
        List<Value> l = em.createQuery("select v FROM Value v where v.field.id = :" + Value.P_FIND_IN_METAFIELD_P_IDCAMPO,Value.class)
                .setParameter(Value.P_FIND_IN_METAFIELD_P_IDCAMPO,idCampo).getResultList();
        for(Value v:l) {
            logOperatore(Loggers.LOG_ADMIN,Level.FINEST, "Cancellazione campo-Cancello valore {0,number,#}",v.getId());
            v.setField(null);
            em.remove(v);
        }
        l.clear();
        em.flush();
//        em.createQuery("DELETE FROM Value v where v.field.id = :"+Value.P_FIND_IN_METAFIELD_P_IDCAMPO)
//                .setParameter(Value.P_FIND_IN_METAFIELD_P_IDCAMPO,idCampo).executeUpdate();
        logOperatore(Loggers.LOG_ADMIN,Level.FINEST, "Cancellazione campo-Cancello AllowedValues per campo {0,number,#}",idCampo);
        em.createQuery("DELETE FROM AllowedValue av where av.idField = :"+Value.P_FIND_IN_METAFIELD_P_IDCAMPO)
                .setParameter(Value.P_FIND_IN_METAFIELD_P_IDCAMPO,idCampo).executeUpdate();
        logOperatore(Loggers.LOG_ADMIN,Level.FINEST, "Cancellazione campo-Cancello campo {0,number,#}",idCampo);
        em.createNamedQuery(MetaField.QR_REMOVE)
                .setParameter(MetaField.P_REMOVE_P_IDCAMPO,idCampo)
                .executeUpdate();
    }

}
