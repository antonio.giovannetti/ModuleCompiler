package net.mysoftworks.modulecompiler.bl.validators;

import net.mysoftworks.modulecompiler.model.meta.*;
import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.model.values.RawValue;
import net.mysoftworks.modulecompiler.model.values.Value;
import org.w3c.dom.Document;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

//@Named
public class DichiarazioneValidator {


    public Set<ValidationMessage> validate(Dichiarazione dichiarazione, Document domPratica) {
        Set<ValidationMessage> out = new HashSet<>();
        for (Map.Entry<Long, Value> e:dichiarazione.getValues().entrySet()) {
            Value v = e.getValue();
            MetaField mf = v.getField();
            if (v.getRawValues().size() < mf.getMinOccurrences()) {
                out.add(
                new ValidationMessage(mf,ValidationMessage.TYPE.ERROR ,"fe.validation.field.minoccurrences", mf.getMinOccurrences()));
            }
            Class<? extends MetaField> cl = mf.getClass();
            if (MetaTextField.class.equals(cl)) validateMetaTextField(out,v,(MetaTextField) mf,dichiarazione,domPratica);
            if (MetaFileField.class.equals(cl)) validateMetaFileField(out,v,(MetaFileField) mf,dichiarazione,domPratica);
            if (MetaListField.class.equals(cl)) validateMetaListField(out,v,(MetaListField) mf,dichiarazione,domPratica);
            if (MetaTemporalField.class.equals(cl)) validateMetaTemporalField(out,v,(MetaTemporalField) mf,dichiarazione,domPratica);
            if (MetaAnagraficaField.class.equals(cl)) validateMetaAnagraficaField(out,v,(MetaAnagraficaField) mf,dichiarazione,domPratica);
        }

        return out;
    }

    private void validateMetaAnagraficaField(Set<ValidationMessage> out,Value v,MetaAnagraficaField mf, Dichiarazione dichiarazione, Document domPratica) {
    }

    private void validateMetaTemporalField(Set<ValidationMessage> out,Value v,MetaTemporalField mf, Dichiarazione dichiarazione, Document domPratica) {
    }

    private void validateMetaListField(Set<ValidationMessage> out,Value v,MetaListField mf, Dichiarazione dichiarazione, Document domPratica) {
    }

    private void validateMetaTextField(Set<ValidationMessage> out,Value v,MetaTextField mtf,Dichiarazione dichiarazione, Document domPratica) {
        if (mtf.getPattern() != null) {
            for(RawValue rv:v.getRawValues()) {
                if (mtf.getPattern()!=null) {
                    boolean matches = mtf.getPattern().matcher(rv.getStringValue()).matches();
                    if (!matches) {
                        out.add(
                                new ValidationMessage(mtf,ValidationMessage.TYPE.ERROR ,"fe.validation.field.text.pattern",rv.getStringValue(),mtf.getRegex() ));

                    }
                }
                }
        }

    }
    private void validateMetaFileField(Set<ValidationMessage> out,Value v,MetaFileField mf, Dichiarazione dichiarazione, Document domPratica) {
    }

}
