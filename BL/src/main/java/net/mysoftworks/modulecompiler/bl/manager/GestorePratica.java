package net.mysoftworks.modulecompiler.bl.manager;

import net.mysoftworks.modulecompiler.bl.utils.JaxbSerializer;
import net.mysoftworks.modulecompiler.model.*;
import net.mysoftworks.modulecompiler.model.meta.*;
import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.model.values.Pratica;
import net.mysoftworks.modulecompiler.model.values.Value;
import net.mysoftworks.modulecompiler.render.GestoreRender;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.keycloak.representations.idm.UserRepresentation;
import org.w3c.dom.Document;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.Principal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class GestorePratica extends AbstractEJB {
    private static final Logger logger = Logger.getLogger(GestorePratica.class.getName());
    private @PersistenceContext(unitName = "mc-datasource")
    EntityManager em;

    @Resource
    EJBContext ctx;

    @EJB
    KeycloakUtil keycloakUtil;

//    @Inject
//    GestoreValue gestoreValue;

    @EJB
    GestoreDichiarazione gestoreDichiarazione;

    @EJB
    GestoreRender gestoreRender;


    @Transactional
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Document domPraticaHtml(Long id,Map<String, Object> modelMap) {
        Pratica p = em.find(Pratica.class, id);
        JaxbSerializer ju = new JaxbSerializer();
        ju.build(p, Pratica.class);
        List<Dichiarazione> l = em.createNamedQuery(Dichiarazione.QR_ALL_BY_PRATICA, Dichiarazione.class)
                .setParameter(Dichiarazione.P_ID_PRATICA, id).getResultList();
        Map<String, String> attributesMap = new HashMap<>();
        if (modelMap==null) modelMap = new HashMap<>();
        for (Dichiarazione dic : l) {
//            modelMap.put("uuid",UUID.randomUUID().toString());
            attributesMap.put("id", dic.getId().toString());
            attributesMap.put("idMeta", dic.getIdMetaDichiarazione().toString());
            String out = gestoreRender.viewDichiarazione(dic.getId(), modelMap, false);
            ju.addRootNode(out, "dichiarazione", attributesMap);
        }
        return ju.getDocument();
    }


    @Transactional
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Document domPratica(Long id) {
        Pratica p = em.find(Pratica.class, id);
        if (p == null) throw new RuntimeException("La pratica " + id + " non esiste");
        JaxbSerializer ju = new JaxbSerializer();
        ju.build(p, Pratica.class);

//        Map<Long, MetaDichiarazione> mds= new HashMap<>();
//        for (Dichiarazione d : p.getDichiarazioni()) {
//            MetaDichiarazione md = d.getMetaDichiarazione();
//            if (!mds.containsKey(md.getId())) {
//                mds.put(md.getId(),md);
//            }
//        }
        ju.addRootNode(findDichiarazioni(id), Dichiarazione.class, null);

        return ju.getDocument();
    }

    private void impostaRuolo(Pratica pratica, RuoloPratica.RuoloCompilazionePratica ruolo) {
        for (Map.Entry<String, String> bRoles : keycloakUtil.businessRoles().entrySet()) {
            if (bRoles.getValue().equals(ruolo.getCodice())) {
                pratica.addRuolo(ruolo, ctx.getCallerPrincipal().getName());
                return;
            }
        }
        throw new RuntimeException("Il ruolo " + ruolo + " non e' gestito");
    }

    public Pratica createPratica(Long idProcedimento) {
        logOperatore(logger, Level.FINE, "Creo la pratica a partire dal procedimento[{0,number,#}]", idProcedimento);
        MetaProcedimento proc = em.find(MetaProcedimento.class, idProcedimento);
        Pratica p = new Pratica();
        p.setProcedimento(proc);
        em.persist(p);
        impostaRuolo(p, RuoloPratica.RuoloCompilazionePratica.ROLE_OWNER);
        impostaRuolo(p, RuoloPratica.RuoloCompilazionePratica.ROLE_COMPILER);
        Document dom = domPratica(p.getId());
        Map<Long, Value> values = new HashMap<>();
        for (MetaDichiarazione d : proc.getDichiarazioni()) {
            logOperatore(logger, Level.FINER, "Scorro la dichiarazione[{1,number,#}] del procedimento[{0,number,#}]", idProcedimento, d.getId());
            Dichiarazione dataDic = new Dichiarazione();
            dataDic.setIdMetaDichiarazione(d.getId());
            dataDic.setPratica(p);

            em.persist(dataDic);
            boolean present = gestoreDichiarazione.init(dataDic, dom);
            if (present) {
                dataDic.setPratica(p);
            }
        }
        return p;
    }


    private Pratica initPratica(Pratica p) {
        logOperatore(logger, Level.FINE, "Init pratica idPratica[{0,number,#}]", p.getId());
        MetaProcedimento proc = p.getProcedimento();
        Set<Long> existingDic = new HashSet<>();
        List<Dichiarazione> allDich = em.createNamedQuery(Dichiarazione.QR_ALL_BY_PRATICA, Dichiarazione.class)
                .setParameter(Dichiarazione.P_ID_PRATICA, p.getId()).getResultList();

        for (Dichiarazione d : allDich) {
            existingDic.add(d.getIdMetaDichiarazione());
        }
        Map<Long, Value> values = new HashMap<>();
        for (MetaDichiarazione d : proc.getDichiarazioni()) {
            logOperatore(logger, Level.FINER, "Scorro la dichiarazione[{0,number,#}]", d.getId());
            if (!existingDic.contains(d.getId())) {
                Dichiarazione dataDic = new Dichiarazione();
                dataDic.setIdMetaDichiarazione(d.getId());
//                p.addDichiarazione(dataDic);
                allDich.add(dataDic);
                dataDic.setPratica(p);
                em.persist(dataDic);
            }
        }
        Document dom = domPratica(p.getId());
        Set<Long> notPresent = new HashSet<>();
        for (Dichiarazione d : allDich) {
            boolean present = gestoreDichiarazione.init(d, dom);
            if (!present) {
                notPresent.add(d.getIdMetaDichiarazione());
            }
        }
        if (CollectionUtils.isNotEmpty(notPresent)) {
            allDich = em.createNamedQuery(Dichiarazione.QR_ALL_BY_PRATICA, Dichiarazione.class)
                    .setParameter(Dichiarazione.P_ID_PRATICA, p.getId()).getResultList();
            Iterator<Dichiarazione> it;
            for (it = allDich.iterator(); it.hasNext(); ) {
                Dichiarazione d = it.next();
                if (notPresent.contains(d.getIdMetaDichiarazione())) {
                    logOperatore(logger, Level.FINER, "Devo rimuovere (XPATH) la dichiarazione per meta {0,number,#}", d.getIdMetaDichiarazione());
                    em.remove(d);
                }
            }
        }
        return p;
    }

    public Pratica findPratica(Long idPratica) {
        logOperatore(logger, Level.FINE, "Cerco la pratica {0,number,#}", idPratica);
        Pratica out = em.find(Pratica.class, idPratica);
        if (out != null) {
            initPratica(out);
        }
        return out;
    }

    public List<Dichiarazione> findDichiarazioni(Long idPratica) {
        logOperatore(logger, Level.FINE, "Cerco le dichiarazioni per pratica {0,number,#}", idPratica);
        return em.createNamedQuery(Dichiarazione.QR_ALL_BY_PRATICA)
                .setParameter(Dichiarazione.P_ID_PRATICA, idPratica).getResultList();
    }

    public List<RuoloPratica> findRuoliPratica(Long idPratica) {
        logOperatore(logger, Level.FINE, "Cerco i ruoli dela pratica {0,number,#}", idPratica);
        return em.createNamedQuery(Pratica.QR_RUOLI_PRATICA)
                .setParameter(Pratica.P_ID_PRATICA, idPratica).getResultList();
    }

    private static final String NQ_SEARCH_PRATICHE="select pratica1_.id" +
            " from MC_V_DICHIARAZIONE dichiarazi0_" +
            " inner join MC_V_PRATICA pratica1_ on dichiarazi0_.ID_PRATICA=pratica1_.id" +
            " inner join MC_V_RUOLI_PRATICA ruoli2_ on pratica1_.id=ruoli2_.ID_PRATICA" +
            " where (ruoli2_.UTENTE=?"
            + " and (MOD(ruoli2_.RUOLO_BITWISE,2) <> 0)) or dichiarazi0_.COMPILATORE=?"
//                + " and (ruoli2_.RUOLO_BITWISE&1 <> 0)"
            + " order by pratica1_.DATA_MODIFICA DESC";

    public List<Pratica> searchPratiche() {
        Principal p = ctx.getCallerPrincipal();
        if (p == null) {
            throw new RuntimeException("L'utenza connessa risulta null");
        }
        logOperatore(logger, Level.FINE, "Cerco le pratiche per l''utente {0}", p.getName());
        // @see: https://www.geeksforgeeks.org/bitwise-algorithms/

        List<BigInteger> ids = em.createNativeQuery(NQ_SEARCH_PRATICHE)
                .setParameter(1, p.getName())
                .setParameter(2, p.getName())
                .getResultList();

        Set<Long> ids2 = ids.stream().map(bi -> bi.longValue()).collect(Collectors.toSet());
        List<Pratica> lOwner = null;
        if (ids2.size()!=0) {
            lOwner = em.createQuery("SELECT p FROM net.mysoftworks.modulecompiler.model.values.Pratica p WHERE p.id IN :ids", Pratica.class).setParameter("ids", ids2).getResultList();
        } else {

        }

//        List lOwner = em.createNamedQuery(Pratica.QR_ALL_BY_UTENTE)
//                .setParameter(Pratica.ALL_BY_UTENTE_P_UTENTE, p.getName())
//                .getResultList();

        return lOwner;
    }

    @Transactional
    public void deletePratica(Long id) {
        logOperatore(logger, Level.FINE, "Cancello la pratica {0,number,#}", id);
        Pratica p = em.find(Pratica.class, id);
        List<Dichiarazione> dics = findDichiarazioni(id);
        for (Dichiarazione d : dics) {
            d.setPratica(null);
            em.remove(d);
        }
        em.remove(p);
    }

    public void validate(Long idPratica) {
//        Pratica p = findPratica(idPratica);
        List<Dichiarazione> allDich = em.createNamedQuery(Dichiarazione.QR_ALL_BY_PRATICA, Dichiarazione.class)
                .setParameter(Dichiarazione.P_ID_PRATICA, idPratica).getResultList();
        Document dom = domPratica(idPratica);
        for(Dichiarazione d: allDich) {
            gestoreDichiarazione.validate(d,dom);
        }

    }

}
