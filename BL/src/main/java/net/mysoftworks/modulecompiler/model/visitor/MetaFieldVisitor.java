package net.mysoftworks.modulecompiler.model.visitor;

import net.mysoftworks.modulecompiler.model.meta.*;

public interface MetaFieldVisitor {
    void visit(MetaTextField metaTextField);
    void visit(MetaFileField metaFileField);
    void visit(MetaAnagraficaField metaAnagraficaField);
    void visit(MetaTemporalField metaTemporalField);
    void visit(MetaListField metaListField);
}
