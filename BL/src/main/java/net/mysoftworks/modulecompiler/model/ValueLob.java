package net.mysoftworks.modulecompiler.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Lob
 */
@Entity
@Table(name="MC_LOBS")
public class ValueLob implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "LOB")
	@Lob
	private byte[] value;

	public ValueLob() {
	}

	public ValueLob(Long id, byte[] value) {
		super();
		this.id = id;
		this.value = value;
	}

	public ValueLob(Long id) {
		this.id = id;
	}


	@XmlAttribute
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlTransient
	public byte[] getValue() {
		return value;
	}

	public void setValue(byte[] value) {
		this.value = value;
	}

//	@XmlElement
	@XmlTransient
	public String getStringValue() {
		return value!=null ? new String(value) : null;
	}
}
