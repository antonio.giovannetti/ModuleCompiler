package net.mysoftworks.modulecompiler.model.values;


import net.mysoftworks.modulecompiler.model.meta.MetaField;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import java.util.HashSet;
import java.util.Set;

/**
 * MetaGruppo generated by hbm2java
 */
@Entity
@Table(name="MC_V_VALUE")
@NamedQueries({
		@NamedQuery(name = Value.QR_FIND_COUNT_IN_METAFIELD,
				query = "SELECT COUNT(v) FROM Value v where v.field.id = :"+Value.P_FIND_IN_METAFIELD_P_IDCAMPO),
		@NamedQuery(name = Value.QR_FIND_IN_METAFIELD,
				query = "SELECT v FROM Value v where v.dichiarazione.id = :"+Value.P_IDDIC +
		" AND v.field.nome = :"+Value.P_CAMPO)
})
public class Value implements java.io.Serializable {
	public static final String QR_FIND_COUNT_IN_METAFIELD = "Value.QR_FIND_COUNT_IN_METAFIELD";
	public static final String QR_FIND_IN_METAFIELD = "Value.QR_FIND_IN_METAFIELD";
	public static final String P_FIND_IN_METAFIELD_P_IDCAMPO = "idCampo";
	public static final String P_IDDIC = "idDic";
	public static final String P_CAMPO = "nomeCampo";

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private Long id;

//	@Column(name="ID_PRATICA",insertable = false,updatable = false)
//	private Long idPratica;
//
//	@ManyToOne
//	@JoinColumn(name = "ID_PRATICA")
//	private Pratica pratica;


	@ManyToOne
	@JoinColumn(name = "ID_DATA_DIC")
	private Dichiarazione dichiarazione;

	@PreUpdate
	public void preUpdate() {
        if (dichiarazione!=null) dichiarazione.preUpdate();
	}

	@PrePersist
	public void prePersist() {
		if (dichiarazione!=null) dichiarazione.preUpdate();
	}


	@ManyToOne(fetch = FetchType.EAGER,optional = false)
	@JoinColumn(name = "ID_FIELD")
	private MetaField field;

	@Column(name = "ID_FIELD", insertable = false,updatable = false)
	private Long idField;

	public Long getIdField() {
		return idField;
	}

//	    @XmlElement(name="rawvalues")
    @OneToMany(mappedBy = "value",cascade = CascadeType.ALL,fetch = FetchType.EAGER,orphanRemoval = true)
//	@Transient
	private Set<RawValue> rawValues = new HashSet<>();

    public Set<RawValue> getRawValues() {
		return rawValues;
	}

	public void setRawValues(Set<RawValue> rawValues) {
		this.rawValues = rawValues;
	}

	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    @XmlTransient
    public MetaField getField() {
		return field;
	}

	public void setField(MetaField field) {
		this.field = field;
		idField = (field!=null && field.getId()!=null) ? field.getId() : idField;
	}

	@XmlTransient
	public Dichiarazione getDichiarazione() {
		return dichiarazione;
	}

	public void setDichiarazione(Dichiarazione dichiarazione) {
		this.dichiarazione = dichiarazione;
	}
}

