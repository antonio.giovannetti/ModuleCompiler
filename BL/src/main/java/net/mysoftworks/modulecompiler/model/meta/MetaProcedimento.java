package net.mysoftworks.modulecompiler.model.meta;


import net.mysoftworks.modulecompiler.model.ValueLob;
import net.mysoftworks.modulecompiler.model.enumeration.StatiProcedimento;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.*;

/**
 * Metadati del procedimento
 */
@Entity
@Table(name="MC_M_PROCEDIMENTO")
@NamedQueries({
		@NamedQuery(name = MetaProcedimento.QR_DUMP,
				query = "SELECT new MetaProcedimento(mp.id,mp.stato,mp.descrizione,mp.dateInfo,mp.compilabilita,mp.visibilita,mp.icon,mp.parent) FROM MetaProcedimento mp" ),
		@NamedQuery(name = MetaProcedimento.QR_SUMMARY,
				query = "SELECT new MetaProcedimento(mp.id,mp.stato,mp.descrizione,mp.dateInfo,mp.compilabilita,mp.visibilita,mp.icon,mp.parent) FROM MetaProcedimento mp where "
						+ "mp.id = :" + MetaProcedimento.P_IDPROC),
		@NamedQuery(name = MetaProcedimento.QR_FIND_DIC_ASSOC,
				query = "SELECT d FROM MetaProcedimento mp join mp.dichiarazioni d where d.id = :" + MetaProcedimento.P_IDDIC
						+ " AND mp.id = :" + MetaProcedimento.P_IDPROC),
		@NamedQuery(name = MetaProcedimento.QR_LISTA,
				// new MetaProcedimento(mp.id,mp.stato,mp.descrizione,mp.dateInfo,mp.compilabilita,mp.visibilita,mp.icon,mp.parent)
				query = "SELECT mp FROM MetaProcedimento mp ORDER BY mp.dateInfo.dataCreazione NULLS FIRST"),
		@NamedQuery(name = MetaProcedimento.QR_LISTA_NO_CHILDREN,
				// new MetaProcedimento(mp.id,mp.stato,mp.descrizione,mp.dateInfo,mp.compilabilita,mp.visibilita,mp.icon,mp.parent)
				query = "SELECT mp FROM MetaProcedimento mp"+
		" where mp.id not in (select distinct mp2.parent FROM MetaProcedimento mp2 WHERE mp2.parent IS NOT NULL ) ORDER BY mp.dateInfo.dataCreazione NULLS FIRST"),
//		@NamedQuery(name = MetaProcedimento.QR_LISTA_ALL,
//				query = "SELECT mp.allegati FROM MetaProcedimento mp where mp.id =:" + MetaProcedimento.P_IDPROC),
		@NamedQuery(name = MetaProcedimento.QR_METADATA_IMPACT,query = "select p.stato, count(p.id) FROM Pratica p WHERE p.idProcedimento = :" +
				 MetaProcedimento.P_IDPROC+" GROUP BY p.stato"),
		@NamedQuery(name = MetaProcedimento.QR_METADATA_NOTIFY,query = "select distinct r.user FROM Dichiarazione d JOIN d.pratica p JOIN p.ruoli r WHERE p.idProcedimento = :" +
				MetaProcedimento.P_IDPROC )

})
public class MetaProcedimento extends MetaRestrictions implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	public static final String QR_METADATA_NOTIFY = "MetaProcedimento.QR_METADATA_NOTIFY";
	public static final String QR_METADATA_IMPACT = "MetaProcedimento.QR_METADATA_IMPACT";
	public static final String QR_SUMMARY = "MetaProcedimento.summary";
	public static final String QR_DUMP = "MetaProcedimento.dump";
	public static final String QR_LISTA = "MetaProcedimento.LISTA_PROCEDIMENTI";
	public static final String QR_LISTA_NO_CHILDREN = "MetaProcedimento.LISTA_PROCEDIMENTI_NO_CHILDREN";
	public static final String QR_LISTA_ALL = "MetaProcedimento.LISTA_ALLEGATI_PROCEDIMENTO";
	public static final String P_HAS_PARENT = "hasParent";
	public static final String P_IDPROC = "ID_PROC";
	public static final String P_IDDIC = "ID_DIC";
	public static final String QR_FIND_DIC_ASSOC = "MetaProcedimento.QR_FIND_DIC_ASSOC";

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private Long id;

	public MetaProcedimento() {
		this.id = -1L;
	}
	public MetaProcedimento(Long id,Integer stato, String descrizione, MetaDateInfo dateInfo,Integer compilabilita,Integer visibilita,String icon,Long parent) {
		this.id = id;
		this.stato = stato;
		this.descrizione = descrizione;
		this.dateInfo = dateInfo;
		this.compilabilita = compilabilita;
		this.visibilita = visibilita;
		this.icon=icon;
		this.parent=parent;
	}

	@Column(name="STATO")
	private Integer stato= StatiProcedimento.OFFLINE.bytewise();

	@Column(name="DESCRIZIONE")
	private String descrizione;

	@Column(name="ICON")
	private String icon;

	@ManyToMany(cascade = {CascadeType.MERGE,CascadeType.DETACH,CascadeType.REMOVE,CascadeType.PERSIST},fetch = FetchType.LAZY)
	@JoinTable(name = "MC_M_PR_ALL",joinColumns = @JoinColumn(name="PR"),
			inverseJoinColumns = @JoinColumn(name="ALLEG"))
//	@Transient
	private Set<MetaFileField> allegati = new HashSet<>();


	@ManyToMany(cascade = {CascadeType.MERGE,CascadeType.DETACH,CascadeType.REMOVE},fetch = FetchType.LAZY/*,mappedBy = "procedimento"*/)
	@JoinTable(name = "MC_M_PR_DIC",joinColumns = @JoinColumn(name="PR"),
	inverseJoinColumns = @JoinColumn(name="DIC"))
	@OrderColumn(name = "POS")
	private List<MetaDichiarazione> dichiarazioni = new ArrayList<>();

	@OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_GUIDA")
	private ValueLob guidaCompilazione;

	@Column(name = "ID_GUIDA",insertable = false,updatable = false)
	private Long idGuida;

	@Column(name = "ID_PARENT")
	private Long parent;

//	@Transient
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name="MC_M_PROCEDIMENTO", joinColumns=@JoinColumn(name="ID_PARENT"))
	@Column(name = "ID")
	private List<Long> children = null;

	@XmlAttribute
	public Long getParent() {
		return parent;
	}

	public void setParent(Long parent) {
		this.parent = parent;
	}

	@XmlElementWrapper(name = "children")
	@XmlElement(name="child")
	public List<Long> getChildren() {
		return children;
	}

	public void setChildren(List<Long> children) {
		this.children = children;
	}

	@XmlAttribute
	public Long getIdGuida() {
		return idGuida;
	}
	public void setIdGuida(Long idGuida) {
		this.idGuida = idGuida;
	}

	public ValueLob getGuidaCompilazione() {
		return guidaCompilazione;
	}

	public void setGuidaCompilazione(ValueLob guidaCompilazione) {
		this.guidaCompilazione = guidaCompilazione;
		if (guidaCompilazione!=null) idGuida = guidaCompilazione.getId(); else idGuida = null;
	}

	public List<MetaDichiarazione> getDichiarazioni() {
		return dichiarazioni;
	}

	public void setDichiarazioni(List<MetaDichiarazione> dichiarazioni) {
		this.dichiarazioni = dichiarazioni;
	}

	public Set<MetaFileField> getAllegati() {
		return allegati;
	}

	public void setAllegati(Set<MetaFileField> allegati) {
		this.allegati = allegati;
	}

	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlAttribute
	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public boolean isOffline() {
		return StatiProcedimento.OFFLINE.bytewise().equals(stato);
	}
	public boolean isCompilabile() {
		return (StatiProcedimento.COMPILABILE.bytewise() & stato) !=0;
	}
	public boolean isConsultabile() {
		return (StatiProcedimento.CONSULTABILE.bytewise() & stato) !=0;
	}
	public boolean isInviabile() {
		return (StatiProcedimento.INVIABILE.bytewise() & stato) !=0;
	}

	public void setOffline() {
		stato = StatiProcedimento.OFFLINE.bytewise();
	}
	public void setCompilabile() {
		stato = StatiProcedimento.COMPILABILE.bytewise();
	}
	public void setConsultabile() {
		stato = StatiProcedimento.CONSULTABILE.bytewise();
	}
	public void setInviabile() {
		stato = StatiProcedimento.INVIABILE.bytewise();
	}

	@XmlAttribute
	public Integer getStato() {
		return stato;
	}

	public void setStato(Integer stato) {
		this.stato = stato;
	}

	@XmlAttribute
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	@Embedded
	private MetaDateInfo dateInfo=new MetaDateInfo();

	@XmlElement
	public MetaDateInfo getDateInfo() {
		return dateInfo;
	}

	@PrePersist
	public void preP(){
		dateInfo.setDataCreazione(new Date());
	}

	@PreUpdate
	public void preU(){
		dateInfo.setDataModifica(new Date());
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("MetaProcedimento{");
		sb.append("id=").append(id);
		sb.append(", stato=").append(stato);
		sb.append(", descrizione='").append(descrizione).append('\'');
		sb.append(", allegati=").append(allegati);
		sb.append(", dateInfo=").append(dateInfo);
		sb.append('}').append(super.toString());
		return sb.toString();
	}

}
