package net.mysoftworks.modulecompiler.model.values;

import net.mysoftworks.modulecompiler.model.meta.MetaDichiarazione;
import net.mysoftworks.modulecompiler.render.Renderable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "MC_V_DICHIARAZIONE")
@NamedQueries({
        @NamedQuery(name = Dichiarazione.QR_ALL_ID_BY_PRATICA,query =
                "SELECT d.id FROM MetaProcedimento mp join mp.dichiarazioni md, Dichiarazione d WHERE d.idMetaDichiarazione=md.id and mp.id = d.pratica.idProcedimento and d.pratica.id =:"+
                        Dichiarazione.P_ID_PRATICA + " order by INDEX(md)"),
        @NamedQuery(name = Dichiarazione.QR_ALL_BY_PRATICA,query =
                "SELECT d FROM MetaProcedimento mp join mp.dichiarazioni md, Dichiarazione d WHERE d.idMetaDichiarazione=md.id and mp.id = d.pratica.idProcedimento and d.pratica.id =:"+
                        Dichiarazione.P_ID_PRATICA + " order by INDEX(md)"),


//        @NamedQuery(name = Dichiarazione.QR_ALL_BY_PRATICA,query =
//        "SELECT d FROM Dichiarazione d,MetaProcedimento mp join mp.dichiarazioni md WHERE mp.id=d.pratica.idProcedimento and d.idMetaDichiarazione=md.id and d.pratica.id = :"
//                + Dichiarazione.P_ID_PRATICA /*+ " ORDER BY d.idMetaDichiarazione"*/),
        @NamedQuery(name = Dichiarazione.QR_GET,query =
                "SELECT d FROM Dichiarazione d join fetch d.values WHERE d.id = :" + Dichiarazione.P_ID_DIC)//
//		,@NamedQuery(name = Pratica.QR_DEEP_FETCH_BY_ID,query = "SELECT p FROM Pratica p JOIN FETCH p.ruoli r JOIN FETCH p.values v JOIN FETCH v.rawValues rv WHERE p.id = :" +
//				Pratica.P_ID_PRATICA)
}
)
public class Dichiarazione implements Serializable {
    public static final String QR_GET = "Dichiarazione.QR_GET";
    public static final String QR_ALL_BY_PRATICA = "Dichiarazione.QR_ALL_BY_PRATICA";
    public static final String QR_ALL_ID_BY_PRATICA = "Dichiarazione.QR_ALL_ID_BY_PRATICA";
    public static final String P_ID_PRATICA = "idPrat";
    public static final String P_ID_DIC= "idDic";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @OneToOne()
//    @JoinColumn(name = "ID_META")
//    private MetaDichiarazione metaDichiarazione=new MetaDichiarazione();

    @Column(name = "ID_META")
    private Long idMetaDichiarazione;

    @OneToMany(mappedBy = "dichiarazione",fetch = FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval = true)
    @MapKeyColumn(name="ID_FIELD")
    @OrderBy(value = "ID_FIELD")
    private Map<Long, Value> values=new HashMap<>();

    public void addValue(Value value) {
        values.put(value.getIdField(),value);
        value.setDichiarazione(this);
    }

    @ManyToOne()
    @JoinColumn(name="ID_PRATICA")
    private Pratica pratica;

    /**
     * Identificativo dell'utente compilatore nel sistema di autenticazione (SSO)
     */
    @Column(name = "COMPILATORE")
    private String compilatore;

    @XmlAttribute
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlAttribute
    public String getCompilatore() {
        return compilatore;
    }

    public void setCompilatore(String compilatore) {
        this.compilatore = compilatore;
    }

//    public void setMetaDichiarazione(MetaDichiarazione metaDichiarazione) {
//        this.metaDichiarazione = metaDichiarazione;
//    }

//    @XmlTransient
//    public MetaDichiarazione getMetaDichiarazione() {
//        return metaDichiarazione;
//    }

    @PreUpdate
    public void preUpdate() {
        pratica.preU();
    }

    @PrePersist
    public void prePersist() {
        pratica.preU();
    }

    @XmlAttribute
    public Long getIdMetaDichiarazione() {
        return idMetaDichiarazione;
    }

    public void setIdMetaDichiarazione(Long idMetaDichiarazione) {
        this.idMetaDichiarazione = idMetaDichiarazione;
    }

    @XmlTransient
    public Pratica getPratica() {
        return pratica;
    }

    public void setPratica(Pratica pratica) {
        this.pratica = pratica;
    }

    public Map<Long, Value> getValues() {
        return values;
    }

    public void setValues(Map<Long, Value> values) {
        this.values = values;
    }


}
