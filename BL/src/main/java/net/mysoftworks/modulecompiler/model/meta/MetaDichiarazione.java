package net.mysoftworks.modulecompiler.model.meta;



import net.mysoftworks.modulecompiler.model.ValueLob;
import net.mysoftworks.modulecompiler.render.DichiarazioneRenderer;
import net.mysoftworks.modulecompiler.render.Renderable;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author antonio
 */
@Entity
@Table(name="MC_M_DICHIARAZIONE")
@NamedQueries({
		@NamedQuery(name = MetaDichiarazione.QR_LISTA,
				query = "SELECT new MetaDichiarazione(" +
						"mp.id,mp.spuntabile,mp.descrizione,mp.templateEngine,mp.dateInfo, mp.compilabilita,mp.visibilita) FROM MetaDichiarazione mp"),
		@NamedQuery(name = MetaDichiarazione.QR_LISTA_PER_SELEZIONE, query = "SELECT d.id,d.descrizione,d.dateInfo.dataCreazione FROM MetaDichiarazione d" +
				" WHERE d.id NOT IN (SELECT d.id FROM MetaProcedimento p JOIN p.dichiarazioni d WHERE p.id=:"+MetaDichiarazione.P_PARAM_ID_PROC +") AND (:"+
		MetaDichiarazione.QR_PARAM_Q + " IS NULL OR LOWER(d.descrizione) LIKE :" + MetaDichiarazione.QR_PARAM_Q +")"),
/*
		@NamedQuery(name = MetaDichiarazione.QR_CHECK_SAVE,
				query="select d.pratica.stato,count(distinct d.pratica) from Dichiarazione d where d.idMetaDichiarazione = :"+MetaProcedimento.P_IDDIC
						+ " OR (:" + MetaProcedimento.P_IDPROC + " is null or d.pratica.idProcedimento = :" + MetaProcedimento.P_IDPROC
						+ ") GROUP BY d.pratica.stato")
*/

		@NamedQuery(name = MetaDichiarazione.QR_CHECK_SAVE,
				query="select p.stato,count(distinct p.id) from net.mysoftworks.modulecompiler.model.values.Pratica p where (:"
						+ MetaProcedimento.P_IDPROC + " is null or (p.idProcedimento = :" + MetaProcedimento.P_IDPROC +
						")) or :" + MetaProcedimento.P_IDDIC + " is null or (p.idProcedimento in ( SELECT mp.id FROM MetaProcedimento mp join mp.dichiarazioni md where md.id = :" +
						MetaProcedimento.P_IDDIC
						+")) GROUP BY p.stato")
})
public class MetaDichiarazione extends MetaRestrictions implements java.io.Serializable,Renderable {
	public static final String QR_CHECK_SAVE = "MetaDichiarazione.QR_CHECK_SAVE";
	public static final String QR_LISTA = "MetaDichiarazione.LISTA_DICHIARAZIONI";
	public static final String QR_LISTA_PER_SELEZIONE = "MetaDichiarazione.QR_LISTA_PER_SELEZIONE";
	public static final String P_PARAM_ID_PROC = "idProc";
//	public static final String P_IDMETADIC = "idMetaDich";
	public static final String QR_PARAM_Q = "q";
	public static final String P_PARAMETER_PREFIX ="campo.";
	public static final String P_PARAMETER_CLONE ="clone";
//	private static final String XPATH_TRUE="true()";
	public enum TEMPLATE_ENGINE {
		FTL("FreeMarker"),VELOCITY("Apache Velocity"),JSP("Java Server Pages");
		private String descrizione;
		TEMPLATE_ENGINE(String descrizione) {
			this.descrizione=descrizione;
		}

		public String getDescrizione() {
			return descrizione;
		}
	}

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private Long id;
	public MetaDichiarazione()  {
	}
	public MetaDichiarazione(Long id,
							 boolean spuntabile,
							 String descrizione,
							 TEMPLATE_ENGINE templateEngine,
							 MetaDateInfo dateInfo,
							 Integer compilabilita,Integer visibilita) {

		this.id=id;
		this.spuntabile = spuntabile;
		this.descrizione = descrizione;
		this.templateEngine = templateEngine;
		this.dateInfo = dateInfo;
		this.compilabilita = compilabilita;
		this.visibilita = visibilita;

	}

//	@ManyToMany
//	@JoinTable(name = "MC_M_PR_DIC",joinColumns = @JoinColumn(name="DIC"),
//			inverseJoinColumns = @JoinColumn(name="PR"))
//	private MetaProcedimento procedimento;
//
//	public MetaProcedimento getProcedimento() {
//		return procedimento;
//	}
//
//	public void setProcedimento(MetaProcedimento procedimento) {
//		this.procedimento = procedimento;
//	}

	@Column(name = "SPUNTABILE")
	private boolean spuntabile;

	private String descrizione;

	@Column(name = "MANDATORY_IF")
	private String mandatoryIf=null;

	@Column(name = "PRESENT_IF")
	private String presentIf=null;

	@OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_GUIDA")
	private ValueLob guidaCompilazione;

	@Column(name = "ID_GUIDA",insertable = false,updatable = false)
	private Long idGuida;

	@XmlTransient
	public Long getIdGuida() {
		return idGuida;
	}

	public void setIdGuida(Long idGuida) {
		this.idGuida = idGuida;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "TEMPLATE_ENGINE")
	private TEMPLATE_ENGINE templateEngine=TEMPLATE_ENGINE.FTL;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_TEMPLATE")
	private ValueLob template;

	@Column(name = "ID_TEMPLATE",insertable = false,updatable = false)
	private Long idTemplate;

	@OneToMany(fetch = FetchType.EAGER,mappedBy = "dichiarazione",orphanRemoval = true)
//	@JoinColumn(name="ID_DIC",referencedColumnName = "ID")
	private Set<MetaField> fields=new HashSet<>();

	@Column(name = "MAX_OCCURRENCES")
	private byte maxOccurrences=1;

	@Column(name = "CUSTOM_CONTROLLER")
	private String customController;

	@Override
	public Class getRendererClass() {
		if (StringUtils.isNotBlank(customController)) {
			try {
				return Class.forName(customController);
			} catch (Exception e){
				e.printStackTrace();
				return getDefaultRendererClass();
			}
		}
		return getDefaultRendererClass();

	}

	@Override
	public Class getDefaultRendererClass() {
		return DichiarazioneRenderer.class;
	}

	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMandatoryIf() {
		return mandatoryIf;
	}

	public void setMandatoryIf(String mandatoryIf) {
		this.mandatoryIf = mandatoryIf;
	}

	public String getPresentIf() {
		return presentIf;
	}

	public void setPresentIf(String presentIf) {
		this.presentIf = presentIf;
	}

	@XmlAttribute
	public TEMPLATE_ENGINE getTemplateEngine() {
		return templateEngine;
	}

	public void setTemplateEngine(TEMPLATE_ENGINE templateEngine) {
		this.templateEngine = templateEngine;
	}

	public Set<MetaField> getFields() {
		return fields;
	}

	public void setFields(Set<MetaField> fields) {
		this.fields = fields;
	}

	@XmlAttribute
	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	@XmlAttribute
	public boolean isSpuntabile() {
		return spuntabile;
	}

	public void setSpuntabile(boolean spuntabile) {
		this.spuntabile = spuntabile;
	}

	public ValueLob getTemplate() {
		return template;
	}

	public void setTemplate(ValueLob template) {
		this.template = template;
	}

	@XmlAttribute
	public Long getIdTemplate() {
		return idTemplate;
	}

	@Embedded
	private MetaDateInfo dateInfo=new MetaDateInfo();

	@XmlElement
	public MetaDateInfo getDateInfo() {
		return dateInfo;
	}

	@XmlAttribute
	public byte getMaxOccurrences() {
		return maxOccurrences;
	}

	public void setMaxOccurrences(byte maxOccurrences) {
		this.maxOccurrences = maxOccurrences;
	}

	@PrePersist
	public void preP(){
		dateInfo.setDataCreazione(new Date());
	}

	@PreUpdate
	public void preU(){
		dateInfo.setDataModifica(new Date());
	}

	@XmlAttribute
	public String getCustomController() {
		return customController;
	}

	public void setCustomController(String customController) {
		this.customController = customController;
	}

	public ValueLob getGuidaCompilazione() {
		return guidaCompilazione;
	}

	public void setGuidaCompilazione(ValueLob guidaCompilazione) {
		this.guidaCompilazione = guidaCompilazione;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("MetaDichiarazione{");
		sb.append("id=").append(id);
		sb.append(", spuntabile=").append(spuntabile);
		sb.append(", descrizione='").append(descrizione).append('\'');
		sb.append(", dateInfo=").append(dateInfo);
		sb.append('}');
		return sb.toString();
	}
}
