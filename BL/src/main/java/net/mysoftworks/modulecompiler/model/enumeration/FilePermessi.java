package net.mysoftworks.modulecompiler.model.enumeration;

public enum FilePermessi {
	TXT("File di testo",1,"text/plain"),
	PDF("File PDF",2,"application/pdf"),
	PNG("File PNG",4,"image/png");

	private String descrizione;
	private Integer bytewise;
	private String mime;

	private FilePermessi(String descrizione, Integer bytewise,String mime){
		this.descrizione = descrizione;
		this.bytewise = bytewise;
		this.mime = mime;
	}

	public String getDescrizione(){
		return descrizione;
	}

	public Integer bytewise() {
		return bytewise;
	}

	public String getMime() {
		return mime;
	}
}