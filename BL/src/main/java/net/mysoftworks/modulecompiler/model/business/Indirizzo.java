package net.mysoftworks.modulecompiler.model.business;

import net.mysoftworks.modulecompiler.PropAnnotation;

import javax.persistence.*;
import java.util.Set;

/**
 * GuidaCompilazione
 */
@Entity
@Table(name="INDIRIZZO")
public class Indirizzo implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	public Indirizzo(){}
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private Long id;

	private String via;

	@Transient
	private Set<Long> sampleSet;

	public Set<Long> getSampleSet() {
		return sampleSet;
	}

	public void setSampleSet(Set<Long> sampleSet) {
		this.sampleSet = sampleSet;
	}

	@ManyToOne
	private Comune comune=new Comune();

	private Long civico;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

	public Comune getComune() {
		return comune;
	}

	public void setComune(Comune comune) {
		this.comune = comune;
	}

	@PropAnnotation(label = "Nr.civico",title = "Numero civico")
	public Long getCivico() {
		return civico;
	}

	public void setCivico(Long civico) {
		this.civico = civico;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Indirizzo{");
		sb.append("id=").append(id);
		sb.append(", via='").append(via).append('\'');
		sb.append(", comune='").append(comune).append('\'');
		sb.append(", civico=").append(civico);
		sb.append('}');
		return sb.toString();
	}
}
