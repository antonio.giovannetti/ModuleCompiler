package net.mysoftworks.modulecompiler.model;

import net.mysoftworks.modulecompiler.model.values.Pratica;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "MC_V_RUOLI_PRATICA")
public class RuoloPratica implements Serializable{

    public RuoloPratica(){}

    public RuoloPratica(Pratica pratica, String user,Integer roleBitwise) {
        this.pratica = pratica;
        this.user = user;
        this.roleBitwise = roleBitwise;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @Id
    @JoinColumn(name = "ID_PRATICA",referencedColumnName="ID")
    private Pratica pratica;

    @Column(name = "ID_PRATICA",insertable = false,updatable = false)
    private Long idPratica;

    public Long getIdPratica() {
        return idPratica;
    }

    @Id
    @Column(name = "UTENTE")
    private String user;

//    @Id
//    @Column(name = "RUOLO")
//    private String role;

    @Column(name = "RUOLO_BITWISE")
    private Integer roleBitwise;


    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

//    public String getRole() {
//        return role;
//    }
//
//    public void setRole(String role) {
//        this.role = role;
//    }

    public Integer getRoleBitwise() {
        return roleBitwise;
    }

    public void setRoleBitwise(Integer roleBitwise) {
        this.roleBitwise = roleBitwise;
    }

    @XmlTransient
    public Pratica getPratica() {
        return pratica;
    }

    public void setPratica(Pratica pratica) {
        this.pratica = pratica;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RuoloPratica that = (RuoloPratica) o;
        return Objects.equals(idPratica, that.idPratica) &&
                Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idPratica, user);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RuoloPraticaId{");
        sb.append(", idPratica='").append(idPratica).append('\'');
        sb.append(", user='").append(user).append('\'');
        sb.append(", roleBitwise='").append(roleBitwise).append('\'');
        sb.append('}');
        return sb.toString();
    }


    public static enum RuoloCompilazionePraticaOperator {
        ALL, SOME,NONE;
    }

    public static enum RuoloCompilazionePratica {
        TUTTI("Tutti",255),
        RUOLO5("Ruolo 5",16),
        ROLE_OWNER("owner",9),// 1001
        ROLE_EDITOR("editor",5),// 101
        ROLE_COMPILER("compiler",3),// 11
        ROLE_VIEWER("viewer",1); // 1

        private String codice;
        private Integer bytewise;

        public boolean isOn(Integer val) {
            return (val & bytewise)==bytewise;
        }
        private RuoloCompilazionePratica(String descrizione, Integer bytewise){
            this.codice = descrizione;
            this.bytewise = bytewise;
        }

        public String getCodice(){
            return codice;
        }

        public Integer bytewise() {
            return bytewise;
        }

        public static String describeFigure(Integer fig) {
            StringBuilder result = new StringBuilder();
            for (RuoloCompilazionePratica fp: RuoloCompilazionePratica.values()) {
                if (fp.equals(TUTTI)) continue;
                if ((fp.bytewise() & fig)!=0) {
                    if (result.length()!=0) {
                        result.append(", ");
                    }
                    result.append(fp.getCodice());
                }
            }
            return result.toString();
        }

    }
}
