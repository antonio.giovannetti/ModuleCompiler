package net.mysoftworks.modulecompiler.model.business;

import net.mysoftworks.modulecompiler.PropAnnotation;

import javax.persistence.*;

/**
 * GuidaCompilazione
 */
@Entity
@Table(name="COMUNE")
public class Comune implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private Long id;

	private String cap;

	private String descrizione;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Comune{");
		sb.append("id=").append(id);
		sb.append(", cap='").append(cap).append('\'');
		sb.append(", descrizione='").append(descrizione).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
