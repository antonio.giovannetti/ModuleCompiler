package net.mysoftworks.modulecompiler.model.meta;


import net.mysoftworks.modulecompiler.model.values.RawValue;
import net.mysoftworks.modulecompiler.model.visitor.MetaFieldEntity;
import net.mysoftworks.modulecompiler.render.FieldRenderer;
import net.mysoftworks.modulecompiler.render.Renderable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

/**
 * MetaGruppo generated by hbm2java
 */
@Entity
@Table(name="MC_M_FIELD")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="FIELD_TYPE")
@NamedQueries({
		@NamedQuery(name = MetaField.QR_REMOVE,
				query = "DELETE MetaField mf where mf.id= :"+MetaField.P_REMOVE_P_IDCAMPO),
		@NamedQuery(name = MetaField.QR_LISTA,
				query = "SELECT mf FROM MetaField mf"),
		@NamedQuery(name = MetaField.QR_TFM,
				query = "SELECT mf FROM MetaProcedimento mp JOIN mp.dichiarazioni d JOIN d.fields mf WHERE " +
						"mp.id =:" + MetaField.P_TFM_P_IDPROC + " and d.id =:" + MetaField.P_TFM_P_IDDIC +
						" and mf.nome =:" + MetaField.P_TFM_P_NAME),
		@NamedQuery(name = MetaField.QR_MF_BY_NAME_AND_META,
				query = "select mf from MetaField mf where mf.dichiarazione.id = :"+MetaField.P_TFM_P_META+" and mf.nome = :"+MetaField.P_TFM_P_NAME)
})
public abstract class MetaField implements MetaFieldEntity,java.io.Serializable,Renderable {
	public static final String QR_REMOVE = "MetaField.REMOVE_CAMPO";
	public static final String P_REMOVE_P_IDCAMPO = "idCampo";
	public static final String QR_LISTA = "MetaField.LISTA_CAMPI";
	public static final String QR_TFM = "MetaField.TREE_FIELD_METADATA";
	public static final String QR_MF_BY_NAME_AND_META = "MetaField.BY_NAME_AND_META";
	public static final String P_TFM_P_IDPROC = "idProc";
	public static final String P_TFM_P_IDDIC = "idDic";
	public static final String P_TFM_P_NAME = "fieldName";
	public static final String P_TFM_P_META = "idMeta";



	public enum BUSINESS_FIELD_TYPE {
		LIST("Lista","fas fa-list-ul"),TEXT("Testo","fas fa-italic"),FILE("File","fas fa-file"),TEMPORAL("Data/ora","fas fa-calendar-alt"),ANAG("Anagrafica","fas fa-user");
		private final String desc;
		private final String icon;

		BUSINESS_FIELD_TYPE(String desc,String icon) {
			this.desc = desc;this.icon = icon;
		}

		public String getDesc() {
			return desc;
		}
		public String getIcon() {
			return icon;
		}
	}
	private static final long serialVersionUID = 1L;

	public MetaField(){}
	public MetaField(BUSINESS_FIELD_TYPE ft)  {
		this.fieldType = ft;
	}
	public MetaField(MetaField source) {
		this.etichetta = source.etichetta;
		this.nome = source.nome;
		this.larghezza = source.larghezza;
		this.id=source.id;
		this.fieldType=source.fieldType;
	}

	@Column(name="FIELD_TYPE",insertable = false,updatable = false)
	@Enumerated(EnumType.STRING)
	private BUSINESS_FIELD_TYPE fieldType;

	@XmlAttribute
	public BUSINESS_FIELD_TYPE getFieldType() {
		return fieldType;
	}

	/* [1-12], according to bootstrap gridsystem */
	@Column(name = "LARGHEZZA")
	private byte larghezza=0;

	@Column(name = "ETICHETTA",nullable = false)
	private String etichetta;

	@Column(name = "MIN_OCCURRENCES")
	private byte minOccurrences=1;

	@Column(name = "MAX_OCCURRENCES")
	private byte maxOccurrences=1;

	@ManyToOne()
	@JoinColumn(name = "ID_DIC")
	private MetaDichiarazione dichiarazione;

	@Transient
	private boolean inline = true;

	@XmlTransient
	public MetaDichiarazione getDichiarazione() {
		return dichiarazione;
	}

	public void setDichiarazione(MetaDichiarazione dichiarazione) {
		this.dichiarazione = dichiarazione;
		if (dichiarazione!=null) {
			dichiarazione.getFields().add(this);
		}
	}

	@Override
	public Class getRendererClass() {
		return FieldRenderer.class;
	}

	@Override
	public Class getDefaultRendererClass(){
		return FieldRenderer.class;
	}
	@Transient
	private Long nrValues;

	public void setNrValues(Long nrValues) {
		this.nrValues = nrValues;
	}

	@XmlAttribute
	public Long getNrValues() {
		return nrValues;
	}

	@XmlAttribute
	public byte getLarghezza() {
		return larghezza;
	}

	public void setLarghezza(byte larghezza) {
		this.larghezza = larghezza;
	}

	@XmlAttribute
	public String getEtichetta() {
		return etichetta;
	}

	public void setEtichetta(String etichetta) {
		this.etichetta = etichetta;
	}

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="NOME")
	private String nome;

	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlAttribute
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@XmlAttribute
	public boolean isInline() {
		return inline;
	}

	public void setInline(boolean inline) {
		this.inline = inline;
	}

	@XmlAttribute
	public byte getMinOccurrences() {
		return minOccurrences;
	}

	public void setMinOccurrences(byte minOccurrences) {
		this.minOccurrences = minOccurrences;
	}

	@XmlAttribute
	public byte getMaxOccurrences() {
		return maxOccurrences;
	}

	public void setMaxOccurrences(byte maxOccurrences) {
		this.maxOccurrences = maxOccurrences;
	}
	public abstract Object viewValue(RawValue rawValue);

	public Object editValue(RawValue rawValue){
		return viewValue(rawValue);
	}

	@Override
	public String toString() {
		return "MetaField{" +
				"id=" + id +
				", inline=" + inline +
				", etichetta=" + etichetta +
				", nome='" + nome + '\'' +
				'}';
	}

}
