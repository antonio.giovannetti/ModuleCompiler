package net.mysoftworks.modulecompiler.model.meta;



import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author antonio
 */
@Embeddable
public class MetaDateInfo implements java.io.Serializable {

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE")
	private Date dataCreazione=new Date();

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_MODIFICA")
	private Date dataModifica=new Date();


	@XmlAttribute
	public Date getDataCreazione() {
		return dataCreazione;
	}

	@XmlAttribute
	public Date getDataModifica() {
		return dataModifica;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public void setDataModifica(Date dataModifica) {
		this.dataModifica = dataModifica;
	}

	@Override
	public String toString() {
		return "MetaDateInfo{" +
				"dataCreazione=" + dataCreazione +
				", dataModifica=" + dataModifica +
				'}';
	}
}
