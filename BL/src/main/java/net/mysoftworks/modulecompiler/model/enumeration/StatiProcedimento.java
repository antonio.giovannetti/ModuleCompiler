package net.mysoftworks.modulecompiler.model.enumeration;

public enum StatiProcedimento {
	OFFLINE("Offline",0),
	CONSULTABILE("Consultabile",1),
	COMPILABILE("Compilabile",2),
	INVIABILE("Inviabile",4);

	private String descrizione;
	private Integer bytewise;

	private StatiProcedimento(String descrizione, Integer bytewise){
		this.descrizione = descrizione;
		this.bytewise = bytewise;
	}

	public static String describeStato(Integer stato) {
		StringBuilder result = new StringBuilder();
		for (StatiProcedimento st:StatiProcedimento.values()) {
			if ((st.bytewise() & stato)!=0) {
				if (result.length()!=0) {
					result.append(", ");
				}
				result.append(st.getDescrizione());
			}
		}
		if (stato.equals(0)) result.append(OFFLINE.descrizione);
		return result.toString();
	}

	public String getDescrizione(){
		return descrizione;
	}

	public Integer bytewise() {
		return bytewise;
	}

	public static int switchOff(int src, int bit) {
		return src & ~bit;
	}

    public static int switchOn(int src, int bit) {
        return src & bit;
    }

}