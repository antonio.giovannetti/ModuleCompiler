package net.mysoftworks.modulecompiler.model.visitor;

public interface MetaFieldEntity {
        void accept(MetaFieldVisitor visitor);
}
