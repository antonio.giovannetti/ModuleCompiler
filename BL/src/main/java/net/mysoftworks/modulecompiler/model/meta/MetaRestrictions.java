package net.mysoftworks.modulecompiler.model.meta;

import net.mysoftworks.modulecompiler.model.RuoloPratica;

import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlAttribute;

@MappedSuperclass
public class MetaRestrictions {

    protected Integer compilabilita= RuoloPratica.RuoloCompilazionePratica.TUTTI.bytewise();

    protected Integer visibilita= RuoloPratica.RuoloCompilazionePratica.TUTTI.bytewise();

    public void setCompilabilita(Integer compilabilita) {
        this.compilabilita = compilabilita;
    }

    @XmlAttribute
    public Integer getCompilabilita() {
        return compilabilita;
    }

    public void setVisibilita(Integer visibilita) {
        this.visibilita = visibilita;
    }

    @XmlAttribute
    public Integer getVisibilita() {
        return visibilita;
    }

    public boolean isCompilabile(RuoloPratica.RuoloCompilazionePratica test) {
        return test!=null && (test.bytewise() & compilabilita) !=0;
    }
    public boolean isVisibilile(RuoloPratica.RuoloCompilazionePratica test) {
        return test!=null && (test.bytewise() & visibilita) !=0;
    }

    @Override
    public String toString() {
        return "MetaRestrictions{" +
                "compilabilita=" + compilabilita +
                ", visibilita=" + visibilita +
                '}';
    }
}
