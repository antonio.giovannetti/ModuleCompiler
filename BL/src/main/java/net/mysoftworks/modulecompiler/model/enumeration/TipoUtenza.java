package net.mysoftworks.modulecompiler.model.enumeration;

public enum TipoUtenza{
	ANONIMO("N","Anonimo", false),
	GENERICO("G","Generico", true),
	ASSISTITO("A","Assistito", true),
	TECNICO_ASSEVERATORE("T","Tecnico Asseveratore", false),
	ENTE_ESTERNO("E","Ente Esterno", true),
	INTERMEDIARIO("I","Intermediario", true),
	OPERATORE_BACKOFFICE("O","Operatore Backoffice", false);

	private String codice;
	private String descrizione;
	private boolean supportaCondizioniUtilizzo;

	private TipoUtenza(String codice, String descrizione, boolean supportaCondizioniUtilizzo ){
		this.codice = codice;
		this.descrizione = descrizione;
		this.supportaCondizioniUtilizzo = supportaCondizioniUtilizzo;
	}

	public String getDescrizione(){
		return descrizione;
	}
	public String getCodice(){
		return codice;
	}


	public boolean getSupportaCondizioniUtilizzo() {

		return supportaCondizioniUtilizzo;
	}

	public static TipoUtenza getInstance(String codice){
		if(codice==null){
			return null;
		}
		for(TipoUtenza s: TipoUtenza.values()){
			if(s.getCodice().equals(codice)){
				return s;
			}
		}
		return null;
	}

	public String toString(){
		return descrizione.concat(" (").concat(codice).concat(")");
	}

}