package net.mysoftworks.modulecompiler.model.values;


import net.mysoftworks.modulecompiler.model.meta.MetaDateInfo;
import net.mysoftworks.modulecompiler.model.meta.MetaProcedimento;
import net.mysoftworks.modulecompiler.model.RuoloPratica;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Entity
@Table(name="MC_V_PRATICA")
@NamedQueries({@NamedQuery(name = Pratica.QR_ALL_BY_UTENTE,query = "SELECT DISTINCT p FROM Dichiarazione d JOIN d.pratica p JOIN p.ruoli r WHERE (r.id.user = :" +
		Pratica.ALL_BY_UTENTE_P_UTENTE+" AND mod(r.roleBitwise *128 / 2, 128)<>0) OR ( d.compilatore = :" +
		Pratica.ALL_BY_UTENTE_P_UTENTE + ") ORDER BY p.dateInfo.dataModifica DESC")
		,@NamedQuery(name = Pratica.QR_RUOLI_PRATICA,query = "SELECT p.ruoli FROM Pratica p WHERE p.id = :" +
		Pratica.P_ID_PRATICA)
}
)

// select p.stato, count(p.id), FROM Pratica p WHERE p.idProcedimento = :
public class Pratica implements java.io.Serializable {


	public enum Stato{
		DEMO,BOZZA,INVIATA, RIFIUTATA,EVASA
	}

	public static final String QR_ALL_BY_UTENTE = "Pratica.QR_ALL_BY_UTENTE";
	public static final String QR_RUOLI_PRATICA = "Pratica.QR_RUOLI_PRATICA";
//	public static final String QR_ALL_BY_UTENTE_IN_DIC = "Pratica.QR_ALL_BY_UTENTE_IN_DIC";
	public static final String ALL_BY_UTENTE_P_UTENTE = "principal";
	public static final String P_ID_PRATICA = "idPratica";
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column( name = "STATO")
	private Stato stato=Stato.BOZZA;


	@ManyToOne()
	@JoinColumn(name="ID_PROCEDIMENTO")
	private MetaProcedimento procedimento;

	@Column(name="ID_PROCEDIMENTO",insertable = false,updatable = false)
	private Long idProcedimento;

	@OneToMany
	@JoinTable(name="MC_V_PRA_ALL",
			joinColumns={ @JoinColumn(name="PRATICA", referencedColumnName="ID") },
			inverseJoinColumns={ @JoinColumn(name="ALLEG", referencedColumnName="ID") })
	private Set<Value> allegati;

	@XmlAttribute
	public Long getIdProcedimento() {
		return idProcedimento;
	}

	@OneToMany(fetch = FetchType.EAGER,cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REMOVE,CascadeType.DETACH}
	,mappedBy = "pratica",orphanRemoval = true)
	private Set<RuoloPratica> ruoli=new HashSet<>();

	public void addRuolo(RuoloPratica.RuoloCompilazionePratica ruolo, String utente) {
        ruoli.add(new RuoloPratica(this,utente,ruolo.bytewise()));
    }

	/**
	 *
	 * @param ruolo
	 * @param utente se null lo rimuove per tutti gli utenti
	 */
    public void rimuoviRuolo(RuoloPratica.RuoloCompilazionePratica ruolo, String utente) {
		for (Iterator<RuoloPratica> it= ruoli.iterator(); it.hasNext(); ) {
			RuoloPratica r=it.next();
			if (StringUtils.isEmpty(utente) ||  (r.getUser().equals(utente))) {
				r.setRoleBitwise(r.getRoleBitwise() ^ ruolo.bytewise());
//				it.remove();
			}
		}
	}


	public Set<String> findUtenti(RuoloPratica.RuoloCompilazionePratica ruolo) {
		Set<String> out=new HashSet<>();
    	for (Iterator<RuoloPratica> it= ruoli.iterator(); it.hasNext(); ) {
			RuoloPratica r=it.next();
			if ((r.getRoleBitwise() & ruolo.bytewise()) != 0) {
				out.add(r.getUser());
			}
		}
		return out;
	}

	@Embedded
	private MetaDateInfo dateInfo=new MetaDateInfo();

    @XmlElement
    public MetaDateInfo getDateInfo() {
		return dateInfo;
	}

	@PrePersist
	public void preP(){
		dateInfo.setDataCreazione(new Date());
	}

	@PreUpdate
	public void preU(){
		dateInfo.setDataModifica(new Date());
	}


	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Value> getAllegati() {
		return allegati;
	}

	public void setAllegati(Set<Value> allegati) {
		this.allegati = allegati;
	}

	public MetaProcedimento getProcedimento() {
		return procedimento;
	}

	public void setProcedimento(MetaProcedimento procedimento) {
		this.procedimento = procedimento;
	}

	@XmlAttribute
	public Stato getStato() {
		return stato;
	}

	public void setStato(Stato stato) {
		this.stato = stato;
	}

	@XmlElement
	public Set<RuoloPratica> getRuoli() {
		return Collections.unmodifiableSet(ruoli);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Pratica{");
		sb.append("id=").append(id);
		sb.append(", dateInfo=").append(dateInfo);
		sb.append('}');
		return sb.toString();
	}
}
