package net.mysoftworks.modulecompiler.model.meta;


import net.mysoftworks.modulecompiler.model.values.RawValue;
import net.mysoftworks.modulecompiler.model.visitor.MetaFieldVisitor;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name="MC_M_FILETYPES")
@NamedQueries({
		@NamedQuery(name = MetaFileTypes.QR_LIST_ALL,
				query = "SELECT mft FROM MetaFileTypes mft"),
		@NamedQuery(name = MetaFileTypes.FIND_BY_MIME,
				query = "SELECT mft FROM MetaFileTypes mft where mft.mimeType LIKE :" + MetaFileTypes.P_MIME),
		@NamedQuery(name = MetaFileTypes.FIND_BY_IDS,
				query = "SELECT mft FROM MetaFileTypes mft where mft.id in (:" + MetaFileTypes.P_IDS + ")")
})

public class MetaFileTypes implements java.io.Serializable {
	public static final String QR_LIST_ALL = "MetaFileTypes.LIST_ALL";
	public static final String FIND_BY_MIME = "MetaFileTypes.FIND_BY_MIME";
	public static final String FIND_BY_IDS = "MetaFileTypes.FIND_BY_IDS";
	public static final String P_MIME = "mime";
	public static final String P_IDS = "ids";

	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	@Column(name = "MIME_TYPES")
	private String mimeType;
	public MetaFileTypes(){}

	@XmlAttribute
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@XmlTransient
	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
}
