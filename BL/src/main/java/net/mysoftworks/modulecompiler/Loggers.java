package net.mysoftworks.modulecompiler;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Loggers {
    public static final Logger LOG_ADMIN = Logger.getLogger("net.mysoftworks.modulecompiler.web.admin");
    public static final Logger LOG_FE = Logger.getLogger("net.mysoftworks.modulecompiler.web.fe");
    public static final Logger LOG_WEBSOCKET = Logger.getLogger("net.mysoftworks.modulecompiler.web.websocket");
    public static final Logger LOG_BL = Logger.getLogger("net.mysoftworks.modulecompiler.bl");
    public static final Logger LOG_TEMPLATING = Logger.getLogger("net.mysoftworks.modulecompiler.template");


    public static void logOperatore(Logger log, Level level, String s,Principal pr, Object ... params) {
        List l;
        if (!ArrayUtils.isEmpty(params)) {
            l = new ArrayList<>(Arrays.asList(params));
        } else l=new ArrayList();
        String _patched = "Principal:{"+l.size()+"}-"+s;
        if (pr!=null) {
            l.add(pr.getName());
        } else {
            l.add("ANONIMO");
        }
        log.log(level,_patched,l.toArray());
    }

}
