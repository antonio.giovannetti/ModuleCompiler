package net.mysoftworks.modulecompiler.dto;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class BLException extends Exception implements Serializable {
    private String key;
    private List<String> params;

    public BLException(String key, List<String> params) {
        this.key = key;
        this.params = params;
    }

    public BLException(String key, String ... params) {
        this.key = key;
        this.params = Arrays.asList(params);
    }

    public String getKey() {
        return key;
    }

    public List<String> getParams() {
        return params;
    }

}
