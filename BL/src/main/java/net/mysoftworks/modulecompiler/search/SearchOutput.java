package net.mysoftworks.modulecompiler.search;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class SearchOutput implements Serializable {
    private long draw;
    private long recordsTotal;
    private long recordsFiltered;
    private Collection data;

    public SearchOutput(long draw, long recordsTotal, long recordsFiltered, Collection data) {
        this.draw = draw;
        this.recordsTotal = recordsTotal;
        this.recordsFiltered = recordsFiltered;
        this.data = data;
    }

    public long getDraw() {
        return draw;
    }

    public long getRecordsFiltered() {
        return recordsFiltered;
    }

    public long getRecordsTotal() {
        return recordsTotal;
    }

    public Collection getData() {
        return data;
    }

    public void setDraw(long draw) {
        this.draw = draw;
    }

    public void setRecordsTotal(long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public void setRecordsFiltered(long recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public void setData(Collection data) {
        this.data = data;
    }
}
