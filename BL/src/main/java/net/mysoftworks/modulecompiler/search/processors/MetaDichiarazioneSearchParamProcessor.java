package net.mysoftworks.modulecompiler.search.processors;

import net.mysoftworks.modulecompiler.model.meta.MetaDichiarazione;
import net.mysoftworks.modulecompiler.search.MetaDichiarazioneSearchParam;
import net.mysoftworks.modulecompiler.search.SearchParamProcessor;
import org.apache.commons.lang3.StringUtils;

public class MetaDichiarazioneSearchParamProcessor extends SearchParamProcessor<MetaDichiarazioneSearchParam> {

    public MetaDichiarazioneSearchParamProcessor() {
        super("md");
    }

    public MetaDichiarazioneSearchParamProcessor(String alias) {
        super(alias);
    }

    @Override
    protected Class getEntityType() {
        return MetaDichiarazione.class;
    }

    @Override
    protected void generateWhere(MetaDichiarazioneSearchParam metaDichiarazioneSearchParam) {
        StringBuilder out = new StringBuilder();
        if (StringUtils.isNotEmpty(metaDichiarazioneSearchParam.getDescrizioneLike())) {
            addWhere(alias+".descrizione LIKE ?",'%'+metaDichiarazioneSearchParam.getDescrizioneLike()+'%',CONJUNCTION.AND);
        }
//        metaDichiarazioneSearchParam.getMeta

    }

    @Override
    protected String getJpaNode(String prop) {
        switch (prop){
            case MetaDichiarazioneSearchParam.PROP_ID:return alias+".id";
            case MetaDichiarazioneSearchParam.PROP_DATA_MODIFICA:return alias+".dateInfo.dataModifica";
            case MetaDichiarazioneSearchParam.PROP_DESCRIZIONE:return alias+".descrizione";
        }
        throw new RuntimeException("Proprieta' " + prop + " non valida per "+getClass().getSimpleName());
    }
}
