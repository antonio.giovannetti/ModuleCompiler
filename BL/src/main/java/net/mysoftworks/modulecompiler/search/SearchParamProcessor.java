package net.mysoftworks.modulecompiler.search;

import net.mysoftworks.modulecompiler.bl.manager.AbstractEJB;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class SearchParamProcessor<P extends SearchParam> extends AbstractEJB {
    private static final Logger logger = Logger.getLogger(SearchParamProcessor.class.getName());
    private static final Pattern PATTERN_POSITONAL_PARAM = Pattern.compile("\\?");

    protected final String alias;
    private StringBuilder rawWhere = new StringBuilder();
    private List<Object> params=new ArrayList<>();

    private static final String JPA_RESERVED_SELECT  = "SELECT ";
    private static final String JPA_RESERVED_FROM = "FROM ";
    private static final String JPA_RESERVED_WHERE = "WHERE";
    private static final String JPA_RESERVED_COUNT = "COUNT(";
    private static final String JPA_RESERVED_AS = " AS ";
    private static final String JPA_RESERVED_AND  = " AND";
    private static final String JPA_RESERVED_OR  = " OR";
    private static final String JPA_RESERVED_ORDERBY = " ORDER BY ";
    private static final String JPA_RESERVED_COMMA = ", ";
    public enum CONJUNCTION{AND(JPA_RESERVED_AND),OR(JPA_RESERVED_OR);
        private final String conj;
        CONJUNCTION(String conj) {
            this.conj=conj;
        }
    }

//    @PersistenceContext(unitName = "mc-datasource")
//    protected EntityManager em;

    public SearchParamProcessor(String alias){
        this.alias = alias;
    }

    private StringBuilder generateFrom(P p) {
        return new StringBuilder(JPA_RESERVED_FROM).append(getEntityType().getSimpleName()).append(StringUtils.SPACE).append(alias);

    }

    private StringBuilder generateSelect(P p,boolean forceCount,boolean asMap) {
        StringBuilder out = new StringBuilder();
        if (forceCount || p.isJustCount())  {
            out.append(JPA_RESERVED_COUNT).append(alias).append(')');
        } else {
            List<String> l = p.getColumns();
            for(String prop:l) {
                if (out.length()!=0) out.append(JPA_RESERVED_COMMA);
                out.append(getJpaNode(prop));
                out.append(JPA_RESERVED_AS).append(prop);
            }
            if (asMap) {
                out.insert(0,"new map(");
                out.append(")");
            }
        }
        out.insert(0,JPA_RESERVED_SELECT);
        return out;
    }


    /**
     * Converte i punti interrogativi dei parametri in parametri posizionali. Es aaa?bbb?ccc?ddd --> aaa?1bbb?2ccc?3ddd
     * @param whereCondition
     * @return
     */
    private StringBuffer convertToOrdinalParam(String whereCondition) {
        Matcher m = PATTERN_POSITONAL_PARAM.matcher(whereCondition);
        StringBuffer sb = new StringBuffer();
        int i=1;
        while (m.find()) {
            m.appendReplacement(sb, "?" + i++);
        }
        m.appendTail(sb);
        return sb;
    }

    protected void addWhere(String whereSnippet,CONJUNCTION conjunction) {
        if (rawWhere.length()!=0) {
            rawWhere.append(conjunction.conj);
        }
        rawWhere.append(StringUtils.SPACE).append(whereSnippet);
    }


    protected void addWhere(String whereSnippet, Object param,CONJUNCTION conjunction) {
        addWhere(whereSnippet,conjunction);
        params.add(param);
    }

    protected void addWhere(String whereSnippet, List<Object> params,CONJUNCTION conjunction) {
        addWhere(whereSnippet,conjunction);
        this.params.addAll(params);
    }

    private StringBuilder popolaOrderBy(P p) {

        if (MapUtils.isEmpty(p.getOrders())) {
            return null;
        }
        StringBuilder orderBy = new StringBuilder();
        Iterator<Map.Entry<String,SearchParam.SORT_DIRECTION>> it = p.getOrders().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, SearchParam.SORT_DIRECTION> e = it.next();
            if (orderBy.length()==0) {
                orderBy.append(JPA_RESERVED_ORDERBY);
            } else orderBy.append(JPA_RESERVED_COMMA);
            orderBy.append(getJpaNode(e.getKey())).append(StringUtils.SPACE).append(e.getValue());
        }

        return orderBy;
    }


    private StringBuilder generateSQL(P p,boolean forceCount) {
        StringBuilder qr = new StringBuilder();
        qr.append(generateSelect(p,forceCount,true)).append(StringUtils.SPACE);
        qr.append(generateFrom(p)).append(StringUtils.SPACE);
        if (rawWhere.length()!=0) {
            qr.append(JPA_RESERVED_WHERE).append(convertToOrdinalParam(rawWhere.toString()));
        }
        if (!p.isJustCount() && !forceCount) {
            StringBuilder ob = popolaOrderBy(p);
            if (ob!=null) qr.append(ob);
        }
        return qr;
    }

    private Query bindParams(StringBuilder qr,EntityManager em) {
        if (logger.isLoggable(Level.FINEST)) {
            logOperatore(logger, Level.FINEST,"Esecuzione query:{0} con parametri....",qr);
            int i=0;
            for(Object param:params) {
                logOperatore(logger, Level.FINEST,"Binding param {0,number,#}===>{1}",i++,param);
            }
        }
        Query query = em.createQuery(qr.toString());
        int i=1;
        for(Object param:params) {
            query.setParameter(i++,param);
        }
        return query;
    }

    private Integer applyPaging(P p, Query query) {
        if (!p.isJustCount()) {
                int offset = p.getOffset();
                query.setFirstResult(offset);
                query.setMaxResults(p.getPageSize());
                return offset;
        }
        return null;
    }

    public SearchOutput process(P p, EntityManager em) {
        rawWhere.setLength(0);
        params.clear();
        StringBuilder qrTotal = generateSQL(p,true);
        Query queryTotal = bindParams(qrTotal,em);
        long totalCount = (long) queryTotal.getResultList().get(0);

        generateWhere(p);
        StringBuilder qr = generateSQL(p,false);
        logOperatore(logger, Level.FINE,"Esecuzione query:{0}",qr);
        long totalFiltered=-1;
        Query query = bindParams(qr,em);
        Integer offset = null;

        if (!p.isJustCount()) {
            if (rawWhere.length()!=0) {
                StringBuilder qrCount = generateSQL(p,true);
                Query queryCount = bindParams(qrCount, em);
                totalFiltered = (long) queryCount.getResultList().get(0);
            } else totalFiltered = totalCount;
            offset = applyPaging(p,query);
        }

        List res = query.getResultList();
        logOperatore(logger, Level.FINE,"Risultato: draw:{0,number,#},totalFiltered:{1,number,#},totalCount:{2,number,#},res:{3,number,#}",
                p.getDraw(),totalFiltered,totalCount,res.size());
        return new SearchOutput(p.getDraw(),totalFiltered,totalCount,res);

    }
    protected abstract Class getEntityType();
    protected abstract void generateWhere(P p);
    protected abstract String getJpaNode(String prop);


}
