package net.mysoftworks.modulecompiler.search;

import net.mysoftworks.modulecompiler.model.meta.MetaDichiarazione;

public class MetaDichiarazioneSearchParam extends SearchParam<MetaDichiarazione> {
    public static final String PROP_ID="ID";
    public static final String PROP_DATA_MODIFICA = "DATA_MODIFICA";
    public static final String PROP_DESCRIZIONE = "DESCRIZIONE";

    private String descrizioneLike;
    private Long notIdProc;

    public MetaDichiarazioneSearchParam(int nrPagina, byte pageSize) {
        super(nrPagina, pageSize);
        addColumn(MetaDichiarazioneSearchParam.PROP_ID);
        addColumn(MetaDichiarazioneSearchParam.PROP_DATA_MODIFICA);
        addColumn(MetaDichiarazioneSearchParam.PROP_DESCRIZIONE);
    }

    public String getDescrizioneLike() {
        return descrizioneLike;
    }

    public MetaDichiarazioneSearchParam setDescrizioneLike(String descrizioneLike) {
        this.descrizioneLike = descrizioneLike;
        return this;
    }

    public Long getNotIdProc() {
        return notIdProc;
    }

    public MetaDichiarazioneSearchParam setNotIdProc(Long notIdProc) {
        this.notIdProc = notIdProc;
        return this;
    }

}
