package net.mysoftworks.modulecompiler.search;

import java.util.*;

public abstract class SearchParam<T> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static enum SORT_DIRECTION{ASC,DESC};

    private LinkedHashMap<String,SORT_DIRECTION> orders;

    private int offset=1;
    private byte pageSize=10;
    private int draw;

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public byte getPageSize() {
        return pageSize;
    }

    public void setPageSize(byte pageSize) {
        this.pageSize = pageSize;
    }

    public SearchParam(int offset, byte pageSize) {
        this.offset = offset;
        this.pageSize = pageSize;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    private boolean justCount=false;

    /**
     * Defaults to {@link SORT_DIRECTION#ASC}
     * @param prop
     */
    public void addOrder(String prop) {
        addOrder(prop, SORT_DIRECTION.ASC);
    }

    public void addOrder(String prop, SORT_DIRECTION dir) {
        if (orders==null) {
            orders = new LinkedHashMap<String, SORT_DIRECTION>(); // keep insertion-order
        }
        if (dir==null) dir = SORT_DIRECTION.ASC;
        orders.put(prop, dir);
    }

    public LinkedHashMap<String, SORT_DIRECTION> getOrders() {
        return orders;
    }

    public void resetOrders() {
        if (this.orders != null) this.orders.clear();
    }

    public SearchParam<T> setJustCount(boolean justCount) {
        this.justCount = justCount;
        return this;
    }

    public boolean isJustCount() {
        return justCount;
    }

    private List<String> columns = new ArrayList<>();

    protected void addColumn(String prop){
        columns.add(prop);
    }

    public List<String> getColumns() {
        return Collections.unmodifiableList(columns);
    }
}
