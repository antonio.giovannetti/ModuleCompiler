package net.mysoftworks.modulecompiler.test.jee.jdbcproviders;


import net.mysoftworks.modulecompiler.test.jee.EJBSessionContextMock;

import javax.ejb.embeddable.EJBContainer;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractJDBCConfigProvider {

    protected static Logger logger = Logger.getLogger(AbstractJDBCConfigProvider.class.getName());
    protected String[] dataSet;

    protected String jdbcConnectionString;
    protected String schema;

    protected boolean inMemory = false;
    protected boolean create = false;

    protected Map<String, Object> properties = new HashMap<String, Object>();

    protected abstract DataSource getDataSource();
    public abstract Connection getConnection() throws Exception;
    protected abstract void setSchema(boolean create) throws Exception;
//    protected abstract String createSchemaString() throws Exception;

    public void setDataSet(String[] dataSet) {
        this.dataSet = dataSet;
    }

    protected AbstractJDBCConfigProvider(String jdbcConnectionString, boolean inMemory, boolean create,String schema) {
        super();
        logger.log(Level.INFO, "jdbcConnectionString:{0}", jdbcConnectionString);
        this.jdbcConnectionString = jdbcConnectionString;
        this.schema = schema;
        this.create = create;
//        if (inMemory) {
//            this.jdbcConnectionString += System.currentTimeMillis();
//        }
//        setDefaultProps();
    }

    protected abstract void  ddl() throws Exception;
    public abstract void  dml() throws Exception;

    protected void addJDBCParams(String jdbcParms) {
        if (!"".equals(jdbcParms)) {
            jdbcConnectionString += jdbcParms;
        }
    }


    private void setDefaultProps() {
// 		res.put("hibernate.transaction.factory_class","com.atomikos.icatch.jta.hibernate4.AtomikosPlatform");

//		res.put("hibernate.transaction.manager_lookup_class","com.atomikos.icatch.jta.hibernate3.TransactionManagerLookup"); 
//		res.put("javax.persistence.jdbc.driver",getDriverClass());
//		res.put("javax.persistence.jdbc.url",getConnectionUrl());
        properties.put("hibernate.current_session_context_class", "thread");
//		res.put("hibernate.transaction.factory_class","org.hibernate.transaction.JDBCTransactionFactory");
        properties.put("hibernate.show_sql", "false");
        properties.put("hibernate.format_sql", "true");
        properties.put("hibernate.use_sql_comments", "false");
        properties.put("hibernate.cache.use_second_level_cache", "false");
        properties.put("hibernate.temp.use_jdbc_metadata_defaults", "true");
        properties.put("hibernate.connection.url", jdbcConnectionString);

    }


    public void init() {
        logger.log(Level.FINE, "Provider:{0}\tCreazione DB:{1}\n\tJDBC connection:{2}", new Object[]{getClass().getSimpleName(), create, getConnectionUrl()});
        try {
            setSchema(create);
//            if (inMemory || create) {
//                Class.forName(getDriverClass());
//                Connection conn = DriverManager.getConnection(jdbcConnectionString);
//
//                String creationString = createSchemaString();
//                if (creationString != null) {
//                    logger.log(Level.FINE, "Creo lo schema con:" + creationString);
//                    conn.prepareStatement(creationString).execute();
//                }
//            }
            setDefaultProps();
            ddl();
            publishJNDIDatasource("java:/jdbc/MCDS");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    protected String getConnectionUrl() {
        return jdbcConnectionString;
    }

    protected abstract String getDriverClass();

    //	public Connection getJDBCConnection() {
//		return jdbcConn;
//	}

    public void publishJNDIDatasource(String jndi) throws Exception {
        EJBContainer container = null;
        publishJNDIDatasource(jndi, container);
    }

    public void publishJNDIDatasource(String jndi, SimpleIc ctx) throws Exception {
        DataSource ds = getDataSource();
        ctx.bind(jndi, ds);
    }

    public void publishJNDIDatasource(String jndi, EJBContainer container) throws Exception {
        if (container == null) {
            DataSource ds = getDataSource();
            if (EJBSessionContextMock.nc==null) {
                SimpleIc ctx = SimpleContextFactoryBuilder.createAndActivateCtx();
                ctx.bind(jndi, ds);
            } else  {
                EJBSessionContextMock.nc.bind(jndi, ds);
            }
        } else {
            System.out.println("EJBContainer:" + container);
            container.getContext().bind("java:comp/env/" + jndi, getDataSource());
        }
//		hibernate.connection.datasource
        properties.put("hibernate.connection.datasource", jndi);
    }

    public Map<String, Object> getConnectionProperties() {

        //provideJtaProps(res);
        // Hibernate JNDI datasource
// 		res.put("hibernate.transaction.factory_class","com.atomikos.icatch.jta.hibernate4.AtomikosPlatform");
//		res.put("hibernate.transaction.manager_lookup_class","com.atomikos.icatch.jta.hibernate3.TransactionManagerLookup"); 
//		res.put("javax.persistence.jdbc.driver",getDriverClass());
//		res.put("javax.persistence.jdbc.url",getConnectionUrl());
        properties.put("hibernate.current_session_context_class", "thread");
//		res.put("hibernate.transaction.factory_class","org.hibernate.transaction.JDBCTransactionFactory");
        properties.put("hibernate.show_sql", "false");
        properties.put("hibernate.format_sql", "true");
        properties.put("hibernate.use_sql_comments", "false");
        properties.put("hibernate.cache.use_second_level_cache", "false");
        properties.put("hibernate.temp.use_jdbc_metadata_defaults", "true");

        return properties;
    }

}
