package net.mysoftworks.modulecompiler.test.jee;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


public class JarExpl implements FilenameFilter {

    private FileFilter FOLDER_FILTER = new FileFilter() {
        public boolean accept(File pathname) {
            return pathname.isDirectory();
        }
    };
    private String searchEntry;

    public JarExpl(String searchEntry) {
        this.searchEntry = searchEntry.replaceAll("\\.", "/");
    }

    public boolean accept(File dir, String name) {
        return (name.endsWith("jar") || name.endsWith("war")) && !new File(dir,name).isDirectory();
    }

    public void scanFolder(File[] folders,FilenameFilter filter) {
        if (folders==null) return;
        for (File folder : folders) {
            if (folder.isDirectory()) {
                if (filter == null) filter = this;
                File[] files = folder.listFiles(filter);
                for (File file : files) {
                    try {
                        applyLogic(file,false);
                    } catch (Exception e) {
                        System.err.println("Error in file " + file.getAbsolutePath());
                        e.printStackTrace();
                    }
                }
                files = folder.listFiles(FOLDER_FILTER);
                scanFolder(files, filter);
            } else {
                try {
                    applyLogic(folder,true);
                } catch (Exception e) {
                    System.err.println("Error in file " + folder.getAbsolutePath());
                    e.printStackTrace();
                }            }
        }
    }

    public static void main(String[] args) throws Exception {
        String searchEntry = "org.jboss.logging.Logger";
//        String searchEntry = "org.jboss.naming.remote.client.InitialContextFactory";
        File[] _target = new File[] {
//              new File("/home/antonio/bin/jboss-as-7.1.1.Final/"),
                new File("/home/antonio/.m2/repository/org/jboss"),
                //    			new File("/home/antonio/wk_eng_branch/suap/GET-SUAP-WAS855/GET-SUAP-EAR/target/GET-SUAP/GET-SUAP-Admin-WebLayer.war"),
//      		new File("/home/antonio/wk_eng_branch/suap/GET-SUAP-WAS855/GET-SUAP-EAR/target/GET-SUAP-EAR/GET-SUAP-WebLayer.war"),
//              new File("/home/antonio/workspace-lr/sm/services/trunk/target"),
//              new File("/home/antonio/workspace-lr/sm/smweb/trunk/target")
        };

//    	System.out.println("Finding " + searchEntry + " in ");
//    	for (File file : _target) {
//        	System.out.println("\t" + file.getAbsolutePath());
//		}
        new JarExpl(searchEntry).scanFolder(_target,null);
    }

    public void applyLogic(File file,boolean print) throws Exception {
        if (!file.exists()) return;
        if (print) System.out.println("\n\t=========="+file.getName());
        ZipInputStream zis = new ZipInputStream(new FileInputStream(file));
        ZipEntry ne = zis.getNextEntry();
        List<String> entries = new ArrayList<>();
        while (ne != null) {
            String entryName = ne.getName();
            if (print) { entries.add(ne.getName()); }
//            if (entryName.endsWith(searchEntry)) {
//            System.out.println(entryName);
            if (entryName.startsWith(searchEntry)) {
//            if (entryName.endsWith(searchEntry)) {
//            if (entryName.matches(searchEntry)) {
                System.out.println("File " + file + " matches!!!!!!!!");
                break;
            }
            ne = zis.getNextEntry();
        }
        if (print) {
            java.util.Collections.sort(entries);
            System.out.println(StringUtils.join(entries,'\n'));
        }
        zis.close();

    }


}
