package net.mysoftworks.modulecompiler.test;

import net.mysoftworks.modulecompiler.bl.utils.JaxbSerializer;
import net.mysoftworks.modulecompiler.dto.BLException;
import net.mysoftworks.modulecompiler.model.RuoloPratica;
import net.mysoftworks.modulecompiler.model.meta.MetaField;
import net.mysoftworks.modulecompiler.model.meta.MetaTextField;
import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.model.values.Pratica;
import net.mysoftworks.modulecompiler.model.values.Value;
import net.mysoftworks.modulecompiler.test.jee.BusinessLayerJPAAbstractTest;
import net.mysoftworks.modulecompiler.test.jee.EJBSessionContextMock;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.DatatypeConverter;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.util.*;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class TestPratica extends BusinessLayerJPAAbstractTest {


    @Override
    protected String[] getDataSet() {
//        return null;
        return new String[]{"testData/data.xml","testData/proc.xml","testData/pratica.xml"};
    }

    @Test
    public void mock() {

    }
    @Test
    public void creaPratica() {
        Long idProc=-1L;
        Pratica p= gestorePratica.createPratica(idProc);
        List<Dichiarazione> dics = gestorePratica.findDichiarazioni(p.getId());
        int expDics = 2;
        Assert.assertEquals("Attese " + expDics + " dichiarazioni",expDics,dics.size());


    }
    @Test
    public void getPratica() {
        Long idPratica=-1L;
//        Pratica p = gestorePratica.findPratica(idPratica);
        List<Dichiarazione> dics = gestorePratica.findDichiarazioni(idPratica);
        Assert.assertEquals("Attesa 1 dichiarazione",1,dics.size());
    }


    @Test
    public void addDic() throws BLException {
        Dichiarazione d = null;
        Map<Long,Value> values = new HashMap<>();
        Dichiarazione dicData = gestoreDichiarazione.create(-1L,-1L,values);
        List<Dichiarazione> ld = gestorePratica.findDichiarazioni(-1L);
        Assert.assertEquals("Attese 2 dichiarazioni",2,ld.size());

    }


    @Test
    public void fillRawVal() {
        MetaField mf = em.find(MetaField.class, -1L);
        String newstringval = "AG1";
        List<Object> values = Arrays.asList(newstringval);
        try {
            Value v = gestoreValue.toValue(mf, values, -1L);

            em.persist(v);
            Assert.assertEquals(newstringval,v.getRawValues().iterator().next().getStringValue());
//            System.out.println("v:" + v.getRawValues().iterator().next().getStringValue());
//            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void parseDate() {
//        String s1="2010-01-01T12:00:00Z";

        System.out.println(StringUtils.rightPad("abcdef",7,"0"));

        if (true) return;
        String s1="2020-03-01T02:03:00";
        String s2="12:00:00Z";
        String s3="2010-01-01";
        String s4="2018-04-15T03:10";
        String s5="10:09:58";

        s4=StringUtils.rightPad(s4,22,":00.000");
        Calendar out1 = DatatypeConverter.parseDateTime(s1);
        System.out.println(s1+"===>"+out1);
        Calendar out2 = DatatypeConverter.parseDateTime(s2);
        System.out.println(s2+"===>"+out2);
        Calendar out3 = DatatypeConverter.parseDateTime(s3);
        System.out.println(s3+"===>"+out3);
        Calendar out4 = DatatypeConverter.parseDateTime(s4);
        System.out.println(s4+"===>"+out4);
//        2018-05-09T03:09:48
        String s="2018-05-09T04:00:48";
        String _s=StringUtils.rightPad(s,19,":00");
        _s=StringUtils.rightPad(s,23,".000");
        System.out.println(s+"===>"+_s);
        Calendar c = DatatypeConverter.parseDateTime(_s);
        System.out.println(c);

        System.out.println("============="+DatatypeConverter.parseDateTime(s5).getTime());
    }





    @Test
    public void listDicAssociabili() {
        Long idProc=60L;
        System.out.println(gestoreDichiarazione.dicAssociabili(idProc,null));
    }

    @Test
    public void serializePraticaHtml()  {
        Long idPrat=-1L;
        Node doc = gestorePratica.domPraticaHtml(idPrat,null);
//        Value obj = new Value();
//        obj.setId(111111L);
//        doc = AbstractEJB.serializeEntity(obj);
        String s = JaxbSerializer.documentToString(doc, null);

        System.out.println(s);
        Assert.assertEquals("String non corrispondente:" + s.length(),2651,s.length());
        File parent = new File("target/test-classes");
        try {
            File pFile = File.createTempFile("pratica_" + idPrat, ".zip", parent);
            ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(pFile));
            ZipEntry ze = new ZipEntry("pratica_" + idPrat+".xml");
            zos.putNextEntry(ze);
            zos.write(s.getBytes());
            zos.closeEntry();
            zos.flush();
            zos.finish();
            zos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void serializePratica()  {
        Long idPrat=-1L;
        Node doc = gestorePratica.domPratica(idPrat);
//        Value obj = new Value();
//        obj.setId(111111L);
//        doc = AbstractEJB.serializeEntity(obj);

        System.out.println(JaxbSerializer.documentToString(doc,null));
    }

    @Test
    public void rolesPratica() {
        Long idPratica=-1L;
        Pratica p= gestorePratica.findPratica(idPratica);
        Assert.assertEquals("Atteso un ruolo",1,p.getRuoli().size());

    }

    @Test
    public void checkImpactDic() {
        List<Object[]> out = gestoreDichiarazione.checkSave(-1L,-1L);
        for(Object[] item:out) {
            System.out.println("item: " + StringUtils.join(item,','));

        }
        Assert.assertEquals(1,out.size());

    }
        @Test
    public void checkImpactProc() {
        Long idProc=2L;
        List<Object[]> out = gestoreProcedimento.checkSaveProcedimento(idProc);
        for(Object[] item:out) {
            Pratica.Stato s = (Pratica.Stato) item[0];
            Long count = (Long) item[1];
            System.out.println("=================stato:"+s + " ===>" + count);

        }
    }

    @Test
    public void lista() {
        String principal = "95c516f8-5855-4451-8cd1-088494713c5d";
        mockPrincipal(principal);
//        List<Object[]> lm = em.createQuery("SELECT r.roleBitwise,MOD(r.roleBitwise *128 / 2, 128) FROM RuoloPratica r ")
//                .getResultList();
//        for(Object[] o:lm) {
//            System.out.println("==========="+StringUtils.join(o,','));
//        }
//        List<Pratica> l0 =  em.createQuery("SELECT DISTINCT p FROM Pratica p JOIN p.ruoli r WHERE r.id.user = :" +
//                Pratica.ALL_BY_UTENTE_P_UTENTE+" AND MOD(r.roleBitwise / 2, 1)<>0 ORDER BY p.dateInfo.dataModifica DESC", Pratica.class)
//                .setParameter(Pratica.ALL_BY_UTENTE_P_UTENTE,principal)
//                .getResultList();
//        System.out.println("======================size:" + l0.size());
        List<Pratica> l = gestorePratica.searchPratiche();
        System.out.println("size:" + l.size());
//        Assert.assertEquals("Attesa 1 pratiche per principal " + principal,4,l.size());
        for (Pratica prat:l) {
            System.out.println("=======p:"+prat.getId()+"\tuser1:"+ prat.getId());
//            System.out.println("=======hasRuolo:"+prat.hasRuolo(RuoloPratica.RuoloCompilazionePratica.ROLE_COMPILER));
        }
	Assert.assertEquals("Errore numero pratiche",2,l.size());
    }

    @Test
    public void cancellaPratica() {
        Long idPratica=-1L;
        gestorePratica.deletePratica(idPratica);
        Pratica p = gestorePratica.findPratica(idPratica);
        Assert.assertNull(p);
    }


    @Test
    public void testPresentIf() throws Exception{
        Long idPratica=-1L;
        String xpath = "/Pratica/DichiarazioneContainer/Dichiarazione[@idMetaDichiarazione='-1']/values/entry[key/text()='-5']/value/rawValues/bigDValue > 2";

//        idPratica  = 106L;
//        xpath = "/Pratica/DichiarazioneContainer/Dichiarazione[@idMetaDichiarazione='16']/values/entry[key/text()='124']/value/rawValues/bigDValue > 2";


        Document dom = gestorePratica.domPratica(idPratica);
        System.out.println(JaxbSerializer.documentToString(dom,null));

        XPathExpression xpr = XPathFactory.newInstance().newXPath().compile(xpath);
        System.out.println("evaluate:"+xpr.evaluate(dom));

    }




}
