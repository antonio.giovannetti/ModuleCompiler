package net.mysoftworks.modulecompiler.test;

import net.mysoftworks.modulecompiler.bl.utils.MCPropDescriptor;
import net.mysoftworks.modulecompiler.model.AllowedValue;
import net.mysoftworks.modulecompiler.model.meta.MetaField;
import net.mysoftworks.modulecompiler.model.meta.MetaListField;
import net.mysoftworks.modulecompiler.model.meta.MetaTemporalField;
import net.mysoftworks.modulecompiler.model.meta.MetaTextField;
import net.mysoftworks.modulecompiler.model.business.Indirizzo;
import net.mysoftworks.modulecompiler.test.jee.BusinessLayerJPAAbstractTest;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.junit.Assert;
import org.junit.Test;

import javax.persistence.TemporalType;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class TestCampo extends BusinessLayerJPAAbstractTest {

    @Override
    protected String[] getDataSet() {
        return new String[]{"testData/data.xml","testData/proc.xml","testData/pratica.xml"};
    }

    @Test
    public void toCalendar() {
        MetaTemporalField tf1 = new MetaTemporalField();
        tf1.setTemporalType(TemporalType.TIMESTAMP);
        MetaTemporalField tf2 = new MetaTemporalField();
        tf2.setTemporalType(TemporalType.TIME);
        MetaTemporalField tf3 = new MetaTemporalField();
        tf3.setTemporalType(TemporalType.DATE);
        String[] s = new String[]{"2020-03-01T02:03:00","2010-01-01","10:09:58"};

        for(int i = 0;i< s.length;i++) {
            if (i==0) gestoreValue.toCalendar(tf1,s[i]);
            if (i!=2) gestoreValue.toCalendar(tf2,s[i]);
            if (i!=2) gestoreValue.toCalendar(tf3,s[i]);

        }

    }

    //    @Test
    public void doInsert() {
        MetaTextField mf = new MetaTextField();
        mf.setTextType(MetaTextField.TEXT_TYPE.INPUT);
        mf.setEtichetta("Testetichetta");
        mf.setLarghezza((byte) 1);
        mf.setNome("Testnome");
        gestoreCampo.saveOrUpdate(mf, 1L);

//        em.getTransaction().commit();
    }

    @Test
    public void doUpdateList() {
        MetaListField mlf = new MetaListField();
        mlf.setNome("_lista");
        mlf.setEtichetta("La lista");
        mlf.addValue(new AllowedValue("cod_1","Val1"));
        mlf.addValue(new AllowedValue("cod_2","Val2"));
        MetaListField out = (MetaListField) gestoreCampo.saveOrUpdate(mlf, -1L);
        System.out.println("===================out:" + out);
        Assert.assertEquals("Attesi 2 allowedvalue",2,out.getAllowedValues().size());

    }


        @Test
    public void doUpdate() {
        MetaTextField mf = em.find(MetaTextField.class, -1L);
        em.detach(mf);
//        mf.setTextType(MetaTextField.TEXT_TYPE.INPUT);
//        mf.setEtichetta("Testetichetta");
//        mf.setLarghezza((byte)1);
        String newName="Cognome";
        mf.setNome(newName);
//        mf.setObbligatorio(false);
        gestoreCampo.saveOrUpdate(mf, -1L);
        MetaTextField mf1 = em.find(MetaTextField.class, -1L);
        Assert.assertEquals(mf1.getNome(),newName);
//        em.getTransaction().commit();
    }

    @Test
    public void testEntityMetadata() {
        Map<String, MCPropDescriptor> prop = MCPropDescriptor.factory(Indirizzo.class.getName());
        for(Map.Entry<String,MCPropDescriptor> e:prop.entrySet()) {
            System.out.println(e.getKey()+"====>"+e.getValue());
        }
    }


    @Test
    public void doPopulate() throws InvocationTargetException, IllegalAccessException {
        Indirizzo i = new Indirizzo();
        Map<String,String> map=new HashMap<>();
        map.put("via","via a");
        map.put("civico","566");
        map.put("comune.id","1");
        map.put("comune.cap","00100");
        map.put("comune.descrizione","roma");
        map.put("id","6");
        BeanUtilsBean.getInstance().populate(i,map);
//        BeanUtils.populate(i,map);
        System.out.println(i);
    }
}