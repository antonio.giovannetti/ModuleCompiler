package net.mysoftworks.modulecompiler.test.jee.jdbcproviders;

import java.util.Hashtable;

import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;
import javax.naming.spi.InitialContextFactoryBuilder;
import javax.naming.spi.NamingManager;

public class SimpleContextFactoryBuilder implements InitialContextFactoryBuilder{
	final static SimpleIc ctx = new SimpleIc();
	public InitialContextFactory createInitialContextFactory(Hashtable<?, ?> environment)
			throws NamingException {
		return ctx;
	}
	
	public static SimpleIc getInitialContext() throws NamingException {
		return ctx;
	}
	
	public static SimpleIc createAndActivateCtx() throws NamingException {
		NamingManager.setInitialContextFactoryBuilder(new SimpleContextFactoryBuilder());
		return ctx;
	}
}
