package net.mysoftworks.modulecompiler.test;

import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.bl.manager.GestoreDichiarazione;
import net.mysoftworks.modulecompiler.bl.utils.JaxbSerializer;
import net.mysoftworks.modulecompiler.model.ValueLob;
import net.mysoftworks.modulecompiler.model.enumeration.StatiProcedimento;
import net.mysoftworks.modulecompiler.model.meta.*;
import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.search.MetaDichiarazioneSearchParam;
import net.mysoftworks.modulecompiler.search.SearchParam;
import net.mysoftworks.modulecompiler.search.SearchOutput;
import net.mysoftworks.modulecompiler.test.jee.BusinessLayerJPAAbstractTest;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;

import javax.persistence.TypedQuery;
import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.util.*;
import java.util.List;
import java.util.logging.Level;

public class TestAdmin extends BusinessLayerJPAAbstractTest {

    @Override
    protected String[] getDataSet() {
//        return null;
        return new String[]{"testData/data.xml","testData/proc.xml","testData/pratica.xml"};
    }

    @Test
    public void findMetaDich() {
        Long idMeta = -1L;
        MetaDichiarazione md = em.find(MetaDichiarazione.class,idMeta);
        Assert.assertNotNull("Non ho trovato la MetaDichiarazione:" + idMeta,md);
    }

    @Test
    public void listDicAssociabili() {
        Long idProc=60L;
        System.out.println(gestoreDichiarazione.dicAssociabili(idProc,null));
    }

    @Test
    public void serializeProcedimento()  {
        Long idProc=-1L;
        Document dp = gestoreProcedimento.domProcedimento(idProc);
        String out = JaxbSerializer.documentToString(dp,null);
        System.out.println(out);
//        Assert.assertEquals("Serializzazione procedimento "+idProc+" errata",2086,out.length());
//        if (2508!=out.length()) {
//            System.out.println(out);
//            Assert.fail("Serializzazione procedimento "+idProc+" errata");
//        }
    }

    @Test
    public void clonaDich()  {
        Long idDic=-1L;
//        MetaDichiarazione md = em.find(MetaDichiarazione.class,4L);
//        em.detach(md);
//        em.flush();
        MetaDichiarazione dp = gestoreDichiarazione.clone(idDic,null);
        MetaDichiarazione orig = gestoreDichiarazione.getMeta(idDic);
        testEqDic(dp,orig,true);
//        System.out.println(JaxbSerializer.documentToString(dp));
//        em.getTransaction().commit();
    }

    private void testEqField(MetaField mf1, MetaField mf2,boolean deep) {
        if (deep) {
            Assert.assertNotEquals(mf1.getId(),mf2.getId());
        } else {
            Assert.assertEquals(mf1.getId(),mf2.getId());
        }
        Assert.assertEquals(mf1.getMaxOccurrences(),mf2.getMaxOccurrences());

    }

    private void testEqDic(MetaDichiarazione md1,MetaDichiarazione md2, boolean deep) {
        if (deep) {
            Assert.assertNotEquals(md1.getId(),md2.getId());
        } else {
            Assert.assertEquals(md1.getId(),md2.getId());
        }
        Assert.assertEquals(md1.getCustomController(),md2.getCustomController());
        Assert.assertEquals(md1.getMandatoryIf(),md2.getMandatoryIf());
        Assert.assertEquals(md1.getPresentIf(),md2.getPresentIf());
        Assert.assertEquals(md1.getIdGuida(),md2.getIdGuida());
        if (deep) {
            Assert.assertEquals(md1.getDescrizione(),md2.getDescrizione() + " (copia)");
        } else {
            Assert.assertEquals(md1.getDescrizione(),md2.getDescrizione());
        }

        Assert.assertEquals("Numero diverso campi",md1.getFields().size(),md2.getFields().size());

        Map<String,MetaField> fs = new HashMap<>();
        for(MetaField mf1:md1.getFields()) {
            fs.put(mf1.getNome(),mf1);
        }
        for(MetaField mf2: md2.getFields()) {
            testEqField(mf2,fs.get(mf2.getNome()),deep);
        }
    }

    @Test
    public void clonaProcDeep() {
        doClonaProc(true);
    }

    @Test
    public void clonaProc() {
        doClonaProc(false);
    }
    private void doClonaProc(boolean deep)  {
        Long idProc=-1L;
//        MetaDichiarazione md = em.find(MetaDichiarazione.class,4L);
//        em.detach(md);
//        em.flush();
        MetaProcedimento procClone = gestoreProcedimento.clone(idProc,deep);
        MetaProcedimento orig = gestoreProcedimento.get(idProc);
        Set<MetaFileField> allOrig = orig.getAllegati();
        if (allOrig.size()==0) throw new RuntimeException("Rimossi allegati originali");
        Map<String,MetaDichiarazione> dicsOrig = new HashMap<>();
        String suffix = deep ? GestoreDichiarazione.CLONE_SUFFIX : "";
        for(MetaDichiarazione md: orig.getDichiarazioni()) {
            dicsOrig.put(md.getDescrizione()+suffix,md);
        }
        Assert.assertEquals(procClone.getDescrizione(),orig.getDescrizione() + " (copia)");
        Assert.assertEquals(procClone.getIcon(),orig.getIcon());
        Assert.assertEquals(procClone.getStato(),StatiProcedimento.OFFLINE.bytewise());
        Assert.assertEquals("Numero diverso dichiarazioni",procClone.getDichiarazioni().size(),orig.getDichiarazioni().size());

        for(MetaDichiarazione md: procClone.getDichiarazioni()) {
            if (dicsOrig.get(md.getDescrizione())==null) {
                Assert.fail("La metadichiarazione " + md.getId() + " - "+md.getDescrizione()+" non e' stata clonata");
            }
            testEqDic(md,dicsOrig.get(md.getDescrizione()),deep);
        }
        Assert.assertNotEquals("Procedimento senza allegati",0,allOrig.size());
        Assert.assertEquals("Numero di allegati diverso",allOrig.size(),procClone.getAllegati().size());



//        System.out.println(JaxbSerializer.documentToString(dp));
//        em.getTransaction().commit();
    }


    @Test
    public void guida() {
        String hql = "SELECT mp.guidaCompilazione FROM MetaProcedimento mp  where mp.id =:" + MetaProcedimento.P_IDPROC;
//        MetaLob guida = gestoreProcedimento.getGuidaProc(2L);
        List<ValueLob> l = em.createQuery(hql, ValueLob.class).setParameter(MetaProcedimento.P_IDPROC, -1L).getResultList();
        Assert.assertEquals(l.size(),1);
    }

    @Test
    public void testUnlinkDich() {
        Long idProc = -1L;
        MetaProcedimento mp = em.find(MetaProcedimento.class,idProc);
        int origSize=mp.getDichiarazioni().size();
        gestoreDichiarazione.unlinkDichiarazione(idProc,-1L,false);

        Assert.assertEquals(origSize-1,mp.getDichiarazioni().size());
//        em.getTransaction().commit();

    }


   @Test
    public void testSearch() {
        MetaDichiarazioneSearchParam param=new MetaDichiarazioneSearchParam(0,(byte)1);
//        param.setDescrizioneLike("ind");
        param.setJustCount(true);
        param.addOrder(MetaDichiarazioneSearchParam.PROP_DATA_MODIFICA, SearchParam.SORT_DIRECTION.ASC);
        param.addOrder(MetaDichiarazioneSearchParam.PROP_ID, SearchParam.SORT_DIRECTION.ASC);
        SearchOutput out = metaDichiarazioneSearchParamProcessor.process(param,em);
//        List<Object[]> l = out.getData();
//        for (Object[] row:l) {
//            System.out.println(StringUtils.join(row,"=="));
//        }

        Collection<Object> l = out.getData();
        for (Object m:l) {
            if (m instanceof Map) {
                Set<Map.Entry> es = ((Map)m).entrySet();
                for (Map.Entry e:es) {
                    System.out.println("key:"+e.getKey()+",val:"+e.getValue());
                }
            } else {
                System.out.println("count:" + m);

            }
        }

//        HTTPservl
//        WebSearchParamFactory.create()


    }


    @Test
    public void listdich() {
        List<Dichiarazione> l = em.createQuery("SELECT d FROM MetaProcedimento mp join mp.dichiarazioni md, Dichiarazione d WHERE d.idMetaDichiarazione=md.id and mp.id = d.pratica.idProcedimento and d.pratica.id =:idPrat order by INDEX(md)")
                .setParameter("idPrat",71L)
                .getResultList();
        for(Dichiarazione d:l) {
            System.out.println(d.getId()+ "-" + d.getIdMetaDichiarazione());
        }

    }
    @Test
    public void moveDic() {
        Long idMetaProc = -1L;
        MetaProcedimento mp = em.find(MetaProcedimento.class,idMetaProc);
        Long idToMove = mp.getDichiarazioni().get(0).getId();
        gestoreDichiarazione.moveDic(idMetaProc,idToMove,GestoreDichiarazione.MOVE_DIR.DOWN);
        MetaProcedimento mp2 = em.find(MetaProcedimento.class,idMetaProc);
        Long idMoved = mp.getDichiarazioni().get(1).getId();

        Assert.assertEquals(idToMove,idMoved);

    }

    private void doRemoveProc(Long idMetaProc,boolean checkPratiche) {
        List<Long> l = null;
        TypedQuery<Long> qr = null;
        if (checkPratiche) {
            qr = em.createQuery("SELECT p.id from Pratica p where p.idProcedimento = :idMetaProc", Long.class)
                    .setParameter("idMetaProc", idMetaProc);
            l = qr.getResultList();
            Assert.assertNotEquals(0,l.size());
        }
        gestoreProcedimento.remove(idMetaProc,true);
        if (checkPratiche) {
            l = qr.getResultList();
            Assert.assertEquals(0,l.size());
        }
        MetaProcedimento mp = em.find(MetaProcedimento.class,idMetaProc);
        Assert.assertNull(mp);
    }

    @Test(expected = RuntimeException.class)
    public void removeProcFail() {
        Long idMetaProc = -1L;
        doRemoveProc(idMetaProc,true);
    }

    @Test
    public void removeProcSuccess() {
        Long idMetaProc = -2L;
        doRemoveProc(idMetaProc,false);
    }
    @Test
    public void exportProc() throws Exception {
        Long idProc =17L;
        byte[] out = gestoreLiquibase.exportProc(idProc);
        File f = File.createTempFile("export_liquibase", ".zip");
        IOUtils.write(out, new FileOutputStream(f));
        System.out.println("Exported to " + f.getCanonicalPath());
        if (Desktop.isDesktopSupported()) {
            Desktop.getDesktop().open(f);
        }
    }

    @Test
    public void exportDic() throws Exception {
        Long idDic =-1L;
        byte[] out = gestoreLiquibase.exportDic(idDic);
        File f = File.createTempFile("export_liquibase", ".zip");
        IOUtils.write(out, new FileOutputStream(f));
        System.out.println("Exported to " + f.getCanonicalPath());
    }

    @Test
    public void versionaProc() throws Exception {
        Long idProc = 17L;
        MetaProcedimento old = gestoreProcedimento.get(idProc);
        String oldDesc = old.getDescrizione();
        MetaProcedimento newProc = gestoreProcedimento.versiona(idProc);
        System.out.println("new id:" + newProc.getId());
        Assert.assertEquals("Descrizione non aggiornata", newProc.getDescrizione(), oldDesc+" (copia)");
        System.out.println("===============cancello " + newProc.getId());
        Assert.assertNotEquals("guide uguali",old.getIdGuida(),newProc.getIdGuida());
//        doRemoveProc(newProc.getId(),false);
        // TODO test clonate anche dichiarazioni
    }

}
