package net.mysoftworks.modulecompiler.test.jee.jdbcproviders;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Logger;

import javax.naming.CompositeName;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.Name;
import javax.naming.NameParser;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;

public class SimpleIc implements InitialContextFactory  {
	private static final Logger logger = Logger.getLogger(SimpleIc.class.getName());
		static class Np implements NameParser {

			public Name parse(String name) throws NamingException {
				logger.fine("Parse " + name);
				return new CompositeName(name);
			}
			
		}
	
	    private static Context context;

	    static {
	        try {
	        	final NameParser np = new Np();
	            context = new InitialContext(true) {
	                Map<String, Object> bindings = new HashMap<String, Object>();

	                
	                @Override
					public void close() throws NamingException {
	                	logger.fine("close Naming context");
					}

					@Override
					public NameParser getNameParser(String name) throws NamingException {
						logger.fine("getNameParser String " + name);
						return np;
					}

					@Override
					public NameParser getNameParser(Name name) throws NamingException {
						logger.fine("getNameParser Name " + name);
						return np;
					}

					@Override
	                public void bind(String name, Object obj)
	                        throws NamingException {
						logger.fine("Binding " + obj.getClass() + " to " + name);
	                    bindings.put(name, obj);
	                }

	                @Override
					public Object lookup(Name name) throws NamingException {
	                	logger.fine("Lookup NAME:" + name.toString());
						return bindings.get(name.toString());
					}

					@Override
	                public Object lookup(String name) throws NamingException {
						logger.fine("Lookup:" + name);
	                    return bindings.get(name);
	                }
	            };
	        } catch (NamingException e) { // can't happen.
	            throw new RuntimeException(e);
	        }
	    }

	    public Context getInitialContext(Hashtable<?, ?> environment)
	            throws NamingException {
	        return context;
	    }

	    public static Object lookup(String name) {
	        try {
	            return context.lookup(name);
	        } catch (NamingException e) { // can't happen.
	            throw new RuntimeException(e);
	        }
	    }

	    public static void bind(String name, Object obj) {
	        try {
	            context.bind(name, obj);
	        } catch (NamingException e) { // can't happen.
	            throw new RuntimeException(e);
	        }
	    }
}
