package net.mysoftworks.modulecompiler.test.jee;


import net.mysoftworks.modulecompiler.test.jee.jdbcproviders.SimpleContextFactoryBuilder;
import net.mysoftworks.modulecompiler.test.jee.jdbcproviders.SimpleIc;

import javax.ejb.*;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;
import javax.xml.rpc.handler.MessageContext;
import java.security.Identity;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class EJBSessionContextMock implements SessionContext{

    private Map<Class,Object> businessObjects = new HashMap<Class,Object>();
    private static Principal p;
    public static SimpleIc nc;
    static {
        p=new Principal() {
            @Override
            public String getName() {
                return "MockPrincipal_"+System.currentTimeMillis();
            }
        };
        try {
            nc = SimpleContextFactoryBuilder.createAndActivateCtx();
        } catch (NamingException e) {
            e.printStackTrace();
        }

    }

    static void mockPrincipal(String name) {
        p=new Principal() {
            @Override
            public String getName() {
                return name;
            }
        };
    }
    private static Identity i;

    public <T> void addBO(Class<T> c, T oggetto ){
        String sn = c.getSimpleName();
        String nm = c.getName();
        nc.bind("java:global/MC_EAR/BL/"+sn+"!"+nm,oggetto);
        businessObjects.put(c, oggetto);
    }

    @Override
    public EJBLocalObject getEJBLocalObject() throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EJBObject getEJBObject() throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MessageContext getMessageContext() throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> T getBusinessObject(Class<T> type) throws IllegalStateException {
        return (T) businessObjects.get(type);
    }

    @Override
    public Class getInvokedBusinessInterface() throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean wasCancelCalled() throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EJBHome getEJBHome() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EJBLocalHome getEJBLocalHome() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Properties getEnvironment() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Identity getCallerIdentity() {
//        return i;
//        p.getKeycloakSecurityContext().
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Principal getCallerPrincipal() {
        return p;
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, Object> getContextData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isCallerInRole(Identity idnt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isCallerInRole(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UserTransaction getUserTransaction() throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setRollbackOnly() throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean getRollbackOnly() throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TimerService getTimerService() throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object lookup(String string) throws IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
