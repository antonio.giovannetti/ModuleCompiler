package net.mysoftworks.modulecompiler.test.jee;

import net.mysoftworks.modulecompiler.bl.manager.KeycloakUtil;
import net.mysoftworks.modulecompiler.model.RuoloPratica;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class KeycloakGtwMock extends KeycloakUtil{
    private static Map<String,String> allRoles;
    static {
        allRoles = new HashMap<>();
        allRoles.put("1",RuoloPratica.RuoloCompilazionePratica.ROLE_VIEWER.getCodice());
        allRoles.put("2",RuoloPratica.RuoloCompilazionePratica.ROLE_COMPILER.getCodice());
        allRoles.put("3",RuoloPratica.RuoloCompilazionePratica.ROLE_EDITOR.getCodice());
        allRoles.put("4",RuoloPratica.RuoloCompilazionePratica.ROLE_OWNER.getCodice());
    }

    @Override
    public Set<String> roles() {
        return new HashSet<>(allRoles.values());
    }

    @Override
    public Map<String,String> businessRoles() {
        return allRoles;
    }
}
