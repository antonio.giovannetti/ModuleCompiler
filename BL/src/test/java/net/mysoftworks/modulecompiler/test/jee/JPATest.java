package net.mysoftworks.modulecompiler.test.jee;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import net.mysoftworks.modulecompiler.test.jee.jdbcproviders.AbstractJDBCConfigProvider;
import net.mysoftworks.modulecompiler.test.jee.jdbcproviders.H2ConfigProvider;
import net.mysoftworks.modulecompiler.test.jee.jdbcproviders.MysqlConfigProvider;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.h2.jdbcx.JdbcDataSource;
import org.hibernate.internal.SessionImpl;
import org.junit.runner.Result;
import org.junit.runner.notification.RunListener;

import javax.persistence.*;
import javax.sql.DataSource;
import javax.transaction.Transaction;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Savepoint;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public abstract class JPATest extends RunListener {

    static {
        System.setProperty("java.util.logging.config.file","src/test/resources/logging.properties");
    }

    protected EntityManager em;
    private static Database database;
    private static EntityManagerFactory emFactory;
    private static Properties props;

    private static final String CLASS_NAME = JPATest.class.getName();
    /**
     * Il logger
     */
    protected static final Logger logger = Logger.getLogger(CLASS_NAME);

    protected static AbstractJDBCConfigProvider prov;

    protected abstract String[] getDataSet();

    protected void beforeMethod() {
    }

    private static void loadProps() throws Exception{
        props = new Properties();
        props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("test.properties"));
        if (logger.isLoggable(Level.FINE)) {
            StringBuilder sb=new StringBuilder("Proprieta' caricate dal file test.properties:\n\t");
            for (Map.Entry e : props.entrySet()) {
                sb.append(e.getKey()).append("====>").append(e.getValue()).append("\n\t");
            }
            logger.fine(sb.toString());
        }

    }

    protected boolean useDb() {return true;}


    private static String getSchema() {
        return props.getProperty("bl.db.schema");
    }


//    private static void ddl() throws Exception{
//            Connection connection = prov.getConnection(); //your openConnection logic here
////        connection.prepareStatement("CREATE SCHEMA MC_DB;").execute();
//            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
//            String ddl = "liquibase_db.xml";
//            logger.log(Level.FINE,"Creo il db liquibase tramite lo script {0}",ddl);
//            Contexts cont = new Contexts();
//            database.setDefaultSchemaName(getSchema());
//            LabelExpression le = new LabelExpression();
//            new liquibase.Liquibase(ddl, new ClassLoaderResourceAccessor(), database)
//                    .update(cont, le);
//        database.commit();
//        database.close();
//
//    }

    private static void createEntityManagerFactory() {
        if (prov!=null) {
            String blPu = props.getProperty("bl.pu");
            StringBuilder sb=new StringBuilder("Proprieta' dell'entity manager, ");
            sb.append("blPu:").append(blPu).append("\n\t");
            for (Map.Entry<String,Object> e:prov.getConnectionProperties().entrySet()) {
                sb.append("Property::").append(e.getKey()).append("--->").append(e.getValue()).append("\n\t");
            }
            logger.finest(sb.toString());
            InputStream res = Thread.currentThread().getContextClassLoader().getResourceAsStream("META-INF/persistence.xml");
//            try {IOUtils.copy(res,System.out);} catch (Exception e){}
            emFactory = Persistence.createEntityManagerFactory(blPu, prov.getConnectionProperties());
        }
    }


    public static void dml(Connection connection,String[] dataset) throws Exception {
        if ("true".equalsIgnoreCase(props.getProperty("bl.db.ignore")) || database!=null) return;
        if (ArrayUtils.isEmpty(dataset)) return;
        if (connection==null) {
            connection = prov.getConnection(); //your openConnection logic here
        }
//        sp = connection.setSavepoint();
//        connection.prepareStatement("CREATE SCHEMA MC_DB;").execute();
        database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
        database.setDefaultSchemaName(getSchema());
//		String ddl = "testData/ddl.xml";
        Contexts cont = new Contexts();
        LabelExpression le = new LabelExpression();
        database.setAutoCommit(false);
        for(String ds:dataset) {
            logger.log(Level.FINE,"Popolo il db liquibase usando i dati in {0}",ds);
            Liquibase liquibase = new liquibase.Liquibase(ds, new ClassLoaderResourceAccessor(), database);
            liquibase.update(cont, le);
//            liquibase.futureRollbackSQL(new Contexts(),new LabelExpression(),new PrintWriter(System.out));
        }
//        database.rollback();
//        database.commit();


    }

    @org.junit.BeforeClass
    public final static void setUpClass() throws Exception {
        if (prov!=null ) return;
        if (props==null) loadProps();
        if ("true".equalsIgnoreCase(props.getProperty("bl.db.ignore"))) return;
        String url = null;
        if ("true".equalsIgnoreCase(props.getProperty("bl.db.memory"))) {
            url = "jdbc:h2:mem:db"+System.currentTimeMillis();
        } else {
            url = props.getProperty("bl.jdbc.url");
            if (url == null) {
                File f = File.createTempFile("h2_test_db", "");
                url = f.getAbsolutePath();
            }
        }
        logger.log(Level.FINE,"Url jdbc db h2:" + url);
        prov = new H2ConfigProvider(url,getSchema());
//        prov = new H2ConfigProvider("jdbc:h2:mem:db"+System.currentTimeMillis(),getSchema());
        prov.init();
//        prov.ddl();


//        dbConfigProvider.publishJNDIDatasource("java:/jdbc/MCDS");
        createEntityManagerFactory();
    }




    @org.junit.Before
    public final void setUpinstance() throws Exception {
        if (!"true".equalsIgnoreCase(props.getProperty("bl.db.ignore")))  {
            dml(null,getDataSet());
            em = emFactory.createEntityManager();
            em.getTransaction().begin();
        }
        beforeMethod();

//        em.joinTransaction();
    }

    @org.junit.After
    public final void tearDownInstance() throws Exception {
        if (!"true".equalsIgnoreCase(props.getProperty("bl.db.ignore")))  {
            logger.log(Level.FINE, "Rollback entity manager");
            em.getTransaction().rollback();
            em.close();
        }
    }

    @org.junit.AfterClass
    public final static void tearDownClass() throws Exception {
    }

    @Override
    public void testRunFinished(Result result) throws Exception {
        logger.log(Level.INFO,"Rollback liquibase:"+database);
        if (!"true".equalsIgnoreCase(props.getProperty("bl.db.ignore")) && database!=null) {
            database.rollback();
            database.close();
        }
    }

}
