package net.mysoftworks.modulecompiler.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import net.mysoftworks.modulecompiler.bl.manager.KeycloakUtil;
import net.mysoftworks.modulecompiler.bl.utils.JaxbSerializer;
import net.mysoftworks.modulecompiler.dto.BLException;
import net.mysoftworks.modulecompiler.model.RuoloPratica;
import net.mysoftworks.modulecompiler.model.meta.MetaField;
import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.model.values.Pratica;
import net.mysoftworks.modulecompiler.model.values.Value;
import net.mysoftworks.modulecompiler.test.jee.BusinessLayerJPAAbstractTest;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.RandomStringGenerator;
import org.junit.Assert;
import org.junit.Test;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.RealmRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.DatatypeConverter;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.util.*;

public class TestKeycloak extends BusinessLayerJPAAbstractTest {

    @Override
    protected String[] getDataSet() {
        return null;
    }

//    @Test
    public void addRole() {
        getUser();
        String userId = "458f25f0-06ef-4cf5-85d6-178e406bd6de";
        String clientId ="298b0b00-f829-4702-9522-dd0132c6626d";
        String roleId = "33646269-8a9c-4b49-ac8a-d8c3a9732a93";
        keycloakUtil.addRoleToUser(userId,Arrays.asList(new String[]{roleId}),null);
        getUser();

    }

    @Test
    public void getUser()  {
//        String userId = "458f25f0-06ef-4cf5-85d6-178e406bd6de";
        String userId = "6b1df24f-16cc-4131-b064-c437a91116fe";
        String clientId ="298b0b00-f829-4702-9522-dd0132c6626d";
        String roleId = "33646269-8a9c-4b49-ac8a-d8c3a9732a93";
//        Keycloak cl = keycloakUtil.getUser(userId);
//        RealmResource ur = cl.realm(KeycloakUtil.REALM);
        try {

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            objectMapper.writeValue(System.out, keycloakUtil.getUserInfo(userId));
        } catch (Exception e) {
            e.printStackTrace();
        }
//        cl.close();
//        System.out.println("Realm roles:" + StringUtils.join(ur.roles().realmLevel().listAll(),','));
//        List<RoleRepresentation> rroles = ur.roles().realmLevel().listEffective();
//        for(RoleRepresentation r:rroles) {
//            System.out.println("Realm role:" + r.getId() + " - " + r.getName());
//        }
//        List<RoleRepresentation> croles = ur.roles().clientLevel(clientId).listEffective();
//        for(RoleRepresentation r:croles) {
//            System.out.println("Client role:" + r.getId() + " - " + r.getName());
//        }


    }


//    @Test
    public void creaMockUsers() {
        String pwd = "pwd";

        RandomStringGenerator rg = new RandomStringGenerator.Builder().selectFrom("abcdefghijklmnopqrstuvwxyz1234567890".toCharArray())
                .build();

//        List<String> roles = new ArrayList<>( keycloakUtil.businessRoles() );
        int rs = keycloakUtil.businessRoles().size();
        for (int i=0;i<1;i++) {
            String username = rg.generate(10);
            keycloakUtil.createUser("usr" + username,pwd,
                    Arrays.asList(new String[]{"ruolomock"}),
//                    Arrays.asList(new String[]{"33646269-8a9c-4b49-ac8a-d8c3a9732a93"}),
                    Arrays.asList(new String[]{"editor"})
//                    null
            );
        }

    }


}
