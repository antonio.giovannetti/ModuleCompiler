package net.mysoftworks.modulecompiler.test.jee.jdbcproviders;


import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.h2.jdbcx.JdbcDataSource;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class H2ConfigProvider extends AbstractJDBCConfigProvider {

	private JdbcDataSource ds;
	private Connection conn;
//	public H2ConfigProvider() throws Exception {
////		super("jdbc:h2:mem:db",true,true);
//		this("jdbc:h2:/tmp/mc_db;AUTO_SERVER=TRUE;LOG=1;MODE=Mysql;DB_CLOSE_ON_EXIT=FALSE");
//	}
	public H2ConfigProvider(String urlJDBC,String schema) {
		super(urlJDBC,urlJDBC.startsWith("jdbc:h2:mem:"),true,schema);
	}

//	public H2ConfigProvider(String urlJDBC,String username,String password) {
//		super(urlJDBC,username,password, urlJDBC.startsWith("jdbc:h2:mem:"),true);
//	}


	//	public H2ConfigProvider(){
//		super();
//	}

	protected void ddl() throws Exception{
		Connection connection = getDataSource().getConnection(); //your openConnection logic here
//        connection.prepareStatement("CREATE SCHEMA MC_DB;").execute();
		Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
		String ddl = "liquibase_db.xml";
		logger.log(Level.FINE,"Creo il db liquibase tramite lo script {0}",ddl);
		Contexts cont = new Contexts();
//		database.setDefaultSchemaName(getDefaultSchema());
		LabelExpression le = new LabelExpression();
		new liquibase.Liquibase(ddl, new ClassLoaderResourceAccessor(), database)
				.update(cont, le);

	}


	public void dml() throws Exception {
		if (ArrayUtils.isEmpty(dataSet)) return;
		Connection connection = getDataSource().getConnection(); //your openConnection logic here
//        connection.prepareStatement("CREATE SCHEMA MC_DB;").execute();
		Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
//        database.setAutoCommit(true);
//		database.setDefaultSchemaName(getDefaultSchema());
//		String ddl = "testData/ddl.xml";
		Contexts cont = new Contexts();
		LabelExpression le = new LabelExpression();
		database.setAutoCommit(true);
		for(String ds:dataSet) {
			logger.log(Level.FINE,"Popolo il db liquibase usando i dati in {0}",ds);
			Liquibase liquibase = new liquibase.Liquibase(ds, new ClassLoaderResourceAccessor(), database);
			liquibase.update(cont, le);
//            liquibase.futureRollbackSQL(new Contexts(),new LabelExpression(),new PrintWriter(System.out));
		}
//        database.rollback();
		database.commit();
		database.close();

	}

	protected void setSchema(boolean create) throws Exception {
		Matcher m = Pattern.compile("(jdbc:h2:)(?!mem:)([^;]+)(.*)").matcher(jdbcConnectionString);
		getDataSource().getConnection().prepareCall("CREATE SCHEMA IF NOT EXISTS " + schema + ";").execute();
		jdbcConnectionString +=";SCHEMA=" + schema;
		properties.put("hibernate.default_schema", schema);
		ds = null;
	}

//    protected String createSchemaString() throws Exception {
//		Matcher m = Pattern.compile("(jdbc:h2:)(?!mem:)([^;]+)(.*)").matcher(jdbcConnectionString);
////		if (m.matches()) {
////			new File(m.group(2)+".mv.db").delete();
////		}
//		jdbcConnectionString +=";SCHEMA=" + getDefaultSchema();
//    	return "CREATE SCHEMA IF NOT EXISTS " + getDefaultSchema() + ";SET SCHEMA "+getDefaultSchema()+";";
//    }

	
	@Override
	protected String getDriverClass() {
		return "org.h2.Driver";
	}

	@Override
	public Map<String, Object> getConnectionProperties() {
		Map<String, Object> res = new HashMap<String, Object>();
		res.putAll(super.getConnectionProperties());
		res.put("hibernate.dialect" ,"org.hibernate.dialect.H2Dialect");
//		res.put("eclipselink.target-database","H2");

		return res;
	}

	protected DataSource getDataSource() {
		if (ds==null) {
			ds = new JdbcDataSource();
			ds.setURL(getConnectionUrl());
		}
		return ds;
	}

	@Override
	public Connection getConnection() throws Exception{
		return getDataSource().getConnection();
	}
}
