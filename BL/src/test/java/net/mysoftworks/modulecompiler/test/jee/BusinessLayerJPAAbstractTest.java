package net.mysoftworks.modulecompiler.test.jee;

import net.mysoftworks.modulecompiler.bl.manager.*;
import net.mysoftworks.modulecompiler.render.GestoreRender;
import net.mysoftworks.modulecompiler.search.processors.MetaDichiarazioneSearchParamProcessor;
import org.apache.commons.beanutils.BeanUtils;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.metamodel.Metamodel;
import java.lang.reflect.Field;
import java.security.Principal;
import java.util.*;

/**
 * Classe comune a tutti i test JPA del business layer... consente di avere
 * riferimento a tutti i "service"
 */
public abstract class BusinessLayerJPAAbstractTest extends JPATest {
    private static EJBSessionContextMock ctx;

    private static Set<Object> fakeEJBs = new HashSet<>();
    private static Set<Object> toInitializeEJBs = new HashSet<>();

    private static EMDelegate emDelegateBusiness = new EMDelegate();
    protected static GestoreCampo gestoreCampo;
    protected static GestoreDichiarazione gestoreDichiarazione;
    protected static GestoreProcedimento gestoreProcedimento;
    protected static GestorePratica gestorePratica;
    protected static GestoreValue gestoreValue;
    protected static GestoreRender gestoreRender;
    protected static GestoreLiquibase gestoreLiquibase;
    protected static FieldValidator fieldValidator;

    protected static MetaDichiarazioneSearchParamProcessor metaDichiarazioneSearchParamProcessor;
    protected static KeycloakUtil keycloakUtil;

    protected void resetPersistenceContext() {
        em.flush();
        em.clear();
    }

    @org.junit.BeforeClass
    public final static void businessSetUpClass() throws Exception {
        setUpClass();
        long start = System.currentTimeMillis();
        ctx = new EJBSessionContextMock();
        fakeEJBs.add(ctx);
        gestoreCampo = create(GestoreCampo.class,true);
        gestoreDichiarazione = create(GestoreDichiarazione.class,true);
        gestoreProcedimento = create(GestoreProcedimento.class,true);
        gestorePratica = create(GestorePratica.class,true);
        gestoreValue = create(GestoreValue.class,true);
        gestoreRender = create(GestoreRender.class,true);
        gestoreLiquibase = create(GestoreLiquibase.class,true);
        fieldValidator = create(FieldValidator.class,true);

        if (prov!=null) {
            setPrivateField(GestoreLiquibase.class, "connection", gestoreLiquibase,prov.getConnection());
        }


        metaDichiarazioneSearchParamProcessor = create(MetaDichiarazioneSearchParamProcessor.class,true);
        keycloakUtil = create(KeycloakGtwMock.class,true);
        //
        // Aggiungiamo l'entity manager tra i fake resources
        //
        fakeEJBs.add(emDelegateBusiness);
        // ...i client "strani"
        setPrivateField(GestoreCampo.class, "em", gestoreCampo,emDelegateBusiness);
        setPrivateField(GestoreDichiarazione.class, "em", gestoreDichiarazione,emDelegateBusiness);
        setPrivateField(GestoreProcedimento.class, "em", gestoreProcedimento,emDelegateBusiness);
        setPrivateField(GestorePratica.class, "em", gestorePratica,emDelegateBusiness);
        setPrivateField(FieldValidator.class, "em", fieldValidator,emDelegateBusiness);
        inializzaProprieta();
        System.out.println(BusinessLayerJPAAbstractTest.class.getName() + ".beforeMethod() ["
                + (System.currentTimeMillis() - start) + "] (ms)");

    }

    protected static void mockPrincipal(String name) {
        EJBSessionContextMock.mockPrincipal(name);
    }

    protected static Principal getPrincipal() {
        return ctx.getCallerPrincipal();
    }


    @Override
    public void beforeMethod() {

        emDelegateBusiness.setEM(em);
    }

    private static void inializzaProprieta() {
        // Scorriamo tutte i bean che devono essere inizalizzati
        for (Object o : toInitializeEJBs) {
            impostaProprieta(o, o.getClass());
        }
    }

    private static void impostaProprieta(Object o, Class classe) {
        Field[] declaredFields = classe.getDeclaredFields();
        for (Field field : declaredFields) {
            for (Object bean : fakeEJBs) {
                if (field.getType().isAssignableFrom(bean.getClass())) {
                    setPrivateField(classe, field.getName(), o, bean);
                    break;
                }
            }
        }
        Class superClasse = classe.getSuperclass();
        if (superClasse != null && !Object.class.equals(superClasse)) {
            impostaProprieta(o, superClasse);
        }
    }

    private static <T> T create(Class<T> beanClass, boolean mustBeInitialized) {
        try {
            T instance = beanClass.newInstance();
            fakeEJBs.add(instance);
            if (mustBeInitialized) {
                toInitializeEJBs.add(instance);
            }
            return instance;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private static void setPrivateField(Class clazz, String fieldName, Object oggetto, Object value) {
        try {
            Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(oggetto, value);
        } catch (Exception e) {
            try {
                BeanUtils.setProperty(oggetto, fieldName, value);
            } catch (Exception e1) {
                throw new RuntimeException(e1);
            }

        }
    }

}

class EMDelegate implements EntityManager{
    private EntityManager em;
    void setEM(EntityManager em){
        this.em = em;
    }

    public void persist(Object entity) {
        em.persist(entity);
    }

    public <T> T merge(T entity) {
        return em.merge(entity);
    }

    public void remove(Object entity) {
        em.remove(entity);
    }

    public <T> T find(Class<T> entityClass, Object primaryKey) {
        return em.find(entityClass, primaryKey);
    }

    public <T> T find(Class<T> entityClass, Object primaryKey, Map<String, Object> properties) {
        return em.find(entityClass, primaryKey, properties);
    }

    public <T> T find(Class<T> entityClass, Object primaryKey, LockModeType lockMode) {
        return em.find(entityClass, primaryKey, lockMode);
    }

    public <T> T find(Class<T> entityClass, Object primaryKey, LockModeType lockMode, Map<String, Object> properties) {
        return em.find(entityClass, primaryKey, lockMode, properties);
    }

    public <T> T getReference(Class<T> entityClass, Object primaryKey) {
        return em.getReference(entityClass, primaryKey);
    }

    public void flush() {
        em.flush();
    }

    public void setFlushMode(FlushModeType flushMode) {
        em.setFlushMode(flushMode);
    }

    public FlushModeType getFlushMode() {
        return em.getFlushMode();
    }

    public void lock(Object entity, LockModeType lockMode) {
        em.lock(entity, lockMode);
    }

    public void lock(Object entity, LockModeType lockMode, Map<String, Object> properties) {
        em.lock(entity, lockMode, properties);
    }

    public void refresh(Object entity) {
        em.refresh(entity);
    }

    public void refresh(Object entity, Map<String, Object> properties) {
        em.refresh(entity, properties);
    }

    public void refresh(Object entity, LockModeType lockMode) {
        em.refresh(entity, lockMode);
    }

    public void refresh(Object entity, LockModeType lockMode, Map<String, Object> properties) {
        em.refresh(entity, lockMode, properties);
    }

    public void clear() {
        em.clear();
    }

    public void detach(Object entity) {
        em.detach(entity);
    }

    public boolean contains(Object entity) {
        return em.contains(entity);
    }

    public LockModeType getLockMode(Object entity) {
        return em.getLockMode(entity);
    }

    public void setProperty(String propertyName, Object value) {
        em.setProperty(propertyName, value);
    }

    public Map<String, Object> getProperties() {
        return em.getProperties();
    }

    public Query createQuery(String qlString) {
        return em.createQuery(qlString);
    }

    public <T> TypedQuery<T> createQuery(CriteriaQuery<T> criteriaQuery) {
        return em.createQuery(criteriaQuery);
    }

    public <T> TypedQuery<T> createQuery(String qlString, Class<T> resultClass) {
        return em.createQuery(qlString, resultClass);
    }

    public Query createNamedQuery(String name) {
        return em.createNamedQuery(name);
    }

    public <T> TypedQuery<T> createNamedQuery(String name, Class<T> resultClass) {
        return em.createNamedQuery(name, resultClass);
    }

    public Query createNativeQuery(String sqlString) {
        return em.createNativeQuery(sqlString);
    }

    public Query createNativeQuery(String sqlString, Class resultClass) {
        return em.createNativeQuery(sqlString, resultClass);
    }

    public Query createNativeQuery(String sqlString, String resultSetMapping) {
        return em.createNativeQuery(sqlString, resultSetMapping);
    }

    public void joinTransaction() {
        em.joinTransaction();
    }

    public <T> T unwrap(Class<T> cls) {
        return em.unwrap(cls);
    }

    public Object getDelegate() {
        return em.getDelegate();
    }

    public void close() {
        em.close();
    }

    public boolean isOpen() {
        return em.isOpen();
    }

    public EntityTransaction getTransaction() {
        return em.getTransaction();
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return em.getEntityManagerFactory();
    }

    public CriteriaBuilder getCriteriaBuilder() {
        return em.getCriteriaBuilder();
    }

    public Metamodel getMetamodel() {
        return em.getMetamodel();
    }

    @Override
    public boolean isJoinedToTransaction() {
        return em.isJoinedToTransaction();
    }

    @Override
    public EntityGraph<?> getEntityGraph(String graphName) {
        return em.getEntityGraph(graphName);
    }

    @Override
    public <T> List<EntityGraph<? super T>> getEntityGraphs(Class<T> entityClass) {
        return em.getEntityGraphs(entityClass);
    }

    @Override
    public EntityGraph<?> createEntityGraph(String graphName) {
        return em.createEntityGraph(graphName);
    }

    @Override
    public <T> EntityGraph<T> createEntityGraph(Class<T> rootType) {
        return em.createEntityGraph(rootType);
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String procedureName) {
        return em.createStoredProcedureQuery(procedureName);
    }

    @Override
    public Query createQuery(CriteriaUpdate updateQuery) {
        return em.createQuery(updateQuery);
    }

    @Override
    public Query createQuery(CriteriaDelete deleteQuery) {
        return em.createQuery(deleteQuery);
    }

    @Override
    public StoredProcedureQuery createNamedStoredProcedureQuery(String name) {
        return em.createNamedStoredProcedureQuery(name);
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String procedureName, Class[] resultClasses) {
        return em.createStoredProcedureQuery(procedureName,resultClasses);
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String procedureName, String... resultSetMappings) {
        return em.createStoredProcedureQuery(procedureName,resultSetMappings);
    }
}
