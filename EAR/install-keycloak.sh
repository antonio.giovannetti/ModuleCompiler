#!/bin/sh
CWD=$(dirname $(readlink -f $0))
ADM_USER=admin
ADM_PWD=admin



# DEST_DIR="$CWD/target/"
DEST_DIR=$1

mkdir -p $DEST_DIR

# echo $DEST_DIR


# One command
# curl https://repo1.maven.org/maven2/org/keycloak/keycloak-server-dist/7.0.0/keycloak-server-dist-7.0.0.tar.gz | tar xzf - -C $DEST_DIR
tar xzf /home/ale/kc.tar.gz  -C $DEST_DIR

WF_HOME="$(find $DEST_DIR -mindepth 1 -maxdepth 1 ! -path . -type d -name 'keycl*'| head -n1)"

# One command
# curl https://downloads.jboss.org/keycloak/7.0.1/adapters/keycloak-oidc/keycloak-wildfly-adapter-dist-7.0.1.tar.gz | tar xzf - -C $WF_HOME
tar xzf /home/ale/kca.tar.gz  -C $WF_HOME

chmod a+x $WF_HOME/bin/*sh

$WF_HOME/bin/jboss-cli.sh --file=$WF_HOME/bin/adapter-install-offline.cli

sed -i "s/<resolve-parameter-values>false<\/resolve-parameter-values>/\
<resolve-parameter-values>true<\/resolve-parameter-values>/"  $WF_HOME/bin/jboss-cli.xml

$WF_HOME/bin/jboss-cli.sh --file=$CWD/install-mc.cli -Dstatic.resources.folder=${CWD}/../WebResources/src/main/webapp/static

cp ${CWD}/target/MC_EAR-demo.ear ${WF_HOME}/standalone/deployments/


# JRebel
echo "# Con JRebel bisogna aumentare anche XX:MaxMetaspaceSize a 1024m" >> $WF_HOME/bin/standalone.conf
echo "# JAVA_OPTS=\"\$JAVA_OPTS -agentpath:$HOME/bin/jrebel/lib/libjrebel64.so\"" >> $WF_HOME/bin/standalone.conf


# Installazione REALM
# $WF_HOME/bin/add-user.sh $ADM_USER $ADM_PWD
$WF_HOME/bin/add-user-keycloak.sh -u $ADM_USER -p $ADM_PWD

# Bug fix Wildfly: rimuovo JsonBindingProvider per forzare l'uso ResteasyJackson2Provider
rm -rf $WF_HOME/modules/system/layers/base/org/jboss/resteasy/resteasy-json-binding-provider

$WF_HOME/bin/standalone.sh -Dinclude.demo.data=true &

while true
do
    OUT=$($WF_HOME/bin/jboss-cli.sh -c --commands="read-attribute server-state")
    echo "out:$OUT"
    if [ $OUT = "running" ]
    then
        export PATH="$PATH:$JAVA_HOME/bin"

        $WF_HOME/bin/kcadm.sh config credentials --server http://localhost:8080/auth --realm master --user $ADM_USER --password $ADM_PWD

        $WF_HOME/bin/kcadm.sh create realms --server http://localhost:8080/auth -s realm=MC -s enabled=true
        $WF_HOME/bin/kcadm.sh create partialImport -r MC -s ifResourceExists=SKIP -o -f ../doc/realm-export.json

        $WF_HOME/bin/kcadm.sh create users -s username=a -s enabled=true -r MC
        $WF_HOME/bin/kcadm.sh set-password -r MC --username a --new-password a
        $WF_HOME/bin/kcadm.sh add-roles --cclientid mc-webapp --uusername a --rolename editor -r MC
        $WF_HOME/bin/kcadm.sh add-roles --cclientid mc-webapp --uusername a --rolename compiler -r MC

        $WF_HOME/bin/kcadm.sh create users -s username=b -s enabled=true -r MC
        $WF_HOME/bin/kcadm.sh set-password -r MC --username b --new-password b
        $WF_HOME/bin/kcadm.sh add-roles --cclientid mc-webapp --uusername b --rolename compiler -r MC
        $WF_HOME/bin/kcadm.sh add-roles --cclientid mc-webapp --uusername b --rolename owner -r MC

        $WF_HOME/bin/kcadm.sh create users -s username=c -s enabled=true -r MC
        $WF_HOME/bin/kcadm.sh set-password -r MC --username c --new-password c
        $WF_HOME/bin/kcadm.sh add-roles --cclientid mc-webapp --uusername c --rolename viewer -r MC
        exit 0
    fi
    sleep 1
done
# Fix script per inserire java nel path


