package net.mysoftworks.modulecompiler.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Config {

    private static final Map<ClassLoader, Properties> propMap = new HashMap<ClassLoader, Properties>();

    private static Properties getProperties() {
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        if (propMap.get(cl) != null) {
            return propMap.get(cl);
        }
        final Properties p = new Properties();
        ;
        try {
            p.load(cl.getResourceAsStream("app.properties"));
            propMap.put(cl, p);
        } catch (IOException e) {
        } finally {
        }

        return p;
    }

    public static String getProperty(String propName) {
        return getProperties().getProperty(propName);
    }

}
