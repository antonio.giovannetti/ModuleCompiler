# Module compiler

Progetto per l'editing dinamico di modulistica compilabile on-line.

E' composto di una parte "back-office" di configurazione dei moduli ed una parte "front-end" di compilazione.

## Installazione demo
`mvn clean package -Pdemo -DskipTests`

## Installazione
### Versione standalone con keycloak e risorse forniti esternamente

- Configurare il profilo
- Configurare il datasource ed esporlo sull'indirzzo jndi `java:/jdbc/MCDS2`


### Versione bundle (demo)
#### Prerequisiti
- Ambiente _Linux_
- JDK 1.8
- Apache Maven > 3

#### Comandi
Posizionarsi sulla cartella EAR
```
cd EAR
```

Fare il package del progetto
```
mvn clean package -DskipTests -Pdemo
```

Avviare Wildfly (bundle per Keycloak)
`EAR/target/keycloakx.x.x/bin/standalone.sh` 

Navigare l'applicazione su http://localhost:8080/mc



## Creazione realm
Dall'utenza amministrativa creare il reame importando il file `doc/realm-export.json`

## Popolamento del database con dati di esempio
