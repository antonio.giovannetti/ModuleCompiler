package net.mysoftworks.modulecompiler.report;

import net.mysoftworks.modulecompiler.report.impl.ReportType;
import net.mysoftworks.modulecompiler.report.impl.FlyingSaucerReportGenerator;
import net.mysoftworks.modulecompiler.report.impl.Utils;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.w3c.dom.Document;

import javax.xml.bind.JAXBContext;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.*;
import java.io.File;

public class TestReport {

    private void execute(File f) throws Exception {
        Runtime.getRuntime().exec(new String[]{
                "xdg-open",f.getAbsolutePath()
        });
    }
    @Test
    public void testProcedimento() throws Exception {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(
                Utils.loadClassPathResourceAsString("testProcedimento.xml")
        );
//        doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File("/tmp/prova.xml"));
        byte[] result = new FlyingSaucerReportGenerator().generaXSLReportDaXML(ReportType.PROCEDIMENTO, doc, null);

        final File tmpFile = File.createTempFile("proc_", ".pdf");
        FileUtils.writeByteArrayToFile(tmpFile,result);
        System.out.println("Generato file pdf " + tmpFile.getAbsolutePath());
        if (Desktop.isDesktopSupported()) {
            execute(tmpFile);
        }

    }

    @Test
    public void testPraticaHtml() throws Exception {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(
                Utils.loadClassPathResourceAsString("prat-1.xml")
        );
//        doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File("/tmp/prova.xml"));
        byte[] result = new FlyingSaucerReportGenerator().generaXSLReportDaXML(ReportType.PRATICA, doc, null);

        final File tmpFile = File.createTempFile("pratica_", ".pdf");
        FileUtils.writeByteArrayToFile(tmpFile,result);
        System.out.println("Generato file pdf " + tmpFile.getAbsolutePath());
        if (Desktop.isDesktopSupported()) {
            execute(tmpFile);
        }

    }



    @Test
    public void testPratica() throws Exception {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(
                Utils.loadClassPathResourceAsString("prat71.xml")
        );
//        doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File("/tmp/prova.xml"));
        byte[] result = new FlyingSaucerReportGenerator().generaXSLReportDaXML(ReportType.PRATICA, doc, null);

        final File tmpFile = File.createTempFile("pratica_", ".pdf");
        FileUtils.writeByteArrayToFile(tmpFile,result);
        System.out.println("Generato file pdf " + tmpFile.getAbsolutePath());
        if (Desktop.isDesktopSupported()) {
            execute(tmpFile);
        }

    }


}
