package net.mysoftworks.modulecompiler.report.impl;

import net.mysoftworks.modulecompiler.config.Config;

public enum ReportType {
//    PRATICA("pratica.xsl","/",Config.getProperty("resources.url") ),
    PRATICA("pratica_html.xsl","/",Config.getProperty("resources.systemId") ),
    PROCEDIMENTO("procedimento.xsl","/",Config.getProperty("resources.systemId") );
    private final String name;
    private final String dir;
    private final String htmlSystemId;

    ReportType(String name,String dir,String htmlSystemId) {
        this.name=name;this.dir=dir;this.htmlSystemId=htmlSystemId;
    }

    public String getName() {
        return name;
    }

    public String getDir() {
        return dir;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ReportType{");
        sb.append("name='").append(name).append('\'');
        sb.append(", dir='").append(dir).append('\'');
        sb.append(", htmlSystemId='").append(htmlSystemId).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getHtmlSystemId() {
        return htmlSystemId;
    }

}
