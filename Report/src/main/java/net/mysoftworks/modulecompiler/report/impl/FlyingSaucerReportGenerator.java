package net.mysoftworks.modulecompiler.report.impl;

import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FlyingSaucerReportGenerator {

    public static final String XSL_FOLDER="xsl"+ File.separatorChar;
    protected static Logger log = ReportManagerBean.log;

    private static String documentToString(Document doc) {
        try {
            StringWriter sw = new StringWriter();
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.transform(new DOMSource(doc), new StreamResult(sw));
            return sw.toString();
        } catch (Exception ex) {
            // Fails silently
            return ex.getMessage();
            // throw new RuntimeException("Error converting to String", ex);
        }
    }


    private ByteArrayOutputStream htmlToPDF(Document documentoHtml, String sysId) throws Exception {
        if (sysId==null) {
          sysId = Utils.loadClassPathResourceAsString(XSL_FOLDER);
        }

        log.log(Level.FINE,"Inizio generazione tramite Flyingsaucer (systemid: {0}) da: {1}",new Object[]{sysId,documentoHtml});
        Map params = new HashMap<>();

        ITextRenderer renderer = new ITextRenderer();

        renderer.getSharedContext().setBaseURL(sysId);
//        renderer.setDocument(documentoHtml,sysId);
        renderer.setDocumentFromString(documentToString(documentoHtml),sysId);
        renderer.layout();
        ByteArrayOutputStream pdf = new ByteArrayOutputStream();
        renderer.createPDF(pdf);
        log.log(Level.FINEST,"PDF Generato: Flyingsaucer (systemid: {0}) da: {1}",new Object[]{sysId,documentoHtml});
        return pdf;
    }

    private URL getReportUrl(ReportType reportType) throws Exception{
        return Utils.loadClassPathResource(XSL_FOLDER + reportType.getDir() + reportType.getName());
    }

    private Document dataToHTML(ReportType reportType,Document documentoDati) throws Exception {
        URL tpl = getReportUrl(reportType);
        TransformerFactory tFactory = TransformerFactory.newInstance();
        String systemId = tpl.toExternalForm();
        log.log(Level.FINE,"Ho risolto il foglio di trasformazione come\n\tnome:{0}\n\tsystemid:{1}", new Object[]{tpl,systemId});
        Transformer transformer = tFactory.newTransformer(new javax.xml.transform.stream.StreamSource(tpl.openStream(), systemId));
        log.log(Level.FINEST,"Completo creazione transformer \n\tTransformer:{0}\n\txslStream:{1}",
                new Object[]{transformer,tpl.toExternalForm()});
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DOMSource domSourceDati = new DOMSource(documentoDati);
        DOMResult res = new DOMResult();
        transformer.transform(domSourceDati, res);
        return (Document) res.getNode();
    }

    public byte[] generaXSLReportDaXML(ReportType reportType, Document documentoDati, ReportManagerBean.OUTPUT_FORMAT format) throws Exception {
        log.log(Level.FINE, "Genero report da trasformare con xsl per {0}, nel formato {1}", new Object[]{reportType,format});

        if (log.isLoggable(Level.FINEST)) {
            log.log(Level.FINEST,"XML dati:{0}",documentToString(documentoDati));
        }
        Document docHtml = dataToHTML(reportType, documentoDati);
        log.log(Level.FINE,"Trasformazione XSL avvenuta correttamente::{0}",new Object[]{docHtml});

        if (log.isLoggable(Level.FINEST)) {
            log.log(Level.FINEST,"XSL trasformato:{0}",documentToString(docHtml));
        }

        ByteArrayOutputStream baos = null;
        if (reportType.getHtmlSystemId()!=null) {
            baos = htmlToPDF(docHtml, Utils.asPathElement(reportType.getHtmlSystemId()));
        } else {
            baos = htmlToPDF(docHtml, getReportUrl(reportType).toExternalForm());
        }
        return baos.toByteArray();
    }


}
