package net.mysoftworks.modulecompiler.report.iface;


import net.mysoftworks.modulecompiler.report.impl.FlyingSaucerReportGenerator;
import net.mysoftworks.modulecompiler.report.impl.ReportType;
import org.w3c.dom.Document;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Local
public interface ReportManager {
    Report generateFromXsl(AvailableReports report, Document data);
}
