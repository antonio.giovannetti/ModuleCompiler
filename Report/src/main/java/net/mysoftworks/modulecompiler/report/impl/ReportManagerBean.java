package net.mysoftworks.modulecompiler.report.impl;


import net.mysoftworks.modulecompiler.report.iface.AvailableReports;
import net.mysoftworks.modulecompiler.report.iface.Report;
import net.mysoftworks.modulecompiler.report.iface.ReportManager;
import org.w3c.dom.Document;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.URLConnection;
import java.util.logging.Logger;

@Stateless
public class ReportManagerBean implements ReportManager {
    public static Logger log = Logger.getLogger(ReportManagerBean.class.getName());

    public enum OUTPUT_FORMAT{PDF,HTML,SVG}

    private ReportType fromAvailable(AvailableReports report) {
        if (AvailableReports.PRATICA.equals(report)) {
            return ReportType.PRATICA;
        }
        if (AvailableReports.PROCEDIMENTO.equals(report)) {
            return ReportType.PROCEDIMENTO;
        }
        throw new RuntimeException("Il report " + report + " non e' implementato");
    }

    public Report generateFromXsl(AvailableReports report, Document data) {
        try {
            byte[] content = new FlyingSaucerReportGenerator().generaXSLReportDaXML(fromAvailable(report), data, OUTPUT_FORMAT.PDF);
            String mimeType = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(content));
            return new Report(content,data.getLocalName(),mimeType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
