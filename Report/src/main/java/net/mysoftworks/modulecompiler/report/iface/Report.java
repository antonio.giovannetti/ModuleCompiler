package net.mysoftworks.modulecompiler.report.iface;

import java.io.Serializable;

public class Report implements Serializable{
    private byte[] content;
    private String name;
    private String mime;

    public Report(byte[] content, String name, String mime) {
        this.content = content;
        this.name = name;
        this.mime = mime;
    }

    public byte[] getContent() {
        return content;
    }

    public String getName() {
        return name;
    }

    public String getMime() {
        return mime;
    }
}
