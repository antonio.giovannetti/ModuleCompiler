package net.mysoftworks.modulecompiler.report.impl;

import org.w3c.dom.Document;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Utils {
    public static URL loadClassPathResource(String resName) throws IOException {
        Enumeration<URL> resources = Utils.class.getClassLoader().getResources(resName);
        if (resources.hasMoreElements()) {
            URL out = resources.nextElement();
            if (resources.hasMoreElements()) {
                ReportManagerBean.log.log(Level.WARNING,"Ci sono piu'' occorrenze per la risorsa {0}",resName);
            }
            return out;
        } else return null;
    }


    public static String loadClassPathResourceAsString(String resName) throws IOException {
        URL out = loadClassPathResource(resName);
        if (out==null) return null;
        return out.toExternalForm();
    }
    public static String asPathElement(String resource) throws IOException {
        if (resource.endsWith(File.separator)) return resource;
        return resource + File.separatorChar;
    }




    public static String documentToString(Document doc) {
        try {
            StringWriter sw = new StringWriter();
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            transformer.transform(new DOMSource(doc), new StreamResult(sw));
            return sw.toString();
        } catch (Exception ex) {
            // Fails silently
            return ex.getMessage();
            // throw new RuntimeException("Error converting to String", ex);
        }
    }

}
