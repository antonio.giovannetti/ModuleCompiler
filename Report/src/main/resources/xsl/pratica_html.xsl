<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                exclude-result-prefixes="xsl" >

    <xsl:template match="/Pratica" ><xsl:param name="icon" select="procedimento/icon" />

        <html xml:lang="it" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
            <head>
                <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css"></link>
                <link rel="stylesheet" href="css/mc-print.css"></link>
                <link rel="stylesheet" href="css/font-awesome.min.css"></link>
                <title>Pratica <xsl:value-of select="@id"/></title>
            </head>
            <body class="bg-success" >
                <h3 class="text-left">Procedimento <em><xsl:value-of select="procedimento/@descrizione" /></em></h3>
                <xsl:apply-templates select="dichiarazioneContainer/dichiarazione" />
            </body></html>
    </xsl:template>

    <xsl:template match="dichiarazione" ><xsl:param name="metaDich" select="/Pratica/procedimento/dichiarazioni[@id=current()/@idMeta]" />
        <div class="panel panel-default dichiarazione">
            <div class="panel-heading" ><h4><xsl:value-of select="$metaDich/@descrizione" /></h4></div>
            <div class="panel-body">
                <xsl:value-of select="text()" disable-output-escaping="yes" />
            </div>
        </div>
    </xsl:template>

    <!--<xsl:template match="dichiarazione/text()">-->
        <!--<xsl:text disable-output-escaping="yes" ><xsl:copy /></xsl:text>-->
    <!--</xsl:template>-->

</xsl:stylesheet>