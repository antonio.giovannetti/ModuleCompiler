<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                exclude-result-prefixes="xsl" >
<xsl:output omit-xml-declaration="yes" indent="yes" />
<xsl:template match="/MetaProcedimento" >

    <html xml:lang="it" xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css"></link>
        <link rel="stylesheet" href="css/mc-print.css"></link>
        <title>Titolo pdf(HTML title tag)</title>
    </head>
    <body>
    <h3><xsl:value-of select="@descrizione" /></h3><div class="pull-right">
        <span><b>Data creazione: </b><xsl:value-of select="dateInfo/@dataCreazione" /></span><br />
        <span><b>Data ultima modifica: </b><xsl:value-of select="dateInfo/@dataModifica" /></span></div><div class="clearfix"></div>
        <!--<span class="btn btn-lg btn-info">Test button</span>-->
        <hr /><xsl:apply-templates select="guidaCompilazione" />
        <hr /><xsl:apply-templates select="dichiarazioni" />
    </body></html>
</xsl:template>

    <xsl:template match="guidaCompilazione" >
        <h4>Guida alla compilazione</h4>
        <div class="alert alert-info"><xsl:value-of select="stringValue/text()" disable-output-escaping="yes" /></div>
    </xsl:template>


    <xsl:template match="dichiarazioni" >
        <div class="panel panel-default dichiarazione">
            <div class="panel-heading"><span class="text-danger"><xsl:value-of select="@descrizione" /></span><small class="pull-right">
                <span><span ><b>Data creazione: </b></span><xsl:value-of select="dateInfo/@dataCreazione" /></span><br />
                <span><span ><b>Data ultima modifica: </b></span><xsl:value-of select="dateInfo/@dataModifica" /></span></small><div class="clearfix"></div>
            </div>
            <div class="panel-body container">
                <xsl:apply-templates select="template" />
                <div class="row"><div class="text-center" style="border: 1px solid; border-radius:5px">CAMPI</div></div>
                <div class="striped">
                    <xsl:apply-templates select="fields" mode="row"/>
                </div>

                <!--<table><tbody><xsl:apply-templates select="fields" mode="table"/></tbody></table>-->
                <!--<ul><xsl:apply-templates select="fields" mode="ul"/></ul>-->
            </div>
        </div>
    </xsl:template>

    <xsl:template match="template" >
        <div class="alert alert-info">
            <h6>Template</h6>
                <xsl:apply-templates select="stringValue/text()"  />
        </div>
    </xsl:template>

    <xsl:template match="stringValue/text()" ><xsl:value-of select="." disable-output-escaping="yes"/>
        <!--<xsl:copy>-->
            <!--<xsl:value-of select="text()" disable-output-escaping="yes" />-->
        <!--</xsl:copy>-->
    </xsl:template>

    <xsl:template match="fields" mode="row">
        <div class="row">
            <div class="col-xs-1"><xsl:value-of select="@id" /></div>
            <div class="col-xs-1"><xsl:value-of select="@fieldType" /></div>
            <div class="col-xs-3"><xsl:value-of select="@nome" /></div>
            <div class="col-xs-3"><xsl:value-of select="@etichetta" /></div>


        </div>
    </xsl:template>


    <xsl:template match="fields" mode="ul">
        <li><xsl:value-of select="concat(@id,'-',@nome,'-',@etichetta)" /></li>
    </xsl:template>


    <xsl:template match="fields" mode="table">
        <!--<li><xsl:value-of select="concat(@id,'-',@nome,'-',@etichetta)" /></li>-->
        <tr >
            <td ><xsl:value-of select="@id" /></td>
            <td ><xsl:value-of select="@nome" /></td>
            <td ><xsl:value-of select="@etichetta" /></td>
        </tr>
    </xsl:template>


</xsl:stylesheet>