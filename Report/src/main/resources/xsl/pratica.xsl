<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                exclude-result-prefixes="xsl" >

    <xsl:template match="/Pratica" ><xsl:param name="icon" select="procedimento/icon" />

        <html xml:lang="it" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
            <head>
                <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css"></link>
                <link rel="stylesheet" href="css/mc-print.css"></link>
                <link rel="stylesheet" href="css/font-awesome.min.css"></link>
                <title>Pratica <xsl:value-of select="@id"/></title>
            </head>
            <body class="bg-success" >
                <h3 class="text-left">Procedimento <xsl:value-of select="procedimento/@descrizione" /></h3>
                <xsl:apply-templates select="DichiarazioneContainer/Dichiarazione" />
            </body></html>
    </xsl:template>

    <xsl:template match="Dichiarazione" ><xsl:param name="metaDich" select="/Pratica/procedimento/dichiarazioni[@id=current()/@idMetaDichiarazione]" />
        <div class="panel panel-default dichiarazione">
            <div class="panel-heading" ><strong><xsl:value-of select="$metaDich/@descrizione" /></strong></div>
            <div class="panel-body"><xsl:apply-templates select="values/entry"><xsl:with-param name="idDich" select="@idMetaDichiarazione" /></xsl:apply-templates></div>
        </div>
    </xsl:template>


    <xsl:template match="entry" >
        <xsl:param name="idDich" />
        <xsl:param name="metaField" select="/Pratica/procedimento/dichiarazioni[@id=$idDich]/fields[@id=current()/key]" />
        <div ><span class="label " ><xsl:value-of select="$metaField/@etichetta"/></span>&#160;
            <span class="values bg-primary" >
                <xsl:for-each select="value/rawValues"><xsl:if test="position()!=1">, &#160;</xsl:if>
                    <xsl:apply-templates />
                </xsl:for-each>

            </span>
        </div>
    </xsl:template>

    <xsl:template match="stringValue | dateValue" >
        <strong><xsl:value-of select="text()"/></strong>
    </xsl:template>

</xsl:stylesheet>
