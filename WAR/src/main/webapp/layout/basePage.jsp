<%@ page import="net.mysoftworks.modulecompiler.web.common.sd.SessionDataInterceptor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%
response.setHeader("Cache-Control","no-cache, no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevent caching at the proxy server
%>
<head>
<s:include value="../common/head.jsp" />
<title><tiles:insertAttribute name="titoloPagina" /></title>

</head>
<!-- fine header -->
<body class="bg-info <%if (request.isUserInRole("editor") &&
SessionDataInterceptor.isAdminPage()) {%> editor parallax<% } %>">

<s:include value="../common/header.jsp" />
<div class="container-fluid">
	<tiles:insertAttribute name="contenuti"/>
		<div id="footerhp">
			<tiles:insertAttribute name="footer"/>
		</div>
</div>
</body>
</html>
