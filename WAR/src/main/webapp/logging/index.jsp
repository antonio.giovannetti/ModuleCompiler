<%@page import="java.util.logging.Level"%>
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="java.net.InetAddress" %>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<title>Log Level Configuration</title><base>
<link href="log.css?rnd=<%=System.currentTimeMillis()%>" rel="stylesheet" type="text/css" />
</head>
<body >

    <%
    String s_platform = (String) request.getParameter("platform");
    boolean hasLog4j = false;
    try {Class.forName("org.apache.log4j.Logger");  hasLog4j = true;} catch(Exception e){}
    boolean hasSl4fj = false;
    try {Class.forName("org.slf4j.Logger");  hasSl4fj = true;} catch(Exception e){}
    boolean hasJbossLogging = false;
    try {Class.forName("org.jboss.logging.Logger");  hasJbossLogging = true;} catch(Exception e){}

    String s_targetLogger = (String) request.getParameter("logNameFilter");
    %>
    <div id="content">
        <h1>Log Level Configuration&nbsp;<select class="log_system">
                	<option <%if ("jdk".equals(s_platform) || s_platform==null) out.print("selected"); %> value="jdk">JDK</option>
	               <%if (hasLog4j) { %><option <%if ("log4j".equals(s_platform)) out.print("selected"); %> value="log4j">Log4j</option><% } %>
	               <%if (hasSl4fj) { %><option <%if ("slf4j".equals(s_platform)) out.print("selected"); %> value="slf4j">Slf4j</option><% } %>
	               <%if (hasJbossLogging) { %><option <%if ("JBoss".equals(s_platform)) out.print("selected"); %> value="JBoss">JBoss</option><% } %>
        </select><a href="?rnd=<%=System.currentTimeMillis()%>" >Reload</a><span class="item_selection"><%
            InetAddress lh=InetAddress.getLocalHost();
            out.print(lh.getCanonicalHostName()+"-->"+lh.getHostAddress());%></span></h1>

    <%if ("jdk".equals(s_platform) || s_platform==null) { %>
        <div class="jdklogger"><jsp:include page="loggingJDK.jsp"></jsp:include></div>
    <% } %>     
    <%if ("log4j".equals(s_platform) && hasLog4j) { %>
        <div class="log4jlogger"><jsp:include page="loggingLog4j.jsp"></jsp:include></div>
    <% } %>     
    <%if ("slf4j".equals(s_platform) && hasSl4fj) { %>
        <div class="slf4jlogger"><jsp:include page="loggingSlf4j.jsp"></jsp:include></div>
    <% } %>
        <%if ("JBoss".equals(s_platform) && hasJbossLogging) { %>
        <div class="jbosslogger"><jsp:include page="loggingJboss.jsp"></jsp:include></div>
        <% } %>

    </div>

</body>
<script type="text/javascript">
jQuery(document).ready(function(){
	var filterByName=jQuery(".filtering input[name='logNameFilter']");
    var filterByLevel=jQuery(".filtering select.log_level");
    var itemFound = jQuery(".filtering .item_found");

    var i = 0;
	var _selectAll = function(evt){
		var _isChecked = jQuery(this).prop('checked');
		jQuery("#loggers").find(".row:visible .cell input").each(function(idx,elem){
			jQuery(elem).prop('checked', _isChecked);
		})
	}	
	
	var _filter = function(evt,param){
		i = 0;
		var re1,re2 = undefined;
        if (filterByLevel.val()) {
            re1 = new RegExp("level"+(param || filterByLevel.val())+"$");
        }
        re2 = new RegExp(param || filterByName.val());

        jQuery("#loggers .row:not(.header)").each(function(idx,elem){
			elem = jQuery(elem);
            var text = elem.children(":nth-child(2)").text();
            _toggle = re1 ? re1.test(elem.children(":nth-child(3)").attr('class')) : true;
            _toggle = _toggle && (re2 ? re2.test(text) : true);

			if (_toggle) i++;
			elem.toggle(_toggle);
 		});
        itemFound.text(i);
		jQuery("#loggers .row .cell input").each(function(idx,elem){
			jQuery(elem).prop('checked', false);
		})
		
	}
<%if (s_targetLogger!=null && !"".equals(s_targetLogger) ) {out.print("_filter(null,'"+ s_targetLogger +"');");} %>
	jQuery("#content").find('select.log_system').change(function(){
		window.location = "./?platform=" + jQuery(this).val();
	}).end();

    filterByName.keyup(_filter);
    filterByLevel.change(_filter);
    itemFound.text(jQuery("#loggers .row").length);
	
	jQuery("#loggers").find(".select_all").click(_selectAll);
})


</script>

</html>