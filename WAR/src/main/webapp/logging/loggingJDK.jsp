<%@page import="javax.naming.NameClassPair"%>
<%@page import="javax.naming.NamingEnumeration"%>
<%@page import="javax.naming.NamingException"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.logging.LogManager"%>
<%@page import="java.util.logging.Logger"%><%
    Level[] logLevels = { Level.ALL,Level.CONFIG,Level.FINE,Level.FINER,Level.FINEST,Level.INFO,Level.OFF,Level.SEVERE,Level.WARNING};
    String targetOperation = (String) request.getParameter("operation");
    String s_targetLogger = (String) request.getParameter("logger");
    String s_targetLogLevel = (String) request.getParameter("newLogLevel");
    Level targetLogLevel = null;
    if (s_targetLogLevel!=null) {
        if (!"NULL".equals(s_targetLogLevel)) targetLogLevel = Level.parse(s_targetLogLevel);
    }


    if ("Create".equals(targetOperation)) {
        if (s_targetLogger!=null && !"".equals(s_targetLogger)) {
            Logger logger = LogManager.getLogManager().getLogger(s_targetLogger);
            if (logger!=null && targetLogLevel!=null) {
                logger.setLevel(targetLogLevel);
            } else {
                out.print("<h1>logger " + s_targetLogger + " is null</h1");
            }
        }
    }

%><div class="filtering">
    <div class="item_found cell"  ></div>
    <div class="item_selection cell">Filter Loggers by name (RegExp):<input name="logNameFilter" type="text"
                                                                       size="50" value="<%=(s_targetLogger == null ? "" : s_targetLogger)%>"
                                                                       class="filterText" /> and level:
        <select class="log_level" name="filterLevel"><option value="" >Any</option>
            <option  value="-">NULL</option>
            <% for (int cnt = 0; cnt < logLevels.length; cnt++) { %>
            <option  value="<%=logLevels[cnt].getName()%>"><%=logLevels[cnt].getName()%></option>
            <% } %>
        </select>
    </div>
    <div class="item_selection cell cell4" >
        <button class="wizard" ><span class="none" >^org\.hibernate\.SQL|^org\.hibernate\.type</span>Hibernate</button>
        <button class="wizard"><span class="none" >^it\.eng.*</span>it.*</button>
        <button class="wizard"><span class="none" >^com\.*</span>com.*</button>
        <button class="wizard"><span class="none" >^org\.*</span>org.*</button>
    </div><div class="clear"></div>
</div>


<form action="index.jsp" name="addLogForm" method="post">
<div class="addLogger" >
    <div class="header row">
        <span class="cell cell1">&nbsp;</span>
        <span class="cell cell2">Add logger<input type="text" name="logger" class="cell4"/></span>
        <span class="cell cell4">Level:
        <select class="log_level" name="newLogLevel">

            <% for (int cnt = 0; cnt < logLevels.length; cnt++) { %>
            <option  value="<%=logLevels[cnt].getName()%>"><%=logLevels[cnt].getName()%></option>
            <% } %>

        </select><input type="submit" name="operation" value="Create" /></span>
    </div>

</div></form><form action="index.jsp" name="logFilterForm" method="post">
    <input type="hidden" name="operation" value="changeLogLevel" />
<div id="loggers" >
            <div class="header row"><span class="cell cell1">&nbsp;</span>
                <span class="cell cell2">Logger</span>
                <span class="cell cell4">Change Log Level To</span>
            </div><div class="header row select_all"><span class="cell cell1"><input class="select_all" type="checkbox" /></span>
                <span class="cell cell2">[SELECT ALL]</span>

                <span class="cell cell4">
                    <%
                        for (int cnt = 0; cnt < logLevels.length; cnt++) {
								Level l = logLevels[cnt];
                                String url = "index.jsp?platform=jdk&operation=changeLogLevel&logNameFilter=" + request.getParameter("logNameFilter")
                                        + "&newLogLevel="
                                        + l.getName();
								%><input type="submit" name="newLogLevel" value="<%=l%>"><%
                        }%><input type="submit" name="newLogLevel" value="NULL">
                </span>
            </div>
<%
				LogManager lm = LogManager.getLogManager();
                Enumeration<String> loggers = lm.getLoggerNames();        
                Map<String,Logger> loggersMap = new HashMap<String,Logger>(128);
                Logger rootLogger = Logger.getGlobal();

                if (!loggersMap.containsKey(rootLogger.getName())) {
                    loggersMap.put(rootLogger.getName(), rootLogger);
                }
                while (loggers.hasMoreElements()) {
                    String s_logger = loggers.nextElement();
                    Logger logger = Logger.getLogger(s_logger);
                    loggersMap.put(s_logger, logger);
                }
                Set loggerKeys = loggersMap.keySet();

                String[] keys = new String[loggerKeys.size()];

                keys = (String[]) loggerKeys.toArray(keys);

                Arrays.sort(keys, String.CASE_INSENSITIVE_ORDER);
                for (int i = 0; i < keys.length; i++) {
				
                    Logger logger = (Logger) loggersMap.get(keys[i]);
                    if (logger==null || logger.getName()==null || "".equals(logger.getName())) continue;
					
                    // MUST CHANGE THE LOG LEVEL ON LOGGER BEFORE GENERATING THE LINKS AND THE
                    // CURRENT LOG LEVEL OR DISABLED LINK WON'T MATCH THE NEWLY CHANGED VALUES
                    if ("changeLogLevel".equals(targetOperation)) {
                    	if (s_targetLogger==null) {
                    		String s_logger = request.getParameter("logger_" + logger.getName());
                    		if ("on".equals(s_logger)) {
                    			logger.setLevel(targetLogLevel);
                    		}
                    		
                    	} else {
                    		if (logger.getName().equals(s_targetLogger)) {
                    			Logger selectedLogger = (Logger) loggersMap.get(s_targetLogger);
                    			selectedLogger.setLevel(targetLogLevel);
                    		}
                    	}
                    }
                    String loggerName = null;
                    String loggerEffectiveLevel = null;
                    String loggerParent = null;
                    if (logger != null) {
                        loggerName = logger.getName();
                        if (logger.getLevel()!=null) {
                            loggerEffectiveLevel = logger.getLevel().getName();
                        } else {
                            loggerEffectiveLevel = "-";
                        }
                        // loggerEffectiveLevel = String.valueOf(logger.getEffectiveLevel());
                        loggerParent = (logger.getParent() == null ? null : logger.getParent().getName());

                    }
                    String url = "?platform=jdk&operation=changeLogLevel&logger=" + loggerName;

%>
            <div class="row"><span class="cell cell1"><input name="logger_<%=loggerName%>" type="checkbox" /></span>
                <span class="cell cell2"><%=loggerName%></span>
                <span class="cell cell4 level<%=loggerEffectiveLevel%>">
                    <%
                        for (int cnt = 0; cnt < logLevels.length; cnt++) {
								Level l = logLevels[cnt];
                                if (logger.getLevel() != l) {
                    				%><a  href='<%=url + "&newLogLevel=" + l.getName()%>'>[<%=l %>]</a><%
                        		} else {
                                    out.print("<span class=\"selected\">["+logger.getLevel()+"]</span>");
                                }
                        
                        }
 %><a href='<%=url%>'>[NULL]</a>
                </span>
            </div>

            <%
                }
            %>    
        </div>
</form>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery(".filtering .wizard").click(function() {
            jQuery(".filtering .filterText").val(jQuery(this).children('span').text()).trigger('keyup');
        });

    })
</script>