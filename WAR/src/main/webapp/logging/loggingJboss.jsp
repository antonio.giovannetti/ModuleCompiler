<%@page import="org.jboss.logging.Logger.Level"%>


<%@page import="java.util.Arrays"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Enumeration"%>
<%@ page import="org.jboss.logging.Logger" %>
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%

    Level[] logLevels = { Level.DEBUG,Level.ERROR,Level.FATAL,Level.INFO,Level.TRACE,Level.WARN};
    String targetOperation = (String) request.getParameter("operation");
    String s_targetLogger = (String) request.getParameter("logger");
    String s_targetLogLevel = (String) request.getParameter("newLogLevel");
    Level targetLogLevel = null;
    if (s_targetLogLevel!=null) {
        if (!"NULL".equals(s_targetLogLevel)) targetLogLevel = Level.valueOf(s_targetLogLevel);
    }


    if ("Create".equals(targetOperation)) {
        if (s_targetLogger!=null && !"".equals(s_targetLogger)) {
            Logger logger = org.jboss.logging.Logger.getLogger(s_targetLogger);
            if (logger!=null && targetLogLevel!=null) {
//                logger.setLevel(targetLogLevel);
            } else {
                out.print("<h1>logger " + s_targetLogger + " is null</h1");
            }
        }
    }

%><div class="filtering">
    <div class="item_found cell"  ></div>
    <div class="item_selection cell">Filter Loggers by name (RegExp):<input name="logNameFilter" type="text"
                                                                            size="50" value="<%=(s_targetLogger == null ? "" : s_targetLogger)%>"
                                                                            class="filterText" /> and level:
        <select class="log_level" name="filterLevel"><option value="" >Any</option>
            <option  value="-">NULL</option>
            <% for (int cnt = 0; cnt < logLevels.length; cnt++) { %>
            <option  value="<%=logLevels[cnt].name()%>"><%=logLevels[cnt].name()%></option>
            <% } %>
        </select>
    </div>
    <div class="item_selection cell cell4" >
        <button class="wizard" ><span class="none" >^org\.hibernate\.SQL|^org\.hibernate\.type</span>Hibernate</button>
        <button class="wizard"><span class="none" >^it\.eng.*</span>it.*</button>
        <button class="wizard"><span class="none" >^com\.*</span>com.*</button>
        <button class="wizard"><span class="none" >^org\.*</span>org.*</button>
    </div><div class="clear"></div>
</div>


<form action="index.jsp" name="addLogForm" method="post">
    <div class="addLogger" >
        <div class="header row">
            <span class="cell cell1">&nbsp;</span>
            <span class="cell cell2">Add logger<input type="text" name="logger" class="cell4"/></span>
            <span class="cell cell4">Level:
        <select class="log_level" name="newLogLevel">

            <% for (int cnt = 0; cnt < logLevels.length; cnt++) { %>
            <option  value="<%=logLevels[cnt].name()%>"><%=logLevels[cnt].name()%></option>
            <% } %>

        </select><input type="submit" name="operation" value="Create" /></span>
        </div>

    </div></form><form action="index.jsp" name="logFilterForm" method="post">
    <input type="hidden" name="operation" value="changeLogLevel" />
    <div id="loggers" >
        <div class="header row"><span class="cell cell1">&nbsp;</span>
            <span class="cell cell2">Logger</span>
            <span class="cell cell4">Change Log Level To</span>
        </div><div class="header row select_all"><span class="cell cell1"><input class="select_all" type="checkbox" /></span>
        <span class="cell cell2">[SELECT ALL]</span>

        <span class="cell cell4">
                    <%
                        for (int cnt = 0; cnt < logLevels.length; cnt++) {
                            Level l = logLevels[cnt];
                            String url = "index.jsp?platform=jdk&operation=changeLogLevel&logNameFilter=" + request.getParameter("logNameFilter")
                                    + "&newLogLevel="
                                    + l.name();
                    %><input type="submit" name="newLogLevel" value="<%=l%>"><%
            }%><input type="submit" name="newLogLevel" value="NULL">
                </span>
    </div>

    </div>
</form>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery(".filtering .wizard").click(function() {
            jQuery(".filtering .filterText").val(jQuery(this).children('span').text()).trigger('keyup');
        });

    })
</script>
