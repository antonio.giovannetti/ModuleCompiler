<%@page import="org.apache.log4j.LogManager"%>
<%@page import="org.apache.log4j.Level"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.Enumeration"%>
<%

Level[] logLevels = { Level.ALL,Level.DEBUG,Level.ERROR,Level.FATAL,Level.INFO,Level.OFF,Level.TRACE,Level.WARN};
String targetOperation = (String) request.getParameter("operation");
String s_targetLogger = (String) request.getParameter("logger");
String s_targetLogLevel = (String) request.getParameter("newLogLevel");
Level targetLogLevel = null;
if (s_targetLogLevel!=null) {
	targetLogLevel = Level.toLevel(Integer.parseInt(s_targetLogLevel));
}
%>
<div id="loggers">
            <div class="row"><span class="cell cell1l"></span>
                <span class="cell cell2l">Logger</span>

                <span class="cell cell3l">Parent Logger</span>
                <span class="cell cell4l">Current Level</span>

                <span class="cell cell5l">Change Log Level To</span>
            </div>
<%

					


                Enumeration<Logger> loggers = LogManager.getCurrentLoggers();
                Map<String,Logger> loggersMap = new HashMap<String,Logger>(128);
                Logger rootLogger = Logger.getRootLogger();

                if (!loggersMap.containsKey(rootLogger.getName())) {
                    loggersMap.put(rootLogger.getName(), rootLogger);
                }
                while (loggers.hasMoreElements()) {
                	Logger logger = loggers.nextElement();
                    loggersMap.put(logger.getName(), logger);
                }
                Set loggerKeys = loggersMap.keySet();

                String[] keys = new String[loggerKeys.size()];

                keys = (String[]) loggerKeys.toArray(keys);

                Arrays.sort(keys, String.CASE_INSENSITIVE_ORDER);
                for (int i = 0; i < keys.length; i++) {
				
                    Logger logger = (Logger) loggersMap.get(keys[i]);
					
                    // MUST CHANGE THE LOG LEVEL ON LOGGER BEFORE GENERATING THE LINKS AND THE
                    // CURRENT LOG LEVEL OR DISABLED LINK WON'T MATCH THE NEWLY CHANGED VALUES
                    if ("changeLogLevel".equals(targetOperation)
                            && s_targetLogger.equals(logger.getName())) {

                        Logger selectedLogger = (Logger) loggersMap
                                .get(s_targetLogger);
					
                        selectedLogger.setLevel(targetLogLevel);
                    }

                    String loggerName = null;
                    String loggerEffectiveLevel = null;
                    String loggerParent = null;
                    if (logger != null) {
                        loggerName = logger.getName();
                        if (logger.getLevel()!=null) loggerEffectiveLevel = logger.getEffectiveLevel().toString(); else loggerEffectiveLevel = "-";
                        // loggerEffectiveLevel = String.valueOf(logger.getEffectiveLevel());
                        loggerParent = (logger.getParent() == null ? null : logger.getParent().getName());

                    }

%>
            <div class="row"><span class="cell cell1l"></span>
                <span class="cell cell2l"><%=loggerName%></span>

                <span class="cell cell3l"><%=loggerParent%></span>
                <span class="cell cell4l"><%=loggerEffectiveLevel%></span>
                <span class="cell cell5l">
                    <%
                        for (int cnt = 0; cnt < logLevels.length; cnt++) {
								Level l = logLevels[cnt];
                                String url = "index.jsp?platform=log4j&operation=changeLogLevel&logger="
                                        + loggerName
                                        + "&newLogLevel="
                                        + l.toInt();

                                if (logger.getLevel() == l) {
                    				%><span class="selected"><%=l.toString()%></span><%
                        		} else {
                    			%> <a  href='<%=url%>'>[<%=l%>]</a>&nbsp; <%
     							}
                        
                        }
 %>
                </span>
            </div>

            <%
                }
            %>    
        </div>
