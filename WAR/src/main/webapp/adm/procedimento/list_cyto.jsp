<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="cyto" style="width: 800px; height: 600px; "></div>
<script>
        /* CYTO */
        var nodes = [];
        <s:iterator var="p" value="procedimenti">
        nodes.push({id: '<s:property value="id" />',label: '<s:property value="descrizione" />'});

        <s:if test="parent!=null">nodes.push({id: 'from_<s:property value="parent" />_to_<s:property value="id" />',source:'<s:property value="parent" />',target:'<s:property value="id" />'});</s:if>
        </s:iterator>
        var cy = cytoscape({
            container: document.getElementById('cyto'), // container to render in
            elements: nodes,
            style: [ // the stylesheet for the graph
                {
                    selector: 'node',
                    style: {
                        'background-color': '#666',
                        'label': 'data(id)'
                    }
                },

                {
                    selector: 'edge',
                    style: {
                        'width': 3,
                        'line-color': '#ccc',
                        'target-arrow-color': '#ccc',
                        'target-arrow-shape': 'triangle'
                    }
                }
            ],

            layout: {
                name: 'grid',
                rows: 1
            }

        });

</script>