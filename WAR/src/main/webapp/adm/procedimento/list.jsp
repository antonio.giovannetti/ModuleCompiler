<%@ taglib prefix="s" uri="/struts-tags" %>


<div class="well"><h3>Lista procedimenti
    <span class="pull-right">
    <s:a namespace="/admin" action="procedimento" method="add" cssClass="btn btn-info btn-sm "><i class="fas fa-plus" aria-hidden="true"></i>&nbsp;Nuovo</s:a>

    <s:a title="Backup completo" namespace="/admin" action="procedimento" method="backupCfg" cssClass="btn btn-info btn-sm "><i class="far fa-save" aria-hidden="true"></i>&nbsp;Backup</s:a>

    </span>
</h3></div>
<div id="${uuid}">
    <div class="well"><span class="btn btn-sm btn-default websocket">WSK</span></div>
    <%--<div class="btn btn-sm btn-default">--%>
        <%--<i class="octicon octicon-s octicon-git-branch" ></i>--%>
    <%--</div>--%>

    <%--<div class="btn btn-default">--%>
        <%--<i class="octicon octicon-m octicon-git-branch" ></i>--%>
    <%--</div>--%>

    <%--<div class="btn btn-default">--%>
        <%--<i class="octicon octicon-l octicon-git-branch" ></i>--%>
    <%--</div>--%>

        <s:form theme="simple" action="procedimento.search" method="POST" >
        <table class="stripe col-xs-12">
            <thead>
            <tr>
                <th class="col-xs-7">Descrizione</th>
                <th class="col-xs-2">Data creazione</th>
                <th class="col-xs-3">Stato</th>
                <th></th>
            </tr>

            </thead>
            <tbody>
            <s:iterator var="procedimento" value="procedimenti">
                <s:url var="toDetail" namespace="/admin" method="detail" action="procedimento"><s:param name="procedimento.id" value="#procedimento.id" /></s:url>

                <tr class="top10">
                    <td class="col-xs-5">
                        <a <s:if test="#procedimento.parent!=null">disabled</s:if> href='<s:property value="toDetail" />' class="btn btn-info text-left col-xs-12">
                            <i class="<s:property value='#procedimento.icon' /> fa-2x"></i>&nbsp;<s:property value="#procedimento.descrizione" />
                            <i class="fas fa-arrow-alt-circle-right fa-2x pull-right"></i></a>
                        <%--<s:if test="#procedimento.dataFine!=null">disabled</s:if>--%>

                        <%--<s:a namespace="/admin" cssClass="btn btn-info text-left col-xs-12" theme="simple" method="detail" action="procedimento" >--%>
                            <%--<s:param name="procedimento.id" value="#procedimento.id" />--%>
                            <%--<i class="<s:property value='#procedimento.icon' /> fa-2x"></i>&nbsp;<s:property value="#procedimento.descrizione" />--%>
                            <%--<i class="fas fa-arrow-alt-circle-right fa-2x pull-right"></i></s:a>--%>
                        </td>
                    <td class="col-xs-2"><s:date name="#procedimento.dateInfo.dataCreazione" /></td>

                    <td class="col-xs-2" title="Stato <s:property value='#procedimento.stato' />"><s:property
                            value="@net.mysoftworks.modulecompiler.model.enumeration.StatiProcedimento@describeStato(#procedimento.stato)" /></td>
                    <td class="col-xs-3 text-right"><s:a cssClass="btn btn-sm btn-default" action="procedimento" namespace="/admin" method="stampa">
                        <s:param name="procedimento.id" value="#procedimento.id" /><i class="fas fa-print" ></i></s:a>
                        <s:a cssClass="btn btn-sm btn-default" title="Clona" action="procedimento" namespace="/admin" method="clona">
                            <s:param name="procedimento.id" value="#procedimento.id" />
                            <s:param name="deep" value="false" /><i class="far fa-copy" ></i></s:a>
                        <s:a cssClass="btn btn-sm btn-default" title="Clona (anche dichiarazioni)" action="procedimento" namespace="/admin" method="clona">
                            <s:param name="procedimento.id" value="#procedimento.id" />
                            <s:param name="deep" value="true" />
                            <i class="fas fa-copy" ></i></s:a>
                        <s:a cssClass="btn btn-sm btn-default" title="Versiona" action="procedimento" namespace="/admin" method="versiona">
                            <s:param name="procedimento.id" value="#procedimento.id" />
                            <%--<i class="fas fa-share-alt" ></i>--%>
                            <i class="octicon octicon-s octicon-git-branch" ></i>
                        </s:a>
                        <%--<s:a cssClass="btn btn-sm btn-danger delete" data-text="Cancellare il procedimento %{#procedimento.id}?"  title="Cancella" action="procedimento" namespace="/admin" method="remove">--%>
                            <%--<s:param name="procedimento.id" value="#procedimento.id" />--%>
                            <%--<s:param name="deep" value="false" /><i class="far fa-trash-alt" ></i></s:a>--%>
                        <s:if test="children.isEmpty">
                        <a class="btn btn-sm btn-danger delete"
                           href='<s:property value="deleteProc" />'
                           data-id-proc='<s:property value="#procedimento.id" />' data-deep="false"
                           data-text="Cancellare il procedimento %{#procedimento.id}?" title="Cancella"
                        ><i class="far fa-trash-alt" ></i></a>
                        <a class="btn btn-sm btn-danger delete"
                           href='<s:property value="deleteProc" />' data-id-proc='<s:property value="#procedimento.id" />' data-deep="true"
                           data-text="Con questa operazione si cancelleranno anche <b>tutte</b> le dichiarazioni contenute nel procedimento %{#procedimento.id}. Confermare?"
                           title="Cancella (anche dichiarazioni)"
                        ><i class="fas fa-trash-alt" ></i></a></s:if><s:else>
                            <span title="Cancellare prima i figli" class="btn btn-sm btn-danger" disabled><i class="far fa-trash-alt" ></i></span>
                            <span title="Cancellare prima i figli" class="btn btn-sm btn-danger" disabled><i class="fas fa-trash-alt" ></i></span></s:else>
                        <%--<s:a cssClass="btn btn-sm btn-danger delete" data-text="Con questa operazione si cancelleranno anche <b>tutte</b> le dichiarazioni contenute nel procedimento %{#procedimento.id}. Confermare?"  title="Cancella (anche dichiarazioni)" action="procedimento" namespace="/admin" method="remove">--%>
                            <%--<s:param name="procedimento.id" value="#procedimento.id" />--%>
                            <%--<s:param name="deep" value="true" />--%>
                            <%--<i class="fas fa-trash-alt" ></i></s:a>--%>
                    </td>
                </tr>
            </s:iterator>


            </tbody>
        </table>

    </s:form>



</div>
<s:url var="checkSaveProc" namespace="/admin" method="checkSaveProc" action="service" />
<s:url var="doDeleteProc" action="procedimento" namespace="/admin" method="remove" />
        <%--<s:include value="list_cyto.jsp" />--%>
        <%--<s:include value="list_vis.jsp" />--%>
        <%--<s:include value="list_jit.jsp" />--%>
<script>
    jQuery(document).ready(function() {
        var ctx = jQuery("#${uuid}");
        jQuery(".websocket",ctx).click(function () {
            var connect = function (url) {
                return new Promise(function(resolve, reject,message) {
                    var server = new WebSocket(url);
                    console.log("url:" + url)
                    console.info(server);
                    server.onopen = function() {
                        resolve(server);
                    };
                    server.onerror = function(err) {
                        reject(err);
                    };
                    server.onmessage = function() {
                        console.info(arguments)
                        // message(server,arguments);
                    };

                });
            }


            var wsurl= MC_UTILS.webContext.replace(/^https?/,"ws");
            connect(wsurl+"/wsk/pratica").then(function (websocket) {
                websocket.send({"a":"aaaaaaaaaaaaaaaa","b":"bbbbb"});
                // websocket.close();
            },function (websocket) {

            },function (websocket) {
                console.info(arguments);
            })
        })
        jQuery('table',ctx).DataTable({
            "language": {
                "url": MC_UTILS.resourcesUrl + "/js/datatables_1.10.19/lang_it.json"
            }
        });
        ctx.find(".delete").click(function(e) {
            e.preventDefault();
            var _data = jQuery(this).data();
            jQuery.ajax({
                url: '<s:property value="checkSaveProc" />',
                data: _data,
                success: function (data) {
                    var onConfirm = function () {
                        window.location = '<s:property value="doDeleteProc" />?procedimento.id='+_data.idProc;
                    }
                    if (data.messages && data.messages.length != 0) {
                        jQuery(e.target)
                            .data("confirm-ok", onConfirm)
                            .data("confirm-text", data.messages[0]);
                        new MC_MODAL.confirm(e);
                    } else {
                        onConfirm();
                    }
                }
            })
        })


    })

</script>