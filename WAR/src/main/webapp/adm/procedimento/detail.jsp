<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="${uuid}">
    <s:if test="!ancestors.isEmpty">
        <h5 class="hierarchy parents">Genitori:<ul>
            <s:iterator value="ancestors" var="anc">
            <li><s:a cssClass="btn btn-link" namespace="/admin" method="detail" action="procedimento">
                    <s:param name="procedimento.id" value="id" /><i class="<s:property value='icon' />"></i>&nbsp;<s:property value="descrizione" />
                 - <s:date name="dateInfo.dataCreazione"/>
            </s:a><%-- <ul> --%>
                    </li><div><i class="fas fa-caret-down fa-2x"></i></div>
                        </s:iterator><%--<s:iterator value="ancestors" var="anc"></ul></s:iterator>--%>
        </ul>
        </h5></s:if>
    <div class="alert alert-info text-info"><h3><small class="col-xs-12 col-sm-3 col-md-2 col-lg-2 ">[Procedimento <s:property value="procedimento.id"/>]</small>
        <em class="col-xs-12 col-sm-9 col-md-6 col-lg-8 "><s:property
        value="procedimento.descrizione" default="NUOVO"/></em>
    <span class="pull-right ">
        <span title="Guida alla compilazione" data-id-guida="<s:property value="procedimento.idGuida"/>" class="btn btn-info btn-sm guida"><i class="fas fa-question" ></i></span>
    <s:a namespace="/admin" action="procedimento" method="list" cssClass="btn btn-info btn-sm"><i class="fas fa-list" aria-hidden="true"></i></s:a>
    <s:a title="Backup configurazione procedimento" namespace="/admin" action="procedimento" method="backupCfg" cssClass="btn btn-info btn-sm ">
        <s:param value="procedimento.id" name="procedimento.id" />
        <i class="fas fa-hdd" aria-hidden="true"></i>&nbsp;Backup</s:a>
    </span><div class="clearfix"></div>
</h3></div>
    <s:if test="!children.isEmpty">
        <h4 class="hierarchy children"><div><i class="fas fa-caret-down fa-2x"></i>Procedimenti figli<i class="fas fa-caret-down fa-2x"></i></div><ul>
            <s:iterator value="children" var="child">
                <li><s:a cssClass="btn btn-link" namespace="/admin" method="detail" action="procedimento">
                    <s:param name="procedimento.id" value="id" /><i class="<s:property value='icon' />"></i>&nbsp;<s:property value="descrizione" />
                    - <s:date name="dateInfo.dataCreazione"/>
                </s:a></li>
            </s:iterator><%--<s:iterator value="ancestors" var="anc"></ul></s:iterator>--%>
        </ul></h4></s:if>
<div class="container-fluid">

    <s:form theme="simple" cssClass="form-horizontal" action="procedimento!save" >
        <s:hidden key="procedimento.id" />
        <s:include value="button_bar.jsp" />
        <div class="form-group">
            <span class="col-xs-12 col-sm-6 col-md-6 col-lg-4 " title="See fontawesome.io">
                <select name="procedimento.icon" class="form-control col-xs-8" style="display: inline">
<s:iterator value="@net.mysoftworks.modulecompiler.web.common.action.ProcedimentoAction@getIcons()" var="s">
    <option <s:if test="#s==#attr.procedimento.icon">selected</s:if> value="<s:property />"><s:property value='s' /></option>
</s:iterator>
                </select>&nbsp;<i class="<s:property value='procedimento.icon' /> fa-2x"></i>
            </span>

            <span class="col-xs-12 col-sm-6 col-md-3 col-lg-4 ">
<label class="">Data creazione:</label>
<span class=""><s:date name="procedimento.dateInfo.dataCreazione"/></span>
        </span>
            <span class="col-xs-12 col-sm-6 col-md-3 col-lg-4 ">
            <label class="">Data modifica:</label>
<span class=""><s:date name="procedimento.dateInfo.dataModifica" /></span>
            </span>
        </div>


        <div class="form-group">
            <label class="col-xs-2" for="${uuid}p_descrizione">Descrizione</label>
            <div class="col-xs-8">
                <input id="${uuid}p_descrizione" class="form-control" name="procedimento.descrizione"
                       value="<s:property value="procedimento.descrizione" />"/></div>
        </div>
        <div class="row form-group">
            <label class="col-xs-2" for="${uuid}p_stato">Stato</label>
            <div class="col-xs-4">
                <select multiple class="form-control" id="${uuid}p_stato" name="statoProcedimento">
                    <s:iterator value="@net.mysoftworks.modulecompiler.model.enumeration.StatiProcedimento@values()"
                                var="sp">
                        <option
                                <s:if test="%{(procedimento.stato band #sp.bytewise()) != 0 || (procedimento.stato + #sp.bytewise()) ==0 }">selected</s:if>
                                value="<s:property value="%{#sp.bytewise()}" />"><s:property value="#sp.descrizione"
                        /></option>
                    </s:iterator>
                </select></div>


            <div class="col-xs-3">
                <label class="col-xs-4 text-right" for="${uuid}p_compilabilita">Compilabilita</label>

                <div class="col-xs-8">
                    <select multiple class="form-control" id="${uuid}p_compilabilita" name="compilabilita">
                        <s:iterator value="@net.mysoftworks.modulecompiler.model.RuoloPratica$RuoloCompilazionePratica@values()"
                                    var="cmp"><s:if test="#cmp.name()!= 'TUTTI'">
                            <option
                                    <s:if test="%{(procedimento.compilabilita band #cmp.bytewise()) != 0}">selected</s:if>
                                    value="<s:property value="%{#cmp.bytewise()}" />"><s:property
                                    value="#cmp.codice"/></option></s:if>
                        </s:iterator>
                    </select></div>
            </div>
            <div class="col-xs-3">
                <label class="col-xs-4 text-right" title="<s:property value="visibilita" />" for="${uuid}p_visibilita">Visibilita</label>
                <div class="col-xs-8">
                    <select multiple class="form-control" id="${uuid}p_visibilita" name="visibilita">
                        <s:iterator value="@net.mysoftworks.modulecompiler.model.RuoloPratica$RuoloCompilazionePratica@values()"
                                    var="cmp"><s:if test="#cmp.name()!= 'TUTTI'">
                            <option
                                    <s:if test="%{(procedimento.visibilita band #cmp.bytewise()) != 0}">selected</s:if>
                                    value="<s:property value="%{#cmp.bytewise()}" />"><s:property
                                    value="#cmp.codice"/></option></s:if>
                        </s:iterator>
                    </select></div>

            </div>
        </div>
        <div class="clearfix"></div>
        <div class="alert alert-info allegati">
            <h4>Allegati [<s:property value="allegati.size" />]<span class="pull-right">
                    <span class="btn btn-info btn-sm add-allegato"><i class="fas fa-plus" aria-hidden="true"></i></span>
            </span></h4>
            <div class="row">
                <div class="col-xs-6">Descrizione</div>
                <div class="col-xs-3">Tipo</div>
                <div class="col-xs-1">Obbl.</div>
                <div class="col-xs-1">Cancella</div>

            </div>
            <div class="lista-allegati container-fluid"><div class="collapse row"><input type="hidden" />
                <div class="col-xs-6"><input type="text" class="form-control" ></div>
                <div class="col-xs-3"><select name="allegati.fileTypes" multiple>
                    <s:iterator value="@net.mysoftworks.modulecompiler.model.enumeration.FilePermessi@values()" var="fp">
                        <option value="${fp.bytewise()}"><s:property value="#fp.mime" /></option>
                    </s:iterator>
                </select></div><div class="col-xs-1"><input type="checkbox" /></div>
                <div class="col-xs-1"><span class="btn btn-xs btn-danger"><i class="fas fa-trash"></i></span></div>

            </div>
</div>
        </div>

        <div class="alert striped">
            <h4>Dichiarazioni<span class="pull-right"><s:a title="Aggiungi" theme="simple" namespace="/admin" action="metaDichiarazione" method="add" cssClass="btn btn-info btn-sm">
                <s:param name="procedimento.id" value="procedimento.id" />
                <i class="fas fa-plus" aria-hidden="true"></i></s:a>
            <s:a title="Riusa" theme="simple" namespace="/admin" action="metaDichiarazione" method="search" cssClass="btn btn-info btn-sm search-dic" >
                <s:param name="procedimento.id" value="procedimento.id" />
                <i class="fas fa-search" aria-hidden="true"></i></s:a></span></h4>
            <hr/>
            <div class="container-fluid listaDichiarazioni">
            <div class="row bg-primary unstriped">
                <div class="col-xs-5">Descrizione</div>
                <div class="col-xs-2">Compilabilita</div>
                <div class="col-xs-3">Visibilita</div>
                <div class="col-xs-2"></div>
            </div>
            <s:iterator value="procedimento.dichiarazioni" var="metaDichiarazione" status="st">
                <div class="row top10">
                    <div class="col-xs-5">
                        <s:a cssClass="btn btn-info btn-block" theme="simple" namespace="/admin" method="detail" action="metaDichiarazione">
                            <s:param name="metaDichiarazione.id" value="#metaDichiarazione.id"/>
                            <s:param name="procedimento.id" value="procedimento.id"/>
                            <s:property value="#metaDichiarazione.id"/>-<s:property value="#metaDichiarazione.descrizione"/>
                        <i class="fas fa-arrow-alt-circle-right fa-lg pull-right"></i></s:a></div>
                    <div class="col-xs-2">
                            <s:property
                                    value="@net.mysoftworks.modulecompiler.model.RuoloPratica$RuoloCompilazionePratica@describeFigure(#metaDichiarazione.compilabilita)"/>
                    </div>
                    <div class="col-xs-3">
                            <s:property
                                    value="@net.mysoftworks.modulecompiler.model.RuoloPratica$RuoloCompilazionePratica@describeFigure(#metaDichiarazione.visibilita)"/>
                    </div>
                    <div class="text-right col-xs-2"><span class="pull-left">
                        <s:url var="urlClonaDich" namespace="/admin" method="clona" action="metaDichiarazione">
                            <s:param name="metaDichiarazione.id" value="#metaDichiarazione.id"/>
                            <s:param name="procedimento.id" value="procedimento.id"/>
                        </s:url>
                        <a title="Clona" href="<s:property value='urlClonaDich' />" data-confirm-title="Clona dichiarazione <s:property value='#metaDichiarazione.id' />" class="btn btn-xs btn-default clone confirm">
                            <i class="fas fa-copy" aria-hidden="true"></i></a>
                        <s:a title="Disassocia" cssClass="btn btn-xs btn-warning disassocia" theme="simple" namespace="/admin" method="unlinkDichiarazione" action="service">
                        <s:param name="idDic" value="#metaDichiarazione.id"/>
                        <s:param name="idProc" value="procedimento.id"/>
                            <i class="fas fa-unlink" aria-hidden="true"></i></s:a>
                        <s:a title="Disassocia e cancella" cssClass="btn btn-xs btn-danger disassocia" theme="simple" namespace="/admin" method="unlinkDichiarazione" action="service">
                            <s:param name="idDic" value="#metaDichiarazione.id"/>
                            <s:param name="idProc" value="procedimento.id"/>
                            <s:param name="cancel" value="true"/>
                            <i class="fas fa-trash " aria-hidden="true"></i></s:a></span>
                        <span class="pull-right col-xs-6">
                        <s:if test="!#st.first"><i title="Sposta su" data-id-dic="<s:property value="#metaDichiarazione.id"/>" data-move-dir="UP" class="pull-left fas fa-arrow-circle-up fa-lg" ></i></s:if>
                        <s:if test="!#st.last"><i title="Sposta giu" data-id-dic="<s:property value="#metaDichiarazione.id"/>"  data-move-dir="DOWN" class="pull-right fas fa-arrow-circle-down fa-lg" ></i></s:if>
                            </span>
                    </div>
                </div>

            </s:iterator></div>
        </div>

    <s:include value="button_bar.jsp" />
    </s:form>
</div></div>
<s:url var="disassocia" namespace="/admin" action="procedimento" method="unlinkDichiarazione">
    <s:param name="procedimento.id" value="procedimento.id"/></s:url>
<s:url var="searchDic" namespace="/admin" action="procedimento" method="searchDic">
    <s:param name="idProc" value="procedimento.id"/></s:url>
<s:url var="addOrCloneDic" namespace="/admin" method="addOrCloneDic" action="service">
    <s:param name="idProc" value="procedimento.id"/></s:url>
<s:url var="saveOrUpdateGuidaProc" namespace="/admin" method="saveOrUpdateGuidaProc" action="service">
    <s:param name="idProc" value="procedimento.id"/></s:url>
<s:url var="formGuida" namespace="/admin" method="formGuida" action="service">
    <s:param name="procedimento.id" value="procedimento.id"/></s:url>
<s:url var="saveOrUpdateGuidaProc" namespace="/admin" method="saveOrUpdateGuidaProc" action="service">
    <s:param name="idProc" value="procedimento.id"/></s:url>
<s:url var="moveDic" namespace="/admin" method="moveDic" action="service">
    <s:param name="idProc" value="procedimento.id"/></s:url>
<s:url var="checkSaveProc" namespace="/admin" method="checkSaveProc" action="service">
    <s:param name="idProc" value="procedimento.id"/></s:url>
<s:url var="toDetail" namespace="/admin" method="detail" action="procedimento"></s:url>

<script>

    jQuery(document).ready(function(){
        var ctx = jQuery('#${uuid}');
        ctx.find('.listaDichiarazioni .disassocia').click(function(e){
            e.preventDefault();
            jQuery(e.target).data("confirm-title",jQuery(this).attr('title'))
                .data("confirm-ok",function(){
                    jQuery.ajax({url:e.currentTarget.href}).then(function(data){
                        MC_MODAL.printMessages(data).promise.then(function(){
                            jQuery(e.target).closest('.row').remove();
                        })
                    })
                });
            new MC_MODAL.confirm(e);
        })

        var size=2.8;var incr=.7;
        var els = ctx.find(".parents li a").toArray();
        // els.reverse();
        for(var i=els.length;i>=0;i--) {
            jQuery(els[i]).css('font-size',size+"em");
            size *=incr;
            size = Math.max(size,.8);
        }
        // ctx.find(".parents li a").each(function(idx,elem){
        //     jQuery(elem).css('font-size',size+"em");
        //     size *=incr;
        // })

        ctx.find(".search-dic").click(function(e){
            e.preventDefault();
            var ajaxOpt = {
                url:'<s:property value="addOrCloneDic" />',
                data:{},
                success:function(){
                    window.location.reload();
                }
            }
            MC_MODAL.info({
                title:'Cerca dicharazione da associare',
                class:"modal-lg",
                buttons:[new MC_MODAL.BUTTONTYPE("Link", "btn btn-success","button.link",true,"fas fa-link"),
                    new MC_MODAL.BUTTONTYPE("Clone", "btn btn-info","button.clone",true,"fas fa-copy"),
                    MC_MODAL.buttons.chiudiButton[0]],
                url:'<s:property value="searchDic" />'
            }).promise.then(function (data,modal) {
                jQuery.each(modal.find('form').serializeArray(), function(idx, kv) {
                    ajaxOpt.data[kv.name] = kv.value;
                });

                if (data.key=="button.link") { // Link
                    ajaxOpt.data.q='link';
                }
                if (data.key=="button.clone") { // Clone
                    ajaxOpt.data.q='clone';
                }
                if (ajaxOpt.data.idDic) {
                    jQuery.ajax(ajaxOpt);
                }

            })
        })

        ctx.find("select[name='procedimento.icon']").change(function(e){
            jQuery(this).next().removeClass()
                .addClass(jQuery(this).val() + " fa-2x")
            // ctx.find('h3 i:first').removeClass()
            //     .addClass(jQuery(this).val());
        });
        var doAddAllegato = function (evt,data,idx) {
            idx = idx || ctx.find(".allegati .lista-allegati > div:not(.collapse)").length;
            var clone = ctx.find(".allegati .lista-allegati > div:first-child").clone().removeClass("collapse");
            ctx.find(".allegati .lista-allegati").append(clone);
            clone.find('input').attr('name','allegati['+idx+'].nome');
            clone.find('select').attr('name','allegati['+idx+'].fileTypes');
            clone.find(':hidden').attr('name','allegati['+idx+'].id');
            if (data)  {
                clone.find('input').val(data.nome);
                clone.find(':hidden').val(data.id);
                clone.find('select option').each(function(idx,val){
                    val.selected = (1*val.value & data.fileTypes) != 0;
                });
            }
        }
        ctx.find(".allegati .add-allegato").click(doAddAllegato);

        ctx.find(".allegati .lista-allegati").on("click",".row:not(.collpse) .btn-danger ",function(){
            jQuery(this).closest('.row').remove();
        });
        <s:if test="allegati!=null">
        <s:iterator value="allegati" var="allegato" status="st">
        doAddAllegato(null,{'nome':'<s:property value="nome" />',
            'id':<s:property value="id" />,
            'fileTypes':<s:property value="fileTypes" />});
        </s:iterator>
        </s:if>


        ctx.find(".guida").click(function (e) {
          var _nfo = MC_MODAL.info({
              title: '<s:text name="fe.pratica.dich.guida_compilazione.title" />',
              urlData:jQuery(this).data(),
              url: '<s:property value="formGuida" />'
          });
            _nfo.ajaxContentPromise.then(function (value) {
                MC_UTILS.addRichEditor(_nfo.modal);
            })
            _nfo.promise.then(function (button,modal) {
                modal.find('form').ajaxSubmit({
                    url:'<s:property value="saveOrUpdateGuidaProc" />',
                    success: function () {
                        window.location.reload(true);
                    }
                })
            })
        })
        ctx.find(".fa-arrow-circle-up, .fa-arrow-circle-down").click(function(e){
            var me = jQuery(e.target);
            me.data("confirm-ok",function() {
                jQuery.ajax({
                    url: '<s:property value="moveDic" />',
                    data: {
                        idDic: me.data("id-dic"),
                        moveDir:me.data("move-dir")
                    },
                    success: function () {
                        window.location.reload(true);
                    }
                })
            });
            new MC_MODAL.confirm(e);

        })
        ctx.find(".save").click(function(e){
            e.preventDefault();
            var onConfirm = function(nuovo) {
                ctx.find('form').ajaxSubmit({
                    // data:{"method:save":"Submit"},
                    // dataType:'json',
                    success:function (data) {
                        if (nuovo) {
                            window.location.href = '<s:property value="toDetail" />?procedimento.id=' + data.messages[0];
                        } else window.location.reload(true);
                    },
                    error:function (data) {
                        MC_MODAL.printMessages(data.responseJSON);
                    }
                })
            }
            <s:if test="procedimento.id==null">onConfirm(true);</s:if><s:else>
            jQuery.ajax({
                url: '<s:property value="checkSaveProc" />',
                success:function(data){

                    if (data.messages && data.messages.length!=0) {
                        jQuery(e.target)
                            .data("confirm-ok",onConfirm)
                            .data("confirm-text",data.messages[0]);
                        new MC_MODAL.confirm(e);
                    } else {
                        onConfirm();
                    }
                }
            })
            </s:else>
        });




    })

</script>