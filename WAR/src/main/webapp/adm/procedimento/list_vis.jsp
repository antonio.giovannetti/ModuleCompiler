<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="vis"></div>
<script>
    jQuery(document).ready(function() {
        /* VIS */
        var nodes = [];
        var edges = [];
        <s:iterator var="p" value="procedimenti">
        nodes.push({id: '<s:property value="id" />',label: '<s:property value="descrizione" />'})
        <s:if test="parent!=null">edges.push({from:<s:property value="parent" />,to:'<s:property value="id" />'});</s:if>
        </s:iterator>
        var network = new vis.Network(jQuery('#vis')[0], {nodes: nodes,edges: edges}, {});
    })
</script>