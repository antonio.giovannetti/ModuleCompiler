<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="${uuid}" class="row">
    <div class="col-xs-4">
    <label class="" for="${uuid}cl_subType">Presentazione</label>
        <select name="campo.listType" class="form-control" id="${uuid}cl_subType">
            <s:iterator value="@net.mysoftworks.modulecompiler.model.meta.MetaListField$LIST_TYPE@values()"
                        var="cmp">
                <option <s:if test="%{campo.listType == #cmp}">selected</s:if>
                        value="<s:property value="%{#cmp.name()}" />"><s:property
                        value="%{#cmp.name()}" /></option>
            </s:iterator>
        </select>
    </div>
    <div class="col-xs-8 allowed_values_container alert alert-info">
        <div class="text-left"><strong>Valori permessi</strong>
            <span class="pull-right">
                <button type="button" class="add btn btn-sm btn-success"><i class="fas fa-plus"></i></button>
                <button type="button" class="import btn btn-sm btn-success"><i class="fas fa-upload"></i></button>


            </span></div>
        <div class="row"><div class="col-xs-1"></div><div class="col-xs-4">Codice</div><div class="col-xs-4">Descrizione</div></div>
        <div class="row">
        <div class="col-xs-12 allowed_values">
            <ol></ol></div>
        </div>

    </div>


</div>
<script>
    jQuery(document).ready(function(){
        var ctx = jQuery('#${uuid} .allowed_values_container');
        function addAllowedValue(cod,val,refresh) {
            var l = ctx.find('.allowed_values .allowed_value').length;
            var tpl = '<li class="top10 col-xs-12 allowed_value"><div class="col-xs-4"><input class="form-control" name="allowedCod" ';
            if (cod) {
                tpl+='value="'+ cod+'"';
            }
            tpl+='/></div><div class="col-xs-7"><input class="form-control" name="allowedVal" ';
            if (val) {
                tpl+='value="'+ val+'"';
            }
            tpl+='/></div><span class="btn btn-danger remove-av confirm"><i class="fas fa-trash"></i></span></li>';

            // tpl = '<li class="well" ></li>'
            ctx.find('.allowed_values ol').append(tpl);
            // if (refresh) {
            //     jQuery('.allowed_values ol',ctx).sortable();
            // }

        }
        ctx.find('.add').click(function () {
            addAllowedValue(undefined,undefined,true);
        })

        <s:iterator value="campo.allowedValues" var="allowed" status="st">
        addAllowedValue('<s:property value="#allowed.codice" />','<s:property value="#allowed.descrizione" />');
        </s:iterator>

        jQuery('.allowed_values ol',ctx).dragsort()
        ctx.on("click",".remove-av",function(e){
            jQuery(this).closest("li").remove()
        })

    })

</script>