<%@ taglib prefix="s" uri="/struts-tags" %>
<%--<div class="row ">--%>
    <%--<div class="col-xs-3 form-group">--%>
        <%--<label class="" for="${uuid}c_nome">Nome</label>--%>
        <%--<input class="form-control" id="${uuid}c_nome" name="campo.nome" value="<s:property--%>
                    <%--value="campo.nome" />" />--%>
    <%--</div>--%>
<%--</div>--%>

<div class="row top10">
    <div class="col-xs-3 form-group">
        <label class="" for="${uuid}cl_subType">Presentazione</label>
        <select class="form-control" id="${uuid}cl_subType">
            <s:iterator value="@net.mysoftworks.modulecompiler.model.meta.MetaTextField$TEXT_TYPE@values()"
                        var="cmp">
                <option <s:if test="%{campo.textType == #cmp}">selected</s:if>
                        value="<s:property value="%{#cmp.name()}" />"><s:property
                        value="%{#cmp.name()}" /></option>
            </s:iterator>
        </select>
    </div>
    <div class="col-xs-3 form-group">
        <label class="" for="${uuid}ct_regex">Regex</label>
        <input class="form-control" id="${uuid}ct_regex" name="campo.regex" value="<s:property value="campo.regex" />" />
    </div>
    <div class="col-md-4 col-xs-12">
        <label class="" for="${uuid}c_jtype">Java type</label>
        <select name="campo.javaType" class="form-control" id="${uuid}c_jtype">
            <s:iterator value="allowedJavaTypes"
                        var="t">
                <option <s:if test="%{campo.javaType == #t}">selected</s:if>
                        value="<s:property />"><s:property /></option>
            </s:iterator>
        </select>
    </div>
    <%--<label class="col-xs-1" for="${uuid}cl_subType">Presentazione</label>--%>
    <%--<div class="col-xs-3">--%>
        <%--<select class="form-control" id="${uuid}cl_subType">--%>
            <%--<s:iterator value="@net.mysoftworks.modulecompiler.model.meta.MetaTextField$TEXT_TYPE@values()"--%>
                        <%--var="cmp">--%>
                <%--<option <s:if test="%{campo.textType == #cmp}">selected</s:if>--%>
                        <%--value="<s:property value="%{#cmp.name()}" />"><s:property--%>
                        <%--value="%{#cmp.name()}" /></option>--%>
            <%--</s:iterator>--%>
        <%--</select>--%>
    <%--</div>--%>
    <%--<div class="col-xs-8">--%>
        <%--<div class="row form-group">--%>
            <%--<label class="col-xs-1" for="${uuid}ct_regex">Regex</label>--%>
            <%--<div class="col-xs-3">--%>
                <%--<input class="form-control" id="${uuid}ct_regex" name="campo.regex" value="<s:property--%>
                <%--value="campo.regex" />" />--%>
            <%--</div>--%>
        <%--</div>--%>

    <%--</div>--%>
</div>
