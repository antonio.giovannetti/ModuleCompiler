<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="${uuid}" class="row">
    <div class="col-xs-12">
    <label class="pull-left" for="${uuid}cl_subType">Tipo permesso (or)<br><input class="filter form-control" placeholder="filtro" /></label>
        <select multiple class="form-control" style="height: 20em;width: 40%" name="filePermessi" id="${uuid}cl_subType">
            <s:iterator value="mimes" var="mime">
                <option <s:if test="filePermesso(id)">selected</s:if> value="<s:property value="id" />" title="<s:property value="mimeType" />"><s:property value="@org.apache.commons.lang3.StringUtils@abbreviate(mimeType,60)" /></option>
            </s:iterator>
        </select>

    </div>
</div>
<script>
    jQuery(document).ready(function () {
        var ctx = jQuery("#${uuid}");

        ctx.find(".filter").keyup(function () {
            var match = this.value.toUpperCase();
            jQuery('option',ctx).each(function(idx,elem){
                var title = elem.text.toUpperCase();
                jQuery(elem).toggle(title.indexOf(match)!==-1)
            })
        });
    })



</script>