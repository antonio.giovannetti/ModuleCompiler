<%@ taglib prefix="s" uri="/struts-tags" %>
<%--<div class="row ">--%>
    <%--<div class="col-xs-3 form-group">--%>
        <%--<label class="" for="${uuid}c_nome">Nome</label>--%>
        <%--<input class="form-control" id="${uuid}c_nome" name="campo.nome" value="<s:property--%>
                    <%--value="campo.nome" />" />--%>
    <%--</div>--%>
<%--</div>--%>

<div id="${uuid}" class="row top10">
    <div class="col-xs-4 form-group">
        <label class="" for="${uuid}cl_subType">Sottotipo</label>
        <select name="campo.temporalType" class="form-control temporal-type" id="${uuid}cl_subType">
            <s:iterator value="@javax.persistence.TemporalType@values()" var="cmp">
                <option <s:if test="%{campo.temporalType == #cmp}">selected</s:if>
                        value="<s:property />"><s:property /></option>
            </s:iterator>
        </select>
    </div>
    <div class="col-xs-2">
        <label class="" for="${uuid}cd_inrange">In range</label>
        <s:checkbox key="campo.inRange" theme="simple" />
    </div>
    <div class="col-xs-3">
        <label class="" for="${uuid}cd_from">Da</label>
        <%-- FORMAT: 1985-04-12T23:20:50.52 --%>
        <input type="datetime-local" class="temporal form-control" id="${uuid}cd_from" name="campo.intDa" value="<s:date name="campo.intDa" format="yyyy-MM-dd'T'hh:mm:ss.00"/>" />
    </div>
    <div class="col-xs-3">
        <label class="" for="${uuid}cd_to">A</label>
        <input type="datetime-local" class="temporal form-control" id="${uuid}cd_to" name="campo.intA" value="<s:date name="campo.intA" format="yyyy-MM-dd'T'hh:mm:ss.00"/>" />
    </div>

    <div class="col-xs-12 collapse DATEcontainer">DATE params</div>
    <div class="col-xs-12 collapse TIMESTAMPcontainer">TIMESTAMP params</div>
    <div class="col-xs-12 collapse TIMEcontainer">TIME params</div>

</div>
<script>
    jQuery(document).ready(function () {
        var ctx = jQuery('#${uuid}');
        ctx.find('.temporal-type').change(function () {
            var cInfo = jQuery(this).val();
            ctx.find('.collapse').hide().filter('.'+cInfo+'container').show();
            if (cInfo=='DATE') {
                ctx.find(".temporal").attr('type','date');
            }
            if (cInfo=='TIMESTAMP') {
                ctx.find(".temporal").attr('type','datetime-local');
            }
            if (cInfo=='TIME') {
                ctx.find(".temporal").attr('type','time');
            }


        })
        ctx.find('.collapse.<s:property value="campo.temporalType" />container').show();

    })

</script>
