<%@ page import="net.mysoftworks.modulecompiler.model.meta.MetaField" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div id="${uuid}" class="container-fluid">
<s:if test="!ajax">
    <div class="well"><h3>Campo <s:property value="campo.id" />[<s:property value="campo.fieldType" />]

    <small class="text-info pull-right">
        <s:a theme="simple" cssClass="btn btn-warning" namespace="/admin" action="metaDichiarazione" method="detail"  >
            <i class="fas fa-arrow-left" aria-hidden="true"></i> Torna alla metaDichiarazione <s:property value="metaDichiarazione.id" />
            <s:param name="metaDichiarazione.id" value="metaDichiarazione.id" />
            <s:param name="procedimento.id" value="procedimento.id" />
        </s:a>
        <small class="text-info">
            <s:a theme="simple" cssClass="btn btn-warning"  namespace="/admin" action="procedimento" method="detail"  >
                <i class="fas fa-arrow-left" aria-hidden="true"></i> Torna al procedimento <s:property value="procedimento.id" />
                <s:param name="procedimento.id" value="procedimento.id" />
            </s:a>

        </small>
    </small></h3>
</div></s:if>
    <s:form theme="simple" cssClass="form-inline" action="campo.action" method="POST" >
        <s:hidden theme="simple" key="campo.id" />
        <s:hidden theme="simple" key="metaDichiarazione.id" />
        <s:hidden theme="simple" key="procedimento.id" />
        <div class="row ">
            <div class="col-md-4 col-xs-12">
                <label class="" for="${uuid}c_nome">Nome</label>
                    <input class="form-control" id="${uuid}c_nome" name="campo.nome" value="<s:property
                    value="campo.nome" />" />
            </div>

            <div class="col-md-4 col-xs-12">
                <label class="" for="${uuid}c_etich">Etichetta</label>
                <input class="form-control" id="${uuid}c_etich" name="campo.etichetta" value="<s:property
                value="campo.etichetta" />" />
            </div>

        </div>
        <div class="row top10">
            <div class="col-md-2 col-xs-12">
                <label class="pull-left" for="${uuid}c_largh">Larghezza</label>
<s:select cssClass="form-control" theme="simple" list="#{'0':'auto','1':'1','2':'2','3':'3','4':'4','5':'5','6':'6','7':'7','8':'8','9':'9','10':'10','11':'11','12':'12'}" key="campo.larghezza" />
            </div>
            <div class="col-md-3">
                <label class="pull-left" for="${uuid}f_min_inst">Min occ</label>
                <div class="col-xs-6">
                    <input type="number" min="0" max="20" id="${uuid}f_min_inst" class="form-control" name="campo.minOccurrences"
                           value="<s:property value="campo.minOccurrences" />"/></div>
            </div>
            <div class="col-md-3">
                <label class="pull-left" for="${uuid}f_max_inst">Max occ</label>
                <div class="col-xs-6">
                    <input type="number" min="1" max="20" id="${uuid}f_max_inst" class="form-control" name="campo.maxOccurrences"
                           value="<s:property value="campo.maxOccurrences" />"/></div>
            </div>
        </div><hr/>
        fieldType:<s:property value="campo.fieldType" />
        <s:if test="@net.mysoftworks.modulecompiler.model.meta.MetaField$BUSINESS_FIELD_TYPE@LIST==campo.fieldType"><s:include
                value="list.jsp" /></s:if>
        <s:if test="@net.mysoftworks.modulecompiler.model.meta.MetaField$BUSINESS_FIELD_TYPE@TEXT==campo.fieldType"><s:include
                value="text.jsp" /></s:if>
        <s:if test="@net.mysoftworks.modulecompiler.model.meta.MetaField$BUSINESS_FIELD_TYPE@FILE==campo.fieldType"><s:include
                value="file.jsp" /></s:if>
        <s:if test="@net.mysoftworks.modulecompiler.model.meta.MetaField$BUSINESS_FIELD_TYPE@TEMPORAL==campo.fieldType"><s:include
                value="datetime.jsp" /></s:if>
        <s:if test="@net.mysoftworks.modulecompiler.model.meta.MetaField$BUSINESS_FIELD_TYPE@ANAG==campo.fieldType"><s:include
                value="anag.jsp" /></s:if>
        <hr />

        <div class="col-md-12 text-right">
            <button class="btn btn-success confirm" type="submit" name="method:saveOrUpdate"><i class="fas fa-save"></i>&nbsp;Salva/Aggiorna</button>
        </div>
    </s:form>
</div>
<script>
    jQuery(document).ready(function () {
        jQuery("#${uuid} #${uuid}f_max_inst").change(function(){
            // jQuery("#${uuid} #${uuid}f_max_inst"). if (1*jQuery(this).val()>1)
        })
    })


</script>