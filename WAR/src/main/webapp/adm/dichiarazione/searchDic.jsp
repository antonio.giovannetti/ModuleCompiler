<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="${uuid}" class="container-fluid">
    <form class="form-horizontal">
        <table>
            <thead>
            <tr><th>Id</th><th>Descrizione</th><th>Data modifica</th><th></th></tr>
            </thead><tbody></tbody>
        </table>
    <div class="collapse striped">
    </div></form>
</div>
<s:url var="dicAssociabili" namespace="/admin" method="dicAssociabili" action="service">
    <s:param name="idProc" value="#parameters.idProc"/></s:url>
<s:url var="linkDic" namespace="/admin" method="linkDic" action="service">
    <s:param name="idProc" value="#parameters.idProc"/></s:url>
<s:url var="linkDic" namespace="/admin" method="linkDic" action="service">
    <s:param name="idProc" value="#parameters.idProc"/></s:url>

<script>
    jQuery(document).ready(function(){
        var ctx = jQuery('#${uuid}');
        var coll = ctx.find(".collapse");
        // https://datatables.net/manual/server-side
        jQuery('table',ctx).DataTable({
            "pageLength":10,
            "lengthMenu": [ 1, 2, 3, 5, 10 ],
            serverSide:true,
            processing:false,
            autoWidth: false,
            ajax: {
                url:'<s:property value="dicAssociabili" />',
                data:function(d,settings){
                    d.q = d.search.value;
                    return d;
                },
             },
            columnDefs:[{
                "targets": 3,
                "searchable": false,
                render:function(data){
                    return '<input type="radio" name="idDic" value="' +
                        data['ID']+'" />';},
                "data": null
            }],
            columns: [
                { "data": "ID" },
                { "data": "DESCRIZIONE" },
                { "data": "DATA_MODIFICA" }
            ],
            "language": {
                "url": MC_UTILS.resourcesUrl + "/js/datatables_1.10.19/lang_it.json"
            }
        });

        return;
        ctx.find('.cerca').keyup(function(){
            if (!jQuery(this).val()) return;
            jQuery.ajax({
                url: '<s:property value="dicAssociabili" />',
                data: {q:jQuery(this).val()},
                success: function(data){
                    if (data.output) {
                        coll.empty();
                        jQuery.each(data.output,function(idx,elem){
                            var d = moment(elem[2]).locale('it').format('LLL');
                            coll.append("<div class='row' data-id-dic="+elem[0]+"><div class='col-xs-8'><input type='radio' name='idDic' value='"+elem[0]
                                +"' />"+elem[0]+" - "+elem[1]+"</div><div class='col-xs-4'>"+d+"</div></div>");
                        })
                        coll.show();
                    } else coll.hide();
                }
            })
        })
    })
</script>