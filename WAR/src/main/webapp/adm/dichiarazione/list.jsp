<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="well"><h3>Lista dichiarazioni</h3></div>
<div id="${uuid}" class="container-fluid">
    <s:form theme="simple" action="procedimento.search" method="POST" >
        <div>
        <s:iterator var="procedimento" value="procedimenti">
            <div class="row" >
                <div class="col-xs-6"><s:property value="#procedimento.descrizione" /></div>
                <div class="col-xs-2"><s:date name="#procedimento.dateInfo.dataCreazione" /></div>
                <div class="col-xs-2"><s:property
                        value="@StatiProcedimento@describeStato(#procedimento.stato)" /></div>
                <div class="col-xs-2"><s:a theme="simple" namespace="/admin" method="detail" action="procedimento" >Vai
                    <s:param name="procedimento.id" value="#procedimento.id" />
                </s:a>
                </div>
            </div>
        </s:iterator></div>
    </s:form>
</div>