<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="${uuid}" class="container-fluid">
    <form class="form-horizontal">
    <div class="form-group">
        <label class="" for="${uuid}cl_cerca">Cerca</label>
        <div class="col-xs-8 pull-right">
            <input id="${uuid}cl_cerca" class="cerca form-control " /></div>
    </div>
    <div class="collapse striped">
    </div></form>
</div>
<s:url var="dicAssociabili" namespace="/admin" method="dicAssociabili" action="service">
    <s:param name="idProc" value="#parameters.idProc"/></s:url>

<script>
    jQuery(document).ready(function(){
        var ctx = jQuery('#${uuid}');
        var coll = ctx.find(".collapse");
        ctx.find('.cerca').keyup(function(){
            if (!jQuery(this).val()) return;
            jQuery.ajax({
                url: '<s:property value="dicAssociabili" />',
                data: {q:jQuery(this).val()},
                success: function(data){
                    if (data.output) {
                        coll.empty();
                        jQuery.each(data.output,function(idx,elem){
                            d = moment(elem[2]).locale('it').format('LLL');
                            coll.append("<div class='row' data-id-dic="+elem[0]+"><div class='col-xs-8'><input type='radio' name='idDic' value='"+elem[0]
                                +"' />"+elem[0]+" - "+elem[1]+"</div><div class='col-xs-4'>"+d+"</div></div>");
                        })
                        coll.show();
                    } else coll.hide();
                }
            })
        })
    })
</script>