<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="${uuid}" class="container-fluid">

<div class="well"><h3><small class="col-xs-3">[Dichiarazione riusabile <s:property value="metaDichiarazione.id" />]</small><em><s:property value="metaDichiarazione.descrizione" default="(descrizione)"/></em>
    <span class="pull-right">
    <span title="Guida alla compilazione" data-id-guida="<s:property value="metaDichiarazione.idGuida" />" class="btn btn-info btn-sm guida"><i class="fas fa-question"></i></span>
    <s:a title="Backup configurazione dichiarazione" namespace="/admin" action="metaDichiarazione" method="backupCfg" cssClass="btn btn-info btn-sm ">
    <s:param value="metaDichiarazione.id" name="metaDichiarazione.id" />
    <i class="fas fa-hdd" aria-hidden="true"></i>&nbsp;Backup</s:a>


        <s:a title="Torna al procedimento" theme="simple" cssClass="btn btn-info btn-sm" namespace="/admin" action="procedimento" method="detail" >
        <s:param name="procedimento.id" value="procedimento.id" />
        <i class="fas fa-arrow-left" aria-hidden="true"></i></s:a></span>
</h3><div class="clearfix"></div>
</div>
    <s:form theme="simple" cssClass="form-horizontal" namespace="/admin" action="metaDichiarazione!save" method="POST" >
        <s:hidden theme="simple" key="procedimento.id" />
        <s:hidden theme="simple" key="metaDichiarazione.id" />
        <div class="col-md-12 text-right">
            <button class="btn btn-success save" type="submit" name="method:save" ><i class="fas fa-save"></i>&nbsp;Salva/Aggiorna</button>
        </div>
<div class="dati-dichiarazione">
        <div class="form-group"><label class="col-md-2 col-xs-6" >Data creazione</label><div class="col-md-4 col-xs-6"><s:date
                name="metaDichiarazione.dateInfo.dataCreazione"
        /></div><label class="col-md-2 col-xs-6 text-right" >Data modifica</label><div class="col-md-4 col-xs-6"><s:date
                name="metaDichiarazione.dateInfo.dataModifica"
        /></div></div>
        <div class="form-group">
            <label class="col-md-2" for="${uuid}d_descrizione">Descrizione</label>
            <div class="col-md-6">
                <input id="${uuid}d_descrizione" class="form-control" name="metaDichiarazione.descrizione"
                       value="<s:property value="metaDichiarazione.descrizione" />"/></div>
        </div>
        <div class="form-group" >
            <label class="col-md-2" for="${uuid}d_present_if">Presente se (XPath)<i title="Presente se" class="btn btn-sm btn-info fas fa-question pull-right" ></i></label>
            <div class="col-md-10">
                <input title="Deve essere valutata come boolean" id="${uuid}d_present_if" class="form-control" name="metaDichiarazione.presentIf"
                       value="<s:property value="metaDichiarazione.presentIf" />"/></div>
        </div>
        <div class="form-group" >
            <label class="col-md-2" for="${uuid}d_mandatory_if">Obbl. se (XPath)<i title="Obbligatorio se" class="btn btn-sm  btn-info fas fa-question pull-right" ></i></label>
            <div class="col-md-10">
                <input title="Deve essere valutata come boolean" id="${uuid}d_mandatory_if" class="form-control" name="metaDichiarazione.mandatoryIf"
                       value="<s:property value="metaDichiarazione.mandatoryIf" />"/></div>
        </div>

        <div class="form-group">
            <div class="col-md-3">
                <label class="pull-left" for="${uuid}d_compilabilita">Compilabilit&agrave;</label>

                <div class="col-md-8">
                    <select multiple class="form-control" id="${uuid}d_compilabilita" name="compilabilita">
                        <s:iterator value="@net.mysoftworks.modulecompiler.model.RuoloPratica$RuoloCompilazionePratica@values()"
                                    var="cmp"><s:if test="#cmp.name()!= 'TUTTI'">
                            <option <s:if test="#cmp.isOn(metaDichiarazione.compilabilita)">selected</s:if>
                                    value="<s:property value="%{#cmp.bytewise()}" />"><s:property
                                    value="#cmp.codice" /></option></s:if>
                        </s:iterator>
                    </select></div>
            </div>

            <div class="col-md-3">
                <label class="pull-left" title="<s:property value="visibilita" />" for="${uuid}d_visibilita">Visibilit&agrave;</label>
                <div class="col-md-8">
                    <select multiple class="form-control" id="${uuid}d_visibilita" name="visibilita">
                        <s:iterator value="@net.mysoftworks.modulecompiler.model.RuoloPratica$RuoloCompilazionePratica@values()"
                                    var="cmp"><s:if test="#cmp.name()!= 'TUTTI'">
                            <option <s:if test="#cmp.isOn(metaDichiarazione.visibilita)">selected</s:if>
                                    value="<s:property value="%{#cmp.bytewise()}" />"><s:property
                                    value="#cmp.codice" /></option></s:if>
                        </s:iterator>
                    </select></div>

            </div>

            <div class="col-md-3">
                <label class="pull-left" for="${uuid}d_max_inst">Max occ</label>
                <div class="col-xs-6">
                    <input type="number" min="1" max="20" id="${uuid}d_max_inst" class="form-control" name="metaDichiarazione.maxOccurrences"
                           value="<s:property value="metaDichiarazione.maxOccurrences" />"/></div>
            </div>
            <div class="col-md-3">
                <label class="text-right" for="d_spunt">Spuntabile</label>
                <s:checkbox id="d_spunt" label="Spuntabile" key="metaDichiarazione.spuntabile" theme="simple" />
            </div>
        </div>

    <div class="form-group cController">
        <label class="col-xs-2" for="${uuid}d_controller">Custom controller</label>
        <div class="col-xs-10"><input id="${uuid}d_controller" class="form-control" name="metaDichiarazione.customController"
                       value="<s:property value="metaDichiarazione.customController" />"/></div>

    </div>

        <div class="form-group form-group">
            <label class="col-md-1" for="${uuid}d_template">Template</label>
            <div class="col-md-8">
                <textarea name="testoTemplate" id="${uuid}d_template" class="tinyMCE col-xs-12" style="height: 20em;font-family: monospace;font-variant-caps: normal" ><s:property value="testoTemplate"/></textarea>
                </div>
            <pre class="col-md-3"><strong>Template engine</strong>:<s:property value="metaDichiarazione.templateEngine.descrizione" />
                <p>Variabili disponibili:
procedimento==&gt;MetaProcedimento
pratica==&gt;Pratica
values==&gt;Map&lt;Long, Value&gt;
ru.renderField('fieldName',boolean[=renderLabel])
uuid==&gt;String
metaDichiarazione==&gt;MetaDichiarazione
                </p>
                <p>Available vars in <strong class="pull-right2">field</strong>
fieldMeta==&gt;MetaField<br/>
                </p></pre>

        </div>
</div>

        <div class="alert campi striped">
            <h4>Campi</h4>
            <h5>Aggiungi
                <s:iterator value="@net.mysoftworks.modulecompiler.model.meta.MetaField$BUSINESS_FIELD_TYPE@values()">
                    <s:a title="Aggiungi" theme="simple" namespace="/admin" action="campo" method="add" cssClass="btn btn-info add-field">
                        <s:param name="metaDichiarazione.id" value="metaDichiarazione.id" />
                        <s:param name="fieldType" value="name()" />
                        <s:param name="procedimento.id" value="procedimento.id" /><i class="<s:property value="icon"/>" aria-hidden="true"></i> <s:property value="desc"/></s:a>
                </s:iterator>
            </h5>
            <div class="row bg-primary unstriped">
                <div class="col-md-3">Nome</div>
                <div class="col-md-6">Id-etichetta</div>
                <div class="col-md-1">Valori</div>
            </div>

                <s:iterator value="campi" var="campo" status="st" >
                    <div class="row top5" data-name="<s:property value="nome" />">
                        <div class="col-md-3">
                            <s:a cssClass="text-left btn btn-info btn-block" theme="simple" method="detail" namespace="/admin" action="campo" >
                                <strong><s:property value="nome" /></strong>
                                <s:param name="idCampo" value="#campo.id" />
                                <s:param name="metaDichiarazione.id" value="metaDichiarazione.id" />
                                <s:param name="procedimento.id" value="procedimento.id" /><i class="fas fa-arrow-alt-circle-right fa-lg pull-right"></i>
                                <i class="pull-left <s:property value="fieldType.icon"/>" aria-hidden="true"></i></s:a></div>
                        <div class="col-md-6"><s:property value="id" />-<strong><s:property value="etichetta" /></strong></div>
                        <div class="col-md-1"><s:property value="nrValues" /></div>



                        <div class="col-md-2 text-right">
                            <s:a data-text="Verranno cancellati anche ${campo.nrValues} valori nelle pratiche. Confermare?" data-title="Cancellazione campo" theme="simple" namespace="/admin" action="campo" method="delete"
                                 cssClass="btn btn-sm btn-danger delete" ><i class="fas fa-trash" ></i>
                                <s:param name="idCampo" value="#campo.id" />
                                <s:param name="metaDichiarazione.id" value="metaDichiarazione.id" />
                                <s:param name="procedimento.id" value="procedimento.id" />
                            </s:a></div>

                            <%--<div class="btn btn-sm btn-danger">Elimina</div></div>--%>
                            <%--<div class="col-md-1"><div class="btn btn-sm btn-warning">Disassocia</div></div>--%>
                    </div>

                </s:iterator>
            <%--</s:iterator>--%>
        </div>
        <div class="col-md-12 text-right">
            <button class="btn btn-success save" type="submit" name="method:save" ><i class="fas fa-save"></i>&nbsp;Salva/Aggiorna</button>
        </div>
    </s:form>
    <div class="collapse xpathHelp">
        <span class="col-xs-12"><a target="_blank" href="https://www.w3schools.com/xml/" class="btn btn-link text-right">XPath reference</a></span>
        <label class="col-xs-3">Id Dichiarazione:</label>
        <%--<s:textfield type="number" cssClass="dicId" value="metaDichiarazione.id"></s:textfield><br />--%>
        <input type="number" class="dicId" value="<s:property value="metaDichiarazione.id" />" /><br />
        <label class="col-xs-3">Id field:</label> <input type="number" class="fieldId" /><br />
        <span class="col-xs-3">BigDecimal:</label> <input type="radio" name="valType" value="bigDValue"/></span>
        <span class="col-xs-3">Date:<input type="radio" name="valType" value="dateValue"/></span>
        <span class="col-xs-3">Boolean:<input type="radio" name="valType" value="booleanValue"/></span>
        <span class="col-xs-3">String:<input type="radio" name="valType" value="stringValue"/></span>
        <textarea name="xpath" value=""  class="col-xs-12"></textarea><div class="clearfix"></div>

     </div>
</div>
<s:url var="formGuida" namespace="/admin" method="formGuida" action="service">
    <s:param name="metaDichiarazione.id" value="metaDichiarazione.id" /></s:url>
<s:url var="saveOrUpdateGuidaDich" namespace="/admin" method="saveOrUpdateGuidaDich" action="service">
    <s:param name="idDic" value="metaDichiarazione.id"/></s:url>
<s:url var="checkSaveDich" namespace="/admin" method="checkSaveDich" action="service" >
    <s:param name="idDic" value="metaDichiarazione.id" suppressEmptyParameters="true"/>
    <s:param name="idProc" value="procedimento.id" suppressEmptyParameters="true"/></s:url>
<script>
    jQuery(document).ready(function(){
        var ctx = jQuery('#${uuid}');
        ctx.find(".xpathHelp input").change(function(){
            var container = ctx.find(".xpathHelp");
            // /Pratica/DichiarazioneContainer/Dichiarazione[@idMetaDichiarazione='16']/values/entry[key/text()='124']/value/rawValues/bigDValue > 2
            var xpath = "/Pratica/DichiarazioneContainer/Dichiarazione";
            if (1*container.find(".dicId").val()) {
                xpath+="[@idMetaDichiarazione='" + container.find(".dicId").val() +"']";
            }
            xpath+="/values/entry";
            if (1*container.find(".fieldId").val()) {
                xpath+="[key/text()='" + container.find(".fieldId").val() +"']";
            }
            xpath+="/value/rawValues/";
            var type = container.find("[name='valType']:checked").val() || "";
            xpath+=type;
            container.find("textarea").val(xpath);
        })
        ctx.find(".campi .row.top5").on("dragstart",function (ev) {
            ev.originalEvent.dataTransfer.setData("text/plain", "\${ru.renderField('" + jQuery(this).data().name + "',true)}");

        });
        ctx.find('.dati-dichiarazione label i').click(function () {
            var me=jQuery(this);
            MC_MODAL.info({
                title: 'XPath help <em>' + me.attr("title")+"</em>",
                elem: ctx.find(".xpathHelp").show()
            }).promise.then(function(button,modal){
                var xpath = modal.find("textarea").val();
                me.parent().next().find("input").val(xpath);

            })
        })
        ctx.find(".campi .delete")
            .click(function(e){
                e.preventDefault();
                jQuery(e.target).data("confirm-ok",function() {
                    jQuery.ajax(e.currentTarget.href, {
                        success: function () {
                            window.location.reload(true);
                        }
                    })
                });
                new MC_MODAL.confirm(e);
            });
        ctx.find(".guida").click(function (e) {
            var _nfo = MC_MODAL.info({
                title: '<s:text name="fe.pratica.dich.guida_compilazione.title" />',
                urlData:jQuery(this).data(),
                url: '<s:property value="formGuida" />'
            });
            _nfo.ajaxContentPromise.then(function (value) {
                MC_UTILS.addRichEditor(_nfo.modal);
            })
            _nfo.promise.then(function (button, modal) {
                modal.find('form').ajaxSubmit({
                    url: '<s:property value="saveOrUpdateGuidaDich" />'
                })
            })
        })
        ctx.find(".save").click(function(e){
            e.preventDefault();
            jQuery.ajax({
                url: '<s:property value="checkSaveDich"  />',
                success:function(data){
                    var opt = jQuery(e.target);
                    opt.data("confirm-text",data.messages[0])
                        .data("confirm-ok",function(e2) {
                            ctx.find('form').ajaxSubmit({
                                dataType:'json',
                                success:function () {
                                    window.location.reload(true);
                                },
                                error:function (data) {
                                    MC_MODAL.printMessages(data.responseJSON);
                                }
                            })
                        });

                    new MC_MODAL.confirm(e);
                }
            })
        });


        return;


        MC_UTILS.addRichEditor(ctx,{
            // extended_valid_elements : "a[class|name|href|target|title|onclick|rel],script[type|src],iframe[src|style|width|height|scrolling|marginwidth|marginheight|frameborder],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],#if",
            // valid_elements:"*",
            // valid_children:"*",
            //
            // entity_encoding : "raw",
            // encoding:"xml",
            // plugins: "code",
            // toolbar: "code",
            // menubar: "tools",
            plugins: 'codesample code ',
            codesample_languages: [
                {text: 'HTML/XML', value: 'markup'},
                {text: 'JavaScript', value: 'javascript'},
                {text: 'CSS', value: 'css'}
            ],
            toolbar: 'codesample code'

        });
    })
</script>


