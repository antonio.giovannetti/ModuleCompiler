<%@ taglib prefix="s" uri="/struts-tags" %>
<label class="col-xs-4 text-right" title="<s:property value="visibilita" />" for="${uuid}p_visibilita">Visibilita
    :<s:property value="visibilita" />-<s:property value="#visibilita" />
</label>
<div class="col-xs-8"><s:debug />
<select multiple class="form-control" id="${uuid}p_visibilita">
    <s:iterator value="@net.mysoftworks.modulecompiler.model.RuoloPratica$RuoloCompilazionePratica@values()"
                var="cmp">
        <option <s:if test="%{(visibilita band #cmp.bytewise()) != 0}">selected</s:if>
                value="<s:property value="%{#cmp.bytewise()}" />"><s:property
                value="#cmp.codice" />-<s:property value="%{(visibilita band #cmp.bytewise()) != 0}" /></option>
    </s:iterator>
</select></div>
