<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="${uuid}">
<% if  (request.isUserInRole("owner")) { %>
<div class="panel panel-default collapsible">
    <div class="panel-heading"><h4><s:text name="fe.pratica.list.header_creazione" /></h4></div>
    <div class="panel-body">
    <table class="stripe responsive">
        <thead>
        <tr>
            <th><s:text name="fe.procedimento.titolo" /></th>
        </tr>
        </thead>
        <tbody>
        <s:iterator var="procedimento" value="procedimenti">
            <s:if test="stato != 0">
                <tr class="top10">
                    <s:url var="creaPratica" namespace="/fe" action="pratica" method="crea" ><s:param name="idProcedimento" value="id" /></s:url>
                    <td><a href="<s:property value="creaPratica" />" title="<s:text name="fe.pratica.list.titolo_btn_crea" /> pratica <s:property value="#procedimento.descrizione" />" class="col-xs-11 btn-link confirm">
                        <i class="fa-2x <s:property value="#procedimento.icon" />"></i>&nbsp;<s:property value="#procedimento.descrizione" />
                    </a></td>
                </tr></s:if>
        </s:iterator>


        </tbody>
    </table>

        <%--<s:iterator var="procedimento" value="procedimenti">--%>
        <%--<s:if test="stato != 0">--%>
        <%--<div class="row top10">--%>
            <%--<s:url var="creaPratica" namespace="/fe" action="pratica" method="crea" ><s:param name="idProcedimento" value="id" /></s:url>--%>
            <%--<a href="<s:property value="creaPratica" />" title="<s:text name="fe.pratica.list.titolo_btn_crea" /> pratica <s:property value="#procedimento.descrizione" />" class="col-xs-11 btn-link confirm">--%>
                <%--<i class="fa-2x <s:property value="#procedimento.icon" />"></i>&nbsp;<s:property value="#procedimento.descrizione" />--%>
            <%--</a>--%>
            <%--&lt;%&ndash;<% } else { %>&ndash;%&gt;--%>
            <%--&lt;%&ndash;<div class="col-xs-11"><i class="fa-2x <s:property value="#procedimento.icon" />"></i>&nbsp;<s:property value="#procedimento.descrizione" /></div>&ndash;%&gt;--%>
        <%--</div></s:if>--%>
    <%--</s:iterator>--%>


    </div>
</div>
<% } else {%>
<div class="alert alert-danger" role="alert">
    <s:text name="fe.pratica.list.noowner" ></s:text>
</div>
<% } %>
<%--
<s:if test="!pratiche.isEmpty">
<div class="panel panel-default collapsible pratica-list">
    <div class="panel-heading"><h4><s:text name="fe.pratica.list.header_modifica" /></h4></div>
    <div class="panel-body">
    <s:iterator var="p" value="pratiche">
        <s:set var="owner" value="isOwner(ruoli)" />
        <div class="row top10 text-left <s:if test="!owner">no-owner</s:if> ">
            <div class="col-xs-6">
            <s:url var="linkPratica" namespace="/fe" action="pratica" method="detail" >
                <s:param name="pratica.id" value="#p.id" />
            </s:url>

            <a href="<s:property value="linkPratica" />" <s:if test="owner">title="<s:text name="fe.pratica.list.titolo_btn_modifica_owner" />"</s:if>
                    <s:else>title="<s:text name="fe.pratica.list.titolo_btn_modifica_noowner" />"</s:else>
                    class='btn <s:if test="owner">btn-primary</s:if><s:else>btn-info</s:else> btn-block'>
                <span class="pull-left"><i class="fa-2x <s:property value="#p.procedimento.icon" />"></i>&nbsp;<s:property value="#p.procedimento.descrizione" /></span>
                <i class="fas fa-arrow-alt-circle-right fa-2x pull-right" aria-hidden="true"></i>
                <div class="clearfix"></div>
            </a>
            </div><div class="col-xs-5 col-sm-4"><b class="">[<s:date name="#p.dateInfo.dataCreazione" />, <s:date name="#p.dateInfo.dataModifica" />]</b></div>

            <div class="col-xs-1 col-sm-2 text-right">
                <s:if test="%{canPrint(#attr.p.ruoli)}">
                <s:url var="urlStampa" namespace="/fe" action="pratica" method="stampa" ><s:param name="pratica.id" value="#p.id" /></s:url>
                <a title="<s:text name="common.btn.print" />" href="<s:property value="urlStampa" />" class="btn btn-default btn-sm"><i class="fas fa-print" aria-hidden="true"></i></a>
                </s:if>
                <s:if test="owner">
                <s:a data-text="Cancellare la pratica <b>%{#p.id} - %{#p.procedimento.descrizione}</b>?" title="Cancella" theme="simple" namespace="/fe" action="pratica" method="delete" cssClass="btn btn-danger btn-sm confirm">
                    <s:param name="pratica.id" value="#p.id" />
                    <i class="fas fa-trash" aria-hidden="true"></i></s:a></s:if>

            </div>
        </div>
    </s:iterator></div>
</div></s:if><s:else>
    <div class="alert alert-danger" role="alert">
        <s:text name="fe.pratica.list.empty" ></s:text>
    </div>

</s:else>

--%>

<s:if test="!pratiche.isEmpty">
    <div class="panel panel-default collapsible pratica-list">
    <div class="panel-heading"><h4><s:text name="fe.pratica.list.header_modifica" /></h4></div>
    <div class="panel-body">
        <table class="stripe responsive">
            <thead>
            <tr>
                <th >Procedimento</th>
                <th >Data creazione</th>
                <th >Data modifica</th>
                <th ></th>
            </tr></thead>
            <tbody><s:iterator var="p" value="pratiche">
                <s:set var="owner" value="isOwner(ruoli)" />
                <s:url var="linkPratica" namespace="/fe" action="pratica" method="detail" ><s:param name="pratica.id" value="#p.id" /></s:url>
                <tr>
<td>
    <a href="<s:property value="linkPratica" />" <s:if test="owner">title="<s:text name="fe.pratica.list.titolo_btn_modifica_owner" />"</s:if>
       <s:else>title="<s:text name="fe.pratica.list.titolo_btn_modifica_noowner" />"</s:else>
       class='btn <s:if test="owner">bg-primary</s:if><s:else>bg-info</s:else> btn-block'>
        <span class="text-left col-xs-12"><i class="fa-2x <s:property value="#p.procedimento.icon" />"></i>&nbsp;<s:property value="#p.procedimento.descrizione" /></span>
        <div class="clearfix"></div>
    </a></td>
                    <td><b class=""><s:date name="#p.dateInfo.dataCreazione" /></b></td>
                    <td><b class=""><s:date name="#p.dateInfo.dataModifica" /></b></td>
                    <td>
                        <s:if test="%{canPrint(#attr.p.ruoli)}">
                            <s:url var="urlStampa" namespace="/fe" action="pratica" method="stampa" ><s:param name="pratica.id" value="#p.id" /></s:url>
                            <a title="<s:text name="common.btn.print" />" href="<s:property value="urlStampa" />" class="btn btn-default btn-sm"><i class="fas fa-print" aria-hidden="true"></i></a>
                        </s:if>
                        <s:if test="owner">
                            <s:a data-text="Cancellare la pratica <b>%{#p.id} - %{#p.procedimento.descrizione}</b>?" title="Cancella" theme="simple" namespace="/fe" action="pratica" method="delete" cssClass="btn btn-danger btn-sm confirm">
                                <s:param name="pratica.id" value="#p.id" />
                                <i class="fas fa-trash" aria-hidden="true"></i></s:a></s:if>

                    </td>
                </tr></s:iterator></tbody>
        </table>

    </div>

    </div>
 </s:if>
</div>


<script>
    jQuery(document).ready(function () {
        jQuery('#${uuid} table').DataTable({
            "language": {
                "url": MC_UTILS.resourcesUrl + "/js/datatables_1.10.19/lang_it.json"
            }
        });
        MC_UTILS.createStandardWebSocketCall({url:"/wsk/user",
            msgFormatter:function(data){
                var compiled = _.template('<s:text name="admin.dichiarazione.change.assignee" escapeJavaScript="true"/>');
                var _msg = "<s:text name="admin.dichiarazione.change.assignee" escapeJavaScript="true"/>"; // TODO: introdurre un sistema di js templating
                return compiled(jQuery.extend(data, {link:'<s:property value="linkPratica" />'.replace(/[^=]*$/,'')}))
            }});
    })


</script>