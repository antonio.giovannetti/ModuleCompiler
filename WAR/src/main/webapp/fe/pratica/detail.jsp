<%@ taglib prefix="s" uri="/struts-tags" %>
<s:if test="pratica.ruoli.size>1">
    <h2>Soggetti coinvolti </h2>
    <s:iterator value="pratica.ruoli">
        <div>
            <span>User:<strong><s:property value="user" /></strong>, ruolo/i:<strong><s:property value="@net.mysoftworks.modulecompiler.model.RuoloPratica$RuoloCompilazionePratica@describeFigure(roleBitwise)" /></strong></span>
        </div>
    </s:iterator></s:if>


<s:set var="noowner" value="!owner" />
<div id="${uuid}" >
<h1 ><i class="<s:property value="procedimento.icon" />"></i> <s:property value="procedimento.descrizione" />
    <s:if test="procedimento.idGuida!=null">
    <span class="btn btn-default guida" data-id-guida="<s:property value="procedimento.idGuida" />">
        <i class="fas fa-info-circle fa-2x"></i>
    </span></s:if>

    <span class="pull-right">
        <s:if test="%{canPrint()}">
        <s:url var="urlStampa" namespace="/fe" action="pratica" method="stampa" ><s:param name="pratica.id" value="pratica.id" /></s:url>
        <a title="<s:text name="common.btn.print" />" href="<s:property value="urlStampa" />" class="btn btn-default btn-sm"><i class="fas fa-print fa-2x" aria-hidden="true"></i></a></s:if>
        <s:url var="urlBackList" namespace="/fe" action="praticaList" ></s:url>
        <a title="<s:text name="common.btn.back_list" />"  href="<s:property value="urlBackList" />" class="btn btn-default btn-sm"><i class="fas fa-list fa-2x" aria-hidden="true"></i></a>

    </span><div class="clearfix"></div>
</h1>

<s:iterator value="dichiarazioni" var="dichiarazione">
<s:set var="metaDichiarazione" value="metaDichiarazione(idMetaDichiarazione)" />
    <s:if test="%{canView(#attr.dichiarazione)}">
<div title="Dichiarazione <s:property value="id" />" data-mxo="<s:property value="#attr.metaDichiarazione.maxOccurrences" />" data-id-meta="<s:property value="#attr.metaDichiarazione.id" />" data-dichiarazione.id="<s:property value="id" />" class="panel panel-default collapsible dichiarazione dichiarazione-<s:property value="#attr.metaDichiarazione.id" />">
    <div class="panel-heading">
        <s:if test="#attr.metaDichiarazione.spuntabile">
            <label class="switch">
                <input type="checkbox" checked class="collapsible_handler">
                <span class="slider round"></span>
            </label>
            <%--<input class="pull-left collapsible_handler" type="checkbox" />--%>
        </s:if>
        <span class="testo"><span class="badge"></span><s:property value="#attr.metaDichiarazione.descrizione" />
        </span>



        <div class="btn-toolbar pull-right" role="toolbar" aria-label="...">
            <s:if test="#attr.metaDichiarazione.idGuida!=null"><span title="Guida alla compilazione" class="btn btn-sm btn-default guida" data-id-guida="<s:property value="#attr.metaDichiarazione.idGuida" />">
          <i class="fas fa-info-circle fa-2x"></i>
    </span></s:if>
        <div class="btn-group" role="group" aria-label="...">
    <s:if test="%{canChangeCompiler(#attr.dichiarazione)}"><span title="Cambia compilatore" class="btn btn-default btn-sm change-auth-btn"><i class="fas fa-user-edit fa-2x"></i></span></s:if>

            <%--<%if (request.isUserInRole("editor")) { %>--%>
    <%--<span title="Cambia compilatore" class="btn btn-default btn-sm change-auth-btn"><i class="fas fa-user-edit"></i></span>--%>
    <%--<% } %>--%>
    <%--<%if (request.isUserInRole("compiler")) { %>--%>
        <s:if test="%{canChangeCompiler(#attr.dichiarazione) && #attr.metaDichiarazione.maxOccurrences>1}"><span title="Duplica" class="btn btn-default btn-sm addDic"><i class="far fa-clone fa-2x"></i></span></s:if>
    <%--<% } %>--%>

            <s:if test="%{canDelete(#attr.dichiarazione)}"><span title="Cancella dichiarazione" data-dichiarazione.id="<s:property value="key"/>" class="btn btn-default btn-sm btn-danger removeDic"><i class="fas fa-trash fa-2x"></i></span></s:if>

</div>

            <s:if test="%{canModify(#attr.dichiarazione)}">
                <span title="Modifica" class="btn btn-default btn-sm editDic btn-icon-text"><i class="fas fa-edit fa-2x"></i><s:text name="fe.pratica.dich.btn_modifica" /></span>
                <div class="btn-group gr-save-undo hide" role="group" aria-label="...">
<span title="Annulla" class="btn btn-default btn-sm cancelEdit btn-icon-text"><i class="fas fa-window-close fa-2x"></i><s:text name="fe.pratica.dich.btn_annulla" /></span>
<span title="Salva" class="btn btn-success btn-sm saveDic btn-icon-text"><i class="fas fa-save fa-2x"></i><s:text name="fe.pratica.dich.btn_salva" /></span>
            </div></s:if><s:else>
            <s:if test="#attr.dichiarazione.compilatore!=null">
            <div title="Dati compilatore" data-user-id="<s:property value="#attr.dichiarazione.compilatore" />"
                 class="btn btn-default btn-sm user-info" >
                <%--<i class="far fa-user"></i>--%>
                    <%--<span class="fa-stack ">--%>
  <i class="far fa-user fa-2x"></i>
  <i class="far fa-question-circle fa-1x"></i>
<%--</span>--%>
            </div></s:if></s:else></div><div class="clearfix"></div>
    </div>
    <div class="panel-body <s:if test="dichiarazioni.size>3 && noowner">collapse</s:if>"></div>
</div></s:if>


</s:iterator>
    <div class="change-auth-dialog collapse">
        <s:form theme="simple" action="dichiarazione!changeCompiler" namespace="/fe">
            <input name="dichiarazione" type="hidden"/>
            <s:hidden name="pratica.id" key="id"/>
        <input class="form-control" /><input name="user" type="hidden"/></s:form>
    </div>
</div>

<s:url var="compilerList" namespace="/fe" action="dichiarazione" method="compilerList" >
    <s:param name="pratica.id" value="pratica.id" /></s:url>

<s:url var="removeDich" namespace="/fe" action="dichiarazione" method="remove" >
    <s:param name="pratica.id" value="pratica.id" /></s:url>
<s:url var="editDich" namespace="/fe" action="dichiarazione" method="edit" >
    <s:param name="pratica.id" value="pratica.id" /></s:url>
<s:url var="viewDich" namespace="/fe" action="dichiarazione" method="view" >
    <s:param name="pratica.id" value="pratica.id" /></s:url>
<s:url var="saveDich" namespace="/fe" action="dichiarazione" method="save" >
    <s:param name="pratica.id" value="pratica.id" /></s:url>
<s:url var="guida" namespace="/fe" action="pratica" method="guida" >
    <s:param name="procedimento.id" value="procedimento.id" /></s:url>

<s:url var="linkPratica" namespace="/fe" action="pratica" method="detail" ></s:url>


<script>
    jQuery(document).ready(function () {
        var ctx = jQuery('#${uuid}');
        MC_UTILS.createStandardWebSocketCall({url:"/wsk/pratica/<s:property value="pratica.id" />",
            msgFormatter:function(data){
                return "Il procedimento "+data.descProc+" &#232; stato modificato. La pagina verr&#224; ricaricata";
            }});
        MC_UTILS.createStandardWebSocketCall({url:"/wsk/user",
            msgFormatter:function(data){
                var compiled = _.template('<s:text name="admin.dichiarazione.change.assignee" escapeJavaScript="true"/>');
                var _msg = "<s:text name="admin.dichiarazione.change.assignee" escapeJavaScript="true"/>"; // TODO: introdurre un sistema di js templating
                return compiled(jQuery.extend(data, {link:'<s:property value="linkPratica" />?pratica.id='.replace(/[^=]*$/,'')}))

                <%--_msg--%>
                    <%--.replace(/\{0\}/,'<s:property value="linkPratica" />?pratica.id=')--%>
                    <%--.replace(/\{1\}/g,data.idPratica)--%>
                    <%--.replace(/\{2\}/g,data.idDich);--%>
            }});

        var reNumDic = function (){
            var map = {},cursors = {},re = /.*dichiarazione-(\d+).*/;
            jQuery(".dichiarazione",ctx).each(function(idx,elem){
                var idx = elem.className.replace(re,"$1");
                if (!map[idx]) map[idx]=1; else map[idx]++;
                cursors[idx]=1;
             })
            jQuery(".dichiarazione",ctx).each(function(idx,elem){
                var idx = elem.className.replace(re,"$1");
                if (map[idx]!==1) {
                    jQuery(elem).find(".panel-heading .testo .badge").text(cursors[idx] + " di " + map[idx]);
                } else {
                    jQuery(elem).find(".panel-heading .testo .badge").text("");
                }
                cursors[idx]++;
            })
        }
        reNumDic();
        ctx
            .on('click','.editDic',function(e){
            e.preventDefault();
            var panel = jQuery(this).closest('.panel');
            MC_UTILS.checkSessionTimeout().then(
                function(){
                    var body = panel.find('.panel-body').addClass('bg-danger');
                    panel.find(".btn.editDic").addClass("hide");
                    panel.find(".btn-group.hide").removeClass("hide");
                    jQuery.ajax({
                        url: '<s:property value="editDich" />',
                        data: panel.data(),
                        success: function(resp){
                            if (panel.is(".collapsible") && !panel.find(".panel-body").is(":visible")) {
                                panel.find("._handler").trigger('click');
                            }
                            body.html(resp);
                        }
                    });
                },function(){
                    MC_MODAL.info({buttons:MC_MODAL.buttons.okCancelButtons, modalType:MC_MODAL.modalTypes.warning,text:"La sessione HTTP \u00E8 scaduta, si prega di ricaricare la pagina"})
                        .promise.then(function () {
                            window.location.reload(true);
                        })
                }
            );

            })
            .on('click','.cancelEdit',function(e) {
                e.preventDefault();
                var panel = jQuery(this).closest('.panel');
                var body = panel.find('.panel-body').removeClass('bg-danger');
                body.load('<s:property value="viewDich" />',body.parent().data(),function(){
                    panel.find(".btn.editDic").removeClass("hide");
                    panel.find(".btn-toolbar .gr-save-undo").addClass("hide");
                });
            })
            .on('click','.saveDic',function(e) {
                var panel = jQuery(this).closest('.panel');
                panel.find('.panel-body form').trigger('submit');
            })
            .on('click','.addDic',function(e){
                var me = jQuery(this);
                var panel = me.closest('.panel');
                jQuery(e.target)
                    .data("confirm-title",'Duplicare la dichiarazione?')
                    .data("confirm-ok",function () {
                        var panels = ctx.find(".panel.dichiarazione.dichiarazione-" + panel.data()['idMeta']);
                        var mxo = 1*panel.data()['mxo'];
                        if (mxo > panels.length) {
                            var newDich = panel.before(panel.clone(true)).addClass("clone panel-danger").find(".panel-body").removeClass("collapse");
                            newDich.find('.editDic').trigger('click');
                            reNumDic();
                        } else {
                            MC_MODAL.info({buttons:MC_MODAL.buttons.okButton, modalType:MC_MODAL.modalTypes.warning,text:"Raggiunto il numero massimo di dichiarazioni:" + mxo})
                        }


                    });
                new MC_MODAL.confirm(e);

                    // .find("form").append("<input type='hidden' name='clone' value='true' />")
            })
            .on('click','.removeDic',function(e){
                e.preventDefault();
                var that = this;
                var panel = jQuery(this).closest('.panel');
                var panels = ctx.find(".panel.dichiarazione.dichiarazione-" + panel.data()['dichiarazione.id'])
                if (panels.length == 1) {
                    MC_MODAL.info({ buttons:MC_MODAL.buttons.okButton, modalType:MC_MODAL.modalTypes.warning,text:"Impossible eliminare l'unica dichiarazione del tipo <b>"+
                        panel.find('.panel-heading .testo').text()+"</b>"})
                    return;
                }
                MC_UTILS.checkSessionTimeout().then(
                    function() {
                        jQuery(e.target)
                            .data("confirm-ok",function () {
                                if (panel.hasClass('clone')) {
                                    panel.remove();
                                    reNumDic();
                                } else {
                                    jQuery.ajax({
                                        url: '<s:property value="removeDich" />',
                                        data: panel.data(),
                                        success: function () {
                                            panel.remove();
                                            reNumDic();
                                        }
                                    })
                                }
                            });
                        new MC_MODAL.confirm(e);
                    },function(){
                        MC_MODAL.info({buttons:MC_MODAL.buttons.okCancelButtons, modalType:MC_MODAL.modalTypes.warning,text:"La sessione HTTP \u00E8 scaduta, si prega di ricaricare la pagina"})
                            .promise.then(function () {
                            window.location.reload(true);
                        })
                    })

                // .find("form").append("<input type='hidden' name='clone' value='true' />")
            })
        <%--.on('click','.cancelEdit',function(e){--%>
                <%--e.preventDefault();--%>
                <%--var body = jQuery(this).closest('.panel-body').removeClass('bg-danger');--%>
                <%--body.load('<s:property value="viewDich" />',body.parent().data());--%>

        <%--})--%>
            .on('click','.change-auth-btn',function(e){
            e.preventDefault();
            var dlg = jQuery(".change-auth-dialog",ctx);
            var panel = jQuery(this).closest(".panel");
            var data = panel.data();
            var _btns = MC_MODAL.buttons.okCancelButtons.slice();
            _btns.push(new MC_MODAL.BUTTONTYPE("Assegna a me", "btn btn-info","button.assign-to-me"))
            var _nfo = MC_MODAL.info({buttons:_btns, modalType:MC_MODAL.modalTypes.info,
                title:'<s:text name="fe.pratica.dich.change_compiler.dlgtitle" escapeHtml="false" />',
                elem:dlg.show()});
            dlg.find("input.form-control")
                .val('')
                .autoComplete({
                    source: function(term, response){
                        jQuery.getJSON('<s:property value="compilerList" />', {q:term}, function(data){
                            response(data.output.data); });
                    },
                    renderItem:function (item, search){
                        var label = item.username + " [" +item.firstName + " "+item.lastName + ']';
                        return '<div class="autocomplete-suggestion" data-id="'+ item.id+'" data-val="' + label + '">'
                            + label + '</div>';
                    },
                    onSelect: function(e, term, item){
                        dlg.find("input[name='user']:hidden").val(item.data("id"));
                        _nfo.modal.find(".btn-success").removeAttr("disabled")
                    }
                });


            _nfo.promise.then(function (button,modal) {
                if (button.key==='button.assign-to-me') {
                    dlg.find('form:hidden').val();
                }
                if (button.key==='button.ok') {
                    var userId = modal.find('form input[name=\'user\']:hidden').val();
                    modal.find('form').ajaxSubmit({data:data,
                    success: function(){
                        panel.find(".user-info").data("user-id",userId);
                        MC_UTILS.addUserInfo({elem:panel.find(".user-info"),title:"Compilatore"});
                    }});
                }
                dlg.find("input.form-control").autoComplete("destroy");
            }).fail(function(){
                dlg.find("input.form-control").autoComplete("destroy");
            });
            _nfo.modal.find(".btn-success").attr("disabled","disabled")


        });

        ctx.on('submit','form',function(e){
            e.preventDefault();
            var form = jQuery(this);
            MC_UTILS.checkSessionTimeout().then(
                function(){
                    var panel = form.closest('.panel');
                    var clone = panel.hasClass("clone");
                    var url = '<s:property value="saveDich" />';
                    if (clone) url+="&clone=true";
                    form.ajaxSubmit({
                        url:url,
                        success:function(){
                            var body = form.closest('.panel-body').removeClass('bg-danger');
                            body.load('<s:property value="viewDich" />',body.parent().data(),function(){
                                panel.find(".btn.editDic").removeClass("hide");
                                panel.find(".btn-toolbar .gr-save-undo").addClass("hide");
                            })
                        },
                        error: function (data) {
                            MC_MODAL.printMessages(data.responseJSON);
                        }
                    })
                },function(){
                    MC_MODAL.info({buttons:MC_MODAL.buttons.okCancelButtons, modalType:MC_MODAL.modalTypes.warning,text:"La sessione HTTP \u00E8 scaduta, si prega di ricaricare la pagina"})
                        .promise.then(function () {
                        window.location.reload(true);
                    })
                }
            );



        })
            .find(".panel.dichiarazione").each(function(){
            var body = jQuery(this).children('.panel-body');
            jQuery.ajax({
                contentType:'application/x-www-form-urlencoded; charset=ISO-8859-1',
                url: '<s:property value="viewDich" />',
                data: body.parent().data(),
                success : function(content){
                    body.html(content);
                }
            });

            MC_UTILS.addUserInfo({elem:jQuery(this).find(".user-info"),title:"Compilatore"});
        });

        ctx.find(".guida").click(function (e) {
            var data = jQuery(this).data();
            MC_UTILS.checkSessionTimeout().then(
                function() {
                    MC_MODAL.info({
                        url: '<s:property value="guida" />',
                        urlData: data,
                        title: '<s:text name="fe.pratica.dich.guida_compilazione.title" />',
                        // elem: content.show(),
                        buttons: MC_MODAL.buttons.chiudiButton
                    })
                })
        })

    })
</script>
