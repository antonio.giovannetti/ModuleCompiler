<div class="alert alert-info">

<h4>Utenze di prova</h4>
<table style="width:50%;font-size: 1em" class="table table-striped" >
    <thead>
    <tr>
        <th>User</th>
        <th>Pwd</th>
        <th>Roles</th>
        <th>Note</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>a</td>
        <td>a</td>
        <td>editor,compiler</td>
        <td>Può creare e modificare i procedimenti</td>
    </tr>
    <tr>
        <td>b</td>
        <td>b</td>
        <td>compiler,owner</td>
        <td>Può creare pratiche(owner) e compilarne sezioni (compiler)</td>
    </tr>
    <tr>
        <td>c</td>
        <td>c</td>
        <td>viewer</td>
        <td>Può visualizzare pratiche</td>
    </tr>
    </tbody>
</table></div>