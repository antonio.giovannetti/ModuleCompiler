<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page import="org.keycloak.KeycloakSecurityContext" %>
<%@ page import="org.keycloak.representations.IDToken" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.HashSet" %>
<html>
<head>
    <jsp:include page="common/head.jsp" />
    <title>MC home page</title>
    <style>
        /*body {overflow: hidden}*/
        .homeBlock .well:hover {
            transition: all 400ms ease;
            background-color: #b3ffb3;
        }
        .homeBlock .well{
            height: 10em;
            overflow: hidden;
            transition: all 0.4s ease;
            font-variant:small-caps;
            background-color: #75AA75;
        }
        .homeBlock p {
            margin-top: 1em;
            height:2em;
            overflow: auto;
            font-size: x-large;
            font-family: monospace;
            font-style: italic;
        }
        .homeBlock .well a{
            font-size: 1.2em;
        }
        .homeBlock .well .btn-default{
            background-color: #cce5cd;
        }
    </style>
</head>
<s:if test="#attr.parameters.status!=null"><body class="container-fluid error bg-danger"></s:if>
<s:else><body class="container-fluid"></s:else>
<jsp:include page="common/header.jsp" />
<s:if test="#attr.parameters.status!=null"><h1 class="error-status">Errore:<s:property value="#attr.parameters.status" /></h1></s:if>

<% if ("true".equals(System.getProperty("include.demo.data"))) { %>
<jsp:include page="userLegend.jsp" />
<% } %>

<div class="row">
    <div class=" col-md-4  col-xs-12 homeBlock"><div class="well"><i class="pull-left text-success fas fa-4x fa-wrench" aria-hidden="true"></i>
        <h3 class="pull-right">Procedimento</h3 ><div class="clearfix"></div>
        <p>Configurazione dei procedimenti</p>
        <a class="btn btn-success pull-right" href="admin/procedimento!list.action" class="pull-right" >Vai&nbsp;<i class="fas fa-arrow-alt-circle-right text-info" aria-hidden="true"></i></a>
    </div></div>
    <div class="col-md-4 col-xs-12 homeBlock"><div class="well"><i class="pull-left text-success fas fa-4x fa-edit" aria-hidden="true"></i>
        <h3 class="pull-right">Compilazione</h3 ><div class="clearfix"></div>
        <p>Compilazione e modifica pratiche</p>
        <a href="fe/praticaList.action" class="btn btn-success pull-right" >Vai&nbsp;<i class="fas fa-arrow-alt-circle-right text-info" aria-hidden="true"></i></a>
    </div></div>
    <div class="col-md-4 col-xs-12 homeBlock"><div class="well"><i class="pull-left text-success fas fa-4x fa-users" aria-hidden="true"></i>
        <h3 class="pull-right">Keycloak</h3 ><div class="clearfix"></div>
        <p>Gestione utenze</p>
        <a href="/auth" class="btn btn-success pull-right" >Vai&nbsp;<i class="fas fa-arrow-alt-circle-right text-info" aria-hidden="true"></i></a>
    </div></div>
</div>
</body>
<script>
    jQuery(document).ready(function () {
        jQuery(".homeBlock .well").hover(function () {
            jQuery(this).css({"height":"14em"});
        },function () {
            jQuery(this).css({"height":"10em"});

        })
    })
    
    
</script>
</html>