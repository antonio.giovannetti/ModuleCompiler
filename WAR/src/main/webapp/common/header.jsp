<%@ taglib prefix="s" uri="/struts-tags" %>
<% String logout = "${keycloak.authserver.url}/realms/MC/protocol/openid-connect/logout?redirect_uri=%2Fmc";
%>
<div class="headerPlaceholder" ></div>
<header role="banner" class="text-center bg-primary" >
       <small>MC header</small>
       <div class="pull-right"><a title="Home page" class="btn btn-default" href="<%=request.getContextPath()%>/" >
                     <i class="fas fa-home" aria-hidden="true"></i></a>
<% if (request.getUserPrincipal()!=null) {
    String principal = request.getUserPrincipal().getName();
%><span data-user-id="<%=principal%>" title="Informazioni utente connesso" class="btn btn-default user-info "><i class="fas fa-user" aria-hidden="true"></i></span>
           <%--<div class="tooltip bs-tooltip-bottom" role="tooltip">--%>
<%--<div class="text-left"><label >Principal2: </label><%=nome%></div>--%>
<%--<div class="text-left"><label >Nome: </label><b><%=token%></b></div>--%>
<%--<div class="text-left"><label >Ruoli: </label><u><%=StringUtils.join(roles,',') %></u>--%>
           <%--</div></div>--%>


           <a title="Logout" data-confirm-text="Si vuole uscire?" class="btn btn-danger confirm" href="/mc/logout.jsp" >
              <i class="fas fa-power-off" aria-hidden="true"></i></a><% } %><div class="clearfix"></div></div></div>
</header>

<div class="container-fluid"><s:if test="hasActionMessages()">
       <div class="alert alert-success"><s:actionmessage /></div></s:if>
<s:if test="hasActionErrors()"><div class="alert alert-danger"><s:actionerror /></div></s:if>
</div>
<script>
    var st = 90;
    jQuery(document).ready(function() {
        MC_UTILS.addUserInfo({elem:jQuery("header .user-info"),title:"Utente connesso"})
        // jQuery("header .userinfo").click(function () {
        //     var right = $(this).next().css("right");
        //     $(this).next().css("right",right != "0px" ? "0px" : "-450px");
        // })

        jQuery(window).on('scroll', function () {
            if (jQuery(document).scrollTop() > st) {
                jQuery('header').addClass('shrink');
            } else {
                jQuery('header').removeClass('shrink');
            }
        });
        if (jQuery(document).scrollTop() > st) {
            jQuery('header').addClass('shrink');
        }
    })

</script>
