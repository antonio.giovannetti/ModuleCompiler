    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="description"
          content="Module compiler" />
    <meta name="keywords" content="Module compiler" />
    <link rel="icon" href="${resources.url.webclient}/favicon.png">
    <noscript>Your browser does not support JavaScript!</noscript>
    <link rel="stylesheet" href="${resources.url.webclient}/bootstrap-3.3.7-dist/css/bootstrap-datetimepicker.min.css" />
    <script src="${resources.url.webclient}/js/jquery.min.js"></script>

    <%--<!-- JS file -->--%>
    <%--<script src="${resources.url.webclient}/js/easy-autocomplete/jquery.easy-autocomplete.min.js"></script>--%>

    <%--<!-- CSS file -->--%>
    <%--<link rel="stylesheet" href="${resources.url.webclient}/js/easy-autocomplete/easy-autocomplete.min.css">--%>

    <%--<!-- Additional CSS Themes file - not required-->--%>
    <%--<link rel="stylesheet" href="${resources.url.webclient}/js/easy-autocomplete/easy-autocomplete.themes.min.css">--%>


    <script src="${resources.url.webclient}/js/autocomplete/jquery.auto-complete.js"></script>
    <link rel="stylesheet" href="${resources.url.webclient}/js/autocomplete/jquery.auto-complete.css">

    <%--Bootstrap 4--%>
    <%--<link rel="stylesheet" href="${resources.url.webclient}/bs4/css/bootstrap.css">--%>
    <%--<script src="${resources.url.webclient}/bs4/js/popper.js"></script>--%>
    <%--<script src="${resources.url.webclient}/bs4/js/bootstrap.min.js"></script>--%>

    <%--Bootstrap 3--%>
    <link rel="stylesheet" href="${resources.url.webclient}/bootstrap-3.3.7-dist/css/bootstrap.min.css" />
    <script src="${resources.url.webclient}/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <%--<link rel="stylesheet" type="text/css" href="${resources.url.webclient}/css/font-awesome.min.css" />--%>
    <link rel="stylesheet" type="text/css" href="${resources.url.webclient}/css/fontawesome-free-5.0.13/web-fonts-with-css/css/fontawesome-all.css" />

    <link rel="stylesheet" href="${resources.url.webclient}/css/mc.css" />
    <link rel="stylesheet" href="${resources.url.webclient}/css/mc-fonts.css" />
    <link rel="stylesheet" href="${resources.url.webclient}/css/mc_slider.css" />

    <link rel="stylesheet" href="${resources.url.webclient}/octicon/octicon.css" />

    <%--<script src="${resources.url.webclient}/js/cytoscape.min.js"></script>--%>

    <%--<script src="${resources.url.webclient}/vis/vis.min.js"></script>--%>
    <%--<link rel="stylesheet" href="${resources.url.webclient}/vis/vis.min.css" />--%>

<!-- JIT -->
    <!-- CSS Files -->
    <%--<link type="text/css" href="${resources.url.webclient}/jit/Examples/css/base.css" rel="stylesheet" />--%>
    <%--<link type="text/css" href="${resources.url.webclient}/jit/Examples/css/Hypertree.css" rel="stylesheet" />--%>

    <!--[if IE]><!--<script language="javascript" type="text/javascript" src="${resources.url.webclient}/jit/Extras/excanvas.js"></script>--><![endif]-->

    <!-- JIT Library File -->
    <%--<script language="javascript" type="text/javascript" src="${resources.url.webclient}/jit/jit.js"></script>--%>


    <%--<link rel="stylesheet" href="${resources.url.webclient}/css/jquery.cookiebar.css">--%>

    <%--<script src="${resources.url.webclient}/js/text-fill.min.js"></script>--%>
    <%--<script src="${resources.url.webclient}/js/owl.carousel.js"></script>--%>
    <%--<script src="${resources.url.webclient}/js/venobox.js"></script>--%>
    <%--<script src="${resources.url.webclient}/js/invert-color.js"></script>--%>
    <script src="${resources.url.webclient}/js/typeahead.bundle.min.js"></script>

    <%--<script src="${resources.url.webclient}/js/ddrop/jquery-sortable.js"></script>--%>
    <%--<script src="${resources.url.webclient}/js/ddrop/drag-sort.js"></script>--%>
    <script src="${resources.url.webclient}/js/ddrop/jquery.dragsort-0.5.2.js"></script>

    <%--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAND-VarQiaS91M2gh-PhSk89oZVvFi7T0&libraries=places"></script>--%>

    <script src="${resources.url.webclient}/js/moment-with-locales.min.js"></script>
    <script src="${resources.url.webclient}/bootstrap-3.3.7-dist/js/bootstrap-datetimepicker.min.js"></script>
    <script src="${resources.url.webclient}/js/mc-utils.js"></script>
    <script src="${resources.url.webclient}/js/mc-jqplugins.js"></script>
    <script src="${resources.url.webclient}/js/mc-modal.js"></script>

    <link rel="stylesheet" type="text/css" href="${resources.url.webclient}/js/datatables_1.10.19/css/jquery.dataTables.min.css"/>
    <script src="${resources.url.webclient}/js/datatables_1.10.19/jquery.dataTables.min.js"></script>

    <link rel="stylesheet" type="text/css" href="${resources.url.webclient}/js/datatables_1.10.19/css/responsive.dataTables.min.css"/>
    <script src="${resources.url.webclient}/js/datatables_1.10.19/dataTables.responsive.min.js"></script>

    <%--<script src="${resources.url.webclient}/js/keycloak.js"></script>--%>

    <%--<script src="${resources.url.webclient}/js/jquery.cookiebar.js"></script>--%>
    <%--<script src="${resources.url.webclient}/js/cookie.js"></script>--%>

    <script src="${resources.url.webclient}/js/tinymce/tinymce.min.js" type="text/javascript"></script>
    <script src="${resources.url.webclient}/js/tinymce/jquery.tinymce.min.js" type="text/javascript"></script>
    <script src="${resources.url.webclient}/js/jquery.form.min.js"></script>

    <script src="${resources.url.webclient}/js/underscore-min.js"></script>


    <%--Prism--%>
    <link rel="stylesheet" type="text/css" href="${resources.url.webclient}/prism/prism.css"/>
    <script src="${resources.url.webclient}/prism/prism.js" type="text/javascript"></script>

    <script>MC_UTILS = MC_UTILS || {};
            MC_UTILS.resourcesUrl="${resources.url.webclient}";
            MC_UTILS.webContext = window.location.href.replace(/(http:\/\/[^\/]+\/[^\/]+)(.*)/,"$1");

    jQuery.ajaxSetup({
        statusCode: {
            302:function(){
                console.info(arguments);
                alert("Session expired");
            }
        }
    })

    </script>

