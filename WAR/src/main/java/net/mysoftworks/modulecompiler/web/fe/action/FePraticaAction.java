package net.mysoftworks.modulecompiler.web.fe.action;

import net.mysoftworks.modulecompiler.bl.dto.UserDTO;
import net.mysoftworks.modulecompiler.bl.manager.GestorePratica;
import net.mysoftworks.modulecompiler.bl.manager.GestoreValue;
import net.mysoftworks.modulecompiler.bl.manager.KeycloakUtil;
import net.mysoftworks.modulecompiler.model.RuoloPratica;
import net.mysoftworks.modulecompiler.model.meta.MetaDichiarazione;
import net.mysoftworks.modulecompiler.model.ValueLob;
import net.mysoftworks.modulecompiler.model.meta.MetaProcedimento;
import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.model.values.Pratica;
import net.mysoftworks.modulecompiler.model.values.RawValue;
import net.mysoftworks.modulecompiler.report.iface.AvailableReports;
import net.mysoftworks.modulecompiler.report.iface.Report;
import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.web.common.action.Download;
import net.mysoftworks.modulecompiler.web.common.action.ProcedimentoAction;
import net.mysoftworks.modulecompiler.web.common.injectors.SessionDataInjector;
import net.mysoftworks.modulecompiler.web.fe.sd.GrantHelper;
import net.mysoftworks.modulecompiler.web.fe.sd.SDPratica;
import net.mysoftworks.modulecompiler.web.utils.DownloadBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.w3c.dom.Document;

import javax.ejb.EJB;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;


public class FePraticaAction extends ProcedimentoAction {

    public static final String DETAIL_CREA="detail_crea";
    public static final String DETAIL="detail";
    public static final String LIST_DELETE="list_delete";

    @EJB
    private GestorePratica gestorePratica;

    @EJB
    private GestoreValue gestoreValue;

    private SDPratica sdPratica;
    private Long idProcedimento;

    public boolean isOwner() {
        return sdPratica.isOwner();
    }

    public boolean canDelete(Dichiarazione d) {
        return canChangeCompiler(d);
    }

    public boolean canModify(Dichiarazione d) {
        return sdPratica.canModify(d);
    }

    public boolean canPrint() {
        List<RuoloPratica> rps = gestorePratica.findRuoliPratica(getPratica().getId());
        return new GrantHelper(rps).canPrint();
    }

    public boolean canView(Dichiarazione d) {
        return sdPratica.canView(d);
    }

    public boolean canChangeCompiler(Dichiarazione d) {
        return sdPratica.canChangeCompiler(d);
    }

    public boolean isOwner(Dichiarazione d) {
        return sdPratica.isOwner(d);
    }
//    /**
//     * Definire l'eventuale logica di permesso di modifica compilatore per singola dichiarazione
//     * @return
//     */
//    public boolean isCanChangeCompiler(Dichiarazione d) {
//        return ServletActionContext.getRequest().isUserInRole(RuoloPratica.RuoloCompilazionePratica.ROLE_COMPILER.getCodice()) ;
//    }

    @EJB
    private KeycloakUtil keycloakUtil;

    private String userId;
    private UserDTO userDTO;

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String userInfo() {
        // TODO: verificare se si dispone dei diritti
        logOperatore(Loggers.LOG_FE,Level.INFO,"Recupero le info per l''utente >{0}<", userId);
        userDTO = keycloakUtil.getUserInfo(userId);
        return "userInfo";
    }


    public void setIdProcedimento(Long idProcedimento) {
        this.idProcedimento = idProcedimento;
    }

    public String delete() {
        Pratica p = gestorePratica.findPratica(getPratica().getId());
        setPratica(p);
        logOperatore(Loggers.LOG_FE,Level.INFO,"Cancello la pratica {0,number,#}", getPratica().getId());
        if (isOwner()) {
            gestorePratica.deletePratica(getPratica().getId());
            setPratica(null);
            return LIST_DELETE;
        }
        throw new RuntimeException("Non si dispone dei diritti per cancellare la pratica");
    }

    public String stampa() {

        if (canPrint()){
            logOperatore(Loggers.LOG_FE,Level.INFO, "Stampo la pratica {0,number,#}", getPratica().getId());
            Map<String, Object> model = FeDichiarazioneAction.getDefaultMap();
            model.put("print",true);
            Document domPratica = gestorePratica.domPraticaHtml(getPratica().getId(),model);
            Report report = reportManager.generateFromXsl(AvailableReports.PRATICA, domPratica);
            String name = report.getName();
            if (StringUtils.isEmpty(name)){
                name = "pratica_" + getPratica().getId() + ".pdf";
            }
            DownloadBean db = new DownloadBean(report.getContent(),name,report.getMime());
            return Download.prepareAndRedirect(db);
        }
        throw new RuntimeException("Non si dispone dei diritti per stampare la pratica");
    }


    public String crea() {
        setPratica(null);
        detail();
        return DETAIL_CREA;
    }

    private Long idFileValue;

    public void setIdFileValue(Long idFileValue) {
        this.idFileValue = idFileValue;
    }

    public String downloadFileValue() {
        Object[] v = gestoreValue.checkAndDownload(idFileValue);
        if (v!=null) {
            RawValue rv = (RawValue) v[0];
            ValueLob rvl = (ValueLob) v[1];
            DownloadBean db = new DownloadBean(rvl.getValue(),rv.getStringValue(),null);
            return Download.prepareAndRedirect(db);

        }
        throw new RuntimeException("Non si dispone dei diritti per scaricare l'allegato " + idFileValue);
    }

    public List<Dichiarazione> getDichiarazioni() {
        return sdPratica.getDichiarazioni();
    }



    public MetaDichiarazione metaDichiarazione(Long idMeta) {
        for(MetaDichiarazione md:getProcedimento ().getDichiarazioni()) {
            if (md.getId().equals(idMeta)) return md;
        }
        return null;
    }

    public String detail() {
        Pratica p = getPratica();

        if (p==null) {
            logOperatore(Loggers.LOG_FE,Level.INFO,"Creo pratica per il procedimento {0,number,#}", idProcedimento );
            if (idProcedimento==null) throw new RuntimeException("Non e' stato specificato né il procedimento né la pratica" );
            p = gestorePratica.createPratica(idProcedimento);
            sdPratica = new SessionDataInjector(ServletActionContext.getRequest()).getSessionData(SDPratica.class,p.getId().toString(),true,true);
            List<Dichiarazione> dichiarazioni = gestorePratica.findDichiarazioni(p.getId());
            sdPratica.setPratica(p);
            sdPratica.setDichiarazioni(dichiarazioni);
        } else {
            if (p.getId()!=null) {
                Long idPratica= p.getId();
                logOperatore(Loggers.LOG_FE,Level.INFO,"Visualizzazione pratica {0,number,#}" , idPratica );
                p = gestorePratica.findPratica(idPratica);
                if (p!=null) {
                    List<Dichiarazione> dichiarazioni = gestorePratica.findDichiarazioni(p.getId());
//                    sdPratica.setValues(p.getValues());
                    sdPratica.setPratica(p);
                    sdPratica.setDichiarazioni(dichiarazioni);
                    idProcedimento = p.getProcedimento().getId();
                } else {
                    logOperatore(Loggers.LOG_FE,Level.WARNING,"La pratica {0,number,#} non esiste" ,idPratica);
                    addActionError(getText("fe.pratica.notfound","-",idPratica.toString()));
                }
            }
        }
        if (idProcedimento!=null) {
            sdPratica.setProcedimento(gestoreProcedimento.get(idProcedimento));
        }
        setPratica(p);
        return DETAIL;
    }

    public MetaProcedimento getProcedimento() {
        return sdPratica.getProcedimento();
    }

    public Pratica getPratica() {
        return sdPratica!=null ? sdPratica.getPratica() : null;
    }

    public void setPratica(Pratica pratica ) {
        if (sdPratica!=null) sdPratica.setPratica(pratica );

    }


    private Long idGuida;

    public void setIdGuida(Long idGuida) {
        this.idGuida = idGuida;
    }

    public String guida() {
        ValueLob guida =  gestoreProcedimento.getGuida(idGuida);
        PrintWriter wr = null;
        try {
            wr = ServletActionContext.getResponse().getWriter();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        if (guida!=null) {
            wr.write(guida.getStringValue());
        } else {
            wr.write("Errore nell'accesso alla guida " + idGuida);
        }
        return null;
    }

//    protected void initLookups(Context ctx) {
//        gestoreProcedimento = lookup(GestoreProcedimento.class);
//    }
}
