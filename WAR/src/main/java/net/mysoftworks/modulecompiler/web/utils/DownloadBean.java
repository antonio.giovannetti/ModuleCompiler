package net.mysoftworks.modulecompiler.web.utils;

import java.io.Serializable;

public class DownloadBean implements Serializable {
    private byte[] content;
    private String name;
    private String contentType;

    public DownloadBean(byte[] content, String name,String contentType) {
        this.content = content;
        this.name = name;
        this.contentType = contentType;
    }

    public byte[] getContent() {
        return content;
    }

    public String getName() {
        return name;
    }

    public String getContentType() {
        return contentType;
    }
}
