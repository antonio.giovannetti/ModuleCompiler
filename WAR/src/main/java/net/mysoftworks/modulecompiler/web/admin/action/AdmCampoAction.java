package net.mysoftworks.modulecompiler.web.admin.action;

import net.mysoftworks.modulecompiler.model.*;
import net.mysoftworks.modulecompiler.model.meta.*;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.TemporalType;
import java.lang.reflect.Constructor;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by antonio on 02/07/17.
 */
public class AdmCampoAction extends AdmDichiarazioneAction {
    private static final Logger log = Logger.getLogger(AdmCampoAction.class.getName());

    private List<String> DEFAULT_VALUES=new ArrayList<String>();
//    @Inject
//    private GestoreCampo gestoreCampo;

    /* SEARCH FORM */
    /**
     * @return
     */
    public String list() {
        return "list";
    }

    /* END SEARCH FORM */

    /* DETAIL */
    private Long idCampo;
    private Long idGruppo;
    private Set<MetaField> campi = new HashSet<MetaField>();

    public MetaField getCampo() {
        return dc.getCampo();
    }

    public void setCampo(MetaField mf) {
        dc.setCampo(mf);
        allowedJavaTypes.putAll(MetaTextField.allJavaTypes); // Per ora li metto tutti ignorando il tipo
    }

    public void setIdCampo(Long idCampo) {
        this.idCampo = idCampo;
    }

    public void setIdGruppo(Long idGruppo) {
        this.idGruppo = idGruppo;
    }

    public String detail() {


//        ApplicationContext ac=new ServletApplicationContext(ServletActionContext.getServletContext());
//        new StrutsTilesInitializer().initialize(ac);
        logOperatore(log,Level.FINE,"Ricerca campo {0,number,#}",idCampo);
        MetaField field = gestoreCampo.get(idCampo);
        setCampo(field);
        return INPUT;
    }

    /* END DETAIL */

//    /* SAVE */
//    /**
//     * Clona il campo e lo aassocia alla metaDichiarazione corrente
//     * @return
//     */
//    public String clone() {
//        logOperatore(log,Level.FINE,"Clone campo {0,number,#}",getCampo().getId());
//        MetaDichiarazione dic = getMetaDichiarazione();
//        MetaField campo = getCampo();
//        MetaField clone = null;
//        if (campo instanceof MetaTextField) {
//            clone = new MetaTextField((MetaTextField)campo);
//        }
//        if (campo instanceof MetaTemporalField) {
//            clone = new MetaTemporalField((MetaTemporalField)campo);
//        }
//        if (campo instanceof MetaFileField) {
//            clone = new MetaFileField((MetaFileField)campo);
//        }
//        if (campo instanceof MetaAnagraficaField) {
//            clone = new MetaAnagraficaField((MetaAnagraficaField)campo);
//        }
//        if (clone!=null) {
//            setCampo(clone);
//        } else {
//            logOperatore(log,Level.WARNING,"Impossibile clonare il campo a partire da {0}",campo);
//        }
//        gestoreCampo.save(clone);
//        return INPUT;
//    }


    private String[] allowedVal;
    private String[] allowedCod;

    public void setAllowedVal(String[] allowedVal) {
        this.allowedVal = allowedVal;
    }

    public void setAllowedCod(String[] allowedCod) {
        this.allowedCod = allowedCod;
    }


    private void processAllowedVal() {
        MetaField campo = getCampo();
        if (!(campo instanceof MetaListField)) return;
        if (ArrayUtils.isEmpty(allowedVal) || ArrayUtils.isEmpty(allowedCod)) return;
        if (allowedVal.length - allowedCod.length !=0) throw new RuntimeException("allowedVal e allowedCod hanno dimensioni diverse: " + allowedVal.length + ","+allowedCod.length);
        MetaListField lf = (MetaListField) campo;
        lf.getAllowedValues().clear();
        int i = 0;
        for(String av:allowedVal) {
            lf.addValue(new AllowedValue(allowedCod[i++],av));
        }
        logOperatore(log,Level.FINE,"Salvataggio {0,number,#} allowedVal",i);


    }

    private Integer[] filePermessi;

    public void setFilePermessi(Integer[] filePermessi) {
        this.filePermessi = filePermessi;
    }

    public boolean filePermesso(Integer v) {
        MetaFileField lf = (MetaFileField) getCampo();
        return lf.getFileTypes().contains(v);
    }

    public List<MetaFileTypes> getMimes() {
        return gestoreCampo.getMimeTypes(null);
    }

    private void processFilePermessi() {
        MetaField campo = getCampo();
        if (!(campo instanceof MetaFileField)) return;
        if (ArrayUtils.isEmpty(filePermessi)) return;
        MetaFileField ff = (MetaFileField) campo;
        logOperatore(log,Level.FINE,"Salvataggio file permessi [{0}]",StringUtils.join(filePermessi,','));
        ff.getFileTypes().clear();
        for(Integer _fp:filePermessi) {
            ff.getFileTypes().add(_fp);
        }
    }




    public String saveOrUpdate() {
        processAllowedVal();
        processFilePermessi();
        MetaField campo = getCampo();
//        if (StringUtils.isEmpty(campo.getEtichetta())){
//            addActionError("Etichetta vuota");
//        }
//        String et = getRequest().getParameter("etichettaGruppo");
        logOperatore(log,Level.FINE,"Save/Update campo {0}",campo);
        setCampo(gestoreCampo.saveOrUpdate(campo,getMetaDichiarazione().getId()));
//        impostaEtichettaGruppo(campo);
//        gestoreCampo.update(campo);
        addActionMessage("Campo "+getCampo().getId()+" salvato");
        return INPUT;
    }
    public String delete() {
        logOperatore(log,Level.FINE,"Delete campo {0,number,#}",idCampo);
        gestoreCampo.delete(idCampo);
        setCampo(null);
        return null;
    }


    private MetaField.BUSINESS_FIELD_TYPE fieldType;

    public void setFieldType(MetaField.BUSINESS_FIELD_TYPE fieldType) {
        this.fieldType = fieldType;
    }

    private Map<String,Constructor> allowedJavaTypes = new HashMap<String,Constructor>();

    public Set<String> getAllowedJavaTypes() {
        return allowedJavaTypes.keySet();
    }

    public String add() {
        MetaField newCampo=null;
        if (fieldType==null) {
            logOperatore(log,Level.SEVERE,"Non e'' stato specificato il tipo del campo da creare");
        }
        switch (fieldType){
            case ANAG: newCampo= new MetaAnagraficaField(); break;
            case TEXT: newCampo= new MetaTextField(); break;
            case FILE: newCampo= new MetaFileField(); break;
            case LIST: newCampo= new MetaListField();
                MetaListField lf = (MetaListField) newCampo;
//                lf.getAllowedValues().add(new AllowedValue("1","nr1"));
//                lf.getAllowedValues().add(new AllowedValue("2","nr2"));
//                lf.getAllowedValues().add(new AllowedValue("3","nr3"));
//                lf.getAllowedValues().add(new AllowedValue("4","nr1"));
//                lf.getAllowedValues().add(new AllowedValue("5","nr2"));
//                lf.getAllowedValues().add(new AllowedValue("6","nr3"));
//                lf.getAllowedValues().add(new AllowedValue("7","nr1"));
//                lf.getAllowedValues().add(new AllowedValue("8","nr2"));
//                lf.getAllowedValues().add(new AllowedValue("9","nr3"));
//                lf.getAllowedValues().add(new AllowedValue("10","nr1"));
//                lf.getAllowedValues().add(new AllowedValue("11","nr2"));
//                lf.getAllowedValues().add(new AllowedValue("12","nr3"));
//                lf.getAllowedValues().add(new AllowedValue("13","nr1"));
//                lf.getAllowedValues().add(new AllowedValue("14","nr2"));
//                lf.getAllowedValues().add(new AllowedValue("15","nr3"));

            break;
            case TEMPORAL: newCampo= new MetaTemporalField();
                MetaTemporalField tf = (MetaTemporalField) newCampo;
                tf.setTemporalType(TemporalType.TIME);

            break;
        }
        logOperatore(log,Level.INFO,"Aggiungo campo tipo {0}",fieldType);
        setCampo(newCampo);
        return INPUT;
    }

//    @Override
//    protected void initLookups(Context ctx) {
//        super.initLookups(ctx);
//        gestoreCampo=lookup(GestoreCampo.class);
//    }
}
