package net.mysoftworks.modulecompiler.web.common.sd;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface SessionDataDecorator {
    String requestParam();
    boolean optional() default false;
    boolean forceCreate() default false;
}
