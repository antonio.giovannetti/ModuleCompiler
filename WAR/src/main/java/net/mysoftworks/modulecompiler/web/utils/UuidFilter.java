package net.mysoftworks.modulecompiler.web.utils;

import net.mysoftworks.modulecompiler.web.utils.UuidFilterNoStack;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Stack;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.LogManager;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class UuidFilter extends UuidFilterNoStack {

	public static final String UUID_STACK_ATTRIBUTE = "uuidStack";


	public void destroy() {
	}

	public UuidFilter() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		@SuppressWarnings("unchecked")
		Stack<String> stack = (Stack<String>) request.getAttribute(UUID_STACK_ATTRIBUTE);
		if (stack == null) {
			stack = new Stack<String>();
			request.setAttribute(UUID_STACK_ATTRIBUTE,stack);
		} else {
			logger.finest("Stack content:"+StringUtils.join(stack,','));
		}
		Object oldUuid = request.getAttribute(UUID_ATTRIBUTE);
		String uuid = UUID_PREFIX + UUID.randomUUID();
		logger.log(Level.FINER,"Set uuid request attribute and push into stack:{0}",uuid);
		stack.push(uuid);
		request.setAttribute(UUID_ATTRIBUTE, uuid);
		try{
			filterChain.doFilter(request, response);
		} catch (Exception e) {
			logRequest(request,e);
			throw  e;
		}
		request.setAttribute(UUID_ATTRIBUTE, oldUuid);
		stack.pop();
	}

	public void init(FilterConfig arg0) throws ServletException {
	}

}
