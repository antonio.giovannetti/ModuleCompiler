package net.mysoftworks.modulecompiler.web.websocket;

import net.mysoftworks.modulecompiler.bl.manager.GestorePratica;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.Map;

@ServerEndpoint(value = "/wsk/pratica/{pratica-id}",encoders = {PushMessageEncoder.class})
public class WSKPratica {

    @Inject
    GestorePratica gestorePratica;

    @OnOpen
    public void onOpen(Session session,@PathParam("pratica-id") Long idPratica,EndpointConfig config){
        PraticaSessionManager.getInstance().addSession(session,false,idPratica);
    }

    @OnClose
    public void onClose(Session session){
        System.out.println("Close Connection ...");
        PraticaSessionManager.getInstance().removeSession(session);
//        Principal pr = session.getUserPrincipal();
//        if (pr!=null) {
//            allSessions.remove(pr.getName());
//        } else {
//            System.out.println("Close Connection without user...");
//
//        }
    }
    @OnMessage
    public String onMessage(String message){
        System.out.println("Message from the client: " + message);
        String echoMsg = "Echo from the server : " + message;
        return echoMsg;
    }
    @OnError
    public void onError(Throwable e){
        e.printStackTrace();
    }

}
