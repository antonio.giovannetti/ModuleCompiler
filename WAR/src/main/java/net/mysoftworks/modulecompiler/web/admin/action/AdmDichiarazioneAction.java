package net.mysoftworks.modulecompiler.web.admin.action;

import net.mysoftworks.modulecompiler.bl.manager.GestoreCampo;
import net.mysoftworks.modulecompiler.bl.manager.GestoreDichiarazione;
import net.mysoftworks.modulecompiler.model.meta.MetaDichiarazione;
import net.mysoftworks.modulecompiler.model.meta.MetaField;
import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.render.DichiarazioneRenderer;
import net.mysoftworks.modulecompiler.web.common.action.Download;
import net.mysoftworks.modulecompiler.web.common.sd.SDDichiarazione;
import net.mysoftworks.modulecompiler.web.utils.DownloadBean;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.validation.SkipValidation;

import javax.ejb.EJB;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by antonio on 02/07/17.
 */
public class AdmDichiarazioneAction extends AdmProcedimentoAction {

    protected SDDichiarazione dc;
//    @Inject
    @EJB
    private GestoreDichiarazione gestoreDichiarazione;

//    @Inject
    @EJB
    protected GestoreCampo gestoreCampo;

    private List<MetaDichiarazione> dichiarazioni;
    public List<MetaDichiarazione> getMetaDichiarazioni() {
        return dichiarazioni;
    }

    @SkipValidation
    public String list() {
        dichiarazioni = gestoreDichiarazione.getLista();
        return "list";
    }

    /* END SEARCH FORM */

    /* DETAIL */
    public MetaDichiarazione getMetaDichiarazione() {
        return dc.getMetaDichiarazione();
    }

    public void setDichiarazione(MetaDichiarazione dichiarazione) {
        dc.setMetaDichiarazione(dichiarazione);
        if (CollectionUtils.isNotEmpty(dc.getCampi())) {
            for(MetaField mf:dc.getCampi()) {
                mf.setNrValues(gestoreCampo.findValuesWithField(mf.getId()));
            }
        }
    }

    public List<MetaField> getCampi() {
        return dc.getCampi();
    }

    @SkipValidation
    public String add() {
        MetaDichiarazione dich = new MetaDichiarazione();
        setDichiarazione(dich);
        return INPUT;
    }

    @SkipValidation
    public String detail() {
//        ApplicationContext ac=new ServletApplicationContext(ServletActionContext.getServletContext());
//        new StrutsTilesInitializer().initialize(ac);
        Long idDichiarazione = getMetaDichiarazione().getId();
        logOperatore(Loggers.LOG_ADMIN,Level.INFO,"Vista metaDichiarazione {0,number,#}",idDichiarazione);
        MetaDichiarazione dic = gestoreDichiarazione.getMeta(idDichiarazione);
        if (dic==null) {
            addActionError("La metaDichiarazione " + idDichiarazione + " non e' stata trovata");
            return ERROR;
        }
        if (dic.getTemplate()!=null) {
            testoTemplate = new String(dic.getTemplate().getValue());
        }
//        initCampi(dic);
        setDichiarazione(dic);

        return INPUT;
    }

    protected Integer or(Integer[] raw) {
        Integer st=0;
        for (Integer stato:raw) {
            st|=stato;
        }
        return st;
    }

    public void setCompilabilita(Integer[] compilabilita) {
        getMetaDichiarazione().setCompilabilita(or(compilabilita));
    }

    public void setVisibilita(Integer[] visibilita) {
        getMetaDichiarazione().setVisibilita(or(visibilita));
    }

    private String testoTemplate;

    public void setTestoTemplate(String testoTemplate) {
        this.testoTemplate = testoTemplate;
    }

    public String getTestoTemplate() {
        return testoTemplate;
    }


    public void validateDic() {
        MetaDichiarazione d = getMetaDichiarazione();
        XPath xp = XPathFactory.newInstance().newXPath();
        try {
            if (StringUtils.isNotBlank(d.getPresentIf())) xp.compile(d.getPresentIf());
        }catch (Exception e) {
            addActionError("Xpath presentIf:" + e.getMessage());
//            throw new RuntimeException(e.getMessage());
        }
        try {
            if (StringUtils.isNotBlank(d.getMandatoryIf())) xp.compile(d.getMandatoryIf());
        }catch (Exception e) {
            addActionError("Xpath mandatoryIf:" +e.getMessage());
//            throw new RuntimeException(e.getMessage());
        }
    }

    public String save() {
        validateDic();
        MetaDichiarazione d = getMetaDichiarazione();
        if (StringUtils.isNotEmpty(d.getCustomController())) {
            try {
                Class<?> cl = Class.forName(d.getCustomController());
                if (!DichiarazioneRenderer.class.isAssignableFrom(cl)) {
                    logOperatore(Loggers.LOG_ADMIN,Level.WARNING,"La classe {0} deve estendere {1}",d.getCustomController(),DichiarazioneRenderer.class.getName());
                    addActionError("La classe '" + d.getCustomController() + "' deve estendere " + DichiarazioneRenderer.class.getName());
                }
            } catch (ClassNotFoundException e) {
                logOperatore(Loggers.LOG_ADMIN,Level.WARNING,"La classe {0} non esiste",d.getCustomController());
                addActionError("La classe '" + d.getCustomController() + "' non esiste");
            }
        }
        if (hasActionErrors()) {
            return AJAX_ERROR;
        }
        logOperatore(Loggers.LOG_ADMIN,Level.FINE,"Salvataggio metaDichiarazione {0}, template:{1} ",d,testoTemplate);
        MetaDichiarazione md = gestoreDichiarazione.save(d,getProcedimento(),testoTemplate);
        setDichiarazione(md);
        return AJAX_SUCCESS;
    }

    public String clona() {
        logOperatore(Loggers.LOG_ADMIN,Level.FINE,"Clono metaDichiarazione {0,number,#} del procedimento {1,number,#}",getMetaDichiarazione().getId(),getProcedimento().getId());
        MetaDichiarazione mdClone = gestoreDichiarazione.clone(getMetaDichiarazione().getId(),getProcedimento().getId());
        setDichiarazione(mdClone);
        return DETAIL_REDIRECT;
    }

    public String backupCfg() throws  Exception {
        byte[] content = null;
        String fName = "backup";
        if (getMetaDichiarazione().getId()!=null) {
            logOperatore(Loggers.LOG_ADMIN,Level.FINE,"Backup cfg dichiarazione {0,number,#}",getMetaDichiarazione().getId());
            fName +="_dic" + getMetaDichiarazione().getId()+".zip";
            content = gestoreLiquibase.exportDic(getMetaDichiarazione().getId());
        } else {
            throw new RuntimeException("Impossibile effettuare backup cfg per id dichiarazione null");
        }
        DownloadBean db = new DownloadBean(content,fName,"application/zip");
        return Download.prepareAndRedirect(db);
    }



}
