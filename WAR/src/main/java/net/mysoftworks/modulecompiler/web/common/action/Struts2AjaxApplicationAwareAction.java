package net.mysoftworks.modulecompiler.web.common.action;

import net.mysoftworks.modulecompiler.search.SearchOutput;

import java.util.Collection;

public abstract class Struts2AjaxApplicationAwareAction extends Struts2ApplicationAwareAction {
    protected SearchOutput output;
    public class Messages {
        Collection<String> errors;
        Collection<String> messages;
        public Messages(Collection<String> errors, Collection<String> messages) {
            this.errors = errors;
            this.messages = messages;

        }

        public SearchOutput getOutput() {
            return output;
        }

        public Collection<String> getErrors() {
            return errors;
        }

        public Collection<String> getMessages() {
            return messages;
        }
    }
    protected static final String AJAX_SUCCESS = "ajax_success";
    protected static final String AJAX_ERROR = "ajax_error";

    protected static final String AJAX_SUCCESS_DATATABLE = "ajax_success_datatable";
    protected static final String AJAX_ERROR_DATATABLE = "ajax_error_datatable";

    protected static final String FORBIDDEN = "forbidden";


    public SearchOutput getOutputDatatable() {
        return output;
    }

    public Messages getMessages() {
        return new Messages(getActionErrors(),getActionMessages());
    }

}
