package net.mysoftworks.modulecompiler.web.searchparam;

import net.mysoftworks.modulecompiler.search.SearchParam;

import javax.servlet.http.HttpServletRequest;

public abstract class WebSearchParamFactory<T extends SearchParam>  {
    private static final String PARAM_PAGESIZE = "length";
    private static final String PARAM_NR_OFFSET = "start";
    private static final String PARAM_DRAW = "draw";

    public static <T extends SearchParam> T create(HttpServletRequest request, Class<T> clazz){
        String _psize = request.getParameter(PARAM_PAGESIZE);
        Byte pageSize = null;
        if (_psize!=null) {
            pageSize = Byte.parseByte(_psize);
        } else {
            throw new RuntimeException("Il parametro " + PARAM_PAGESIZE + " non e' presente");
        }

        String _offset = request.getParameter(PARAM_NR_OFFSET);
        Integer offset = null;
        if (_offset!=null) {
            offset = Integer.parseInt(_offset);
        } else {
            throw new RuntimeException("Il parametro " + PARAM_NR_OFFSET + " non e' presente");
        }

        String _draw = request.getParameter(PARAM_DRAW);
        Integer draw = null;
        if (_draw!=null) {
            draw = Integer.parseInt(_draw);
        } else {
            throw new RuntimeException("Il parametro " + PARAM_DRAW + " non e' presente");
        }

        try {
            T out = clazz.getConstructor(int.class, byte.class).newInstance(offset, pageSize);
            if (draw != null) out.setDraw(draw); else out.setDraw(Integer.MIN_VALUE);
            return out;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public abstract void populate(HttpServletRequest request,T t);


}
