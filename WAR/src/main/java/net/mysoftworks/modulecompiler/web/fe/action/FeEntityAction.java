package net.mysoftworks.modulecompiler.web.fe.action;

import org.apache.commons.lang3.StringUtils;


public class FeEntityAction extends FeDichiarazioneAction {

    private String entityKey;
    private String entityType;
    public void setEntityKey(String entityKey) {
        this.entityKey = entityKey;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String input() {
        if (StringUtils.isEmpty(entityKey)) {
            // New instance
        } else {
            // Editing
        }

        return INPUT;
    }
}
