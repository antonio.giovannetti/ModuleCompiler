package net.mysoftworks.modulecompiler.web.websocket;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class PushMessage implements Serializable {
    public static final String ID_PRATICA="idPratica";
    public static final String ID_DICHIARAZIONE="idDich";
    public static final String DESC_PROC="descProc";
    public enum TYPE{
        NOTIFICA_CAMBIO_COMPILATORE_DICHIARAZIONE,
        NOTIFICA_MODIFICA_PROCEDIMENTO}
    public TYPE type;
    private Map<String, Serializable> data;
    public String snippet;

    public PushMessage(TYPE type, String snippet) {
        this.type = type;
        this.snippet = snippet;
    }
    public void addKey(String key,Serializable value) {
        if (data==null) data=new HashMap<>();
        data.put(key,value);
    }

    public Map<String, Serializable> getData() {
        return data;
    }
}
