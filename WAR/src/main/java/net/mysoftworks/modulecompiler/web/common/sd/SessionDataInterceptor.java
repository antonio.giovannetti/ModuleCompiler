package net.mysoftworks.modulecompiler.web.common.sd;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import net.mysoftworks.modulecompiler.web.common.injectors.SessionDataInjector;
import org.apache.struts2.ServletActionContext;

public class SessionDataInterceptor extends AbstractInterceptor {

    public static final String BUSINESS_CONTEXT_PARAM="MC_BUSINESS_CONTEXT";
    public static final String BUSINESS_CONTEXT_VAL_FE="FE";
    public static final String BUSINESS_CONTEXT_VAL_ADMIN="ADMIN";

    public static boolean isAdminPage() {
        return SessionDataInterceptor.BUSINESS_CONTEXT_VAL_ADMIN.equals(
                ServletActionContext.getRequest().getAttribute(BUSINESS_CONTEXT_PARAM));
    }

    public String intercept(ActionInvocation actionInvocation) throws Exception {
        String ns = actionInvocation.getProxy().getNamespace().toLowerCase();
        if (ns.equals("/fe")) {
            ServletActionContext.getRequest().setAttribute(BUSINESS_CONTEXT_PARAM,BUSINESS_CONTEXT_VAL_FE);
        }
        if (ns.equals("/admin")) {
            ServletActionContext.getRequest().setAttribute(BUSINESS_CONTEXT_PARAM,BUSINESS_CONTEXT_VAL_ADMIN);
        }

        Object action = actionInvocation.getAction();
        new SessionDataInjector(ServletActionContext.getRequest()).injectSessionData(action);
//        Class<?> clazz = action.getClass();
//        while (!Object.class.equals(clazz)) {
//            SessionDataInjector.injectSessionData(action);
//            clazz = clazz.getSuperclass();
//        }
        return actionInvocation.invoke();
    }
}
