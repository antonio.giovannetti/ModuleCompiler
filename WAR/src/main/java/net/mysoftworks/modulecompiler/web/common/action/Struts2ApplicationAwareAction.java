package net.mysoftworks.modulecompiler.web.common.action;

import com.opensymphony.xwork2.ActionSupport;
import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.dto.FileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.multipart.MultiPartRequestWrapper;
import org.apache.struts2.dispatcher.multipart.UploadedFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Struts2ApplicationAwareAction extends ActionSupport {
//    protected static final Logger log = Logger.getLogger(Struts2ApplicationAwareAction.class.getName());

    public boolean isAjax() {
        return "XMLHttpRequest".equals(ServletActionContext.getRequest().getHeader("X-Requested-With"));
    }

    protected HttpServletRequest getRequest() {return ServletActionContext.getRequest();}
    protected HttpServletResponse getResponse() {return ServletActionContext.getResponse();}

    public Principal getPrincipal() {
        return ServletActionContext.getRequest().getUserPrincipal();
    }

    protected void logOperatore(Logger log, Level level, String s, Object ... params) {
        Loggers.logOperatore(log,level,s,getPrincipal(),params);
    }

    public static Map<String, FileUpload[]> readUploadedFiles() throws Exception {
        if (!ServletFileUpload.isMultipartContent(ServletActionContext.getRequest())) return null;
        Map<String, FileUpload[]> out = new HashMap<>();
        MultiPartRequestWrapper multiWrapper = (MultiPartRequestWrapper) ServletActionContext.getRequest();
        Enumeration<String> fileParameterNames = multiWrapper.getFileParameterNames();
        while (fileParameterNames != null && fileParameterNames.hasMoreElements()) {
            String paramName = fileParameterNames.nextElement();
            UploadedFile[] uploaded = multiWrapper.getFiles(paramName);
            FileUpload[] fis=new FileUpload[uploaded.length];
            int i = 0;
            String[] names=multiWrapper.getFileNames(paramName);
            for (UploadedFile uf: uploaded) {
                ByteArrayOutputStream baos=new ByteArrayOutputStream();
                IOUtils.copy(new FileInputStream((File) uf.getContent()),baos);
                byte[] bytes = baos.toByteArray();
                FileUpload fi = new FileUpload(
                        bytes,names[i],
                        multiWrapper.getContentType());
                fis[i++]=fi;


            }
            out.put(paramName,fis);
        }


        return out;
    }

}
