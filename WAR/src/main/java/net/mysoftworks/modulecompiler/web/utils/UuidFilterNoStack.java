package net.mysoftworks.modulecompiler.web.utils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Stack;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class UuidFilterNoStack implements Filter {

	public static final String UUID_ATTRIBUTE = "uuid";
	public static final String UUID_PREFIX= "uuid_";

	protected static final Logger logger = Logger.getLogger("uuid");


	public void destroy() {
	}

	public UuidFilterNoStack() {
	}

	protected void logRequest(ServletRequest request,Exception e) {
		StringBuilder _log=new StringBuilder("\n=== Si e' verificato un errore nella request HTTP ====");
		if (request!=null) {
			_log.append("\n\tLocale:").append(request.getLocale())
					.append("\n\tContentType:").append(request.getContentType())
					.append("\n\tRemoteAddr:").append(request.getRemoteAddr())
					.append("\n\tRemoteHost:").append(request.getRemoteHost());
			if (request instanceof HttpServletRequest) {
				HttpServletRequest r = (HttpServletRequest)request;
				_log.append("\n\tRequestURI:").append(r.getRequestURI());
				_log.append("\n\tMethod:").append(r.getMethod());
				Enumeration<String> hn = r.getHeaderNames();
				while (hn.hasMoreElements())  {
					String h=hn.nextElement();
					_log.append("\n\tHeader:").append(h).append("-->[");
					Enumeration<String> values = r.getHeaders(h);
					int i=0;
					while (values.hasMoreElements())  {
						if (i!=0) _log.append(',');
						_log.append(values.nextElement());
						i++;
					}
					_log.append(']');
				}
			} else {
				_log.append("\n\tRequest class:").append(request.getClass().getName());
			}

		} else {
			_log.append("\nRequest is NULL!!!!!!!!!");
		}
		logger.log(Level.SEVERE,_log.toString(),e);

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		@SuppressWarnings("unchecked")
		Object oldUuid = request.getAttribute(UUID_ATTRIBUTE);
		String uuid = UUID_PREFIX + UUID.randomUUID();
		logger.finer("Set uuid request attribute:"+uuid);
		request.setAttribute(UUID_ATTRIBUTE, uuid);

		try{
			filterChain.doFilter(request, response);
		} catch (Exception e) {
			logRequest(request,e);
			throw  e;
		}
		request.setAttribute(UUID_ATTRIBUTE, oldUuid);
	}

	public void init(FilterConfig arg0) throws ServletException {
	}


}
