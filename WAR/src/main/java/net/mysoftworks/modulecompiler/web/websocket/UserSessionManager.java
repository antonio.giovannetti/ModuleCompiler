package net.mysoftworks.modulecompiler.web.websocket;

import net.mysoftworks.modulecompiler.Loggers;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import javax.websocket.Session;
import java.security.Principal;
import java.util.*;
import java.util.logging.Level;

public class UserSessionManager {

    private static UserSessionManager instance;
    private static final Object lock = new Object();

    public static UserSessionManager getInstance() {
        UserSessionManager r = instance;
        if (r == null) {
            synchronized (lock) {
                r=instance;
                if (r == null) {
                    r = new UserSessionManager();
                    instance = r;
                }
            }
        }
        return r;
    }

    // Hashtable is synchronized
    protected Map<String, Set<Session>> authSessions = new Hashtable<>();
    protected Set<Session> anonSessions = Collections.synchronizedSet(new HashSet<>());

    void addSession(Session session, boolean allowAnon) {
        if (session == null) return;
        Principal pr = session.getUserPrincipal();
        Loggers.logOperatore(Loggers.LOG_WEBSOCKET, Level.FINEST, "Aggiunta la sessione {0}", pr, session);
        if (pr == null) {
            if (!allowAnon) return;
            anonSessions.add(session);
        } else {
            Set<Session> sessions = authSessions.get(pr.getName());
            if (sessions == null) {
                sessions = new HashSet<>();
                authSessions.put(pr.getName(), sessions);
            }
            sessions.add(session);
        }
    }

    void removeSession(Session session) {
        if (session == null) return;
        Principal pr = session.getUserPrincipal();
        if (pr == null) {
            Loggers.logOperatore(Loggers.LOG_WEBSOCKET, Level.FINEST, "Cancello la sessione anonima {}", null, session);
            anonSessions.remove(session);
        } else {
            Loggers.logOperatore(Loggers.LOG_WEBSOCKET, Level.FINEST, "Cancello una sessione per l'utente", pr);
            Set<Session> _sessions = authSessions.get(pr.getName());
            if (_sessions != null) {
                _sessions.remove(session);
                if (_sessions.isEmpty()) {
                    authSessions.remove(pr.getName());
                }

            }
        }

    }

    public void sendMessage(PushMessage message, String userId) {
        if (MapUtils.isEmpty(authSessions)) {
            Loggers.logOperatore(Loggers.LOG_WEBSOCKET, Level.WARNING, "Non ci sono utenti autenticati", null);
            return;
        }
        if (StringUtils.isEmpty(userId)) {
            Loggers.logOperatore(Loggers.LOG_WEBSOCKET, Level.WARNING, "Non posso notificare ad utente null", null);
            return;
        }
        Set<Session> m = authSessions.get(userId);
        if (m != null) {
            for (Session session : m) {
                try {
                    session.getBasicRemote().sendObject(message);
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }
    }


    public void broadcastMessage(PushMessage message) {

    }
}
