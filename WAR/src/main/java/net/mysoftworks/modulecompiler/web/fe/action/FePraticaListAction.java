package net.mysoftworks.modulecompiler.web.fe.action;

import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.bl.manager.GestorePratica;
import net.mysoftworks.modulecompiler.bl.manager.GestoreProcedimento;
import net.mysoftworks.modulecompiler.model.RuoloPratica;
import net.mysoftworks.modulecompiler.model.meta.MetaProcedimento;
import net.mysoftworks.modulecompiler.model.values.Pratica;
import net.mysoftworks.modulecompiler.web.common.action.Struts2AjaxApplicationAwareAction;
import net.mysoftworks.modulecompiler.web.common.injectors.SessionDataInjector;
import net.mysoftworks.modulecompiler.web.fe.sd.GrantHelper;
import net.mysoftworks.modulecompiler.web.fe.sd.SDPratica;
import org.apache.struts2.ServletActionContext;

import javax.ejb.EJB;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;


public class FePraticaListAction extends Struts2AjaxApplicationAwareAction {

    public static final String LIST="list";

    @EJB
    private GestorePratica gestorePratica;
    @EJB
    private GestoreProcedimento gestoreProcedimento;
    private List<MetaProcedimento> procedimenti;

    public List<MetaProcedimento> getProcedimenti() {
        return procedimenti;
    }
//    public boolean hasRuoli(RuoloPratica.RuoloCompilazionePraticaOperator op,Set<RuoloPratica> ruoliPratica ,RuoloPratica.RuoloCompilazionePratica ... ruoli) {
//        System.out.println("\n\n==========hasRuoli:" +ruoli);
//        return super.hasRuoli(op,ruoliPratica,ruoli);
//    }

    public boolean isOwner(Set<RuoloPratica> ruoli) {
        return new GrantHelper(ruoli).isOwner();
    }

    public boolean canPrint(Set<RuoloPratica> ruoli) {
        return new GrantHelper(ruoli).canPrint();
    }

    private List<Pratica> pratiche;
    public List<Pratica> getPratiche() {
        return pratiche;
    }

    public String execute() {
        logOperatore(Loggers.LOG_FE,Level.INFO,"Lista dei procedimenti e delle pratiche");
        procedimenti = gestoreProcedimento.getLista(true);
        pratiche = gestorePratica.searchPratiche();
        new SessionDataInjector(ServletActionContext.getRequest()).purgeAll(SDPratica.class);
//        praticheComp = gestorePratica.searchPraticheCompDichiarazione();
        return LIST;
    }


}
