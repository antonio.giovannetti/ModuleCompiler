package net.mysoftworks.modulecompiler.web.common.injectors;

import net.mysoftworks.modulecompiler.render.EJBInjector;
import net.mysoftworks.modulecompiler.web.common.sd.SDProcedimento;
import net.mysoftworks.modulecompiler.web.common.sd.SessionData;
import net.mysoftworks.modulecompiler.web.common.sd.SessionDataDecorator;
import net.mysoftworks.modulecompiler.web.fe.sd.SDPratica;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
//import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SessionDataInjector  {
    private static final Logger log = Logger.getLogger(SessionDataInjector.class.getName());
    private static void logOperatore(Logger log, Level level, String s, Object ... params) {
        log.log(level,s,params);
    }

    public static final String SESSION_DATA_ATTRIBUTE = "MC_SESSION_DATA_ATTRIBUTE";
    public static final String REQUEST_DATA_ATTRIBUTE = "MC_REQUEST_DATA_ATTRIBUTE";

    private HttpServletRequest request;

    public SessionDataInjector(HttpServletRequest request) {
        this.request = request;
    }

    public <P> void injectSessionData(Object actionInstance) {
        if (actionInstance==null) return;
        EJBInjector.injectEJB(actionInstance,true);
        Class<?> clazz = actionInstance.getClass();
        while (!Object.class.equals(clazz)) {
            doInjectSessionData(clazz,actionInstance);
            clazz = clazz.getSuperclass();
        }
    }

    private Map<Class,Map<String, SessionData>> getMap() {
        HttpSession session = getSession();
        Map<Class,Map<String, SessionData>> out = (Map<Class, Map<String, SessionData>>) session.getAttribute(SESSION_DATA_ATTRIBUTE);
        if (out==null) {
            out=new HashMap<>();
            session.setAttribute(SESSION_DATA_ATTRIBUTE,out);
        }
        return out;
    }

    private <SD extends SessionData> Map<String, SessionData> getSessionDataMap(Class<SD> cl) {
        Map<Class, Map<String, SessionData>> m = getMap();
        Map<String, SessionData> msd = m.get(cl);
        if (msd == null) {
            logOperatore(log, Level.FINER, "getSessionDataMap, creo mappa per {0}",cl);
            msd=new HashMap<>();
            m.put(cl,msd);
        }
        return msd;
    }

    public <SD extends SessionData> SD getSessionData(Class<SD> cl, String key,boolean create,boolean removeNull) {
        Map<String, SessionData> msd = getSessionDataMap(cl);
        SD out = (SD) msd.get(key);
        SessionDataDecorator ann = cl.getAnnotation(SessionDataDecorator.class);
        String requestParam = ann.requestParam();
        logOperatore(log, Level.FINER, "getSessionData class:{0},key:{1},create:{2},removeNull:{3}, existing:{4}",cl,key,create,removeNull,out);

        if (out==null && create) {
            logOperatore(log, Level.FINEST, "Creating session data for class:{0},key:{1},create:{2},removeNull:{3}, existing:{4}",cl,key,create,removeNull,out);
            out = createSessionData(cl,key);
            msd.put(key,out);
            request.setAttribute(REQUEST_DATA_ATTRIBUTE+requestParam,out.getKey());
        } else {
            logOperatore(log, Level.FINE, "Use existing session data for class:{0},key:{1},create:{2},removeNull:{3}, existing:{4}",cl,key,create,removeNull,out);
        }
        if (key!=null && removeNull) msd.remove(null);
        return out;
    }

    public <SD extends SessionData> void purgeAll(Class<SD> cl) {
        getMap().remove(cl);
    }

    public <SD extends SessionData> void purge(Class<SD> cl,String key) {
        if (StringUtils.isEmpty(key))  {
            logOperatore(log, Level.WARNING, "Unable to purge sessiondata {0} using empty key",cl);
            return;
        }
        Map<String, SessionData> m = getMap().get(cl);
        if (MapUtils.isNotEmpty(m)) {
            m.remove(key);
        } else {
            logOperatore(log, Level.WARNING, "No instances of sessiondata {0}",cl);
        }
    }

    private HttpSession getSession() {
        HttpSession session = request.getSession();
        return session;
    }

    private <SD extends SessionData> SD createSessionData(Class<SD> cl, String key) {
        SD sessionData = null;
        try {
            Constructor<SD> constr = cl.getConstructor(String.class);
            sessionData = constr.newInstance(key);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return sessionData;
    }

//    public static void updateSessionData(SessionData sd) {
//        Class cl = sd.getClass();
//        Map<String, SessionData> msd = getSessionDataMap(cl);
//        String sessionAttribute = cl.getName()+"_"+sd.getKey();
//        logOperatore(log, Level.FINE, "Update sessiondata {0} with session attribute: {1}",sd,sessionAttribute);
//        getSession().setAttribute(sessionAttribute,sd);
//    }

    private void doInjectSessionData(Class clazz,Object actionInstance) {

        logOperatore(log, Level.FINER, "Checking for injectables in action {1} of type {0}",clazz,actionInstance);
        for(Field f: clazz.getDeclaredFields()) {
            logOperatore(log, Level.FINEST, "Found field {0}#{1}",actionInstance,f);

            if (SessionData.class.isAssignableFrom(f.getType())) {
                Class<SessionData> cl = (Class<SessionData>) f.getType();
                SessionDataDecorator ann = cl.getAnnotation(SessionDataDecorator.class);
                logOperatore(log, Level.FINEST, "Found injectable field {0}#{1}",actionInstance,f);
                if (ann != null) {
                    if (StringUtils.isNotEmpty(ann.requestParam())) {
                        String requestParam = ann.requestParam();
                        boolean forceCreate = true; //ann.forceCreate();
                        HttpSession session = getSession();
                        String requestValue = request.getParameter(requestParam);
                        if (StringUtils.isEmpty(requestValue)) {
                            Object _requestValue = request.getAttribute(REQUEST_DATA_ATTRIBUTE + requestParam);
                            if (_requestValue!=null) requestValue = _requestValue.toString();
                            logOperatore(log, Level.FINER, "Reading request attribute {0}: {1}", REQUEST_DATA_ATTRIBUTE+requestParam,requestValue);
                        }
                        if (StringUtils.isEmpty(requestValue) && ann.optional()) return;
                        SessionData sessionData  = null;
//        if (requestParam != null) {
//            logOperatore(log, Level.FINER, "Read session attribute: {0}_{1}",sessionDataClass.getSimpleName(),requestParam);
//            o = (T)session.getAttribute(sessionDataClass.getSimpleName()+"_"+requestParam);
//        }
                        logOperatore(log, Level.FINER, "Request param {0}: {1}", requestParam,requestValue);
                        sessionData = getSessionData(cl,requestValue,forceCreate,false);

                        logOperatore(log, Level.FINE, "Injecting instance of {0} into {1}",
                                sessionData,actionInstance);
//                        SessionData sessionData = getSessionData(cl, ann.requestParam(), false);
                        try {
                            f.setAccessible(true);
                            f.set(actionInstance,sessionData);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    } else {
                        throw new RuntimeException("SessionData " + cl + " needs [" + ann.requestParam() + "] request param to inject in " + actionInstance);
                    }

                } else {
                    logOperatore(log, Level.WARNING, "Class {0} has no @SessionDataDecorator annotation",cl);
                }

            }
        }
    }

}
