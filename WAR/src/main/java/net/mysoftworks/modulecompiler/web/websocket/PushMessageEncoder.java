package net.mysoftworks.modulecompiler.web.websocket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import java.io.Serializable;

public class PushMessageEncoder implements Encoder.Text<PushMessage> {

//    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String encode(PushMessage pushMessage) throws EncodeException {
//        synchronized (objectMapper) {
            try {
                return new ObjectMapper().writeValueAsString(pushMessage);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
//        }
        return null;
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
