package net.mysoftworks.modulecompiler.web.fe.action;

import net.mysoftworks.modulecompiler.bl.manager.KeycloakUtil;
import net.mysoftworks.modulecompiler.dto.BLException;
import net.mysoftworks.modulecompiler.dto.FileUpload;
import net.mysoftworks.modulecompiler.bl.manager.GestoreDichiarazione;
import net.mysoftworks.modulecompiler.bl.manager.GestoreProcedimento;
import net.mysoftworks.modulecompiler.model.RuoloPratica;
import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.model.values.Pratica;
import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.render.GestoreRender;
import net.mysoftworks.modulecompiler.search.SearchOutput;
import net.mysoftworks.modulecompiler.web.common.action.Struts2AjaxApplicationAwareAction;
import net.mysoftworks.modulecompiler.web.common.action.Struts2ApplicationAwareAction;
import net.mysoftworks.modulecompiler.web.fe.sd.GrantHelper;
import net.mysoftworks.modulecompiler.web.fe.sd.SDPratica;
import net.mysoftworks.modulecompiler.web.utils.UuidFilter;
import net.mysoftworks.modulecompiler.web.websocket.PushMessage;
import net.mysoftworks.modulecompiler.web.websocket.UserSessionManager;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.keycloak.representations.idm.UserRepresentation;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.*;
import java.util.logging.Level;


public class FeDichiarazioneAction extends Struts2AjaxApplicationAwareAction {
    private static final String REQUEST_URI_PARAM="requestURI";
    private static final String CONTEXT_PATH_PARAM="contextPath";
    @EJB
    private GestoreDichiarazione gestoreDichiarazione;

    @EJB
    private GestoreRender gestoreRender;

    @EJB
    private KeycloakUtil keycloakUtil;


    public String changeCompiler() {
//        if (!isCanChangeCompiler()) {
//            addActionError(getText("fe.pratica.dich.change_compiler.cannot_change"));
//            return FORBIDDEN;
//        }

        if  (dichiarazione==null) {
            Loggers.logOperatore(Loggers.LOG_FE,Level.SEVERE,"Non posso rceuperare la dicharazione",getPrincipal());
            return ERROR;
        }
        if (sdPratica.canChangeCompiler(dichiarazione)) {
            String user = ServletActionContext.getRequest().getParameter("user");
            if (StringUtils.isEmpty(user)) {
                addActionError(getText("fe.pratica.dich.change_compiler.unspecified_compiler"));
                return AJAX_ERROR;
            }
            if (dichiarazione.getId()==null) {
                addActionError("Id dichiarazione e' null");
                return AJAX_ERROR;
            }
            try {
                if (getPrincipal().getName().equals(user)) {
                    gestoreDichiarazione.changeCompiler(dichiarazione.getId(), null);
                } else {
                    gestoreDichiarazione.changeCompiler(dichiarazione.getId(), user);
//                    String msg = getText("admin.dichiarazione.change.assignee", null, Arrays.asList("/mc/fe/pratica!detail.action?pratica.id=", getPratica().getId(), dichiarazione.getId()));
                    PushMessage pm = new PushMessage(PushMessage.TYPE.NOTIFICA_CAMBIO_COMPILATORE_DICHIARAZIONE,null/*"window.location.reload();"*/);
                    pm.addKey(PushMessage.ID_PRATICA,getPratica().getId());
                    pm.addKey(PushMessage.ID_DICHIARAZIONE,dichiarazione.getId());
                    UserSessionManager.getInstance().sendMessage(pm,user);
                }
                sdPratica.updateCompiler(dichiarazione.getId(), user);
            } catch (Exception e) {
                Loggers.LOG_FE.log(Level.WARNING,"Errore",e);
                addActionError("Errore:"+e.getMessage());
            }
            return AJAX_SUCCESS;
        }
        throw new RuntimeException("Non si dispone dei diritti per modificare il compilatore");

    }


    public String compilerList() {
        String q = ServletActionContext.getRequest().getParameter("q");
        output = keycloakUtil.usersByClientRole("compiler", q);
        return AJAX_SUCCESS;
    }

    public static Map<String,Object> getDefaultMap() {
        Map<String,Object> out = new HashMap<>();
        out.put(UuidFilter.UUID_ATTRIBUTE,ServletActionContext.getRequest().getAttribute(UuidFilter.UUID_ATTRIBUTE));
        out.put(REQUEST_URI_PARAM,ServletActionContext.getRequest().getRequestURI());
        out.put(CONTEXT_PATH_PARAM,ServletActionContext.getRequest().getContextPath());
//        System.out.println("\n\n========RequestURI:" + ServletActionContext.getRequest().getRequestURI());
//        System.out.println("========ContextPath:" + ServletActionContext.getRequest().getContextPath());
//        System.out.println("========ServletPath:" + ServletActionContext.getRequest().getServletPath());
//        System.out.println("========PathTranslated:" + ServletActionContext.getRequest().getPathTranslated());
//        System.out.println("========SC-ContextPath:" + ServletActionContext.getServletContext().getContextPath());
//        System.out.println("========SC-ServerInfo:" + ServletActionContext.getServletContext().getServerInfo());
        return out;
    }


    private Object[] partToValue(Part part) {
        logOperatore(Loggers.LOG_FE,Level.FINER,"Submitted part {0}",part);
        byte[] content = null;
        try {
            content = IOUtils.toByteArray(part.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileUpload out = new FileUpload(content,part.getName(),part.getContentType());
        return new Object[]{out};
    }

    private Map<String,Object[]> requestToMap() {
        HttpServletRequest req = ServletActionContext.getRequest();
        Map<String,Object[]> out = new HashMap<>();
        Enumeration<String> _p = req.getParameterNames();
        while(_p.hasMoreElements()) {
            String key = _p.nextElement();
            String[] val = req.getParameterValues(key);
            out.put(key,val);
        }
        try {
            if (CollectionUtils.isNotEmpty(req.getParts())) {
                for (Part p:req.getParts()) {
                    out.put(p.getName(),partToValue(p));
                }
            }
        } catch (Throwable  e) {
            e.printStackTrace();
        }


        return out;
    }

    @EJB
    private GestoreProcedimento gestoreProcedimento;

    private SDPratica sdPratica;
    private Dichiarazione dichiarazione=new Dichiarazione();


    public Dichiarazione getDichiarazione() {
        return dichiarazione;
    }

    public String view() {
        logOperatore(Loggers.LOG_FE,Level.FINE,"Visualizza dichiarazione {0,number,#}",dichiarazione.getId());
//        String text = gestoreRender.viewDichiarazione(dichiarazione.getId(), getDefaultMap(),false);
        String text = gestoreRender.viewDichiarazione(dichiarazione.getId(), getDefaultMap(),false);
        try {
//            text.get
//            getResponse().getWriter().write(text);
            getResponse().getOutputStream().write(text.getBytes());
        } catch (Exception e) {
            addActionError(e.getMessage());
            e.printStackTrace();
            logOperatore(Loggers.LOG_FE,Level.SEVERE,"Errore rendering viewDichiarazione metaDichiarazione:"+dichiarazione,e);
        }
        return null;
    }

    public String edit() {
        logOperatore(Loggers.LOG_FE,Level.INFO,"Inizio modifica dichiarazione {0,number,#}",dichiarazione.getId());
        String text = gestoreRender.viewDichiarazione(dichiarazione.getId(), getDefaultMap(),true);
        try {
            getResponse().getOutputStream().write(text.getBytes());
        } catch (Exception e) {
            addActionError(e.getMessage());
            e.printStackTrace();
            logOperatore(Loggers.LOG_FE,Level.SEVERE,"Errore rendering viewDichiarazione dichiarazione:"+dichiarazione,e);
        }
        return null;
    }


    private void validateDich() {

    }


    public String save() throws Exception {
        logOperatore(Loggers.LOG_FE,Level.INFO,"Salva dichiarazione {0,number,#}",dichiarazione.getId());
        Pratica p = getPratica();
        try {
            Dichiarazione dich = gestoreDichiarazione.submit(p.getId(),dichiarazione.getId(),requestToMap(),Struts2ApplicationAwareAction.readUploadedFiles());
            sdPratica.updateDichiarazione(dich);
        } catch (BLException t) {
            logOperatore(Loggers.LOG_FE,Level.SEVERE,"Errore nel savataggio dichiarazione",t);
            addActionError(getText(t.getKey(),t.getParams()));
            return AJAX_ERROR;
        }
        return AJAX_SUCCESS;
    }

    public String remove() throws Exception {
        logOperatore(Loggers.LOG_FE,Level.INFO,"Cancello dichiarazione {0,number,#}",dichiarazione.getId());
        gestoreDichiarazione.removeData(dichiarazione.getId());
        return null;
    }

    private File fileUpload;
    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }


    public Pratica getPratica() {
        return sdPratica.getPratica();
    }

    public void setPratica(Pratica pratica ) {
        sdPratica.setPratica(pratica );
    }
//    protected void initLookups(Context ctx) {
//        gestoreDichiarazione = lookup(GestoreDichiarazione.class);
//    }


}
