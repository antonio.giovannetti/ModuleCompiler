package net.mysoftworks.modulecompiler.web.common.sd;

import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.model.meta.MetaProcedimento;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;

import java.util.List;
import java.util.logging.Level;

/**
 * Created by antonio on 02/07/17.
 */
@SessionDataDecorator(requestParam = "procedimento.id",optional = false)
public class SDProcedimento extends SessionData<Long> {
//    public static final String REQUEST_KEY ="procedimento.id";

    private MetaProcedimento procedimento = new MetaProcedimento();
    private List<MetaProcedimento> ancestors;
    private List<MetaProcedimento> children;

    /**
     * Business Logic to find key from request
     *
     * @param _key
     */
    public SDProcedimento(String _key) {
        if (StringUtils.isNotEmpty(_key)) {
            key=Long.parseLong(_key);
            procedimento.setId(key);
        } else {
            key=null;
            procedimento.setId(null);
            Loggers.logOperatore(Loggers.LOG_FE, Level.WARNING,"Creazione SDProcedimento con id null", ServletActionContext.getRequest().getUserPrincipal());
            //            throw new RuntimeException("Key is null for SDProcedimento ");
        }
    }


    public MetaProcedimento getProcedimento() {
        return procedimento;
    }

    public void setProcedimento(MetaProcedimento procedimento) {
        this.procedimento = procedimento;
    }

    public List<MetaProcedimento> getAncestors() {
        return ancestors;
    }

    public void setAncestors(List<MetaProcedimento> ancestors) {
        this.ancestors = ancestors;
    }

    //    public Long getKey() {
//        if (procedimento!=null) return procedimento.getId();
//        return null;
//    }

//    @Override
//    public String getRequestParameterName() {
//        return REQUEST_KEY;
//    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SDProcedimento{");
        sb.append("procedimento=").append(procedimento);
        sb.append(", key=").append(key);
        sb.append('}');
        return sb.toString();
    }

    public void setChildren(List<MetaProcedimento> children) {
        this.children = children;
    }

    public List<MetaProcedimento> getChildren() {
        return children;
    }
}
