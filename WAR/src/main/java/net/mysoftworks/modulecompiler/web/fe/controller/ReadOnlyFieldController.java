package net.mysoftworks.modulecompiler.web.fe.controller;

import net.mysoftworks.modulecompiler.model.values.Value;
import net.mysoftworks.modulecompiler.render.DichiarazioneRenderer;
import net.mysoftworks.modulecompiler.render.FieldRenderer;

public class ReadOnlyFieldController extends FieldRenderer {

    public ReadOnlyFieldController(DichiarazioneRenderer dichiarazioneRender, Value value, boolean withLabel,boolean inline) {
        super(dichiarazioneRender,value, withLabel,false,inline);
    }

}
