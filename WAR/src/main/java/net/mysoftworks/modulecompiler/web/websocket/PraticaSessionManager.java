package net.mysoftworks.modulecompiler.web.websocket;

import javax.websocket.Session;

public class PraticaSessionManager extends UserSessionManager {
    public static final String PRATICA_PROP = "idPratica";
    private static PraticaSessionManager instance;
    private static final Object lock = new Object();

    public static PraticaSessionManager getInstance() {
        PraticaSessionManager r = instance;
        if (r == null) {
            synchronized (lock) {
                r=instance;
                if (r == null) {
                    r = new PraticaSessionManager();
                    instance = r;
                }
            }
        }
        return r;
    }

    void addSession(Session session, boolean allowAnon, Long idPratica) {
        if (session == null) return;
        if (idPratica == null) throw new RuntimeException("idPratica non puo' essere null");
        session.getUserProperties().put(PRATICA_PROP, idPratica);
        super.addSession(session, allowAnon);
    }

    public void broadcastMessage(PushMessage message) {

    }
}
