package net.mysoftworks.modulecompiler.web.common.action;

import net.mysoftworks.modulecompiler.bl.manager.GestoreProcedimento;
import net.mysoftworks.modulecompiler.model.meta.MetaDichiarazione;
import net.mysoftworks.modulecompiler.model.meta.MetaProcedimento;
import net.mysoftworks.modulecompiler.report.iface.AvailableReports;
import net.mysoftworks.modulecompiler.report.iface.Report;
import net.mysoftworks.modulecompiler.report.iface.ReportManager;
import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.web.common.sd.SDProcedimento;
import net.mysoftworks.modulecompiler.web.common.sd.SessionDataDecorator;
import net.mysoftworks.modulecompiler.web.utils.DownloadBean;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;

import javax.ejb.EJB;
import java.util.List;
import java.util.logging.Level;


public abstract class ProcedimentoAction extends Struts2AjaxApplicationAwareAction {
//    protected static final Logger log = Logger.getLogger(ProcedimentoAction.class.getName());

    public static final String FA_ICONS = "fab fa-500px|fab fa-accessible-icon|fab fa-accusoft|fas fa-address-book|fas fa-address-card|fas fa-adjust|fab fa-adn|fab fa-adversal|fab fa-affiliatetheme|fab fa-algolia|fas fa-align-center|fas fa-align-justify|fas fa-align-left|fas fa-align-right|fas fa-allergies|fab fa-amazon|fab fa-amazon-pay|fas fa-ambulance|fas fa-american-sign-language-interpreting|fab fa-amilia|fas fa-anchor|fab fa-android|fab fa-angellist|fas fa-angle-double-down|fas fa-angle-double-left|fas fa-angle-double-right|fas fa-angle-double-up|fas fa-angle-down|fas fa-angle-left|fas fa-angle-right|fas fa-angle-up|fab fa-angrycreative|fab fa-angular|fab fa-app-store|fab fa-app-store-ios|fab fa-apper|fab fa-apple|fab fa-apple-pay|fas fa-archive|fas fa-arrow-alt-circle-down|fas fa-arrow-alt-circle-left|fas fa-arrow-alt-circle-right|fas fa-arrow-alt-circle-up|fas fa-arrow-circle-down|fas fa-arrow-circle-left|fas fa-arrow-circle-right|fas fa-arrow-circle-up|fas fa-arrow-down|fas fa-arrow-left|fas fa-arrow-right|fas fa-arrow-up|fas fa-arrows-alt|fas fa-arrows-alt-h|fas fa-arrows-alt-v|fas fa-assistive-listening-systems|fas fa-asterisk|fab fa-asymmetrik|fas fa-at|fab fa-audible|fas fa-audio-description|fab fa-autoprefixer|fab fa-avianex|fab fa-aviato|fab fa-aws|fas fa-backward|fas fa-balance-scale|fas fa-ban|fas fa-band-aid|fab fa-bandcamp|fas fa-barcode|fas fa-bars|fas fa-baseball-ball|fas fa-basketball-ball|fas fa-bath|fas fa-battery-empty|fas fa-battery-full|fas fa-battery-half|fas fa-battery-quarter|fas fa-battery-three-quarters|fas fa-bed|fas fa-beer|fab fa-behance|fab fa-behance-square|fas fa-bell|fas fa-bell-slash|fas fa-bicycle|fab fa-bimobject|fas fa-binoculars|fas fa-birthday-cake|fab fa-bitbucket|fab fa-bitcoin|fab fa-bity|fab fa-black-tie|fab fa-blackberry|fas fa-blind|fab fa-blogger|fab fa-blogger-b|fab fa-bluetooth|fab fa-bluetooth-b|fas fa-bold|fas fa-bolt|fas fa-bomb|fas fa-book|fas fa-bookmark|fas fa-bowling-ball|fas fa-box|fas fa-box-open|fas fa-boxes|fas fa-braille|fas fa-briefcase|fas fa-briefcase-medical|fab fa-btc|fas fa-bug|fas fa-building|fas fa-bullhorn|fas fa-bullseye|fas fa-burn|fab fa-buromobelexperte|fas fa-bus|fab fa-buysellads|fas fa-calculator|fas fa-calendar|fas fa-calendar-alt|fas fa-calendar-check|fas fa-calendar-minus|fas fa-calendar-plus|fas fa-calendar-times|fas fa-camera|fas fa-camera-retro|fas fa-capsules|fas fa-car|fas fa-caret-down|fas fa-caret-left|fas fa-caret-right|fas fa-caret-square-down|fas fa-caret-square-left|fas fa-caret-square-right|fas fa-caret-square-up|fas fa-caret-up|fas fa-cart-arrow-down|fas fa-cart-plus|fab fa-cc-amazon-pay|fab fa-cc-amex|fab fa-cc-apple-pay|fab fa-cc-diners-club|fab fa-cc-discover|fab fa-cc-jcb|fab fa-cc-mastercard|fab fa-cc-paypal|fab fa-cc-stripe|fab fa-cc-visa|fab fa-centercode|fas fa-certificate|fas fa-chart-area|fas fa-chart-bar|fas fa-chart-line|fas fa-chart-pie|fas fa-check|fas fa-check-circle|fas fa-check-square|fas fa-chess|fas fa-chess-bishop|fas fa-chess-board|fas fa-chess-king|fas fa-chess-knight|fas fa-chess-pawn|fas fa-chess-queen|fas fa-chess-rook|fas fa-chevron-circle-down|fas fa-chevron-circle-left|fas fa-chevron-circle-right|fas fa-chevron-circle-up|fas fa-chevron-down|fas fa-chevron-left|fas fa-chevron-right|fas fa-chevron-up|fas fa-child|fab fa-chrome|fas fa-circle|fas fa-circle-notch|fas fa-clipboard|fas fa-clipboard-check|fas fa-clipboard-list|fas fa-clock|fas fa-clone|fas fa-closed-captioning|fas fa-cloud|fas fa-cloud-download-alt|fas fa-cloud-upload-alt|fab fa-cloudscale|fab fa-cloudsmith|fab fa-cloudversify|fas fa-code|fas fa-code-branch|fab fa-codepen|fab fa-codiepie|fas fa-coffee|fas fa-cog|fas fa-cogs|fas fa-columns|fas fa-comment|fas fa-comment-alt|fas fa-comment-dots|fas fa-comment-slash|fas fa-comments|fas fa-compass|fas fa-compress|fab fa-connectdevelop|fab fa-contao|fas fa-copy|fas fa-copyright|fas fa-couch|fab fa-cpanel|fab fa-creative-commons|fas fa-credit-card|fas fa-crop|fas fa-crosshairs|fab fa-css3|fab fa-css3-alt|fas fa-cube|fas fa-cubes|fas fa-cut|fab fa-cuttlefish|fab fa-d-and-d|fab fa-dashcube|fas fa-database|fas fa-deaf|fab fa-delicious|fab fa-deploydog|fab fa-deskpro|fas fa-desktop|fab fa-deviantart|fas fa-diagnoses|fab fa-digg|fab fa-digital-ocean|fab fa-discord|fab fa-discourse|fas fa-dna|fab fa-dochub|fab fa-docker|fas fa-dollar-sign|fas fa-dolly|fas fa-dolly-flatbed|fas fa-donate|fas fa-dot-circle|fas fa-dove|fas fa-download|fab fa-draft2digital|fab fa-dribbble|fab fa-dribbble-square|fab fa-dropbox|fab fa-drupal|fab fa-dyalog|fab fa-earlybirds|fab fa-edge|fas fa-edit|fas fa-eject|fab fa-elementor|fas fa-ellipsis-h|fas fa-ellipsis-v|fab fa-ember|fab fa-empire|fas fa-envelope|fas fa-envelope-open|fas fa-envelope-square|fab fa-envira|fas fa-eraser|fab fa-erlang|fab fa-ethereum|fab fa-etsy|fas fa-euro-sign|fas fa-exchange-alt|fas fa-exclamation|fas fa-exclamation-circle|fas fa-exclamation-triangle|fas fa-expand|fas fa-expand-arrows-alt|fab fa-expeditedssl|fas fa-external-link-alt|fas fa-external-link-square-alt|fas fa-eye|fas fa-eye-dropper|fas fa-eye-slash|fab fa-facebook|fab fa-facebook-f|fab fa-facebook-messenger|fab fa-facebook-square|fas fa-fast-backward|fas fa-fast-forward|fas fa-fax|fas fa-female|fas fa-fighter-jet|fas fa-file|fas fa-file-alt|fas fa-file-archive|fas fa-file-audio|fas fa-file-code|fas fa-file-excel|fas fa-file-image|fas fa-file-medical|fas fa-file-medical-alt|fas fa-file-pdf|fas fa-file-powerpoint|fas fa-file-video|fas fa-file-word|fas fa-film|fas fa-filter|fas fa-fire|fas fa-fire-extinguisher|fab fa-firefox|fas fa-first-aid|fab fa-first-order|fab fa-firstdraft|fas fa-flag|fas fa-flag-checkered|fas fa-flask|fab fa-flickr|fab fa-flipboard|fab fa-fly|fas fa-folder|fas fa-folder-open|fas fa-font|fab fa-font-awesome|fab fa-font-awesome-alt|fab fa-font-awesome-flag|fab fa-fonticons|fab fa-fonticons-fi|fas fa-football-ball|fab fa-fort-awesome|fab fa-fort-awesome-alt|fab fa-forumbee|fas fa-forward|fab fa-foursquare|fab fa-free-code-camp|fab fa-freebsd|fas fa-frown|fas fa-futbol|fas fa-gamepad|fas fa-gavel|fas fa-gem|fas fa-genderless|fab fa-get-pocket|fab fa-gg|fab fa-gg-circle|fas fa-gift|fab fa-git|fab fa-git-square|fab fa-github|fab fa-github-alt|fab fa-github-square|fab fa-gitkraken|fab fa-gitlab|fab fa-gitter|fas fa-glass-martini|fab fa-glide|fab fa-glide-g|fas fa-globe|fab fa-gofore|fas fa-golf-ball|fab fa-goodreads|fab fa-goodreads-g|fab fa-google|fab fa-google-drive|fab fa-google-play|fab fa-google-plus|fab fa-google-plus-g|fab fa-google-plus-square|fab fa-google-wallet|fas fa-graduation-cap|fab fa-gratipay|fab fa-grav|fab fa-gripfire|fab fa-grunt|fab fa-gulp|fas fa-h-square|fab fa-hacker-news|fab fa-hacker-news-square|fas fa-hand-holding|fas fa-hand-holding-heart|fas fa-hand-holding-usd|fas fa-hand-lizard|fas fa-hand-paper|fas fa-hand-peace|fas fa-hand-point-down|fas fa-hand-point-left|fas fa-hand-point-right|fas fa-hand-point-up|fas fa-hand-pointer|fas fa-hand-rock|fas fa-hand-scissors|fas fa-hand-spock|fas fa-hands|fas fa-hands-helping|fas fa-handshake|fas fa-hashtag|fas fa-hdd|fas fa-heading|fas fa-headphones|fas fa-heart|fas fa-heartbeat|fab fa-hips|fas fa-hire-a-helper|fas fa-history|fas fa-hockey-puck|fas fa-home|fas fa-hooli|fas fa-hospital|fas fa-hospital-alt|fas fa-hospital-symbol|fas fa-hotjar|fas fa-hourglass|fas fa-hourglass-end|fas fa-hourglass-half|fas fa-hourglass-start|fas fa-houzz|fas fa-html5|fas fa-hubspot|fas fa-i-cursor|fas fa-id-badge|fas fa-id-card|fas fa-id-card-alt|fas fa-image|fas fa-images|fas fa-imdb|fas fa-inbox|fas fa-indent|fas fa-industry|fas fa-info|fas fa-info-circle|fas fa-instagram|fas fa-internet-explorer|fas fa-ioxhost|fas fa-italic|fas fa-itunes|fas fa-itunes-note|fas fa-java|fas fa-jenkins|fas fa-joget|fas fa-joomla|fas fa-js|fas fa-js-square|fas fa-jsfiddle|fas fa-key|fas fa-keyboard|fas fa-keycdn|fas fa-kickstarter|fas fa-kickstarter-k|fas fa-korvue|fas fa-language|fas fa-laptop|fas fa-laravel|fas fa-lastfm|fas fa-lastfm-square|fas fa-leaf|fas fa-leanpub|fas fa-lemon|fas fa-less|fas fa-level-down-alt|fas fa-level-up-alt|fas fa-life-ring|fas fa-lightbulb|fas fa-line|fas fa-link|fas fa-linkedin|fas fa-linkedin-in|fas fa-linode|fas fa-linux|fas fa-lira-sign|fas fa-list|fas fa-list-alt|fas fa-list-ol|fas fa-list-ul|fas fa-location-arrow|fas fa-lock|fas fa-lock-open|fas fa-long-arrow-alt-down|fas fa-long-arrow-alt-left|fas fa-long-arrow-alt-right|fas fa-long-arrow-alt-up|fas fa-low-vision|fas fa-lyft|fas fa-magento|fas fa-magic|fas fa-magnet|fas fa-male|fas fa-map|fas fa-map-marker|fas fa-map-marker-alt|fas fa-map-pin|fas fa-map-signs|fas fa-mars|fas fa-mars-double|fas fa-mars-stroke|fas fa-mars-stroke-h|fas fa-mars-stroke-v|fas fa-maxcdn|fas fa-medapps|fas fa-medium|fas fa-medium-m|fas fa-medkit|fas fa-medrt|fas fa-meetup|fas fa-meh|fas fa-mercury|fas fa-microchip|fas fa-microphone|fas fa-microphone-slash|fas fa-microsoft|fas fa-minus|fas fa-minus-circle|fas fa-minus-square|fas fa-mix|fas fa-mixcloud|fas fa-mizuni|fas fa-mobile|fas fa-mobile-alt|fas fa-modx|fas fa-monero|fas fa-money-bill-alt|fas fa-moon|fas fa-motorcycle|fas fa-mouse-pointer|fas fa-music|fas fa-napster|fas fa-neuter|fas fa-newspaper|fas fa-nintendo-switch|fas fa-node|fas fa-node-js|fas fa-notes-medical|fas fa-npm|fas fa-ns8|fas fa-nutritionix|fas fa-object-group|fas fa-object-ungroup|fas fa-odnoklassniki|fas fa-odnoklassniki-square|fas fa-opencart|fas fa-openid|fas fa-opera|fas fa-optin-monster|fas fa-osi|fas fa-outdent|fas fa-page4|fas fa-pagelines|fas fa-paint-brush|fas fa-palfed|fas fa-pallet|fas fa-paper-plane|fas fa-paperclip|fas fa-parachute-box|fas fa-paragraph|fas fa-paste|fas fa-patreon|fas fa-pause|fas fa-pause-circle|fas fa-paw|fas fa-paypal|fas fa-pen-square|fas fa-pencil-alt|fas fa-people-carry|fas fa-percent|fas fa-periscope|fas fa-phabricator|fas fa-phoenix-framework|fas fa-phone|fas fa-phone-slash|fas fa-phone-square|fas fa-phone-volume|fas fa-php|fas fa-pied-piper|fas fa-pied-piper-alt|fas fa-pied-piper-hat|fas fa-pied-piper-pp|fas fa-piggy-bank|fas fa-pills|fas fa-pinterest|fas fa-pinterest-p|fas fa-pinterest-square|fas fa-plane|fas fa-play|fas fa-play-circle|fas fa-playstation|fas fa-plug|fas fa-plus|fas fa-plus-circle|fas fa-plus-square|fas fa-podcast|fas fa-poo|fas fa-pound-sign|fas fa-power-off|fas fa-prescription-bottle|fas fa-prescription-bottle-alt|fas fa-print|fas fa-procedures|fas fa-product-hunt|fas fa-pushed|fas fa-puzzle-piece|fas fa-python|fas fa-qq|fas fa-qrcode|fas fa-question|fas fa-question-circle|fas fa-quidditch|fas fa-quinscape|fas fa-quora|fas fa-quote-left|fas fa-quote-right|fas fa-random|fas fa-ravelry|fas fa-react|fas fa-readme|fas fa-rebel|fas fa-recycle|fas fa-red-river|fas fa-reddit|fas fa-reddit-alien|fas fa-reddit-square|fas fa-redo|fas fa-redo-alt|fas fa-registered|fas fa-rendact|fas fa-renren|fas fa-reply|fas fa-reply-all|fas fa-replyd|fas fa-resolving|fas fa-retweet|fas fa-ribbon|fas fa-road|fas fa-rocket|fas fa-rocketchat|fas fa-rockrms|fas fa-rss|fas fa-rss-square|fas fa-ruble-sign|fas fa-rupee-sign|fas fa-safari|fas fa-sass|fas fa-save|fas fa-schlix|fas fa-scribd|fas fa-search|fas fa-search-minus|fas fa-search-plus|fas fa-searchengin|fas fa-seedling|fas fa-sellcast|fas fa-sellsy|fas fa-server|fas fa-servicestack|fas fa-share|fas fa-share-alt|fas fa-share-alt-square|fas fa-share-square|fas fa-shekel-sign|fas fa-shield-alt|fas fa-ship|fas fa-shipping-fast|fas fa-shirtsinbulk|fas fa-shopping-bag|fas fa-shopping-basket|fas fa-shopping-cart|fas fa-shower|fas fa-sign|fas fa-sign-in-alt|fas fa-sign-language|fas fa-sign-out-alt|fas fa-signal|fas fa-simplybuilt|fas fa-sistrix|fas fa-sitemap|fas fa-skyatlas|fas fa-skype|fas fa-slack|fas fa-slack-hash|fas fa-sliders-h|fas fa-slideshare|fas fa-smile|fas fa-smoking|fas fa-snapchat|fas fa-snapchat-ghost|fas fa-snapchat-square|fas fa-snowflake|fas fa-sort|fas fa-sort-alpha-down|fas fa-sort-alpha-up|fas fa-sort-amount-down|fas fa-sort-amount-up|fas fa-sort-down|fas fa-sort-numeric-down|fas fa-sort-numeric-up|fas fa-sort-up|fas fa-soundcloud|fas fa-space-shuttle|fas fa-speakap|fas fa-spinner|fas fa-spotify|fas fa-square|fas fa-square-full|fas fa-stack-exchange|fas fa-stack-overflow|fas fa-star|fas fa-star-half|fas fa-staylinked|fas fa-steam|fas fa-steam-square|fas fa-steam-symbol|fas fa-step-backward|fas fa-step-forward|fas fa-stethoscope|fas fa-sticker-mule|fas fa-sticky-note|fas fa-stop|fas fa-stop-circle|fas fa-stopwatch|fas fa-strava|fas fa-street-view|fas fa-strikethrough|fas fa-stripe|fas fa-stripe-s|fas fa-studiovinari|fas fa-stumbleupon|fas fa-stumbleupon-circle|fas fa-subscript|fas fa-subway|fas fa-suitcase|fas fa-sun|fas fa-superpowers|fas fa-superscript|fas fa-supple|fas fa-sync|fas fa-sync-alt|fas fa-syringe|fas fa-table|fas fa-table-tennis|fas fa-tablet|fas fa-tablet-alt|fas fa-tablets|fas fa-tachometer-alt|fas fa-tag|fas fa-tags|fas fa-tape|fas fa-tasks|fas fa-taxi|fas fa-telegram|fas fa-telegram-plane|fas fa-tencent-weibo|fas fa-terminal|fas fa-text-height|fas fa-text-width|fas fa-th|fas fa-th-large|fas fa-th-list|fas fa-themeisle|fas fa-thermometer|fas fa-thermometer-empty|fas fa-thermometer-full|fas fa-thermometer-half|fas fa-thermometer-quarter|fas fa-thermometer-three-quarters|fas fa-thumbs-down|fas fa-thumbs-up|fas fa-thumbtack|fas fa-ticket-alt|fas fa-times|fas fa-times-circle|fas fa-tint|fas fa-toggle-off|fas fa-toggle-on|fas fa-trademark|fas fa-train|fas fa-transgender|fas fa-transgender-alt|fas fa-trash|fas fa-trash-alt|fas fa-tree|fas fa-trello|fas fa-tripadvisor|fas fa-trophy|fas fa-truck|fas fa-truck-loading|fas fa-truck-moving|fas fa-tty|fas fa-tumblr|fas fa-tumblr-square|fas fa-tv|fas fa-twitch|fas fa-twitter|fas fa-twitter-square|fas fa-typo3|fas fa-uber|fas fa-uikit|fas fa-umbrella|fas fa-underline|fas fa-undo|fas fa-undo-alt|fas fa-uniregistry|fas fa-universal-access|fas fa-university|fas fa-unlink|fas fa-unlock|fas fa-unlock-alt|fas fa-untappd|fas fa-upload|fas fa-usb|fas fa-user|fas fa-user-circle|fas fa-user-md|fas fa-user-plus|fas fa-user-secret|fas fa-user-times|fas fa-users|fas fa-ussunnah|fas fa-utensil-spoon|fas fa-utensils|fas fa-vaadin|fas fa-venus|fas fa-venus-double|fas fa-venus-mars|fas fa-viacoin|fas fa-viadeo|fas fa-viadeo-square|fas fa-vial|fas fa-vials|fas fa-viber|fas fa-video|fas fa-video-slash|fas fa-vimeo|fas fa-vimeo-square|fas fa-vimeo-v|fas fa-vine|fas fa-vk|fas fa-vnv|fas fa-volleyball-ball|fas fa-volume-down|fas fa-volume-off|fas fa-volume-up|fas fa-vuejs|fas fa-warehouse|fas fa-weibo|fas fa-weight|fas fa-weixin|fas fa-whatsapp|fas fa-whatsapp-square|fas fa-wheelchair|fas fa-whmcs|fas fa-wifi|fas fa-wikipedia-w|fas fa-window-close|fas fa-window-maximize|fas fa-window-minimize|fas fa-window-restore|fas fa-windows|fas fa-wine-glass|fas fa-won-sign|fas fa-wordpress|fas fa-wordpress-simple|fas fa-wpbeginner|fas fa-wpexplorer|fas fa-wpforms|fas fa-wrench|fas fa-x-ray|fas fa-xbox|fas fa-xing|fas fa-xing-square|fas fa-y-combinator|fas fa-yahoo|fas fa-yandex|fas fa-yandex-international|fas fa-yelp|fas fa-yen-sign|fas fa-yoast|fas fa-youtube|fas fa-youtube-square";
    public static String[] getIcons(){
        return FA_ICONS.split("\\|");
    }

    private SDProcedimento pc;

    @EJB
    protected GestoreProcedimento gestoreProcedimento;

    @EJB(lookup = "java:app/Report/ReportManagerBean")
    protected ReportManager reportManager;

//    @Override
//    protected void initLookups(Context ctx) {
//        gestoreProcedimento = lookup(GestoreProcedimento.class);
//    }

    public SDProcedimento getProcedimentoSessionData() {
        return pc;
    }

    /* SEARCH FORM */


    public String stampa() {
        Document domProc = gestoreProcedimento.domProcedimento(getProcedimento().getId());
        Report report = reportManager.generateFromXsl(AvailableReports.PROCEDIMENTO, domProc);
        String name = report.getName();
        if (StringUtils.isEmpty(name)){
            name = "proc_" + getProcedimento().getId() + ".pdf";
        }
        DownloadBean db = new DownloadBean(report.getContent(),name,report.getMime());
        return Download.prepareAndRedirect(db);
    }

    /* END SEARCH FORM */

    /* DETAIL */

    public MetaProcedimento getProcedimento() {
        return getProcedimentoSessionData().getProcedimento();
    }

    public void setProcedimento(MetaProcedimento procedimento) {
        getProcedimentoSessionData().setProcedimento(procedimento);
    }

    public List<MetaProcedimento> getAncestors() {
        return getProcedimentoSessionData().getAncestors();
    }


    protected Integer or(Integer[] raw) {
        Integer st=0;
        for (Integer stato:raw) {
            st|=stato;
        }
        return st;
    }


    public List<MetaDichiarazione> getMetaDichiarazioni() {
        return getProcedimento().getDichiarazioni();
    }

    public List<MetaProcedimento> getChildren() {
        return getProcedimentoSessionData().getChildren();
    }

}
