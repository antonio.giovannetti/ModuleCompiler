package net.mysoftworks.modulecompiler.web.admin.action.service;

import net.mysoftworks.modulecompiler.bl.manager.GestoreDichiarazione;
import net.mysoftworks.modulecompiler.bl.manager.GestoreProcedimento;
import net.mysoftworks.modulecompiler.model.ValueLob;
import net.mysoftworks.modulecompiler.report.iface.ReportManager;
import net.mysoftworks.modulecompiler.search.MetaDichiarazioneSearchParam;
import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.web.searchparam.WebMetaDichiarazionePopulator;
import net.mysoftworks.modulecompiler.web.searchparam.WebSearchParamFactory;
import net.mysoftworks.modulecompiler.web.common.action.Struts2AjaxApplicationAwareAction;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.struts2.ServletActionContext;

import javax.ejb.EJB;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

public class AjaxProcedimento extends Struts2AjaxApplicationAwareAction {
    public static final String FORM_GUIDA = "formGuida";
    @EJB
    private GestoreDichiarazione gestoreDichiarazione;
    @EJB
    private GestoreProcedimento gestoreProcedimento;


    @EJB(lookup = "java:app/Report/ReportManagerBean")
    private ReportManager reportManager;

    private Long idDic;
    private Long idProc;
    private Long idGuida;
    private boolean cancel=false;
    private String q=null;

    public void setQ(String q) {
        this.q = q;
    }

    public void setIdDic(Long idDic) {
        this.idDic = idDic;
    }

    public void setIdProc(Long idProc) {
        this.idProc = idProc;
    }

    public void setCancel(boolean cancel) {
        this.cancel = cancel;
    }

    public void setIdGuida(Long idGuida) {
        this.idGuida = idGuida;
    }

    //    @Override
//    protected void initLookups(Context ctx) {
//        gestoreDichiarazione = lookup(GestoreDichiarazione.class);
//    }

    public String unlinkDichiarazione() {
        try {
            logOperatore(Loggers.LOG_ADMIN, Level.FINE,"Unlink metaDichiarazione {0,number,#} da proc {1,number,#}, cancel:{2}",new Object[]{idDic,idProc,cancel});
            gestoreDichiarazione.unlinkDichiarazione(idProc,idDic,cancel);
        } catch (Exception e) {
            e.printStackTrace();
            addActionError(e.getMessage());
            return AJAX_ERROR;
        }
        if (cancel) {
            addActionMessage(getText("admin.dichiarazionecancellata",new String[]{idDic.toString(),idProc.toString()}));
        } else {
            addActionMessage(getText("admin.dichiarazionerimossa",new String[]{idDic.toString(),idProc.toString()}));
        }

        return AJAX_SUCCESS;
    }

    public String saveOrUpdateGuidaProc() {
        logOperatore(Loggers.LOG_ADMIN,Level.INFO,"saveOrUpdateGuidaProc per proc:{0,number,#}; testo:{1}",idProc,q);

        Long res = gestoreProcedimento.saveOrUpdateGuida(idProc, q);
        return AJAX_SUCCESS;

    }



    public String saveOrUpdateGuidaDich() {
        logOperatore(Loggers.LOG_ADMIN,Level.FINE,"saveOrUpdateGuidaDich per dic:{0,number,#}; testo:{1}",idDic,q);
        gestoreDichiarazione.saveOrUpdateGuida(idDic,q);
        return AJAX_SUCCESS;

    }

    public String dicAssociabili() {
        try {
//            testPrint();
            logOperatore(Loggers.LOG_ADMIN, Level.FINE,"Cerco dic associabili per proc {0,number,#} per parametro {1}",idProc,q);
//            output = gestoreDichiarazione.dicAssociabili(idProc,q);
            MetaDichiarazioneSearchParam param = WebSearchParamFactory.create(ServletActionContext.getRequest(),MetaDichiarazioneSearchParam.class);
            new WebMetaDichiarazionePopulator().populate(ServletActionContext.getRequest(),param);
            output = gestoreDichiarazione.search(param);
            logOperatore(Loggers.LOG_ADMIN, Level.FINEST,"trovate dic associabili per proc {0,number,#}:{1}",
                    idProc,output);


        } catch (Exception e) {
            e.printStackTrace();
            addActionError(e.getMessage());
            return AJAX_ERROR;
        }
        return AJAX_SUCCESS_DATATABLE;
    }


    public String addOrCloneDic() {
        logOperatore(Loggers.LOG_ADMIN, Level.FINE,"Associa metaDichiarazione {0,number,#} al procedimento {1,number,#}, tipo {2}",idDic,idProc,q);
        if ("clone".equals(q)) {
            gestoreDichiarazione.clone(idDic,idProc);
        } else
        if ("link".equals(q)) {
            gestoreDichiarazione.associaDicLink(idProc,idDic);
        } else {
            addActionError("Tipo di associazione non riconosciuto:'"+ q +"'");
            return AJAX_ERROR;
        }
        return AJAX_SUCCESS;
    }

    public String checkSaveProc() {
        logOperatore(Loggers.LOG_FE,Level.FINE,"Verifico impatti per il procedimento {0,number,#}",idProc);
        List<Object[]> praticheImpattate = gestoreProcedimento.checkSaveProcedimento(idProc);
        if (praticheImpattate.size()==0) {
            praticheImpattate.add(new Object[]{"-",0L});
        }
        StringBuilder sb = new StringBuilder();
        for(Object[] item:praticheImpattate) {
            if (!NumberUtils.LONG_ZERO.equals(item[1])) {
                if (sb.length()!=0) sb.append(", ");
                sb.append(getText("admin.dichiarazione.checksave.item", Arrays.asList(item)));
            }
        }
        if (sb.length()==0) sb.append(getText("admin.dichiarazione.checksave.noitem"));
        String msg = getText("admin.dichiarazione.checksave", "-", sb.toString());
        addActionMessage(getText(msg));
        return AJAX_SUCCESS;
    }

    public String checkSaveDich() {
        logOperatore(Loggers.LOG_FE,Level.FINE,"Verifico impatti per la dichiarazione {0,number,#}",idDic);
        List<Object[]> praticheImpattateD = gestoreDichiarazione.checkSave(idDic,idProc);
        if (praticheImpattateD.size()==0) {
            praticheImpattateD.add(new Object[]{"-",0L});
        }
        StringBuilder sb = new StringBuilder();
        for(Object[] item:praticheImpattateD) {
            if (!NumberUtils.LONG_ZERO.equals(item[1])) {
                if (sb.length()!=0) sb.append(", ");
                sb.append(getText("admin.dichiarazione.checksave.item", Arrays.asList(item)));
            }
        }
        if (sb.length()==0) sb.append(getText("admin.dichiarazione.checksave.noitem"));
        String msg = getText("admin.dichiarazione.checksave", "-", sb.toString());
        addActionMessage(getText(msg));
        return AJAX_SUCCESS;
    }

    private GestoreDichiarazione.MOVE_DIR moveDir;

    public void setMoveDir(GestoreDichiarazione.MOVE_DIR moveDir) {
        this.moveDir = moveDir;
    }
    public String moveDic() {
        logOperatore(Loggers.LOG_ADMIN,Level.FINE,"Sposto la dichiarazione {0,number,#} del proc {1,number,#}; direzione {2}",idDic,idProc,moveDir);
        gestoreDichiarazione.moveDic(idProc, idDic, moveDir);
        return null;
    }

    protected ValueLob gc;

    public ValueLob getGc() {
        return gc;
    }
    public String formGuida() {
        if (idGuida!=null) {
            gc = gestoreProcedimento.getGuida(idGuida);
        }
        logOperatore(Loggers.LOG_ADMIN,Level.FINE,"Visualizzazione guida compilazione per id {0,number,#}: {1}",
                idGuida,gc !=null ? gc.getId() : null);
        return FORM_GUIDA;
    }


}
