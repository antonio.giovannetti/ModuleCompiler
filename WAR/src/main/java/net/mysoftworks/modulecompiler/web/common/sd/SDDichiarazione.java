package net.mysoftworks.modulecompiler.web.common.sd;

import net.mysoftworks.modulecompiler.model.meta.MetaDichiarazione;
import net.mysoftworks.modulecompiler.model.meta.MetaField;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * Created by antonio on 02/07/17.
 */
@SessionDataDecorator(requestParam = "metaDichiarazione.id")
public class SDDichiarazione extends SessionData<Long> {
//    public static final String REQUEST_KEY ="metaDichiarazione.id";

    private MetaDichiarazione metaDichiarazione = null;
    private MetaField campo = null;
    private List<MetaField> campi = new ArrayList<>();
    /**
     * Business Logic to find key from request
     *
     * @param _key
     */
    public SDDichiarazione(String _key) {
//        super(request);
        metaDichiarazione = new MetaDichiarazione();
        if (StringUtils.isNotEmpty(_key)) {
            key = Long.parseLong(_key);
            metaDichiarazione.setId(key);
        }
        init();
    }


    public MetaDichiarazione getMetaDichiarazione() {
        return metaDichiarazione;
    }

    public void setMetaDichiarazione(MetaDichiarazione metaDichiarazione) {
        this.metaDichiarazione = metaDichiarazione;
        init();
    }

    private void init() {
        campo=null;
        campi.clear();
        if (metaDichiarazione !=null) {
            // for (MetaField f : metaDichiarazione.getFields()) {
                campi.addAll(metaDichiarazione.getFields());
            // }
            Collections.sort(campi, new Comparator<MetaField>() {
                @Override
                public int compare(MetaField o1, MetaField o2) {
                    return o1.getId().compareTo(o2.getId());
                }
            });
        }
    }

    public List<MetaField> getCampi() {
        return campi;
    }

    public MetaField getCampo() {
        return campo;
    }

    public void setCampo(MetaField campo) {
        this.campo = campo;
    }

//    public Long getKey() {
//        if (metaDichiarazione!=null) return metaDichiarazione.getId();
//        return null;
//    }

//    @Override
//    public String getRequestParameterName() {
//        return REQUEST_KEY;
//    }
}
