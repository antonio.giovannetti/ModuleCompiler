package net.mysoftworks.modulecompiler.web.searchparam;

import net.mysoftworks.modulecompiler.search.MetaDichiarazioneSearchParam;

import javax.servlet.http.HttpServletRequest;

public class WebMetaDichiarazionePopulator extends WebSearchParamFactory<MetaDichiarazioneSearchParam> {

    private static final String PARAM_Q="q";
    private static final String PARAM_IDPROC="idProc";


    @Override
    public void populate(HttpServletRequest request, MetaDichiarazioneSearchParam param) {
        String q=request.getParameter(PARAM_Q);
        param.setDescrizioneLike(q);
        String idProc=request.getParameter(PARAM_IDPROC);
        if (idProc!=null) {
            try {
                param.setNotIdProc(Long.parseLong(idProc));
            } catch (Exception e){}
        }
    }
}
