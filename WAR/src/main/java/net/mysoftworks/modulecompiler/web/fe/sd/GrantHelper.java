package net.mysoftworks.modulecompiler.web.fe.sd;

import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.model.RuoloPratica;
import net.mysoftworks.modulecompiler.model.meta.MetaProcedimento;
import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.model.values.Pratica;
import net.mysoftworks.modulecompiler.web.common.sd.SessionData;
import net.mysoftworks.modulecompiler.web.common.sd.SessionDataDecorator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;

import java.util.*;
import java.util.logging.Level;

public class GrantHelper {

    private Collection<RuoloPratica> ruoliPratica;

    public GrantHelper(Collection<RuoloPratica> ruoliPratica)  {
        this.ruoliPratica = ruoliPratica;
    }

    void initGrantsDichiarazione(List<Dichiarazione> dichiarazioni, Map<Long, boolean[]> grants) {
        grants.clear();
        for(Dichiarazione dichiarazione:dichiarazioni) {
            boolean[]  _grants = new boolean[4];
            grants.put(dichiarazione.getId(),_grants);
            _grants[0] = canView(dichiarazione);
            if (_grants[0]) {
                _grants[1] = canModify(dichiarazione);
                _grants[2] = canChangeCompiler(dichiarazione);
            } else {
                _grants[1]=false;
                _grants[2]=false;
            }

        }

    }

    private boolean hasRuoli(RuoloPratica.RuoloCompilazionePraticaOperator op,RuoloPratica.RuoloCompilazionePratica ... ruoli) {
        boolean out = false;
        for (RuoloPratica.RuoloCompilazionePratica ruolo: ruoli ) {
            boolean hr = hasRuolo(ruolo);
            if (RuoloPratica.RuoloCompilazionePraticaOperator.ALL.equals(op) && !hr) {
                out = false; break;
            }
            if (RuoloPratica.RuoloCompilazionePraticaOperator.SOME.equals(op) && hr) {
                out = true; break;
            }
            if (RuoloPratica.RuoloCompilazionePraticaOperator.NONE.equals(op) && hr) {
                out = false; break;
            }
        }
        return out;
    }

    /**
     * @param ruolo
     */
    private boolean hasRuolo(RuoloPratica.RuoloCompilazionePratica ruolo) {
//        ruoliPratica = ruoliPratica == null ?  getPratica().getRuoli() : ruoliPratica ;
        String utente = ServletActionContext.getRequest().getUserPrincipal().getName();
        for (Iterator<RuoloPratica> it= ruoliPratica .iterator(); it.hasNext(); ) {
            RuoloPratica r=it.next();
            Loggers.logOperatore(Loggers.LOG_FE, Level.FINEST,"Ruolo pratica: {0}, user: {1}, ruolo test: {2}, UserInRole:{3}, isOn:{4}",
                    ServletActionContext.getRequest().getUserPrincipal(),
                    r,utente,ruolo,ServletActionContext.getRequest().isUserInRole(ruolo.getCodice()),ruolo.isOn(r.getRoleBitwise()));
            if (ruolo.isOn(r.getRoleBitwise()) && r.getUser().equals(utente)) {
                if (ServletActionContext.getRequest().isUserInRole(ruolo.getCodice())) {
                    return true;
                } else {
                    Loggers.logOperatore(Loggers.LOG_FE, Level.WARNING,"Per l'utente {0} il sistema SSO non ha impostato il ruolo {1}",
                            ServletActionContext.getRequest().getUserPrincipal(),ruolo.getCodice());
                }
            }
        }
        return false;
    }

    public boolean canView(Dichiarazione d) {
        boolean out = hasRuoli(RuoloPratica.RuoloCompilazionePraticaOperator.SOME,
                RuoloPratica.RuoloCompilazionePratica.ROLE_OWNER,
                RuoloPratica.RuoloCompilazionePratica.ROLE_VIEWER);
        if (!out)  {
            if (ServletActionContext.getRequest().isUserInRole(RuoloPratica.RuoloCompilazionePratica.ROLE_COMPILER.getCodice())) {
                if (StringUtils.isNotBlank(d.getCompilatore())) {
                    String utente = ServletActionContext.getRequest().getUserPrincipal().getName();
                    out = utente.equals(d.getCompilatore());
                }
            }
        }
        return out;
    }


    public boolean canModify(Dichiarazione d) {
        boolean out = false;
        if (canView(d)) {
            if (ServletActionContext.getRequest().isUserInRole(RuoloPratica.RuoloCompilazionePratica.ROLE_COMPILER.getCodice())) {
                if (StringUtils.isNotBlank(d.getCompilatore())) {
                    String utente = ServletActionContext.getRequest().getUserPrincipal().getName();
                    return utente.equals(d.getCompilatore());
                }
            }
            if (!out) {
                out = hasRuoli(RuoloPratica.RuoloCompilazionePraticaOperator.SOME,
                        RuoloPratica.RuoloCompilazionePratica.ROLE_OWNER,
                        RuoloPratica.RuoloCompilazionePratica.ROLE_COMPILER);
            }
        }
        return out ;
    }

    public boolean canChangeCompiler(Dichiarazione d) {
        return hasRuolo(RuoloPratica.RuoloCompilazionePratica.ROLE_OWNER);
    }

    public boolean canPrint() {
        return hasRuolo(RuoloPratica.RuoloCompilazionePratica.ROLE_OWNER);
    }

    public boolean isOwner() {
        return hasRuolo(RuoloPratica.RuoloCompilazionePratica.ROLE_OWNER);
    }


}
