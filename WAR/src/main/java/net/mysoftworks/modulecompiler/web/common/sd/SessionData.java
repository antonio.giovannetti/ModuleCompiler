package net.mysoftworks.modulecompiler.web.common.sd;

import java.io.Serializable;

public abstract class SessionData<T> implements Serializable {
    protected T key;
    public T getKey(){
        return key;
    }
//    public abstract String getRequestParameterName();

    /**
     * Business Logic to find key from request
     * @param key
     */
//    public SessionData(T key) {
//        this.key = key;
//    }

    public void setKey(T key) {
        this.key = key;
    }

    //    public abstract String getKey();

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(getClass().getName());
        sb.append("{key='").append(key).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
