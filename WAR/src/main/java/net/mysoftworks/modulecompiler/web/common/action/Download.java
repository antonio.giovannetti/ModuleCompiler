package net.mysoftworks.modulecompiler.web.common.action;

import com.opensymphony.xwork2.ActionContext;
import net.mysoftworks.modulecompiler.web.utils.DownloadBean;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Download extends Struts2AjaxApplicationAwareAction {

    public static final String GLOBAL_RESULT_DOWNLOAD = "defaultDownloadHandler";
    public static final String KEY_FILE_DOWNLOAD = Download.class.getName();


    private static final long serialVersionUID = 4524913130957129155L;
    private String successCookieName= "fileDownload";
    private String successCookieValue="true";
    private String errorResult="jsonErrorResult";


    private static Logger logger = Logger.getLogger(Download.class.getName());


    private String contentDisposition;
    private String fileName;
    private String contentType;
    private InputStream fileInputStream;

    public static String prepareAndRedirect(DownloadBean file){
        Map<String,Object> parametri = ActionContext.getContext().getContextMap();
        parametri.put(KEY_FILE_DOWNLOAD, file);
        return GLOBAL_RESULT_DOWNLOAD;
    }

    /**
     * Metodo principale...
     * @return
     * @throws Exception
     */
    public String download() throws Exception {
        try{
            Map<String,Object> parametri = ActionContext.getContext().getContextMap();
            DownloadBean file = (DownloadBean) parametri.get(KEY_FILE_DOWNLOAD);
            contentDisposition="attachment;filename=\"" + file.getName() + "\"";
            contentType = file.getContentType();
            fileInputStream = new ByteArrayInputStream(file.getContent());
            fileName  = file.getName();
            Cookie c = new Cookie(successCookieName,successCookieValue);
            c.setPath("/");
            ServletActionContext.getResponse().addCookie(c);
            return SUCCESS;
        }
        catch(Throwable t){
            addActionError(getText("error.download.genericError",new String[]{t.getMessage()}));
            logger.log(Level.SEVERE, "Errore inatteso durante il download: "+t,t);
            return errorResult;
        }
    }


    public String getContentDisposition() {
        return contentDisposition;
    }

    public String getContentType() {
        return contentType;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }


    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }


    /**
     * @return the successCookieName
     */
    public String getSuccessCookieName() {
        return successCookieName;
    }

    /**
     * @return the successCookieValue
     */
    public String getSuccessCookieValue() {
        return successCookieValue;
    }


    /**
     * @return the errorResult
     */
    public String getErrorResult() {
        return errorResult;
    }

}
