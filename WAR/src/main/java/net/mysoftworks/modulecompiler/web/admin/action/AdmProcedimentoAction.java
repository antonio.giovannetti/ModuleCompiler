package net.mysoftworks.modulecompiler.web.admin.action;

import net.mysoftworks.modulecompiler.bl.manager.GestoreLiquibase;
import net.mysoftworks.modulecompiler.model.meta.MetaFileField;
import net.mysoftworks.modulecompiler.model.meta.MetaProcedimento;
import net.mysoftworks.modulecompiler.Loggers;
import net.mysoftworks.modulecompiler.web.common.action.Download;
import net.mysoftworks.modulecompiler.web.common.action.ProcedimentoAction;
import net.mysoftworks.modulecompiler.web.utils.DownloadBean;
import net.mysoftworks.modulecompiler.web.websocket.PraticaSessionManager;
import net.mysoftworks.modulecompiler.web.websocket.PushMessage;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;

import javax.ejb.EJB;
import java.util.*;
import java.util.logging.Level;


public class AdmProcedimentoAction extends ProcedimentoAction {

    public static final String SEARCHDIC = "searchDic";
    public static final String DETAIL_REDIRECT = "detail_redirect";
    public static final String LIST = "list";

    @EJB
    GestoreLiquibase gestoreLiquibase;

    private List<MetaProcedimento> procedimenti;
    public List<MetaProcedimento> getProcedimenti() {
        return procedimenti;
    }

    public String list() {
        logOperatore(Loggers.LOG_ADMIN,Level.INFO,"Lista dei procedimenti per {0}",getPrincipal().getName());
        procedimenti = gestoreProcedimento.getLista(false);
        return LIST;
    }

    public String detail() {
//        ApplicationContext ac=new ServletApplicationContext(ServletActionContext.getServletContext());
//        new StrutsTilesInitializer().initialize(ac);
        Long idProcedimento = getProcedimento().getId();
        logOperatore(Loggers.LOG_ADMIN,Level.INFO,"Vista procedimento {0,number,#}",idProcedimento);

        MetaProcedimento proc = gestoreProcedimento.get(idProcedimento);
        if (proc==null) {
            addActionError("Il procedimento " + idProcedimento + " non è stato trovato");
            return ERROR;
        }
        allegati = new ArrayList<>(proc.getAllegati());
        List<MetaProcedimento> anc = gestoreProcedimento.ancestors(idProcedimento);
        if (CollectionUtils.isNotEmpty(anc)) Collections.reverse(anc);
        getProcedimentoSessionData().setAncestors(anc);
        getProcedimentoSessionData().setChildren(gestoreProcedimento.children(idProcedimento));
        setProcedimento(proc);
        return INPUT;
    }


    protected Integer or(Integer[] raw) {
        Integer st=0;
        for (Integer stato:raw) {
            st|=stato;
        }
        return st;
    }

    public void setStatoProcedimento(Integer[] stati) {
        getProcedimento().setStato(or(stati));
    }

    public void setCompilabilita(Integer[] compilabilita) {
        getProcedimento().setCompilabilita(or(compilabilita));
    }

    public void setVisibilita(Integer[] visibilita) {
        getProcedimento().setVisibilita(or(visibilita));
    }

    public String add() {
        MetaProcedimento proc = getProcedimento();
        if (proc==null) {
            proc = new MetaProcedimento();
            getProcedimentoSessionData().setProcedimento(proc);

        }
        return INPUT;
    }

//    public MetadataChangeImpact checkSave() {
//
//    }

//    private Set<MetaFileField> allegati = new HashSet<>();

    private List<MetaFileField> allegati;

    public List<MetaFileField> getAllegati() {
        return allegati;
    }

    public void setAllegati(List<MetaFileField> allegati) {
        this.allegati = allegati;
    }

    public String versiona() {
        logOperatore(Loggers.LOG_ADMIN,Level.INFO,"Versiona procedimento {0}",getProcedimento().getId());
        MetaProcedimento newVersion = gestoreProcedimento.versiona(getProcedimento().getId());
        getProcedimento().setId(newVersion.getId());
        return DETAIL_REDIRECT;
    }

    public String save() {
        logOperatore(Loggers.LOG_ADMIN,Level.INFO,"Salvataggio procedimento {0}",getProcedimento());
        MetaProcedimento mp = getProcedimento();
        Set<MetaFileField> all = new HashSet<>();
        if (allegati!=null)
        for(MetaFileField a:allegati) {
            if (StringUtils.isNotBlank(a.getNome())) {
                a.setEtichetta(a.getNome());
                all.add(a);
            }
        }
        if (StringUtils.isBlank(mp.getDescrizione())) {
            addActionError(getText("admin.procedimento.save.descr_blank"));
            return AJAX_ERROR;
        } else {

            getProcedimento().setAllegati(all);
//            mp.setAllegati(all);

            mp = gestoreProcedimento.save(getProcedimento());
            getProcedimentoSessionData().setProcedimento(mp);

        }
        Set<String> uc = gestoreProcedimento.listaUtentiCoinvolti(mp.getId());
        PushMessage pm = new PushMessage(PushMessage.TYPE.NOTIFICA_MODIFICA_PROCEDIMENTO,"window.location.reload();");
        pm.addKey(PushMessage.DESC_PROC,mp.getDescrizione());
        for(String user:uc) {
            PraticaSessionManager.getInstance().sendMessage(pm,user);
        }
        addActionMessage(String.valueOf(mp.getId()));
        return AJAX_SUCCESS;
    }

    public String searchDic() {
        return SEARCHDIC;
    }

//    protected MetaLob gc;
//
//    public MetaLob getGc() {
//        return gc;
//    }
//
//    public String formGuida() {
//        if (getProcedimento().getGuidaCompilazione()!=null) {
//            gc = gestoreProcedimento.getGuida(getProcedimento().getIdGuida());
//        }
//        logOperatore(Loggers.LOG_ADMIN,Level.FINE,"Visualizzazione guida compilazione per procedimento {0,number,#}: {1}",
//                getProcedimento().getId(),gc !=null ? gc.getId() : null);
//        return FORM_GUIDA;
//    }

    private boolean deep=false;

    public void setDeep(boolean deep) {
        this.deep = deep;
    }

    public String clona() {
        logOperatore(Loggers.LOG_ADMIN,Level.FINE,"Clona procedimento {0}, deep:",getProcedimento().getId(),deep);
        MetaProcedimento mp = gestoreProcedimento.clone(getProcedimento().getId(),deep);
        getProcedimentoSessionData().setProcedimento(mp);
        return DETAIL_REDIRECT;
    }

    public String remove() {
        logOperatore(Loggers.LOG_ADMIN,Level.FINE,"Cancella procedimento {0}, deep:",getProcedimento().getId(),deep);
        gestoreProcedimento.remove(getProcedimento().getId(),deep);
        getProcedimentoSessionData().setProcedimento(null);
        return "list_remove";
    }

    public String backupCfg() throws  Exception {
        byte[] content = null;
        String fName = "backup_proc";
        if (getProcedimento().getId()!=null) {
            logOperatore(Loggers.LOG_ADMIN,Level.FINE,"Backup cfg procedimento {0,number,#}",getProcedimento().getId());
            fName += getProcedimento().getId()+".zip";
            content = gestoreLiquibase.exportProc(getProcedimento().getId());
        } else {
            logOperatore(Loggers.LOG_ADMIN,Level.FINE,"Backup cfg globale");
            fName += ".zip";
            content = gestoreLiquibase.exportAll();
        }
        DownloadBean db = new DownloadBean(content,fName,"application/zip");
        return Download.prepareAndRedirect(db);
    }


}
