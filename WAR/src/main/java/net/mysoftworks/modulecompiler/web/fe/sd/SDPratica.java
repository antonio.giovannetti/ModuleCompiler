package net.mysoftworks.modulecompiler.web.fe.sd;

import net.mysoftworks.modulecompiler.model.values.Dichiarazione;
import net.mysoftworks.modulecompiler.model.meta.MetaProcedimento;
import net.mysoftworks.modulecompiler.model.values.Pratica;
import net.mysoftworks.modulecompiler.web.common.sd.SessionData;
import net.mysoftworks.modulecompiler.web.common.sd.SessionDataDecorator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * Created by antonio on 02/07/17.
 */
@SessionDataDecorator(requestParam = "pratica.id",forceCreate = false)
public class SDPratica extends SessionData<Long> {

    private Pratica pratica = null;
    private MetaProcedimento procedimento = new MetaProcedimento();
    private Map<Long, boolean[]> grants = new HashMap<>();
    private boolean owner = false;
    private boolean canPrint = false;

    public MetaProcedimento getProcedimento() {
        return procedimento;
    }

    public void setProcedimento(MetaProcedimento procedimento) {
        this.procedimento = procedimento;
    }
    /**
     * Business Logic to find key from request
     *
     * @param _key
     */
    public SDPratica(String _key) {
        if (StringUtils.isNotEmpty(_key)) {
            key = Long.parseLong(_key);
            if (pratica==null)pratica = new Pratica();
            pratica.setId(key);
        }
    }

    public boolean isOwner() {return owner;}
    public boolean isCanPrint() {return canPrint;}
    public Pratica getPratica() {
        return pratica;
    }

    public void setPratica(Pratica pratica) {
        this.pratica = pratica;
        if (pratica!=null) {
            key = pratica.getId();
            owner = new GrantHelper(pratica.getRuoli()).isOwner();
            canPrint = new GrantHelper(pratica.getRuoli()).canPrint();
        } else {
            if (dichiarazioni!=null) dichiarazioni.clear();
            else {
            }
        }
    }

//    private Map<Long,Dichiarazione> dichiarazioni;
//    public Map<Long,Dichiarazione> getDichiarazioni() {
//        return dichiarazioni;
//    }
    private List<Dichiarazione> dichiarazioni;
    public List<Dichiarazione> getDichiarazioni() {
        return dichiarazioni;
    }
    public void updateDichiarazione(Dichiarazione dichiarazione) {
        Iterator<Dichiarazione> it = dichiarazioni.iterator();
        int idx = 0;
        if (it.hasNext()) {
            while (it.hasNext()) {
                Dichiarazione d = it.next();
                if (d.getId().equals(dichiarazione.getId())) {
                    it.remove();
                    break;
                }
                idx++;
            }
        }
        dichiarazioni.add(idx,dichiarazione);
    }

    public void updateCompiler(Long id, String user) {
        if (CollectionUtils.isNotEmpty(dichiarazioni)) {
            Iterator<Dichiarazione> it = dichiarazioni.iterator();
            if (it.hasNext()) {
                while (it.hasNext()) {
                    Dichiarazione d = it.next();
                    if (d.getId().equals(id)) {
                        d.setCompilatore(user);
                        return;
                    }
                }
            }
        }
    }

//    public boolean[] getGrantsByDic(Long idDich){
//        return grants.get(idDich);
//    }

    public void setDichiarazioni(List<Dichiarazione> dichiarazioni) {
        this.dichiarazioni = dichiarazioni;
        new GrantHelper(getPratica().getRuoli()).initGrantsDichiarazione(dichiarazioni,grants);
    }

    public boolean canChangeCompiler(Dichiarazione d) {
        return grants.get(d.getId())[2];
    }

    public boolean isOwner(Dichiarazione d) {
        return grants.get(d.getId())[3];
    }

    public boolean canView(Dichiarazione d) {
        return grants.get(d.getId())[0];
    }

    public boolean canModify(Dichiarazione d) {
        return grants.get(d.getId())[1];
    }
}
